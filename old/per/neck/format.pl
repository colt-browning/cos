#!/usr/bin/perl
# This program formats the output into Html format

require "../common.pl";
if ($#ARGV < 1) {exit(0);}
else { 
	$output = $ARGV[0]; 	
	$LIMIT = $ARGV[1]; 	
	$DB =    $ARGV[2]; 	
}
$#ARGV = -1;

if ($DB == 1) {
	$n = 0;
	while (<>) {
		push @table , [split];
		$n++;
	}

	# Display the output
	if ($output & 1) {
		print "<P><TABLE BORDER=1 Width=90 CELLSPACING=2 CELLPADDING=2><TR>\n";
		print '<TH COLSPAN=1><FONT SIZE="+1">String</FONT><BR></TH>';
       		print "\n<TR><TD>";
		$tot=0;
		for($row=0; $row<=$n; ++$row) {
			for($k=1; $k<=$table[$row][0]; $k++) {
				if ($tot++ % 70 == 0) { print "<br>";}
       				print $table[$row][$k]; 
			}
       		}	
		print "</table>";
	}
	if ($output & 2) {
		print "<P><TABLE BORDER=1 Width=90 CELLSPACING=2 CELLPADDING=2><TR>\n";
		print '<TH COLSPAN=1><FONT SIZE="+1">Colored Beads</FONT><BR></TH>';
       		print "\n<TR><TD>";
		$tot = 0;
		for($row=1; $row<=$n; ++$row) {
			for($k=1; $k<=$table[$row][0]; $k++) {
				if ($tot++ % 40 == 0) { print "<br>";}
				if ($table[$row][$k] == 0) {
					print "<IMG src=ico/redball.gif>";
				}
				if ($table[$row][$k] == 1) {
					print "<IMG src=ico/blueball.gif>";
				}
				if ($table[$row][$k]== 2) {
					print "<IMG src=ico/greenbal.gif>";
				}
				if ($table[$row][$k]== 3) {
					print "<IMG src=ico/newhibal.gif>";
				}
				if ($table[$row][$k]== 4) {
					print "<IMG src=ico/orangeba.gif>";
				}
				if ($table[$row][$k]== 5) {
					print "<IMG src=ico/purpleba.gif>";
				}
				if ($table[$row][$k]== 6) {
					print "<IMG src=ico/yellowba.gif>";
				}
			}
       		}	
		print "</table>";
		if ($n >= $LIMIT) { 
			print "<BR>\n";
			print "<FONT COLOR=ff0000><Blink><B>Oops: </FONT></Blink></B> deBruijn sequence too long ( >". $LIMIT . " Lyndon words)";}
	}
	close (STDIN);
	exit(0);
}

print "<P><TABLE BORDER=1 CELLSPACING=2 CELLPADDING=2><TR>\n";
if ($output & 1) {
	print '<TH COLSPAN=1><FONT SIZE="+1">String</FONT><BR></TH>';
	print "\n";
}
if ($output & 2) {
	print '<TH COLSPAN=1><FONT SIZE="+1">Colored Beads</FONT><BR></TH>';
	print "\n";
}
$count = 0;
while (<>) {
	print "<TR>";
	$count++;
	if ($output & 1) {
		print "<TD align=center>";
		print "$_";
		print "\n";
	}
	$n = length($_)/2 -1;
	@permutation = split;
	if ($output & 2) {
		print "<TD align=center>";
		for ($i=0; $i<$n; $i++) { 
			if ($permutation[$i] == 0) {
				print "<IMG src = ico/redball.gif>";
			}
			if ($permutation[$i] == 1) {
				print "<IMG src = ico/blueball.gif>";
			}
			if ($permutation[$i] == 2) {
				print "<IMG src = ico/greenbal.gif>";
			}
			if ($permutation[$i] == 3) {
				print "<IMG src = ico/newhibal.gif>";
			}
			if ($permutation[$i] == 4) {
				print "<IMG src = ico/orangeba.gif>";
			}
			if ($permutation[$i] == 5) {
				print "<IMG src = ico/purpleba.gif>";
			}
			if ($permutation[$i] == 6) {
				print "<IMG src = ico/yellowba.gif>";
			}
		}
		print "\n";
	}
	if ($count == $LIMIT) { 
		print "</TABLE>";
	 	close (STDIN);
		exit(1);
	}
}

print "</TABLE>";
print "<P>Total output = $count";
close (STDIN);
$returnval; # return value  

