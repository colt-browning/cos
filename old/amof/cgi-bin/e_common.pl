#!/usr/bin/perl

# Utility perl routines for the Educational Object Server.
# Written and copyrighted by Frank Ruskey, 1995, 1996, 1998.
# "Ownership": Frank Ruskey.

require "cgi-lib.pl";

select((select(STDOUT), $| = 1)[$[]);
select((select(STDERR), $| = 1)[$[]);

$BinDir = "";
$htmldocs = "http://theory.cs.uvic.ca/ecos";
$icons = "Ico";
$binaries = "";
$LIMIT = 200;
$LIMITerror = -1;

sub ErrorHeader {
   print "<BR><BLINK><B><FONT COLOR=FF0000>Oops:</FONT></B></BLINK> ";
}

sub ErrorTrailer {
   local($address);
   $address=$ENV{HTTP_REFERER};

   print "<P><A HREF=$address>"; 
   $address =~ s/.*e_//;
   $address =~ s/\..*/G\.gif/;
   print "<IMG SRC=Ico/$address height=50 width=50> Return? </A>\n";
}

sub ErrorMessage {
   &ErrorHeader;
   print "$_[0]<BR>\n";
   &ErrorTrailer;
}

sub LimitError {
   print "</TABLE>";
   &ErrorHeader;
   print "You asked for more objects than the current limit of $LIMIT.<BR>\n";
   &ErrorTrailer;
}

sub ExtraParam {
   print "<BR><P><FONT SIZE=-1>Parameter <I>$_[0]</I> = $_[1] ignored.</FONT>\n";
}

sub OutputIgnored {
   print "<BR><FONT SIZE=-1>Output parameter <I>$_[0]</I> ignored.</FONT>\n";
}

sub MoreInfo{
   print "<HR><A HREF=$htmldocs/$_[0].html>Click here</A> for more 
         information<BR>\n";
}

sub WorkingMoreInfo{
   print "<HR><A HREF=$htmldocs/$_[0].html>Click here</A> for more 
         information\n";
   print " <IMG SRC=$icons/LittleConst.gif ALT=[Under_Const]>\n";
}

# Error message.
sub TooBig {
   &ErrorHeader;
   print "Parameter <I>$_[0]</I> cannot be larger than $_[1].<BR>\n";
   &ErrorTrailer;
}

# Error message.
sub TooSmall {
   &ErrorHeader;
   print "Parameter <I>$_[0]</I> cannot be smaller than $_[1].<BR>\n";
   &ErrorTrailer;
}

# Error message.
sub MustBeEven {
   &ErrorHeader;
   print "Parameter <I>$_[0]</I> must be even.<BR>\n";
   &ErrorTrailer;
}

# Error message.
sub MustBeOdd {
   &ErrorHeader;
   print "Parameter <I>$_[0]</I> must be odd.<BR>\n";
   &ErrorTrailer;
}

# Error message.
sub OutError {
   &ErrorMessage('No output option has been selected!');
}
 
sub OutputIcons {
   print "<TABLE width=100% cellspacing=0 cellpadding=0>";
   print "<TR><TD align=left >";
   print "<A HREF=index.html><IMG ALIGN=left SRC=Ico/amof.gif height=100 width=170 ALT=[ECOS]></A>\n";
   print "<TD align=left width=60%><A HREF=e_$_[0]I.htm><IMG ALIGN=left SRC=Ico/$_[0]I.gif height=50 width=50 ALT=[INFO]></A>\n";
   print "<TD align=right><A HREF=http://www.schoolnet.ca/>";
   print "<IMG ALIGN=RIGHT SRC=Ico/MASCOT-E.GIF BORDER=0 WIDTH=236 "; 
   print ' HEIGHT=90 ALT=[Schoolnet Icon]></A>';
   print "</TR></TABLE>";
   print "<BR CLEAR=right>\n";
}

# Every ECOS output file should start with this.
sub OutputHeader {
   print &PrintHeader;
   print '<!DOCTYPE HTML PUBLIC "-//Netscape Comm. Corp.//DTD HTML//EN">';
   print "\n";
   print '<META HTTP-EQUIV="Reply-to" CONTENT="fruskey@cs.uvic.ca">';
   print "\n";
   print "<HTML><HEAD>\n";  # ?? should this go earlier?
   print "<TITLE>$_[0]</TITLE>\n";
   print '<BASE HREF="http://theory.cs.uvic.ca/amof/">';
   print "</HEAD>\n<BODY>\n";
   &OutputIcons( $_[1] );
   print "<HR><H3>$_[0]</H3>\n";  # ?? Why H3?
}

# Format of output table.
sub TableSetup {
   print "<TABLE BORDER=1 CELLSPACING=2 CELLPADDING=2><TR>\n";
}

# Output a column header (E.g., &ColumnLabel( "Cycle" ) ).
sub ColumnLabel {
   print "<TH COLSPAN=1><FONT SIZE=+1>";
   print "$_[0]";
   print "</FONT><BR></TH>\n";
}

# Every ECOS document should end with this.
sub CloseDocument {
   print "<HR>";
   &OutputIcons( $_[0] );
   print "</BODY></HTML>\n";
   print "<!-- Educational Object Server Output -->\n";
   print "<!-- Copyright Frank Ruskey, 1995, 1996. -->\n";
}

# Strips out all non-numeric characters.  No warning issued.
# For security reasons a call to "MakeNumeric" should precede 
# each use of a numeric input.
sub MakeNumeric {
   $_[0] =~ s/[^0-9]//g;
}

1; # return true

