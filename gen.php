<!DOCTYPE html>
<html>
<head>
  <meta charset="UTF-8">
  <link rel="stylesheet" href="style.css">
</head>

<body id="console">

<?php
  include "web/limits.php";
  include "web/graphics.php";
  include "web/output.php";

  $object = $_GET["obj"];

  include("web/$object/run.php");
?>

<script>
window.onload = function() {
  parent.set_total();
  animate(0);  // kick off animation of objects
};

// Animations are realized by a stack of SVGs placed on top of each other.
// This function removes the (j-1)th SVG from the top of the stack,
// moves the jth SVG to the top of the stack, and calls itself again
// for the (j+1)th SVG.
function animate(j) {
  var c = document.getElementById('animation');
  if (c == null) return;
  var n = c.getElementsByClassName('stack').length;
  if (n <= 1) return;
  // no animation on page or no stack of objects to be animated
  // so we never call this function again
  
  var i = j > 0   ? j-1 : n-1;  // j-1
  var k = j < n-1 ? j+1 : 0;    // j+1
  var svgi = document.getElementById('svg' + i.toString());
  var svgj = document.getElementById('svg' + j.toString());
  svgi.style.zIndex = '0';  // not top of stack
  svgj.style.zIndex = '1';  // top of stack
  setTimeout(animate, 500, k);  // second argument is the delay in milliseconds
}
</script>

</body>
</html>
