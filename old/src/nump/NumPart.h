/* Generating Numerical Partitions */

#include <stdio.h>
#include <string.h>

extern int out_format; /* */
extern int LIMIT;      /* max objects allowed */

#define TRUE	1
#define FALSE	0
#define max(x,y) ( x<y ? y : x )
#define min(x,y) ( x<y ? x : y )
#define LIMIT_ERROR  -1
#define HEIGHT 10
#define WIDTH  10

#define nat_out    1
#define mult_out   2
#define gray_out   4
#define ferrer_out 8
#define conj_out   16
#define self_conj_out 32

int	p[100];
int	count;

void PrintFerrer ( int KK )
{ int i,j;
  
  printf("<TABLE BORDER=1 CELLPADDING=0 CELLSPACING=0>\n");
  for (i=(M==0?2:1); i<=KK; ++i){
     printf("<TR>");
     for (j=1; j<=p[i]; ++j){ 
       printf("<TD WIDTH=%d HEIGHT=%d BORDERCOLOR=BLACK BGCOLOR=RED>", WIDTH, HEIGHT);
       printf("<IMG SRC=ico/tt.gif WIDTH=%d HEIGHT=%d></TD>", WIDTH, HEIGHT);
     }
     printf("</TR>\n");
  }
  printf("</TABLE>\n");
}

void PrintIt ( int KK )
{ int i,j,k, mult, counter, index, durfee;
  int temp[100];

  ++count;
  if (count <= LIMIT) {
     printf("<TR>\n");
     if (out_format & nat_out) {
        printf("<TD ALIGN=CENTER>");
        for (i=(M==0?2:1); i<=KK; printf(" %2d ", p[i++]));
        printf("<BR></TD>\n");
     }
     if (out_format & mult_out) {
        printf("<TD ALIGN=CENTER>");
        printf("[%d,",p[M==0?2:1]);  mult = 1;
        for (i=(M==0?3:2); i<=KK; ++i) {
           if (p[i] == p[i-1]) ++mult;
           else { printf("%d][%d,",mult,p[i]);  mult = 1; }
        }   
        printf("%d]",mult);
        printf("<BR></TD>\n");
     }
     if (out_format & gray_out) {
        printf("<TD ALIGN=CENTER>");
        printf("not yet");
        printf("<BR></TD>\n");
     }
     if (out_format & ferrer_out) {
        printf("<TD ALIGN=CENTER>");
        PrintFerrer( KK );
     }
     if (out_format & self_conj_out){
       printf("<TD ALIGN=CENTER>");
       for(i = 0; i < 100; i++)
	 temp[i] = 0;
       index = (M==0)?2:1;
       for(i = index; i<=KK; i++)
	 {
	   temp[i-index] += (p[i]+1)/2;
	   for(k = 0; k < (p[i]-1)/2 ; k++)
	     temp[k+i-index+1]++;
	 }
        printf("<TABLE BORDER=1 CELLPADDING=0 CELLSPACING=0>\n");

	 for(i = 0; i < ((p[index]+1)/2); i++)
		if(temp[i] < (i+1))
		{
			durfee = i;
			break;
		}	
       for (i=0; i< ((p[index]+1)/2); ++i){
	 printf("<TR>");
	 for (k=1; k<=temp[i]; ++k){ 
	   if((i < durfee) && (k <= durfee)){
	     printf("<TD WIDTH=%d HEIGHT=%d BORDERCOLOR=BLACK BGCOLOR=#ff00ff>", WIDTH, HEIGHT);
	     printf("<IMG SRC=ico/tt.gif WIDTH=%d HEIGHT=%d></TD>", WIDTH, HEIGHT);
	   }
	   else{
	     printf("<TD WIDTH=%d HEIGHT=%d BORDERCOLOR=BLACK BGCOLOR=RED>", WIDTH, HEIGHT);
	     printf("<IMG SRC=ico/tt.gif WIDTH=%d HEIGHT=%d></TD>", WIDTH, HEIGHT);
	   }
	 }
	 printf("</TR>\n");
       }
       printf("</TABLE>\n");
     }
       if (out_format & conj_out) {
	 printf("<TD ALIGN=CENTER>");
	 for(i=(M==0?2:1); i <= KK; i++)
	   temp[i] = p[i];
	 i = (M==0?2:1);
		index = p[i];
		for(i = i +1; i <= KK; i++)
		  if(index < p[i])
		    index = p[i];
        for(i= index; i >0 ;i--){
          counter = 0;
	      for(j = (M==0?2:1); j<=KK; j++){
	        if(temp[j] > 0)
		      counter++;
            temp[j]--;
	      }
          printf(" %2d ",counter);
        }
        printf("<BR></TD>\n");
     }  
     printf("</TR>\n");
  } else { printf("</TABLE>"); exit(LIMIT_ERROR); }
}


/* don't need this now, but will keep it in comment in case
   something goes wriong.

void ProcessInput (int argc, char *argv[])
{  int i;
   nat_out = mult_out = ferrer_out = gray_out = FALSE;
   N = 0;  M = 0;  K = 0;
   for (i=1; i<argc; i++) {
       if (strcmp(argv[i], "-n") == 0) {
          if (i+1 < argc) { N = atoi(argv[++i]); }
       }
       else if (strcmp(argv[i], "-k") == 0) {
          if (i+1 < argc) { K = atoi(argv[++i]); }
       }
       else if (strcmp(argv[i], "-m") == 0) {
          if (i+1 < argc) { M = atoi(argv[++i]); }
       }
       else if (strcmp(argv[i], "-a") == 0) nat_out = TRUE;
       else if (strcmp(argv[i], "-u") == 0) mult_out = TRUE;
       else if (strcmp(argv[i], "-y") == 0) gray_out = TRUE;
       else if (strcmp(argv[i], "-e") == 0) ferrer_out = TRUE;
       else if (strcmp(argv[i], "-l") == 0) {
          if (i+1 < argc) { LIMIT = atoi(argv[++i]); }
       }
       else printf("Spurious input data: %s\n", argv[i]);
   }
}

*/
