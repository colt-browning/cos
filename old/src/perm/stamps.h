
struct stamp
	{
	struct stamp *next;	/* Points to next stamp */
	struct stamp *prev; 	/* Points to previous stamp */
	int value;
	struct perforation *perf[2];
	struct perforation *even_perf;
	struct perforation *odd_perf;
	};

struct perforation
	{
	int left_value;
	int right_value;
	struct stamp *left_link;
	struct stamp *right_link;
	};

