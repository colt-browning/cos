<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">

<HTML><TITLE>Info on Permutations</TITLE>
<BASE HREF="http://theory.cs.uvic.ca/"> 

<A HREF=gen/perm.html>
<IMG SRC=ico/gen.gif align=left border=0 height=33 width=35></A>
<!--#include file="inc/Border.include"-->
<HR>

<H2>Information on Permutations</H2>

A <I>permutation</I> of objects is an arrangement of those objects in some
  order; that is, some object is placed in the first position, another in the
  second position, and so on, until all objects have been placed.
For our purposes, a permutation is a string  a(1), a(2), ..., a(<I>n</I>), 
  where each a(<I>i</I>) is an element of the set
  [<I>n</I>] = {1,2,...,<I>n</I>} and each element occurs precisely
  once.  
For example, the permutations of [3] = {1,2,3} are
  123, 132, 213, 231, 312, 321.

<P>
The number of permutations for <I>n</I> = 1,2,...,10, is
  1, 2, 6, 24, 120, 720, 5040, 40320, 362880, 3628800;
  these, of course, are the factorial numbers 
<I>n</I>! = <I>n</I>&#183;&#183;&#183;2&#183;1.

This is sequence 
  <A HREF="<!--#include file="inc/IntSeq.include"-->Anum=A000142">
  <B>A000142</B>(M1675)</A> in 
<!--#include file="inc/Sloane.include"-->

<!--  <A HREF="mailto:sequences@research.att.com?cc=cos@figaro.uvic.ca
  &text=test&message=what&attach=helpme&subject=from COS
  &body=lookup 1 2 6 24 120 720 5040">database</A>
-->

<P>
Every permutation of [<I>n</I>] corresponds to a unique permutation
  matrix; that is, a 0-1 matrix which contains exactly one 1
  in each row and column.  
If a(<I>i</I>) = <I>j</I> then row i contains a 1 in column <I>j</I>.
Regarding the 1's as rooks, this is equivalent to a 
  collection of <I>n</I> non-taking rooks on a chessboard.  
As an example, the configuration shown below corresponds
  to the permutation 543621.

<P>
<CENTER>
<TABLE BORDER=1 CELLPADDING=0 CELLSPACING=0>
<TR>
<TD><IMG SRC =ico/green.gif height=30 width=30></TD>
<TD><IMG SRC =ico/red.gif height=30 width=30></TD>
<TD><IMG SRC =ico/green.gif height=30 width=30></TD>
<TD><IMG SRC =ico/red.gif height=30 width=30></TD>
<TD><IMG SRC =ico/greenRook.gif height=30 width=30></TD>
<TD><IMG SRC =ico/red.gif height=30 width=30></TD></TR>
<TR>
<TD><IMG SRC =ico/red.gif height=30 width=30></TD>
<TD><IMG SRC =ico/green.gif height=30 width=30></TD>
<TD><IMG SRC =ico/red.gif height=30 width=30></TD>
<TD><IMG SRC =ico/greenRook.gif height=30 width=30></TD>
<TD><IMG SRC =ico/red.gif height=30 width=30></TD>
<TD><IMG SRC =ico/green.gif height=30 width=30></TD></TR>
<TR>
<TD><IMG SRC =ico/green.gif height=30 width=30></TD>
<TD><IMG SRC =ico/red.gif height=30 width=30></TD>
<TD><IMG SRC =ico/greenRook.gif height=30 width=30></TD>
<TD><IMG SRC =ico/red.gif height=30 width=30></TD>
<TD><IMG SRC =ico/green.gif height=30 width=30></TD>
<TD><IMG SRC =ico/red.gif height=30 width=30></TD></TR>
<TR>
<TD><IMG SRC =ico/red.gif height=30 width=30></TD>
<TD><IMG SRC =ico/green.gif height=30 width=30></TD>
<TD><IMG SRC =ico/red.gif height=30 width=30></TD>
<TD><IMG SRC =ico/green.gif height=30 width=30></TD>
<TD><IMG SRC =ico/red.gif height=30 width=30></TD>
<TD><IMG SRC =ico/greenRook.gif height=30 width=30></TD></TR>
<TR>
<TD><IMG SRC =ico/green.gif height=30 width=30></TD>
<TD><IMG SRC =ico/redRook.gif height=30 width=30></TD>
<TD><IMG SRC =ico/green.gif height=30 width=30></TD>
<TD><IMG SRC =ico/red.gif height=30 width=30></TD>
<TD><IMG SRC =ico/green.gif height=30 width=30></TD>
<TD><IMG SRC =ico/red.gif height=30 width=30></TD></TR>
<TR>
<TD><IMG SRC =ico/redRook.gif height=30 width=30></TD>
<TD><IMG SRC =ico/green.gif height=30 width=30></TD>
<TD><IMG SRC =ico/red.gif height=30 width=30></TD>
<TD><IMG SRC =ico/green.gif height=30 width=30></TD>
<TD><IMG SRC =ico/red.gif height=30 width=30></TD>
<TD><IMG SRC =ico/green.gif height=30 width=30></TD></TR>
</TABLE>
</CENTER>

<P>
We generate permutations in 2 different orders.

<H3>Lexicographic Order</H3>

In lexicogrphic order, the permutations are listed in numeric
  order; 12...<I>n</I> is first and <I>n</I>...21 is last.
Here are the permutations of {1,2,3} listed in lexicographic
  order: 123, 132, 213, 231, 312, 321.

<H3>Transposition Order</H3>

In <I>transposition order</I> two successive permutations (including
  the first and last) differ by
  the transposition of two adjacent elements.
The algorithm used is often called the "Johnson-Trotter" algorithm
  but it was discussed earlier in the works of Steinhaus and
  the campanologist ???.
Here are the permutations of {1,2,3} listed by adjacent transpositions
  (for <I>n</I> = 3 there are exactly two such lists, one the reversal
  of the other): 123, 132, 312, 321, 231, 213.

The figure below indicates the movement of elements in the algorithm
  when <I>n</I> = 5  (reading left-to-right).
Notice how the light blue line, which represents element
  5, sweeps back and forth.
This is indicative of the general recursive construction in which the
  largest element <I>n</I> sweeps back and forth in the 
  recursively constructed list of permutations of {1,2,...,<I>n</I>-1}.
The blue and green lines, representing 1 and 2, swap positions only
  once, at the center of the figure.

<P>
<CENTER>
<IMG SRC=ico/gray1.gif ALT = [Johnson-Trotter Weave] height=42 width=738>
</CENTER>

<H3>Young Tableau</H3>
<A NAME="Tableau"></A>

A standard <I>Young tableau</I> of shape
  r<sub>1</sub> &gt;= r<sub>2</sub> &gt;= 
  &#183;&#183;&#183; &gt;= r<sub><I>m</I></sub> &gt; 0,
  where r<sub>1</sub> +  r<sub>2</sub> +
  &#183;&#183;&#183; + r<sub><I>m</I></sub> = n,
  is an arrangement of the integers 
  1,2,...,<I>n</I> into <I>m</I> rows, with the <I>i</I>-th row 
  containing r<sub><I>i</I></sub> elements.
The rows are left justified, the numbers increase along rows, and the 
  numbers increase down the columns.  
The illustration below shows two tableau, both of
  shape 3,1,1.
<P>
<CENTER>
<TABLE BORDER=1 CELLPADDING=8 CELLSPACING=5>
<TR><TD ALIGN=CENTER>             
<TABLE BORDER=1 CELLPADDING=10 CELLSPACING=1>
<TR>
<TD>1</TD><TD>4</TD><TD>5</TD>
</TR>
<TR>
<TD>2</TD>
</TR>
<TR>
<TD>3</TD>
</TR>
</TABLE>
</TD>
<TD ALIGN=CENTER>
<TABLE BORDER=1 CELLPADDING=10 CELLSPACING=1>
<TR>
<TD>1</TD><TD>3</TD><TD>4</TD>
</TR>
<TR>
<TD>2</TD>
</TR>
<TR>
<TD>5</TD>
</TR>
</TABLE>
</TD>
</TABLE> 
</CENTER>

<P>
To every permutation there corresponds in a natural way a pair
  of tableau, both of the same shape.  
The two tableau above are the ones corresponding to the 
  permutation 32451.
This correspondence is known as the Schensted correspondence 
  and is explained in many combinatorics texts.  
See, for example, Knuth vol. 3 <I>Sorting and Searching</I>, 
  or Stanton and White, <I>Constructive Combinatorics</I>
  (Section 3.6, pg. 85-87).

<P>
<HR>
<P>
There is a fascinating connection between lists of permutations
  and the art of Bell Ringing known as Campanology.
<A HREF="http://sun1.bham.ac.uk/r.c.wilson/BUSCR/Webpages.html">Here</A>
  is a collection of web pages connected to bells and ringing.

<P>
<HR>
<P>
Programs available: <IMG SRC=ico/cat.gif height=32 width=43>
<UL>
<LI>
Permutations in lexicographic order:
  <A HREF=dis/distribute.pl.cgi?package=Permlex.p>Pascal program</A>
  <!--#include file="inc/Green.include" -->.
  <A HREF=dis/distribute.pl.cgi?package=Permlex.c>C program</A>
  <!--#include file="inc/Green.include" -->.
<LI>
Permutations by transpositions (Steinhaus-Johnson-Trotter algorithm):
  &nbsp;
  (<A HREF=dis/distribute.pl.cgi?package=SJT.p>Pascal program</A>
  <!--#include file="inc/Green.include" -->) &nbsp;
  (<A HREF=dis/distribute.pl.cgi?package=SJT.c>C program</A>
  <!--#include file="inc/Green.include" -->).
<LI>
  <FONT SIZE=-1>
  Nijenhuis and Wilf's routine for producing the 
  <A HREF="ftp://anusf.anu.edu.au/mld900/combinatorics/nexper.f">next
  permutation</A> (in
  FORTRAN and offsite).
  </FONT>
</UL>

<HR>
<A HREF=gen/perm.html>
<IMG SRC=ico/gen.gif align=left border=0 height=33 width=35></A>
<!--#include file="inc/Border.include"-->

<HR>
<FONT SIZE=-1>
<!--#include file="inc/Wizard.include"--><br>   
<!-- This page maintained by -->
<!-- <A HREF="mailto:cos@theory.csc.uvic.ca">Grandpa Simpson</A> -->
<!-- <IMG SRC=ico/Grandpa_Simpson.gif height=31 width=28>.<BR> -->
It was last updated <!--#echo var="LAST_MODIFIED" -->.<BR>
<!--#exec cgi="inc/cos-counter/counter-nl"--><BR>
<!--#include file="inc/Copyright.include"-->
</FONT>
</HTML>

