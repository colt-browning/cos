<!DOCTYPE html>
<html>
<?php include "head.html"; ?>

<body>

<div id="left">
<?php include "logo.html"; ?>
<nav id="nav0" class="index"></nav>
</div>

<div id="header">
<span id="title">The Combinatorial Object Server</span>
</div>

<div id="info">

<h1>Welcome!</h1>
  
On this website you can
<ul class="enum">
<li>generate combinatorial objects with parameters of your choice</li>
<li>explore various combinatorial algorithms</li>
<li>download free source code for those algorithms (GNU General Public License)</li>
</ul>
Have fun exploring!

<p>
The Combinatorial Object Server was first launched by <a href="http://www.cs.uvic.ca/~ruskey/">Frank Ruskey</a> at the University of Victoria (Canada).
The original website has been shut down for some time, and this new version has been relaunched in 2018 by <a href="http://tmuetze.de">Torsten  M&uuml;tze</a>, <a href="http://www.socs.uoguelph.ca/~sawada/">Joe Sawada</a> and <a href="http://cs.williams.edu/~aaron">Aaron Williams</a>.
We plan to bring the original Combinatorial Object Server content back online step by step, and to extend it by new material.
This website also provides an experimental interface to many of the combinatorial algorithms that are part of the <a href="https://www.jjj.de/fxt/">FXT library</a> written by J&ouml;rg Arndt, to the  <a href="http://pallini.di.uniroma1.it/">nauty toolsuite</a> written by Brendan McKay, and to the <a href="https://users.cecs.anu.edu.au/~bdm/plantri/">plantri and fullgen tool</a> written by Gunnar Brinkmann and Brendan McKay.

<p>
This website is intended as a community project, and welcomes your contributions and feedback (new algorithms/code/references)!
All code is available via the public GIT repository <a href="https://gitlab.com/tmuetze81/cos">https://gitlab.com/tmuetze81/cos</a>.
Please also let us know when you find the material on this website useful for your research.
We are always interested to hear when people are using our work: <img src="email.png" height="22px" style="vertical-align: text-bottom">

<p> 
If you are citing this project, please use one of the following formats:
<ul class="enum">
<li>COS++. The Combinatorial Object Server (2021). http://combos.org.</li>
<li>COS++. The Combinatorial Object Server (2021): Generate permutations. http://combos.org/perm.</li>
</ul>

<p>
<img height="60" src="img/wizard.gif">

</div>

<script src="script.js"></script>

</body>
</html>
