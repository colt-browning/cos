
/* This program will generate the binary trees in Gray code 
 * by showing the positions of those bits changed between consecutive
 * bit strings.
 * The algorithm is from Dr. F.Ruskey's book Combinartorial Generation.
 * Programmer: G.LI @Nov. 1995.
 * Note: the executable binary of this program is BinTreeGray.
 */

program genBin(input,output);
const
  MaxSize = 50;

var
 L : array [1..MaxSize] of integer;
 N,k,i : integer;

procedure neg(n,k,p: integer); forward;
procedure swap(p1,p2: integer);
var  temp : integer;
begin
 temp := L[p1]; L[p1]:=L[p2]; L[p2]:= temp;
 writeln(p1:4,p2:4);
end;

procedure gen(n,k,p: integer);
begin
 if k=1 then gen(n,2,p) else
 if (1<k) and (k<n) then begin
   neg(n,k+1,p);
   if k=n-1 then swap(n+1+p,n+2+p) else
   if k=2 then swap(4+p,6+p)
   else swap(k+2+p,2*k+3+p);
   gen(n-1,k-1,p+2);
 end;
end; { of gen() }

procedure neg(n,k,p: integer);
begin
 if k=1 then gen(n,2,p) else
 if (1<k) and (k<n) then begin
   neg(n-1,k-1,p+2);
   if k=n-1 then swap(n+1+p,n+2+p) else
   if k=2 then swap(4+p,6+p)
   else swap(k+2+p,2*k+3+p);
   gen(n,k+1,p);
 end;
end;
	
begin
	write('Input N='); readln(N);
	write('Input k='); readln(k);

	if k=1 then begin
	  L[1]:=1; L[3]:=1; L[4]:=1;
	  L[2]:=0; L[5]:=0; L[6]:=0;
	end;
	if (1<k) and (k<N) then begin
	  for i:=1 to k do L[i]:=1;
	  L[k+1]:=0; L[k+2]:=1; 
	  for i:=k+3 to k+2+k do L[i]:= 0;
	  for i:=1 to N-k-1 do begin
	    L[2*k+2+2*i-1]:=1;
	    L[2*k+2+2*i]:=0;
	  end;
	end;
        if k=N then begin
	  for i:=1 to N do L[i]:=1;
	  for i:=N+1 to 2*N do L[i]:=0;
	end;
	gen(N,k,0);
end.
