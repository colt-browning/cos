#!/usr/bin/perl

###################################################################################################################
# This program transforms the reference data from references.data as explained in references.info into a webpage  #
# Written by Scott Effler (seffler@csc.uvic.ca)                                                                   #
# June 4, 2000                                                                                                    #
###################################################################################################################

MAIN:

{

open( SIZEFILE, "size" ) || die "Sorry, couldn't open file size";
$datasize = <SIZEFILE>;

$database = "references.data";

if( (-s $database) == $datasize )
{
  open( INPUT, "acknowledge.html" ) || die "Sorry, couldn't open file acknowledge.html";
  print <INPUT>;
}

else
{
  open( OUTFILE, ">acknowledge.html" );

print OUTFILE <<END_of_HTML;
Content-type: text/html

<HTML>

<HEAD>
<TITLE>Combinatorial Object Server (COS) Acknowledgements</TITLE>
</HEAD>

<BODY BACKGROUND="../ico/gray10.gif" TEXT="#000033" LINK="CC0000" VLINK="#330099" ALINK="#FF3300" >
<IMG align=left SRC="../ico/objsvr.gif" height=47 width=240>
<A HREF="../inf/info.html"><IMG ALIGN=right border=0 width=59 height=33 SRC="../ico/objinfo.gif" ALT="[Information Index]"></A>
<A HREF="../cos.html"><IMG ALIGN=right border=0 width=59 height=33 SRC="../ico/objgen.gif" ALT="[Generation Index]"></A>
<A HREF="../root.html"><IMG ALIGN=right border=0 width=78 height=33 SRC="../ico/cos.gif" ALT="[COS homepage]"></A>
<BR clear=all>

<HR noshade>

<A HREF="#GSTUDENTS">Graduate Students</A> |
<A HREF="#USTUDENTS">Undergraduate Students</A> |
<A HREF="#AGENCIES">Agencies</A> |
<A HREF="#SYSTEMS">Systems People</A> |
<A HREF="#SOFTWARE">Software Donations</A> |
<A HREF="#HARDWARE">Hardware Help</A> |
<A HREF="#TYPOS">Typo Detection</A> |
<A HREF="#AWARDS">Awards</A> |
<A HREF="#FACULTY">Faculty Pages</A> |
<A HREF="#COURSE">Course Pages</A> |
<A HREF="#BOOKS">Books</A> |
<A HREF="#INDEX">Large Indexes</A> |
<A HREF="#BOOKMARKS">Bookmarks</A><BR>

<H1 align=center>Acknowledgements</H1>

<P>
Many people have helped in building the Combinatorial "Object Server" (COS) and I thank them all.

<P>COS runs on machines housed in the <A HREF="http://www.csc.uvic.ca">Department of Computer Science</A>
at the <A HREF="http://www.uvic.ca">University of Victoria</A>.

<H2><A NAME="GSTUDENTS">My Graduate Students:</A></H2>

<UL>
<LI><B>Gang Li:</B> (Was the maintainer of the tree page.) Now retired (actually he graduated).<BR>
<LI><B>Udi Taylor:</B> Was the maintainer of the permutations page.<BR>
<LI><B>Malcolm Smith:</B> Wrote the program for generating spanning trees.<BR>
<LI><B>Bette Bultena:</B> Was the maintainer of the Subsets and Combinations page.  Now retired (i.e., graduated).<BR>
<LI><B>Joe Sawada:</B> Polynomials, bracelets, necklaces, and lots of general maintenance and development.<BR>
<LI><B>Scott Lausch:</B> (Created the stamp-folding and Genocchi permutation generators.)
</UL>

<H2><A NAME = "USTUDENTS">Undergraduate Students:</A></H2>
<UL>
<LI><A HREF="http://gulf.uvic.ca/~rsutherl"><B>Ryan Sutherland</B></A> (The background to this page +.)<BR>
<LI><B>Jeremy Schwartzentruber</B> (Setting up the initial server +.)<BR>
<LI><A HREF="http://gulf.uvic.ca/~abultena/"><B>Bette Bultena</B></A>(Program translations.)<BR>
<LI><A HREF="http://www.csc.uvic.ca/%7Eglchen/"><B>Glen Chen</B></A> (Program translations, gif design, counter implementation + MUCH MORE)<BR>
<LI><A HREF="http://www.csc.uvic.ca/%7Enbuchana/"><B>Norm Buchanan</B></A> (work on poset section)<BR>
<LI><A HREF="http://www.csc.uvic.ca/%7Eether/"><B>Melissa Etheridge</B></A> (converting the generation pages to perl cgi scripts). 
<LI><A HREF="http://www.csc.uvic.ca/%7Eqoliddic"><B>Quinn Liddicoat</B></A> is the current maintainer of the COS website.
</UL>

<H2><A NAME = "AGENCIES">Agencies:</A></H2>
<UL>
<LI>NSERC through my operating grant, the equipment grant (used to purchase the Sun Ultra on which the Object Server now runs),
and the departmental NSERC infrastructure grant. 
<LI><A HREF="http://www.ic.gc.ca/ic-data/ic-eng.html">Industry Canada</A> (for their support of<A HREF="http://www.schoolnet.ca/vp/ECOS/">ECOS</A>).
</UL>

<H2><A NAME="SYSTEMS">Systems People:</A></H2>
<UL>
<LI><A HREF="http://www.csc.uvic.ca/%7Ewill/"><B>Will Kastelic</B></A> (For answering all my stupid questions + .....) 
<LI><A HREF="http://www.csc.uvic.ca/~gbroom"><B>Gord Broom</B></A> (For setting up the server on sue.uvic.ca). 
</UL>

<H2><A NAME="SOFTWARE">Software "donations":</A></H2>
<UL>
<LI><B>Carla Savage</B> --- The graphical partition generator.<BR>
<LI><B>Brendan McKay</B> --- The generators of unlabeled and bicoloured graphs: <B>makeg</B> and <B>makebg</B>.
<LI><B>Joe Culberson</B> --- The program for generating unlabelled posets (not yet integrated into COS).<BR>
<LI><A HREF="http://www.cs.newcastle.edu.au/Research/richard/"><B>Richard Webber</B></A> 
--- The program for generating red-black trees.<BR>
<LI><B>John M. Boyer</B> --- The program for generating polyominoes.<BR>
<LI><B>Joe Sawada</B> --- The program for generating unlabelled necklaces.<BR>
<LI><B>Malcolm Smith</B> --- The program for generating spanning trees of graphs.
</UL>

<H2><A NAME = "HARDWARE">Hardware help:</A></H2>
<UL>
<LI><B>Wendy Myrvold</B> For lending me her SPARC-20, "figaro".<BR>
</UL>

<H2><A NAME = "TYPOS">Typo detection:</A></H2>
<UL>
<LI><B>Steve Ward</B> (from MIT).
<!-- ??? -->
<LI><B>Appolo Hogan</B> (from U. Colorado).
<!-- ??? -->
<LI><B>Jan de Heer</B> (from ubn.nl).
<!-- typo on the necklace info page 00211 should have been 00122 -->
</UL>

<P>
Here are some sites that refer to the Object Server or some part of it.

END_of_HTML


open( DATAFILE, "references.data" );

print OUTFILE "<H2><A NAME = \"AWARDS\">Awards that COS has won:</A></H2>\n";

print OUTFILE "<OL>\n";

$linecount = 1;

while( <DATAFILE> )
{
  $line = $_;
  if( ($linecount % 3) == 1 )
  {
    @data = split(/\t/,$line);
  }
  elsif( ($linecount % 3) == 2 )
  {
    if( $data[4] eq "award\n" )
    { 
      print OUTFILE "<LI><A HREF=\"";
      print OUTFILE $data[0];
      print OUTFILE "\">";
      print OUTFILE $line;
      print OUTFILE "</A>\n";
    }
  }
  $linecount++;
}

print OUTFILE "</OL>";

print OUTFILE "<H2><A NAME=\"FACULTY\">Faculty Pages that reference COS:</A></H2>\n";
     
print OUTFILE "<OL>\n";

close( DATAFILE );
open( DATAFILE, "references.data" );
  
$linecount = 1;
        
while( <DATAFILE> )   
{
  $line = $_;
  if( ($linecount % 3) == 1 )
  {
    @data = split(/\t/,$line);
  }
  elsif( ($linecount % 3) == 2 )
  {
    if( $data[4] eq "faculty\n" )
    {
      print OUTFILE "<LI><A HREF=\"";
      print OUTFILE $data[0];
      print OUTFILE "\">";   
      print OUTFILE $line;   
      print OUTFILE "</A>\n";
    }
  }  
  $linecount++;
}
 
print OUTFILE "</OL>";

print OUTFILE "<H2><A NAME=\"COURSE\">Course pages that reference COS:</A></H2>\n";
     
print OUTFILE "<OL>\n";

close( DATAFILE );
open( DATAFILE, "references.data" );
  
$linecount = 1;
        
while( <DATAFILE> )   
{
  $line = $_;
  if( ($linecount % 3) == 1 )
  {
    @data = split(/\t/,$line);
  }
  elsif( ($linecount % 3) == 2 )
  {
    if( $data[4] eq "course\n" )
    {
      print OUTFILE "<LI><A HREF=\"";
      print OUTFILE $data[0];
      print OUTFILE "\">";   
      print OUTFILE $line;   
      print OUTFILE "</A>\n";
    }
  }  
  $linecount++;
}
 
print OUTFILE "</OL>";

print OUTFILE "<H2><A NAME=\"BOOKS\">Books that reference COS:</A></H2>\n";
     
print OUTFILE "<OL>\n";
  
$linecount = 1;

close( DATAFILE );
open( DATAFILE, "references.data" );
        
while( <DATAFILE> )   
{
  $line = $_;
  if( ($linecount % 3) == 1 )
  {
    @data = split(/\t/,$line);
  }
  elsif( ($linecount % 3) == 2 )
  {
    if( $data[4] eq "book\n" )
    {
      print OUTFILE "<LI><A HREF=\"";
      print OUTFILE $data[0];
      print OUTFILE "\">";   
      print OUTFILE $line;   
      print OUTFILE "</A>\n";
    }
  }  
  $linecount++;
}
 
print OUTFILE "</OL>";

print OUTFILE "<H2><A NAME = \"INDEX\">Large Indexes that reference COS:</A></H2>\n";
     
print OUTFILE "<OL>\n";

close( DATAFILE );
open( DATAFILE, "references.data" );
  
$linecount = 1;
        
while( <DATAFILE> )   
{
  $line = $_;
  if( ($linecount % 3) == 1 )
  {
    @data = split(/\t/,$line);
  }
  elsif( ($linecount % 3) == 2 )
  {
    if( $data[4] eq "index\n" )
    {
      print OUTFILE "<LI><A HREF=\"";
      print OUTFILE $data[0];
      print OUTFILE "\">";   
      print OUTFILE $line;   
      print OUTFILE "</A>\n";
    }
  }  
  $linecount++;
}
 
print OUTFILE "</OL>";

print OUTFILE "<H2><A NAME=\"BOOKMARKS\">Pages that have COS bookmarks:</A></H2>\n";
     
print OUTFILE "<OL>\n";

close( DATAFILE );
open( DATAFILE, "references.data" );
  
$linecount = 1;
        
while( <DATAFILE> )   
{
  $line = $_;
  if( ($linecount % 3) == 1 )
  {
    @data = split(/\t/,$line);
  }
  elsif( ($linecount % 3) == 2 )
  {
    if( $data[4] eq "bookmark\n" )
    {
      print OUTFILE "<LI><A HREF=\"";
      print OUTFILE $data[0];
      print OUTFILE "\">";   
      print OUTFILE $line;   
      print OUTFILE "</A>\n";
    }
  }  
  $linecount++;
}
 
print OUTFILE "</OL>";

# footer

print OUTFILE <<END_of_FOOTER;

<HR noshade>
<IMG align=left SRC=../ico/objsvr.gif height=47 width=240>
<A HREF="../inf/info.html"><IMG ALIGN=right border=0 width=59 height=33 SRC="../ico/objinfo.gif" ALT="[Information Index]"></A>
<A HREF="../cos.html"><IMG ALIGN=right border=0 width=59 height=33 SRC="../ico/objgen.gif" ALT="[Generation Index]"></A>
<A HREF="../root.html"><IMG ALIGN=right border=0 width=78 height=33 SRC="../ico/cos.gif" ALT="[COS homepage]"></A>
<BR clear=all>

</BODY>
</HTML>

END_of_FOOTER


close( OUTFILE );

system "chmod 644 acknowledge.html";

open( OUTFILE, "acknowledge.html" );
print <OUTFILE>;

open( SIZEFILE, ">size" );
print SIZEFILE (-s $database);

}

}
