/*
 * Copyright (c) 2024 Rohan Acharya, Torsten Muetze, and Francesco Verciani
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <vector>

#ifdef NDEBUG
#define DEBUG 0
#else
#define DEBUG 1
#endif

class Kary_tree {

  enum class Direction {up, down};

public:
  Kary_tree(int n, int k, bool special = false);
  bool next(int &v, int &dir);  // returns the last rotation applied (vertex and direction)
  void print(bool nl);  // print compact representation of k-ary tree (with newline or not)

  // access tree data
  int root() { return root_; }
  int child(int i, int a) { return children_[i][a]; }
  int parent(int i) { return parent_[i]; }
  int child_index(int p, int c);  // find index i such that children_[p][i] == c; -1 if not found

private:
  int n_;  // number of vertices
  int k_;  // arity (=number of children of each vertex)
  bool special_;  // if this flag is set, this is a special tree in which the largest vertex n always appears on the strictly rightmost branch from the root
                  // the dual graph of this tree is a (k+1)-angulation with one triangle (treated as a fake (k+1)-gon)

  int root_;
  std::vector<std::vector<int>> children_;  // each vertex has k children
  std::vector<int> parent_;  // the parent of each vertex

  // bookkeeping for Algorithm J
  std::vector<int> s_;  // rotation vertex selection
  std::vector<Direction> o_;  // direction array
  std::vector<std::vector<int>> ins_steps_;  // precomputed steps in the insertion sequence; d:=ins_steps_[j]>0 means j moves d steps up, whereas d<0 means j moves d steps down
  std::vector<int> ins_pos_;  // current position in the insertion sequence
  std::vector<bool> dir_change_;  // dir_change_[j]=true means that j has reached the upper or lower end of its zigzag move, changed direction, and now the insertion sequence needs to be recomputed

  void init_tree();  // start with the tree in which every vertex i has the vertex i+1 as its second child
  void init_bookkeeping();

  void insertion_steps(int j);  // precompute the insertion sequence for next zigzag move of vertex j

  void rotate(int j, Direction d);  // rotate vertex j in the given direction
  void left_rotate(int j);  // rotate j to the left by one position
  void right_rotate(int j);  // rotate j tp the right by one position

  void print_rec(int i);
  void check_consistency();  // check consistency of tree data structures
};
