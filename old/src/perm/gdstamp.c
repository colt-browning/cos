#include <stdio.h>
#include "./gd1.2/gd.h"
#include "stamps.h"
#define LIMIT_ERROR -1

int num = 0;

/* tree structure used for perforations
	uses binary tree representation of general trees */
struct tree
	{
	struct perforation *perfor;
	int level;		/* level in the drawing */
	int right_order;	/* these are the order in which the right */
	int left_order;		/* and left values of the perf appear in */
				/* the permutation */
	struct tree *left_child;
	struct tree *right_child;
	struct tree *parent;
	} tree;

void draw_perfs(gdImagePtr im, struct stamp *first, int N, int black,
int option);

struct tree * add_son(struct tree *current, struct tree *temp);

struct tree * find_parent(struct tree *current);

int compute_depth(struct tree *node);

void gif_perf(gdImagePtr im, struct tree *node, int parity, int N, int
black);		/* draws lines on the the gif image to show perforations */ 

void remove_tree(struct tree *node);

void gdStampFolding(struct stamp *first, int N, int option)
	{
	gdImagePtr im;
	int black, white, red, notQuiteBlack, color;
	FILE *out;
	char outName[80];
	struct stamp *current;

	int i;
	int width, height;

/*printf("\n entered gdStampFolding\n");*/
	current = first->next;
	num++;
	sprintf(outName, "/theory/www/per/tmpImages/test_%d_%d.gif",PID, num);

	width = 30 + (N - 1) * 20;
	height = 80 + N * 5;
	im = gdImageCreate(width, height);
	/* allocate background color first */
	white = gdImageColorAllocate(im, 255, 255, 255);
        black = gdImageColorAllocate(im, 0, 0, 0);
	red = gdImageColorAllocate(im, 255, 0, 0);
	notQuiteBlack = gdImageColorAllocate(im, 0, 0, 1);

/*printf("\n Before any loops\n");*/
	/* draw the stamp folding */
	i = 15;
	while (current != first)
		{
/*printf("\n inside first loop\n");*/
		color = black;
		if ((current->value == 1) && (option == 1)) color = red;
		gdImageLine(im, i, 20 + N * 2.5, i, 60 + N * 2.5, color);
		i +=20;
		current = current->next;
		}
/*printf("\n outside first loop\n");*/
	draw_perfs(im, first, N, black, option);    /* draw the
							perforations */

	/* Write to disk and destroy image */
	out = fopen(outName, "wb");
	if (!out)
		{
		printf("The file didn't open.<BR></TR></TABLE>");
		exit(1);
		}
	gdImageGif(im, out);
	fclose(out);
	gdImageDestroy(im);

	printf("<TD>");
	printf("<IMG SRC=\"per/perm/displaygif.pl.cgi?pid=%d&num=%d\">",PID, num);
	printf("</TD>\n");

	return;
	}


void draw_perfs(gdImagePtr im, struct stamp *first, int N, int black, int 
option)
	{
	struct tree *perf_tree;		/* root of tree built */
	struct tree *current_tree;	/* current piece of tree */
	struct tree *temp_tree;
	struct stamp *current;
	int value = 1;			/* order of current in permutation */

	if (N == 1) return;

	/* DO UPWARDS(odd) PERFS FIRST */
	/* set up the tree */
	perf_tree = (struct tree *) malloc (sizeof(struct tree));
	perf_tree->left_child = 0;
	current = first->next;
	current_tree = perf_tree;
	perf_tree->right_child = 0;
	perf_tree->parent = 0;
	perf_tree->right_order = 0;
	perf_tree->left_order = 0;
	perf_tree->perfor = 0;

/*printf("\n Before odd loop\n");*/
	while (current != first)
		{
/*printf("\n iterate odd loop\n");*/
		if ((current->value == N) && (N % 2 == 1))
			{
			current = current->next;
			value++;
			continue;
			}
		if (current->value == (current->perf[1])->left_value)
			/* a left parenthesis */
			{
			temp_tree = (struct tree*) malloc(sizeof(struct
tree));
			temp_tree->perfor = current->perf[1];
			temp_tree->left_order = value;
			temp_tree->left_child = 0;
			temp_tree->right_child = 0;
			current_tree = add_son(current_tree, temp_tree);
			}
		else
			/* a right parenthesis */
			{
			current_tree->right_order = value;
			current_tree = find_parent(current_tree);
			}


		current = current->next;
		value++;
		}

	/* compute the depth values for the tree */
	value = compute_depth (perf_tree->left_child);
	/* draw the perforations */
	gif_perf(im, perf_tree->left_child, 1, N, black);

	/* remove the tree from memory */
	remove_tree(perf_tree);


	if (N == 2 ) return;
        /* DO DOWNWARD(even) PERFS SECOND */
	value = 1;
	temp_tree = 0;
        /* set up the tree */
        perf_tree = (struct tree *) malloc (sizeof(struct tree));
        perf_tree->left_child = 0;
        current = first->next;
        current_tree = perf_tree;
        perf_tree->right_child = 0;
        perf_tree->parent = 0;
        perf_tree->right_order = 0;
        perf_tree->left_order = 0;
        perf_tree->perfor = 0;

/*printf("\n Before even loop\n"); */
        while (current != first)
                {
/*printf("\n iterating even loop\n");*/
		if (option != 2)
    		  {
                  if ((current->value == 1) || ((current->value == N) &&
(N % 2 == 0)))
                        {
                        current = current->next;
                        value++;
                        if (current != 0) continue;
			else break;
                        }
                  }
                if (current->value == (current->perf[0])->left_value)
                        /* a left parenthesis */
                        {
                        temp_tree = (struct tree*) malloc(sizeof(struct
tree));
                        temp_tree->perfor = current->perf[0];
                        temp_tree->left_order = value;
                        temp_tree->left_child = 0;
                        temp_tree->right_child = 0;
                        current_tree = add_son(current_tree, temp_tree);
                        }
                else
                        /* a right parenthesis */
                        {
                        current_tree->right_order = value;
                        current_tree = find_parent(current_tree);
                        }
 
 
                current = current->next;
                value++;
                }

        /* compute the depth values for the tree */
        value = compute_depth (perf_tree->left_child);
        /* draw the perforations */
        gif_perf(im, perf_tree->left_child, 2, N, black);
 
        /* remove the tree from memory */
        remove_tree(perf_tree);
	}


struct tree * add_son(struct tree *current, struct tree *temp)
	{
	if (current->left_child == 0)
		{
		temp->parent = current;
		current->left_child = temp;
		return temp;
		}

	/* otherwise, there are other siblings */
	current = current->left_child;
	while (current->right_child != 0)	/* find the last one */
		{
		current = current->right_child;
		}
	/* and attach to the tree here */
	temp->parent = current;
	current->right_child = temp;
	return temp;
	}


struct tree * find_parent(struct tree *current)
	{
	struct tree *proposal;	/* the proposed parent */

	proposal = current->parent;
	while (proposal->left_child != current)
		{
		current = proposal;
		proposal = proposal->parent;
		}
	current = proposal;

	return current;	
	}


int compute_depth (struct tree *node)
	{
	struct tree *child;
	int highest_level;	/* the level of this node */
	int sib_level = 0;	/* needed to pass on to parents 
				   and possibly other siblings */

	if (node->left_child == 0)	/* lowest level */
		{
		node->level = 1;
		highest_level = 1;
		}
	else				/* has children */
		{
		child = node->left_child;
		node->level = 1 + compute_depth(child);
		highest_level = node->level;
		}

	if (node->right_child != 0)	/* take care of all siblings */
		{
		child = node->right_child;
		sib_level = compute_depth(child);
		}

	if (sib_level > highest_level)
		return sib_level;
	else
		return highest_level;
	}

void gif_perf(gdImagePtr im, struct tree *node, int parity, int N, int
black)
	{
	int hor1, hor2, ver1, ver2;    /* horizontal and vert positions */

	if (node->left_child != 0)
		gif_perf(im, node->left_child, parity, N, black);
	if (node->right_child != 0)
		gif_perf(im, node->right_child, parity, N, black);

	if (parity == 1)	/* odd perforations */
		{
		hor1 = 15 + (node->left_order - 1) * 20;
		hor2 = 15 + (node->right_order - 1) * 20;
		ver1 = 20 + N * 2.5;
		ver2 = ver1 - node->level * 5;
		gdImageLine(im, hor1, ver1, hor1, ver2, black);
		gdImageLine(im, hor1, ver2, hor2, ver2, black);
		gdImageLine(im, hor2, ver2, hor2, ver1, black);
		}
	else			/* even perforations */
		{
		hor1 = 15 + (node->left_order - 1) * 20;
		hor2 = 15 + (node->right_order - 1) * 20;
		ver1 = 60 + N * 2.5;
		ver2 = ver1 + node->level * 5;
		gdImageLine(im, hor1, ver1, hor1, ver2, black);
		gdImageLine(im, hor1, ver2, hor2, ver2, black);
		gdImageLine(im, hor2, ver2, hor2, ver1, black);
		}
	}

void remove_tree(struct tree *node)
	{
	if (node->left_child != 0)
		remove_tree(node->left_child);
	if (node->right_child != 0)
		remove_tree(node->right_child);

	free(node);
	}
