/* This program is translated from binGraybit.p by
 * Programmer: G.LI @Nov. 1995.
 * The executable binary of this program is BinTreeGray(Feb28.96)
 */

#include <stdio.h>
#include "ProcessInput.c"
#include "OutputTrees.c"

#define  MaxSize 50

int
 L[MaxSize],
 P[3],
 N,k,i;

void swap(p1,p2)
int p1,p2;
{
 int temp;
 P[1]=p1; P[2]=p2; PrintIt(2*N,L,P);
 temp = L[p1]; L[p1]=L[p2]; L[p2]= temp;
}

void gen(n,k,p)
int n,k,p;
{
 if (k==1) gen(n,2,p); else
 if (1<k && k<n) { 
   neg(n,k+1,p);
   if (k==n-1) swap(n+1+p,n+2+p); else
   if (k==2) swap(4+p,6+p);
   else swap(k+2+p,2*k+3+p);
   gen(n-1,k-1,p+2);
 }
}

neg(n,k,p)
int n,k,p;
{
 if (k==1) neg(n,2,p); else
 if (1<k && k<n) {
   neg(n-1,k-1,p+2);
   if (k==n-1) swap(n+1+p,n+2+p); else
   if (k==2) swap(4+p,6+p);
   else swap(k+2+p,2*k+3+p);
   gen(n,k+1,p);
 }
}
	
main(argc, argv)
int argc;
char *argv[];
{
	ProcessInput(argc, argv);
        if (out_format & 1) outputP = 1;
	if (out_format & 2) outputB = 1;
	if (out_format & 4) outputG = 1;

	if (M>=0) k = M; else exit(0);
        if (k==0) shift = 2;
	if (k==0) {
	  L[1]=1; L[3]=1; L[4]=1;
	  L[2]=0; L[5]=0; L[6]=0;
	  for(i=1;i<=N-2;i++) {
	    L[6+2*i-1]=1;
	    L[6+2*i  ]=0;
	  }
	}
	if (k==1) {
	  L[1]=1; L[3]=1; L[4]=1;
	  L[2]=0; L[5]=0; L[6]=0;
	  for(i=1;i<=N-3;i++) {
	    L[6+2*i-1]=1;
	    L[6+2*i  ]=0;
	  }
	}
	if (1<k && k<N) {
	  for(i=1;i<=k;i++) L[i]=1;
	  L[k+1]=0; L[k+2]=1; 
	  for(i=k+3;i<=k+2+k;i++) L[i]= 0;
	  for(i=1;i<=N-k-1;i++) {
	    L[2*k+2+2*i-1]=1;
	    L[2*k+2+2*i]=0;
	  }
	}
        if (k==N) {
	  for(i=1;i<=N;i++) L[i]=1;
	  for(i=N+1;i<=2*N;i++) L[i]=0;
	}
	if (k==0) gen(N+1,1,0); else
	   gen(N,k,0);
	PrintIt(2*N,L,P);
        printf("</table><P>The total number of trees is : %4d<BR>\n",num);
	exit(0);
}
