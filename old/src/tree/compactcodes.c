// compactcodes.c
// Prints out all compact codes with n codeword lengths
// remember to compile in gcc using -lm flag

// written by:  Chris Deugau
// Last Update:  March 23rd 2005

int P[100];
int codeLength;
int currArrayRef;

#define AS_HTML 1
#define NO_HTML 2
#define TEST_CASE 3


#include <math.h>
#include "OutputTrees.c"
#include "gd1.2/gd.h"


double log2(double x)
{
  return log10(x)/log10(2);
}

void genTree(int l, double S, int n) 
{
  int i;
  double l_float = floor(log2(2.0/(1.0 - S)));
  double u_float = floor(log2((float) n / (1.0 - S)));

  // bigL is set to the lower limit
  int bigL = max(l, (int) l_float);

  // bigU is set to the upper limit
  int bigU = (int) u_float;

  double SModifier = pow(2.0, -1.0 * (double) bigL);

  if (n == 1) {
    P[currArrayRef+1] = P[currArrayRef];
    PrintIt(codeLength, &P[0], &P[0]);
    return;
  }

  currArrayRef = currArrayRef + 1;
  for (i=bigL; i <= bigU; i++) {
    P[currArrayRef] = i;
    genTree(i, S + SModifier, n-1);

    SModifier = SModifier / 2.0;
  }
  currArrayRef = currArrayRef - 1;
}



int main(int argv, char *argc[])
{
  ProcessInput(argv, argc);
  codeLength = N;

  if (out_format & 1) outputC = 1;
  if (out_format & 8) outComGif = 1;

  currArrayRef = 0;
  genTree(0,0.0,codeLength);


  printf("</table><P>The total number of trees is: %4d<BR>\n",num);
}













