
/*=============================================================================*/
/* Involution, for generating involutions of 1,2,...,N in one-line notation.   */
/* Copyright (C) 1998 Frank Ruskey                                             */
/*                                                                             */
/* This program is free software; you can redistribute it and/or               */
/* modify it under the terms of the GNU General Public License                 */
/* as published by the Free Software Foundation; either version 2              */
/* of the License, or (at your option) any later version.                      */
/*                                                                             */
/* This program is distributed in the hope that it will be useful,             */
/* but WITHOUT ANY WARRANTY; without even the implied warranty of              */
/* MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the               */
/* GNU General Public License for more details.                                */
/*                                                                             */
/* To get a copy of the GNU General Public License write to the Free Software  */
/* Foundation, Inc., 59 Temple Place - Suite 330, Boston, MA  02111-1307, USA. */
/*=============================================================================*/

class Involution {

static int N;
static int count = 0;
static int[] p;         // the permutation.

static void printIt () {
	++count;
	for (int i=0; i<N; ++i) System.out.print( (1+p[i])+" " );
	System.out.println();
}

static void swap( int i, int j ) {
    int temp = p[i];  p[i] = p[j];  p[j] = temp;
}

static void cycle ( int n ) {
	if (n >= N) { printIt();  return; }
	// Do the 1 cycle part of the recurrence
	cycle(n+1);
	// See if we can process 2-cycles
	if (N-n >=2) {
		// Make a 2 cycle...
		swap(n, n+1);
		cycle(n+2);
		// Loop through and replace ....
		for( int i=n-1; i >=0; i-- ) {
			if (p[i]==i) {	// We're swapping out a singleton
				swap(i, n);    swap (n, n+1);
				cycle(n+2);
				swap(n, n+1);  swap(i, n);
			} else {
				// We're swapping 2-cycles
				swap( p[i], n);  swap(i, n+1);
				cycle(n+2);
				swap( i, n+1);   swap( p[i],n);
		    }
		}
		swap(n, n+1);
	}
}

public static void main ( String[] args ) {
	N = Integer.parseInt( args[0] );
	p = new int[N];
	for( int i=0; i < N; i++) p[i] = i;
	cycle( 0 );
	System.out.println( "count = "+count );
}

}




