/*
 * Copyright (c) 2020 Arturo Merino, Ondrej Micka, Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cassert>
#include <vector>
#include "switch.hpp"
#include "utils.hpp"

Switch::Switch(int n, int d) {
  assert((d>=1) && (d<=n) && "switch parameter d must satisfy 1<=d<=n");
  if (d==1) {
    // the switch tau_{n,1} defined in the paper
    std::vector<bool> x(2*n+1,0);
    for (int i=0; i<n; ++i) {
      x[i] = 1;
    }
    init(x, n, 2*n, -1, 2*n, 1);
  } else if (d==2) {
    // the switch tau_{n,2} defined in the paper
    std::vector<bool> x(2*n+1,0);
    for (int i=0; i<n; ++i) {
      x[2*i] = 1;
    }
    init(x, 2*n, 2*n-1, -1, 2*n, 2);
  } else {
    // the switch tau_{n,d,z} with z=1^{(d-1)/2}0^{(d-1)/2} defined in the paper
    assert(d>=3 && (d % 2 == 1) && ((2*n+1) % d == 0) && "d must be odd divisor of 2n+1");
    int c = (2*n+1)/d;
    assert(c>=3 && (c % 2 == 1) && "c must be odd");
    std::vector<bool> x(2*n+1,0);
    for (int i=0; i<(c-1)/2; ++i) {
      x[i*d] = 1;
    }
    for (int j=0; j<c; ++j) {
      for (int i=1; i<=(d-1)/2; ++i) {
        x[j*d+i] = 1;
      }
    }
    init(x, d*(c-1)/2, 2*n+1-d, -1, 2*n+1-d, -d);  // d*(c-1)/2=(2*n+1-d)/2, as c*d=2*n+1
  }
}

void Switch::init(const std::vector<bool> &x, int over, int under, int f, int finv, int eff_shift) {
  x_ = x;
  underlined_ = under;
  overlined_ = over;
  f_ = f;
  finv_ = finv;
  eff_shift_ = eff_shift;
}

std::vector<bool> Switch::get_x(void) {
  return x_;
}

int Switch::get_underlined(void) {
  return underlined_;
}

int Switch::get_overlined(void) {
  return overlined_;
}

int Switch::get_f(void) {
  return f_;
}

int Switch::get_finv(void) {
  return finv_;
}

int Switch::get_eff_shift(void) {
  return eff_shift_;
}

std::vector<bool> Switch::get_x_underlined() {
  auto tmp = x_;
  tmp[underlined_] = !tmp[underlined_];
  return tmp;
}

std::vector<bool> Switch::get_x_overlined() {
  auto tmp = x_;
  tmp[overlined_] = !tmp[overlined_];
  return tmp;
}