<?php
  if (isset($_GET["g"]) && isset($_GET["fmt"]) && isset($_GET["num"]) && isset($_GET["gfx"])) {
    $fmt = intval($_GET["fmt"]);
    $g = min(intval($_GET["g"]), 15);  // hard-wired maximum genus
    $cmd = "code/sgroup/sgroup"
                             . " -g" . strval($g)
                             . (isset($_GET["m"]) ? " -m" . escapeshellarg($_GET["m"]) : "")  /* three optional parameters */
                             . (isset($_GET["c"]) ? " -c" . escapeshellarg($_GET["c"]) : "")
                             . (isset($_GET["e"]) ? " -e" . escapeshellarg($_GET["e"]) : "")
                             . " -l" . strval(limit($fmt))  /* hard-wired maximum number of semigroups */
                             . " 2>&1"  /* this command redirects stderr to stdout */
                             . (($fmt == 2) ? " > file.txt" : "");  /* redirect output to file */                             
    exec($cmd, $result);
    $num = filter_var($_GET["num"], FILTER_VALIDATE_BOOLEAN);
    $gfx = filter_var($_GET["gfx"], FILTER_VALIDATE_BOOLEAN);
    output($result, $fmt, "string_to_sgroup", $g, "svg_sgroup", 0, "svg_wheel", "sgroup_col", $num, $gfx);
  }
?>
