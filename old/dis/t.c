/*
 *  savage.c
 *
 *  This program generates Gray Code sequences of integer partitions, as
 *  implied by Carla D. Savage in her article "Gray Code Sequences of
 *  Partitons" in the Journal of Algorithms 10, 577-595 (1989).
 *
 *  The program is either invoked with one or two integral command line
 *  arguments.  If one argument, n, is present, the list L(n,n) is 
 *  outputted.  L(n,n) is the Gray Code listing of all partitions of n,
 *  starting with the partition 1^n, and terminating with the partition
 *  n .  If two arguments, n and k, are present, the list L(n,k) is 
 *  outputted.  L(n,k) is the Gray Code listing of all partitions of n
 *  with largest part at most k.  L(n,k) starts with 1^n, and terminates
 *  with k^a.b, where a = n/k and b = n mod k.  The one exception to 
 *  this is if (n,k) = (6,4), in which case the listing begins with 
 *  111111, but ends with 411.
 *
 *  The output is formatted with one partition per line, with parts
 *  separated with a single space.
 *
 *     Jesse D. Bingham
 *     April 15, 1999.
 *     University of Victoria
 *     Department of Computer Science
 *     jbingham@csc.uvic.ca
 *
 */

#include <stdio.h>
#include "lists.h"

#define TRUE  1
#define FALSE 0

#define USAGE { printf("usage:\nsav <n> [<k>]\n");  exit(1); }

typedef int bool;

/*
 *  function prototypes
 */
void init_b( int n );
void printObject( char suffix );
void ListX(    int n, int k );
void ListXbar( int n, int k );
void ListY(    int n, int k );
void ListYbar( int n, int k );
void M(    int n, int k, int endI );
void Mbar( int n, int k, int endI );
void L(    int n, int k, int endI );
void Lbar( int n, int k, int endI );

/*
 *  b is the global array used to store a partition.
 */
int *b;
int NN;

bool
elementOfX( int n, int k )
/*
 *  Returns true if (n,k) is a member of X, false otherwise.
 */
{
   if ( n ==  6 && k == 4 ) return(TRUE);
   if ( n ==  6 && k == 6 ) return(TRUE);
   if ( n == 14 && k == 4 ) return(TRUE);
   if ( n == 15 && k == 5 ) return(TRUE);
   if ( n == 12 && k == 7 ) return(TRUE);
   return(FALSE);
}

bool
elementOfY( int n, int k )
/*
 *  Returns true if (n,k) is a member of Y, false otherwise.
 */
{
   if ( n == 11 && k == 5 ) return(TRUE);
   if ( n == 13 && k == 6 ) return(TRUE);
   if ( n == 18 && k == 4 ) return(TRUE);
   if ( n == 20 && k == 5 ) return(TRUE);
   return(FALSE);
}

void
printObject( char suffix )
/*
 *  prints the element of the array b until a zero is encounterred.
 *  the character suffix (either '\n' or ' ') is printed afterwards.
 */
{
   int i = 1;
   int cnt = 0;
   while ( b[i] != 0 ) {
      cnt += b[i];
      printf("%d", b[i++] );
      if ( b[i] != 0 ) printf(" ");
   }
   if ( i > 1 ) printf("%c", suffix );
   if (suffix == '\n' && cnt != NN) printf( "ERROR!!!...............\n" );
}

void 
init_b( int n )
/*
 *  Allocates the space for and initializes the contents of the array b.
 */
{
   int i;
   b = (int*)malloc( (n+2) * sizeof(int) );
   for ( i=1; i<=n+1; i++ ) b[i] = 0;
}

void
ListX( int n, int k )
/*
 *  Given (n,k), which is assumed to be a member of X, the special
 *  case list L(n,k) is outputted.
 */
{
   int listI=0;
   char **list;

   if ( n ==  6 && k == 4 )
      list = L_6_4;
   else if ( n ==  6 && k == 6 )
      list = L_6_6;
   else if ( n == 14 && k == 4 )
      list = L_14_4;
   else if ( n == 15 && k == 5 )
      list = L_15_5;
   else if ( n == 12 && k == 7 )
      list = L_12_7;

   while ( list[listI] != "" ) {
      printObject(' ');
      printf("%s\n", list[listI] );
      listI++;
   }  
}

void
ListXbar( int n, int k )
/*
 *  Given (n,k), which is assumed to be a member of X, the special
 *  case list L(n,k) is outputted in reverse order.
 */
{
   int listI=0;
   int length;
   char **list;

   if ( n ==  6 && k == 4 ) {
      list = L_6_4;
      length = lengthOf_L_6_4;
   }
   else if ( n ==  6 && k == 6 ) {
      list = L_6_6;
      length = lengthOf_L_6_6;
   }
   else if ( n == 14 && k == 4 ) {
      list = L_14_4;
      length = lengthOf_L_14_4;
   }
   else if ( n == 15 && k == 5 ) {
      list = L_15_5;
      length = lengthOf_L_15_5;
   }
   else if ( n == 12 && k == 7 ) {
      list = L_12_7;
      length = lengthOf_L_12_7;
   }
   listI = length-1;
   while ( listI >= 0 ) {
      printObject(' ');
      printf("%s\n", list[listI--] );
   }  
}

void
ListY( int n, int k )
/*
 *  Given (n,k), which is assumed to be a member of Y, the special
 *  case list M(n,k) is outputted.
 */
{
   int listI=0;
   char **list;

   if ( n ==  11 && k == 5 )
      list = M_11_5;
   else if ( n ==  13 && k == 6 ) 
      list = M_13_6;
   else if ( n == 18 && k == 4 ) 
      list = M_18_4;
   else if ( n == 20 && k == 5 ) 
      list = M_20_5;

   while ( list[listI] != "" ) {
      printObject(' ');
      printf("%s\n", list[listI] );
      listI++;
   }  
}

void
ListYbar( int n, int k )
/*
 *  Given (n,k), which is assumed to be a member of Y, the special
 *  case list M(n,k) is outputted in reverse order.
 */
{
   int listI=0;
   int length;
   char **list;

   if ( n ==  11 && k == 5 ) {
      list = M_11_5;
      length = lengthOf_M_11_5;
   }
   else if ( n ==  13 && k == 6 ) {
      list = M_13_6;
      length = lengthOf_M_13_6;
   }
   else if ( n == 18 && k == 4 ) {
      list = M_18_4;
      length = lengthOf_M_18_4;
   }
   else if ( n == 20 && k == 5 ) {
      list = M_20_5;
      length = lengthOf_M_20_5;
   }
   listI = length-1;
   while ( listI >= 0 ) {
      printObject(' ');
      printf("%s\n", list[listI--] );
   }  
}

void 
M( int n, int k, int endI )
/*
 *  Outputs the list M(n,k).  The paramenter endI is the index of the last
 *  nonzero entry in b, i.e. integers added to the 'partial' partition
 *  stored in b are done so starting at b[endI+1].
 */
{
   int i;
   
   /*
    *  if k = 0, M(n,k) = {}
    */
   if (k == 0) {
      return;
   }

   /*
    *  if k = 1, M(n,k) = 1^n
    */
   if (k == 1) {
      for ( i = endI+1; i <= endI+n; i++ ) b[i] = 1;
      printObject('\n');
      for ( i = endI+1; i <= endI+n; i++ ) b[i] = 0;
      return;
   }

   /*
    *  if (n,k) is an element of Y, M(n,k) = List(n,k)
    */
   if ( elementOfY(n,k) ) {
      ListY(n,k);
      return;
   }

   /*
    *  if 2k+1 <= n < 3k-1, M(n,k) = L(n,k-1) o
    *                                  __________
    *                                k.L(n-k,k-1) o
    *
    *                                (k+1).L(n-k-1,k-2) o
    *                                            ___________
    *                                (k+1).(k-1).L(n-2k,k-2) o
    *
    *                                k.k.L(n-2k,k)
    */
   if ((2*k+1 <= n) && (n < 3*k-1)) {
      L(n,k-1,endI);
      b[endI+1] = k; Lbar(n-k,k-1,endI+1);
      b[endI+1] = k+1; L(n-k-1,k-2,endI+1);
      b[endI+1] = k+1; b[endI+2] = k-1; Lbar(n-k-k,k-2,endI+2);
      b[endI+1] = k; b[endI+2] = k; L(n-k-k,k,endI+2);
      b[endI+1] = 0; b[endI+2] = 0;
      return;
   }

   /*
    *  if n = 3k-1, M(n,k) = L(n,k-1) o
    *                           __________
    *                         k.M(n-k,k-1) o
    *
    *                         (k+1).L(n-k-1,k-1) o
    *                                   _____________
    *                         k.k.(k-1).L(n-3k+1,k-1)
    */
   if (n == 3*k-1) {
      L(n,k-1,endI);
      b[endI+1] = k; Mbar(n-k,k-1,endI+1);
      b[endI+1] = k+1; L(n-k-1,k-1,endI+1);
      b[endI+1] = k; b[endI+2] = k; b[endI+3] = k-1; Lbar(n-3*k+1,k-1,endI+3);
      b[endI+1] = 0; b[endI+2] = 0; b[endI+3] = 0;
      return;
   }

   /*
    *  if n > 3k-1, M(n,k) = L(n,k-1) o
    *                          __________
    *                        k.M(n-k,k-1) o
    *
    *                        (k+1).L(n-k-1,k-1) o
    *                                  _____________
    *                        k.k.(k-1).L(n-3k+1,k-1) o
    *
    *                        k.k.k.L(n-3k,k)
    */
   if (n > 3*k-1) {
      L(n,k-1,endI);
      b[endI+1] = k; Mbar(n-k,k-1,endI+1);
      b[endI+1] = k+1; L(n-k-1,k-1,endI+1);
      b[endI+1] = k; b[endI+2] = k; b[endI+3] = k-1; Lbar(n-3*k+1,k-1,endI+3);
      b[endI+1] = k; b[endI+2] = k; b[endI+3] = k; L(n-3*k,k,endI+3);
      b[endI+1] = 0; b[endI+2] = 0; b[endI+3] = 0;
      return;
   }
} 

void 
Mbar( int n, int k, int endI )
/*
 *  Outputs the list M(n,k), in reverse order  The paramenter endI is 
 *  the index of the last nonzero entry in b, i.e. integers added to 
 *  the 'partial' partition stored in b are done so starting at b[endI+1].
 */
{
   int i;

   /*            ______
    *  if k = 0, M(n,k) = {}
    */
   if (k == 0) {
      return;
   }

   /*            ______
    *  if k = 1, M(n,k) = 1^n
    */
   if (k == 1) {
      for ( i = endI+1; i <= endI+n; i++ ) b[i] = 1;
      printObject('\n');
      for ( i = endI+1; i <= endI+n; i++ ) b[i] = 0;
      return;
   }

   /*                               ______   _________
    *  if (n,k) is an element of Y, M(n,k) = List(n,k)
    */
   if ( elementOfY(n,k) ) {
      ListYbar(n,k);
      return;
   }

   /*                       ______       _________
    *  if 2k+1 <= n < 3k-1, M(n,k) = k.k.L(n-2k,k) o
    *
    *                                (k+1).(k-1).L(n-2k,k-2) o
    *                                      ____________
    *                                (k+1).L(n-k-1,k-2) o
    *
    *                                k.L(n-k,k-1) o
    *                                ________
    *                                L(n,k-1)
    */
   if ((2*k+1 <= n) && (n < 3*k-1)) {
      b[endI+1] = k; b[endI+2] = k; Lbar(n-k-k,k,endI+2);
      b[endI+1] = k+1; b[endI+2] = k-1; L(n-k-k,k-2,endI+2);
      b[endI+2] = 0; 
      b[endI+1] = k+1; Lbar(n-k-1,k-2,endI+1);
      b[endI+1] = k; L(n-k,k-1,endI+1);
      b[endI+1] = 0; 
      Lbar(n,k-1,endI);
      return;
   }

   /*               ______
    *  if n = 3k-1, M(n,k) = k.k.(k-1).L(n-3k+1,k-1) o
    *                              ____________
    *                        (k+1).L(n-k-1,k-1) o
    *
    *                        k.M(n-k,k-1) o
    *                        ________
    *                        L(n,k-1)
    */
   if (n == 3*k-1) {
      b[endI+1] = k; b[endI+2] = k; b[endI+3] = k-1; L(n-3*k+1,k-1,endI+3);
      b[endI+2] = 0; b[endI+3] = 0;
      b[endI+1] = k+1; Lbar(n-k-1,k-1,endI+1);
      b[endI+1] = k; M(n-k,k-1,endI+1);
      b[endI+1] = 0;
      Lbar(n,k-1,endI);
      return;
   }

   /*               ______         _________
    *  if n > 3k-1, M(n,k) = k.k.k.L(n-3k,k) o
    *
    *                        k.k.(k-1).L(n-3k+1,k-1) o
    *                              ____________
    *                        (k+1).L(n-k-1,k-1) o
    *
    *                        k.M(n-k,k-1) o
    *                        ________
    *                        L(n,k-1)
    */
   if (n > 3*k-1) {
      b[endI+1] = k; b[endI+2] = k; b[endI+3] = k; Lbar(n-3*k,k,endI+3);
      b[endI+1] = k; b[endI+2] = k; b[endI+3] = k-1; L(n-3*k+1,k-1,endI+3);
      b[endI+2] = 0; b[endI+3] = 0;
      b[endI+1] = k+1; Lbar(n-k-1,k-1,endI+1);
      b[endI+1] = k; M(n-k,k-1,endI+1);
      b[endI+1] = 0;
      Lbar(n,k-1,endI);
      return;
   }
}

void 
Lbar( int n, int k, int endI )
/*
 *  Outputs the list L(n,k), in reverse order  The paramenter endI is 
 *  the index of the last nonzero entry in b, i.e. integers added to 
 *  the 'partial' partition stored in b are done so starting at b[endI+1].
 */
{
   int i;

   /*            ______
    *  if n = 0, L(n,k) = e
    */
   if ( n == 0 ) {
      printObject('\n');
      return;
   }

   /*            ______
    *  if k = 0, L(n,k) = {}
    */
   if ( k == 0 ) {
      return;
   }

   /*            ______
    *  if k = 1, L(n,k) = 1^n
    */
   if ( k == 1 ) {
      for ( i = endI+1; i <= endI+n; i++ ) b[i] = 1;
      printObject('\n');
      for ( i = endI+1; i <= endI+n; i++ ) b[i] = 0;
      return;
   }

   /*                    ______
    *  if (n,k) = (2,2), L(n,k) = 2,11.
    */
   if ( n == 2 && k == 2 ) {
      b[endI+1] = 2;
      printObject('\n');
      b[endI+1] = 1; b[endI+2] = 1;
      printObject('\n');
      b[endI+1] = 0; b[endI+2] = 0;
      return;
   }

   /*                    ______
    *  if (n,k) = (4,3), L(n,k) = 31,22,211,1111.
    */
   if ( n == 4 && k == 3 ) {
      b[endI+1] = 3; b[endI+2] = 1;
      printObject('\n');
      b[endI+1] = 2; b[endI+2] = 2;
      printObject('\n');
      b[endI+1] = 2; b[endI+2] = 1; b[endI+3] = 1;
      printObject('\n');
      b[endI+1] = 1; b[endI+2] = 1; b[endI+3] = 1; b[endI+4] = 1;
      printObject('\n');
      b[endI+1] = 0; b[endI+2] = 0; b[endI+3] = 0; b[endI+4] = 0;
      return;
   }

   /*                               ______   _________
    *  if (n,k) is an element of X, L(n,k) = List(n,k)
    */
   if ( elementOfX(n,k) ) {
      ListXbar(n,k);
      return;
   }

   /*            ______   ______
    *  if n < k, L(n,k) = L(n,n)
    */
   if ( n < k ) {
      Lbar(n,n,endI);
      return;
   }

   /*              ______       _________
    *  if n >= 2k, L(n,k) = k.k.L(n-2k,k) o
    *
    *                       k.(k-1).L(n-2k+1,k-1) o
    *                       ________
    *                       M(n,k-1) 
    */
   if ( n >= 2*k ) {
      b[endI+1] = k; b[endI+2] = k; Lbar(n-k-k,k,endI+2);
      b[endI+1] = k; b[endI+2] = k-1; L(n-k-k+1,k-1,endI+2);
      b[endI+1] = 0; b[endI+2] = 0;
      Mbar(n,k-1,endI);
      return;
   }

   /*               ______             ________
    *  if n = 2k-1, L(n,k) = k.(k-1) o M(n,k-1)
    */
   if ( n == k+k-1 ) {
      b[endI+1] = k; b[endI+2] = k-1;
      printObject('\n');
      b[endI+1] = 0; b[endI+2] = 0;
      Mbar(n,k-1,endI);
      return;
   }

   /*                         ______
    *  if n = 2k-2 and n > 4, L(n,k) = k.(k-2) o
    *
    *                                  (k-1).(k-1) o
    *
    *                                  (k-1).(k-2).1 o
    *                                    __________
    *                                  k.L(k-2,k-3) o
    *
    *                                  (k-1).L(k-1,k-3) o
    *                                  ________
    *                                  L(n,k-2) 
    */
   if ( n == k+k-2 && n > 4 ) {
      b[endI+1] = k; b[endI+2] = k-2;
      printObject('\n');
      b[endI+1] = k-1; b[endI+2] = k-1;
      printObject('\n');
      b[endI+1] = k-1; b[endI+2] = k-2; b[endI+3] = 1;
      printObject('\n'); 
      b[endI+2] = 0; b[endI+3] = 0;
      b[endI+1] = k;   Lbar(k-2,k-3,endI+1);
      b[endI+1] = k-1; L(k-1,k-3,endI+1);
      b[endI+1] = 0; 
      Lbar(n,k-2,endI);
      return;
   }

   /*                    ______     __________
    *  if k <= n < 2k-2, L(n,k) = k.L(n-k.n-k) o
    *  
    *                             (k-1).L(n-k+1,n-k+1) o
    *                             ________
    *                             L(n,k-2) 
    */
   if ( k <= n && n < k+k-2 ) {
      b[endI+1] = k;   Lbar(n-k,n-k,endI+1);
      b[endI+1] = k-1; L(n-k+1,n-k+1,endI+1);
      b[endI+1] = 0; 
      Lbar(n,k-2,endI);
      return;
   }
}

void
L( int n, int k, int endI )
/*
 *  Outputs the list L(n,k).  The paramenter endI is the index of the last
 *  nonzero entry in b, i.e. integers added to the 'partial' partition
 *  stored in b are done so starting at b[endI+1].
 */
{
   int i;

   /*
    *  if n = 0, L(n,k) = e
    */
   if ( n == 0 ) {
      printObject('\n');
      return;
   }

   /*
    *  if k = 0, L(n,k) = {}
    */
   if ( k == 0 ) {
      return;
   }

   /*
    *  if k = 1, L(n,k) = 1^n
    */
   if ( k == 1 ) {
      for ( i = endI+1; i <= endI+n; i++ ) b[i] = 1;
      printObject('\n');
      for ( i = endI+1; i <= endI+n; i++ ) b[i] = 0;
      return;
   }

   /*
    *  if (n,k) = (2,2), L(n,k) = 11,2.
    */
   if ( n == 2 && k == 2 ) {
      b[endI+1] = 1; b[endI+2] = 1;
      printObject('\n');
      b[endI+1] = 2; b[endI+2] = 0;
      printObject('\n');
      b[endI+1] = 0;
      return;
   }

   /*
    *  if (n,k) = (4,3), L(n,k) = 1111,211,22,31.
    */
   if ( n == 4 && k == 3 ) {
      b[endI+1] = 1; b[endI+2] = 1; b[endI+3] = 1; b[endI+4] = 1;
      printObject('\n');
      b[endI+1] = 2; b[endI+2] = 1; b[endI+3] = 1; b[endI+4] = 0;
      printObject('\n');
      b[endI+1] = 2; b[endI+2] = 2; b[endI+3] = 0;
      printObject('\n');
      b[endI+1] = 3; b[endI+2] = 1;
      printObject('\n');
      b[endI+1] = 0; b[endI+2] = 0;
      return;
   }

   /*
    *  if (n,k) is an element of X, L(n,k) = List(n,k)
    */
   if ( elementOfX(n,k) ) {
      ListX(n,k);
      return;
   }

   /*
    *  if n < k, L(n,k) = L(n,n)
    */
   if ( n < k ) {
      L(n,n,endI);
      return;
   }

   /*
    *  if n >= 2k, L(n,k) = M(n,k-1) o
    *                               _____________
    *                       k.(k-1).L(n-2k+1,k-1) o
    *
    *                       k.k.L(n-2k,k)
    */
   if ( n >= 2*k ) {
      M(n,k-1,endI);
      b[endI+1] = k; b[endI+2] = k-1; Lbar(n-k-k+1,k-1,endI+2);
      b[endI+1] = k; b[endI+2] = k; L(n-k-k,k,endI+2);
      b[endI+1] = 0; b[endI+2] = 0;
      return;
   }

   /*
    *  if n = 2k-1, L(n,k) = M(n,k-1) o k.(k-1)
    */
   if ( n == k+k-1 ) {
      M(n,k-1,endI);
      b[endI+1] = k; b[endI+2] = k-1;
      printObject('\n');
      b[endI+1] = 0; b[endI+2] = 0;
      return;
   }

   /*
    *  if n = 2k-2 and n > 4, L(n,k) = L(n,k-2) o
    *                                        __________
    *                                  (k-1).L(k-1,k-3) o
    *
    *                                  k.L(k-2,k-3) o
    *
    *                                  (k-1).(k-2).1 o
    *
    *                                  (k-1).(k-1) o
    *
    *                                  k.(k-2)
    */
   if ( n == k+k-2 && n > 4 ) {
      L(n,k-2,endI);
      b[endI+1] = k-1; Lbar(k-1,k-3,endI+1);
      b[endI+1] = k;   L(k-2,k-3,endI+1);
      b[endI+1] = k-1; b[endI+2] = k-2; b[endI+3] = 1;
      printObject('\n'); 
      b[endI+3] = 0; 
      b[endI+1] = k-1; b[endI+2] = k-1;
      printObject('\n');
      b[endI+1] = k; b[endI+2] = k-2;
      printObject('\n');
      b[endI+1] = 0; b[endI+2] = 0;
      return;
   }

   /*
    *  if k <= n < 2k-2, L(n,k) = L(n,k-2) o
    *                                   ______________
    *                             (k-1).L(n-k+1,n-k+1) o
    *
    *                             k.L(n-k.n-k)
    */
   if ( k <= n && n < k+k-2 ) {
      L(n,k-2,endI);
      b[endI+1] = k-1; Lbar(n-k+1,n-k+1,endI+1);
      b[endI+1] = k;   L(n-k,n-k,endI+1);
      b[endI+1] = 0; 
      return;
   }
}

int
main( int   argc,
      char *argv[] )
{
   int n, k;

   if (argc == 3 ) {
      if ( (n = strtol(argv[1],NULL,10)) <= 0 ) USAGE;
      if ( (k = strtol(argv[2],NULL,10)) <= 0 ) USAGE;
   } else if (argc == 2 ) {
      if ( (n = strtol(argv[1],NULL,10)) <= 0 ) USAGE;
      k = n;
      NN = n;
   } else USAGE;

   init_b(n);
 
   L( n, k, 0 );
}
