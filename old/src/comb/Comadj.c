/******************************************************/
/* This program is describe in Hough and Ruskey,      */
/* "An efficient implementation of the Eades, Hickey, */
/* Read combination generation algorithm, J. Comb.    */
/* Math. and Comb. Computing, 4 (1988) 79-86.         */
/******************************************************/
/*translated from pascal to C by B.Bultena*/


#include <stdio.h>
#include "ProcessInput.c"


#define LEFT            1
#define FORW            1
#define RIHT            (-1)
#define BACK            (-1)
#define TRUE            1
#define FALSE           0



typedef enum {
  TUP, TUQ, TDP, TDQ, PQ, QP, PP, QQ, ULP, ULQ, LUP, LUQ, INIT, FINI
} Moves;

typedef struct StackElement {
  int spec; /*boolean*/
  enum {
    lower, upper
  } side;
  int p;
  Moves nm;   /*The next move*/
} StackElement;



/****************globals*****************/
int bitstring = 0;
int list = 0;
int bitchange = 0;
int n, k;
char A[MAX];   /*The bitstring*/
long P[MAX]; /*contains a record of where the 1's in A are*/
StackElement stk[MAX];


/****************local functions*************/

/*prints out the generated number according to the output format*/
/*note: the array is printed in reverse order*/
/*if b1 = -1, then no bits were changed*/
void printit(b1,b2)
int b1,b2;
{
  int i;
  int beg =1;

  if(!LIMIT)
    return;
  printf("<TR>");
  if(bitstring) {
    printf("<TD ALIGN=CENTER>");
    for (i=N-1; i>=0; i--)
      if (((b1!=-1))&&((i==b1-1)||(i==b2-1)))
	printf("<B><FONT COLOR=008800>%d</FONT></B>",A[i]);
      else printf("%d", A[i]);
    printf("<BR></TD>\n");
  }
  if(list) {
    printf("<TD ALIGN=CENTER>{");
    for(i=0; i<N; i++) {
      if(A[i]) {
	printf("%s%d",beg ? "" : ",",i+1);
	beg = 0;
      }
    }
    printf("}<BR></TD>\n");
  }
  if(bitchange) {
    if(b1!=-1)
      printf("<TD ALIGN=CENTER>%d,%d<BR></TD>\n",b1,b2);
    else
      printf("<TD ALIGN=CENTER><BR></TD>\n");
  }
  LIMIT--; /*count down the number of printable objects*/
}


/*Moves the '1' that is specified in P[n] in A to the left or right*/
/*called by G and Next*/
void Move(n, dir)
int n, dir;
{
  printit(P[n]+1,P[n]+dir+1);
  A[P[n]] = 0;
  P[n] += dir;
  A[P[n]] = 1;
}


/*adjusts the record of stk[level]*/
/*called by G*/
void Init(dir,level)
int dir, level;
{

  if (dir==FORW) {
    stk[level].nm = INIT;
    stk[level].spec = TRUE;
    stk[level].side = upper;
  } else {   /*dir = BACK*/
    stk[level].nm = FINI;
    stk[level].spec = FALSE;
    stk[level].side = lower;
  }
  stk[level].p = 0;   
}


/*Performs the next move depending on the move specified in stk*/
/*called by G and itself*/
void Next(n, k, level, dir)
int n, k, level, dir;
{
  if (!LIMIT)
    return;
  if (k==1) {
    Move(0,-dir);
    return;
  }
  if (k==n-1) {
    if (dir==FORW) {
      if (P[0]==1)
	stk[level].p = 0;
    }
    if (dir==BACK) {
      if (P[k-1]==k-1)
	stk[level].p = k-1;
    }
    Move(stk[level].p,-dir);
    stk[level].p += dir;
    return;
  }
  switch (stk[level].nm) {

  case INIT:
    if (dir==FORW) {
      if (P[k-3]==k-3) {
	Move(k-2,RIHT);
	stk[level].spec = TRUE;
	stk[level].side = upper;
	stk[level].nm = PQ;
      } else {
	if (P[0] == n-k)
	  Init(FORW,level+1);
	Next(n-2,k-2,level+1,FORW);
      }
    } else   /*dir=BACK*/
      Next(n-2,k-2,level+1,BACK);
    break;

  case FINI:
    if (dir==FORW) 
      Next(n-2,k,level+1,FORW);
    else {   /*dir = BACK*/
      if (P[0]==n-k) {
	Move(k-1,LEFT);
	stk[level].spec = FALSE;
	stk[level].side = lower;
	stk[level].nm = LUQ;
      } else {
	if (P[k-1]==k-1)
	  Init(BACK,level+1);
	Next(n-2,k,level+1,BACK);
      }
    }
    break;

  case PP:
    Next(n-4,k-2,level+1,-dir);
    if (dir==BACK && P[k-3]==k-3) {
      stk[level].spec = TRUE;
      stk[level].nm = PQ;
    } else {
      if (A[n-5]==0)
	stk[level].nm = LUP;
      else {
	stk[level].nm = TDP;
	stk[level].p = k-3;
      }
    }
    break;

  case QQ:
    if (dir==FORW) {
      if (P[0]==n-k-2) {
	Move(k-1,RIHT);
	stk[level].nm = FINI;
	stk[level+1].p = 0;
      } else {
	Next(n-4,k-2,level+1,BACK);
	if (A[n-5]==1)
	  stk[level].nm = LUQ;
	else
	  stk[level].nm = TDQ;
      }
    } else {   /*dir = BACK*/
      Next(n-4,k-2,level+1,FORW);
      if (A[n-5]==1)
	stk[level].nm = LUQ;
      else
	stk[level].nm = TDQ;
    }
    break;

  case PQ:
    Move(k-2,RIHT);
    if (A[n-5]==1)
      stk[level].nm = ULQ;
    else
      stk[level].nm = TDQ;
    break;

  case QP:
    Move(k-2,LEFT);
    if (dir==FORW && stk[level].spec) {
      stk[level].spec = FALSE;
      stk[level].nm = PP;
    } else {
      if (A[n-5]==1) {
	stk[level].nm = TDP;
	stk[level].p = k-3;
      } else
	stk[level].nm = ULP;
    }
    break;

  case ULQ:
    Move(k-1,RIHT);
    if (A[n-4]==0)
      stk[level].nm = TUQ;
    else {
      if (stk[level].spec)
	stk[level].nm = QP;
      else
	stk[level].nm = QQ;
    }
    stk[level].side = lower;
    break;

  case LUQ:
    Move(k-1,LEFT);
    if (A[n-4]==0)
      stk[level].nm = TUQ;
    else
      stk[level].nm = QP;
    stk[level].side = upper;
    break;

  case ULP:
    if (dir==BACK && stk[level].spec) {
      Move(k-2,LEFT);
      stk[level].nm = INIT;
    } else {
      Move(k-1,RIHT);
      if (A[n-4]==1)
	stk[level].nm = TUP;
      else
	stk[level].nm = PP;
      stk[level].side = lower;
    }
    break;

  case LUP:
    Move(k-1,LEFT);
    if (A[n-4]==1)
      stk[level].nm = TUP;
    else
      stk[level].nm = PQ;
    stk[level].side = upper;
    break;

  case TUQ:
    Move(k-2,LEFT);
    if (A[n-4]==1) {
      if (stk[level].spec)
	stk[level].nm = QP;
      else {
	if (stk[level].side==upper)
	  stk[level].nm = QP;
	else
	  stk[level].nm = QQ;
      }
    }
    break;

  case TDQ:
    Move(k-2,RIHT);
    if (P[k-2]==P[k-3]+1) {
      if (stk[level].side==upper)
	stk[level].nm = ULQ;
      else
	stk[level].nm = LUQ;
    }
    break;

  case TUP:
    Move(stk[level].p,RIHT);
    if (stk[level].p==k-3) {
      if (stk[level].side==upper)
	stk[level].nm = PQ;
      else
	stk[level].nm = PP;
    } else
      stk[level].p++;
    break;

  case TDP:
    Move(stk[level].p,LEFT);
    if (A[P[stk[level].p]-3]==0) {
      if (stk[level].side==upper)
	stk[level].nm = ULP;
      else
	stk[level].nm = LUP;
    } else
      stk[level].p--;
    break;
  } /*switch*/
}


/*initializes A with all '1's to the right*/
/*P contains the index numbers of the 1's in A*/
/*called by main*/
void Initialize()
{
  int i, diff;

  diff = N-K;
  for (i=0; i<diff; i++)
    A[i] = 0;
  for (i=diff; i<N; i++) {
    A[i] = 1;
    P[i-diff] = i;
  }
}


void G(n, k)
int n, k;
{
  int i;

  if (!LIMIT)
    return;
  if (k==1) {
    for (i=1; i<n; i++)
      Move(0,RIHT);
    return;
  }
  if (k==n-1) {
    for (i=0; i<n-1; i++)
      Move(i,RIHT);
    return;
  }
  G(n-2,k-2);
  Move(k-2,RIHT);
  stk[0].spec = TRUE;
  stk[0].side = upper;
  stk[0].nm = PQ;
  Init(BACK,1);
  do {
    Next(n,k,0,FORW);
  } while ((P[k-1]!=n-2 || P[0]!=n-k-2 || P[k-2]!=n-4) && LIMIT);
  Move(k-1,RIHT);
  G(n-2,k);
}


main(int argc, char **argv)
{
  int count;

  ProcessInput(argc, argv);
  count = LIMIT;

  if(out_format & 1)
    bitstring = 1;
  if(out_format & 2)
    list = 1;
  if(out_format & 4 )
    bitchange = 1;
  Initialize();
  G(N, K);
  printit(-1);
  printf("</TABLE><BR>Total number of combinations = %d<BR>\n",count-LIMIT);
  if(LIMIT)
    return(0);
  else
    return(-1);
}
