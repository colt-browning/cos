<?php
  if (isset($_GET["g"]) && isset($_GET["fmt"])) {
    $fmt = intval($_GET["fmt"]);
    $cmd = "code/orient/orient"
        . " -e" . escapeshellarg($_GET["g"])
        . " -l" . strval(limit($fmt))  /* hard-wired maximum number of spanning trees */
        . " 2>&1"  /* this command redirects stderr to stdout */
        . (($fmt == 2) ? " > file.txt" : "");  /* redirect output to file */
    exec($cmd, $result);
    output($result, $fmt, 0, 0, 0, 0, 0, 0, false, false);
  }
?>