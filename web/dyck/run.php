<?php
  if (isset($_GET["k"]) && isset($_GET["x"]) && isset($_GET["fmt"]) && isset($_GET["num"]) && isset($_GET["gfx"])) {
    $fmt = intval($_GET["fmt"]);
    $cmd = "code/dyck/dyck"
                             . " -k" . min(intval($_GET["k"]), limit_dyck($fmt))  /* hard-wired maximum bitstring length */
                             . ($_GET["x"] == "" ? "" : " -x" . escapeshellarg($_GET["x"]))
                             . " 2>&1"  /* this command redirects stderr to stdout */
                             . (($fmt == 2) ? " > file.txt" : "");  /* redirect output to file */
    exec($cmd, $result);
    $num = filter_var($_GET["num"], FILTER_VALIDATE_BOOLEAN);
    $gfx = filter_var($_GET["gfx"], FILTER_VALIDATE_BOOLEAN);
    output($result, $fmt, "string_to_dyck", 0, "svg_dyck", 0, 0, 0, $num, $gfx);
  }
?>

