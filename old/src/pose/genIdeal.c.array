/* Anton An
 * June 1999
 * 
 * This program generates ideals in lexicographic order for a poset, 
 * using Steiner's Variant. 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_POINTS 50	// max set size
#define NUMBER 0	// display number of ideals only
#define SUBSET 1	// display all ideals

int table[MAX_POINTS+1][MAX_POINTS+1];
int upper[MAX_POINTS+1][MAX_POINTS+1];
int lower[MAX_POINTS+1][MAX_POINTS+1];

int N;			// size of the set
int counter = 0;	// number of ideals
int input_mode;		

/* process the relation table and generate upper/lower 
 * for each element in the poset.
 */
void Process(int* steiner)
{
  int i, j, k, count, left, right; 

  // transitive closure
  for(k = 1; k <= N; k++){
    for(i = 1; i <= N; i++){
      for(j= 1; j <= N; j++){
	table[i][j] = table[i][j] || (table[i][k] && table[k][j]);
      }
    }
  }
  
  for(i = 1; i <= N; i++){
    count = 0;
    for(j = 1; j <= N; j++){
      if(table[i][j])
	upper[i][j] = 1;
      if(table[j][i]){
	lower[i][j] = 1;
	count ++;
      }
    }
    steiner[i] = count - 1;
  }
}

// copy set2 into set1    
void copySet(int* set1, int* set2, int n)
{
  int i;
  for(i = 1; i <= n; i++)
    set1[i] = set2[i];
}

// determine if a set is empty, ie, all elements are 0
int isEmpty(int *set)
{
  int i, flag;
  flag = 1;
  for(i = 1; i <= N; i++){
    if(set[i] != 0)
      {
	flag = 0;
	break;
      }
  }
  return flag;
}  

// find the minimal element in the poset
int findMinimal(int *poset, int *steiner)
{
  int i, min = 0;
  for(i = 1; i <= N; i++)
    if((poset[i]) && (steiner[i] == 0)){
      min = i;
      break;
    }
  if(min == 0)
    {
      printf("A cycle has been found. Please check.");
      printf("</TABLE>");
      exit(1);
    }
  return min;
}

// update the list for finding minimal element
void Update(int *steiner, int *poset, int min)
{
  int i;
  for(i = 1; i <= N; i++)
    if(poset[i] && upper[min][i])
      steiner[i]--;
}

// print out the ideals
void printResult(int *set)
{
  int i, flag = 0;
  if(input_mode == SUBSET){
    printf("<TR>\n<TD ALIGN=CENTER>");
    printf("{");
    for(i = 1; i <= N; i++)
      if(set[i]){
	if(flag)
	  printf(",%d", i);
	else
	  printf("%d", i);
	if(!flag)
	  flag = 1;
      }
    printf("}");
    printf("<BR></TD>\n");
  }
  counter ++;
}

// generate ideals recursively
void Ideal(int *poset, int *ideal, int *steiner)
{
  int i, j, min, count;
  int poset1[N+1], poset2[N+1], ideal1[N+1],
    steiner1[N+1], steiner2[N+1];
  
  if(isEmpty(poset))
    printResult(ideal);
  else{
    min = findMinimal(poset, steiner);
   
    copySet(poset1, poset, N);
    copySet(ideal1, ideal, N);
    copySet(steiner1, steiner, N);
    poset1[min] = 0;
    ideal1[min] = 1;
    Update(steiner1, poset1, min);
   
    copySet(poset2, poset, N); 
    copySet(steiner2, steiner, N);
    Update(steiner2, poset2, min);
    for(i = 1; i <= N; i++)
      if(upper[min][i])
	poset2[i] = 0;

    Ideal(poset2, ideal, steiner2);
    Ideal(poset1, ideal1, steiner1);
  }    
}

void main(int argc, char* argv[])
{
  FILE *input;
  int i, j, left, right;
  int poset[MAX_POINTS+1]; 
  int ideal[MAX_POINTS+1];
  int steiner[MAX_POINTS+1];
  char *fname;
  char *mode;

  if(argc != 3){
    fprintf(stderr, "Usage: genIdeal -flag in_file_name\n");
    exit(1);
  }
  
  mode = argv[1];
  if(!strcmp(mode, "-c"))
    input_mode = NUMBER;
  else if(!strcmp(mode, "-g"))
    input_mode = SUBSET;

  if(input_mode == SUBSET){
    printf("<P><TABLE BORDER=1 CELLSPACING=2 CELLPADDING=2><TR>\n");
    printf("<TH COLSPAN=1><FONT SIZE=+1>Ideals");
    printf("</FONT><BR></TH>\n");
  }

  fname = argv[2];
  if((input = fopen(fname, "r")) == NULL) {
    fprintf(stderr, "Error: could not read file %s\n", fname);
    exit(1);
  }

  // read number of elements in the set
  fscanf(input, "%d", &N);
  
  for(i = 1; i <= N; i++){
    for(j = 1; j <= N; j++){
      table[i][j] = 0;
      lower[i][j] = 0;
      upper[i][j] = 0;
    }
    table[i][i] = 1;
  }

  // store partial orderings
  do {
    fscanf(input,"%d %d", &left, &right);
    table[left][right] = 1;
  }while(left != 0);

  // initialize ideal and poset
  for(i = 1; i <= N; i++){
    poset[i] = 1;
    ideal[i] = 0;
  }
  
  Process(steiner);
  Ideal(poset, ideal, steiner); 
    
  printf("</TABLE><P>%d ideals in total\n", counter);
  fclose(input);
}

       
