/*
 * Copyright (c) 2020 Arturo Merino, Ondrej Micka, Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef TREE_HH
#define TREE_HH

#include <cassert>
#include <vector>
#include <list>
#include <utility>
#include <string>

// ordered rooted tree class
class Tree {
  friend class Tests;

public:

  class Vertex {  // single vertex in the tree
    friend class Tree;
  public:
    explicit Vertex(Vertex *parent, int id) : parent_(parent), parent_list_pos_(), children_(), id_(id) {};
    inline bool is_root() { return parent_ == nullptr; }
    inline bool is_leaf() {
      return (!is_root() && children_.empty()) || (is_root() && children_.size() == 1);
    }
    inline Vertex *get_left_child() { return children_.front(); }
    inline Vertex *get_right_child() { return children_.back(); }
    inline const std::list<Vertex *> &get_children() { return children_; }
    inline std::list<Vertex *>::iterator get_parent_list_pos() { return parent_list_pos_; }
    inline bool is_left_child() { return this == parent_->get_left_child(); }
    inline bool is_right_child() { return this == parent_->get_right_child(); }
    inline Vertex *get_right_sibling() { return *std::next(parent_list_pos_); }
    inline Vertex *get_left_sibling() { return *std::prev(parent_list_pos_); }
    inline int get_id() { return id_; }
    inline Vertex *get_parent() { return parent_; }
    // Get i-th child of the vertex. Assumes that it has i children.
    inline Vertex *get_child(int i) {
      assert((i <= children_.size()) && "vertex does not have that many children");
      int c = 0;
      auto it = children_.cbegin();
      for (; c < i; ++it, ++c) {}
      return *it;
    }
    // Return i such that vertex is i-th child of its parent. Assumes that vertex has a parent.
    inline int get_position() {
      int i = 0;
      Vertex *v = parent_->get_left_child();
      for(; v != this; v = v->get_right_sibling(), ++i) {}
      return i;
    }
    // Check if the vertex is a leaf with no brother, i.e., its parent has degree 2.
    // This does not work if the vertex or its parent is the root, but we never use it in such a situation.
    inline bool is_thin_leaf() {
      assert(!is_root() && "we should never test thinness of the root");
      assert(!parent_->is_root() && "we should never test thinness of a root's child");
      return is_leaf() && parent_->children_.size() == 1;
    }
    // Check if the vertex is a leaf with at least one brother, i.e., its parent has degree >=3.
    // This does not work if the vertex or its parent is the root, but we never use it in such a situation.
    inline bool is_thick_leaf() {
      assert(!is_root() && "we should never test thickness of the root");
      assert(!parent_->is_root() && "we should never test thickness of a root's child");
      return is_leaf() && parent_->children_.size() > 1;
    }

    // Setter for parent so that we never forget to also set parent_list_pos_.
    // If parent is nullptr (i.e., vertex is the root), then pos can be anything.
    inline void set_parent(Vertex *parent, std::list<Vertex *>::iterator pos) {
      parent_ = parent; parent_list_pos_ = pos;
    }
  private:
    Vertex *parent_;
    std::list<Vertex *>::iterator parent_list_pos_;  // position of this vertex in parent's list of children
                                                     // useful for obtaining siblings of the vertex
                                                     // (this depends on the fact that list does not invalidate iterators)
    std::list<Vertex *> children_;  // the list also represents the correct order of children from left to right
    int id_;  // unique numeric identifier in the range from 0 to num_vertices_-1 (this is useful for storing extra data about a vertex into an array)
  };

  class Subtree;  // Class to store additional information about a subtree of the root of a Tree object.

  explicit Tree(std::vector<bool> &dyck_word);
  ~Tree();

  inline Vertex *get_vtx_by_id(int id) { return id_to_vtx_[id]; }
  inline Vertex *get_root() { return root_; }

  // perform tree rotation (tree rotates right, i.e., leftmost child of root becomes the new root)
  void rotate();

  // perform inverse operation to rotate
  void rotate_inverse();

  // pull leaf closer to root
  // The leaf must be pullable to the root, i.e. it must be the left leaf of its parent vertex.
  void pull_to_root(Vertex *leaf);

  // pull leaf away from the root
  // The leaf must be pullable from the root, i.e., it must be leaf and must have a left sibling.
  void pull_from_root(Vertex *leaf);

  // push leaf away from root
  // The leaf must be pushable from the root, i.e., it must be leaf and must have a right sibling.
  void push_from_root(Vertex *leaf);

  // push leaf closer to root
  // The leaf must be pushable to the root, i.e., it must be the right child of its parent vertex.
  void push_to_root(Vertex *leaf);

  // pull operation as defined in the paper
  inline void pull() {
    assert((root_->children_.size() > 0) && !root_->get_left_child()->is_leaf() && "root needs to have a grandchild to pull");
    pull_to_root(root_->get_left_child()->get_left_child());
  };
  // push operation as defined in the paper
  inline void push() {
    assert((root_->children_.size()) && "root needs to have a child to push");
    push_from_root(root_->get_left_child());
  };

  // Make v the root of the tree.
  // The current parent of v will be made the new left child of v.
  void reroot(Vertex *v);

  // Rotate root's children to make v its left child.
  // v has to be a child of the root for this to work.
  void make_first_subtree(Vertex *v);

  inline std::pair<Vertex *, Vertex *> get_centroids() { return centroids_; }
  inline bool has_two_centroids() { return centroids_.second != nullptr; }
  inline int get_potential() { return potential_; }

  // Find the centroid(s) in the tree, that is, the vertex/pair of vertices with minimum potential
  // and the value of this potential. The results can be retrieved get_centroids() and get_potential().
  void centroids_and_potential();

  // Compute the bitstring representation of the subtree at v and append it to bs.
  // Unless v == root_, the edge towards the edge to the root of the surrounding tree is added 1...0.
  void get_subtree_bitstring(Vertex *v, std::list<bool> &bs);

  // Count the vertices in subtree under v, including v (but excluding the parent).
  int get_subtree_vertices(Vertex *v);

  // Fill subtrees with the list of root's subtrees (preserving ordering around the root).
  // The previous content of the list is cleared.
  void make_subtree_list(std::list<Subtree *> &subtrees);

  // Check if the tree is the star rooted in the centroid (first one must be used).
  inline bool is_star() {
    assert (root_ && centroids_.first == root_ && "tree has to be rooted in first centroid");
    return root_->children_.size() == num_vertices_-1;
  }
  // Check if the tree is the dumbbell rooted in the centroid (first one must be used).
  inline bool is_dumbbell() {
    assert (root_ && centroids_.first == root_ && "tree has to be rooted in first centroid");
    return
      has_two_centroids() &&
      (centroids_.first->children_.size() == num_vertices_/2) &&
      (centroids_.second->children_.size() == num_vertices_/2 - 1);
  }

  // Reroot tree in the centroid chosen according to rule (T1) and (D) in the paper.
  // Also the subtrees are ordered cyclically around the centroid/root according to those rules.
  // We also possibly swap centroids so that the root is always centroids_.first.
  // If the tree is star it is simply rooted in the centroid.
  void normalize();

  // Get all leaves in the subtree with root v and append them to leaves list.
  // Leaves will be ordered from left to right.
  void get_leaves(Vertex *v, std::list<Vertex *> &leaves);

  // Check whether the tree is symmetrical (has an automorphism) and return the symmetry (any symmetry
  // has to preserve order of the children). Expects the tree to be rooted in the first centroid and
  // root's children to be ordered according to rule (T1), i.e., you need to run normalize() first.
  // If there is no symmetry, function returns 0.
  // If the tree has two centroids we simply return 1 iff rooting the tree in each centroid gives isomorphic same trees
  // (keeping the order of children such that the other centroid is the first child).
  // If the tree has one centroid there could be symmetry given by rotation of the children.
  // In such a case, we return the smallest positive integer r such that rotating root's children by r gives us an isomorphic tree.
  int get_symmetry();

  #ifndef NDEBUG
  // output the tree into a file in graphviz dot format
  void draw(const std::string &filename, bool show_centroid=true);
  #endif

private:
  Vertex *root_;
  std::vector<Vertex *> id_to_vtx_;
  int num_vertices_;
  int num_edges_;

  // If tree has only one centroid, second entry is nullptr.
  // If first entry is nullptr, then centroids need to be calculated.
  // The centroids need to be recalculated manually when the tree changes.
  std::pair<Vertex *, Vertex *> centroids_;

  // Potential of the tree, i.e., the sum of distances from each vertex to all
  // other vertices, minimized over all possible vertices (centroid minimizes this sum).
  // The potential needs to be recalculated manually when the tree changes.
  int potential_;

  // helper function for destructor; recursively delete subtree
  void destroy_subtree(Vertex *v);

  // helper function for calculating centroids
  // Recursively calculates potential of v with respect to its subtree and writes it to potential.
  // Along the way it also calculates the size of the subtree rooted at v (=number of vertices).
  void subtree_potential(Vertex *v, std::vector<int> &potential, std::vector<int> &size);

  // Normalization differs a lot for the case of one or two centroids, so we split it.
  void normalize_one_centroid();
  void normalize_two_centroids();

  // Find lexicographically smallest rotation of bitstring among admissible rotations specified by second argument.
  // The vector is_start[i] specifies whether rotation of bitstring starting at position i is admissible.
  // This only works if the bitstrings between any two admissible starting positions are Dyck words.
  // This is achieved by Booth's algorithm running in time O(bitstring.size()).
  static int lex_smallest_rotation(const std::vector<bool> &bitstring, const std::vector<bool> &is_start);

  // Detecting tree symmetries differs a lot for the case of one or two centroids, so we split it.
  int get_symmetry_one_centroid();
  int get_symmetry_two_centroids();

  // debug function to check consistency of pointers to parent's children list
  bool check_consistency_parent_list_pos();

  #ifndef NDEBUG
  // recursive helper function for tree drawing
  void draw_subtree(std::ofstream &file, Vertex *v);
  #endif
};


// Class to store additional information about a subtree of the root of a Tree object.
// The edge leading to the root is also considered part of the subtree.
// This class is closely tied to the Tree, it stores no information about the actual structure of
// the subtree, instead it stores a pointer to the tree and reads the structure from it (or from
// its vertices).
// The subtree properties are not automatically updated when the tree changes (such as size of
// the subtree). All these changes have to be done manually.
class Tree::Subtree {
public:
  Subtree(Tree *tree, Vertex *subtree_root);
  inline Vertex *get_root() { return root_; }
  inline Tree *get_tree() { return tree_; }
private:
  Tree *tree_;  // tree of which we are a subtree
  Vertex *root_;  // root of the subtree (=child of the root of the surrounding tree)

public:
  int num_edges_;   // number of edges of the subtree (including the edge leading to the root)
  std::list<bool> bitstring_;  // bitstring representation of the subtree (the list is convenient for computing this recursively)
  std::list<Vertex *> leaves_;  // list of leaves in the subtree from left to right
  // shortcuts
  inline Vertex *get_leftmost_leaf() { return leaves_.front(); }
  inline Vertex *get_rightmost_leaf() { return leaves_.back(); }
  // structures that allows us to efficiently skip over initial non-branching part of the subtree
  Vertex *initial_path_end_;   // end vertex of the initial path
  int initial_path_length_;  // number of edges on the initial path, including the edge to the root of the surrounding tree (e.g. end == root => length = 1)

  // special trees q_i defined in the paper
  static const std::vector<std::vector<bool>> Q_TREES;   // initialized in .cpp file
  static const int Q_TREE_MAX_SIZE;   // number of edges of the largest q_i
};

// a wrapper around std::lexicographical_compare
bool lex_less(const std::vector<bool> &x1, const std::vector<bool> &x2);

#endif
