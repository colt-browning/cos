#!/public/bin/perl

require "../common.pl"; 

$BinDir = "../../bin/tree";
$RootedBIN = "$BinDir/rooted";
$BtreesBIN = "$BinDir/Btrees";
$FtreesBIN = "$BinDir/freetree";
$BintreesBIN = "$BinDir/binarytree";
$GrayBinTreeBIN = "$BinDir/BinTreeGray";
$BinTreeRotBIN = "$BinDir/bintreerot";
$LabelledtreeBIN = "$BinDir/labelled";
$RedBlackBIN = "$BinDir/rbtree";
$PlaneRootedBIN = "$BinDir/rootedplane";
$PlaneFreeBIN = "$BinDir/freeplane";
$PosetFILE = "PosetBinTree.temp";
$CompactCodesBIN = "$BinDir/compactcodes";
$MAXn = 50;

MAIN: 
{
# Read in all the variables set by the form
  if (&ReadParse(*input)) { &ProcessForm; }
}

sub InitializePosetFILE {
     open( FILE, ">$PosetFILE" ) || die "Can't open $PosetFILE:$!\n";
     $first = 2*$input{'n'};
     print FILE "$first\n";
     for ($i=1;$i<=$input{'n'};++$i) {
         $first = 2*$i-1;  $second = 2*$i;
         print FILE "$first  $second\n";
     }
     for ($i=1; $i<$input{'n'};++$i) {
         $first = 2*$i-1;  $second = 2*$i+1;
         print FILE "$first  $second\n";
     }
     for ($i=1; $i<$input{'n'};++$i) {
         $first = 2*$i;  $second = 2*$i+2;
         print FILE "$first  $second\n";
     }
     print FILE "0\n";
     close( FILE );
}

sub TreeHeaders {
   print "<P>";

   # pass correct output format string according to options on form
   $out = 0;                    # output format string
   if ($input{'output1'}) { $out = 1; }
   if ($input{'output2'}) { $out = $out | 2; }
   if ($input{'output3'}) { $out = $out | 4; }
   if ($input{'output4'}) { $out = $out | 8; }

   # set up table headers
   &TableSetup;  # from common.pl
   if($out == 0) { $out = 1; &ColumnLabel($_[0]); }
   if ($input{'output1'} && $_[0] ne '') {&ColumnLabel($_[0]);};
   if ($input{'output2'} && $_[1] ne '') {&ColumnLabel($_[1]);};
   if ($input{'output3'} && $_[2] ne '') {&ColumnLabel($_[2]);};
   if ($input{'output4'} && $_[3] ne '') {&ColumnLabel($_[3]);};
   print "</TR>\n";
}

sub EndEntry{
   print "</table>\n";
   if (int($retval) == 65280) { &LimitError; }
   &MoreInfo($_[0]);
}

sub ProcessForm {

  if ($input{'program'} eq "rooted") {

    &OutputHeader('Rooted Trees Output','tree/RootedTree');  # from common.pl
    if (int($input{'n'}) <= 0) {
	&ErrorMessage('<i>n</i> must be greater than 0');
	&EndEntry('tree/RootedTree');
	exit(0);
    }
    print "Rooted Trees for <I>n</I>= $input{'n'}";
    if ($input{'m'} >0) { print ", and <I>m</I> = $input{'m'}";
    } else { $input{'m'} = $input{'n'}; };
    if ($input{'k'} >0) { print ", <I>lb</I> = $input{'k'}";
    } else { $input{'k'} = 0; };
    if ($input{'u'} >0) { print ", <I>ub</I> = $input{'u'}";
    } else { $input{'u'} = 0; };
    print ".<P>\n";

    if (int($input{'n'}) > $MAXn) { &TooBig('n',$MAXn); exit(0); } 
    if ($input{'m'} < 2) { 
      print "Parameter <I>lb,ub</I> will be voided! Since there is only one 
	     possible tree<P>\n";
      $input{'k'} = 0; $input{'u'} = 0;};
    
    &TreeHeaders('Parent Array','Level Sequence','');
    $retval = system "$RootedBIN -n $input{'n'} -m $input{'m'} -k $input{'k'} -u $input{'u'} -L $LIMIT -o $out";
    &EndEntry('tree/RootedTree');

  } elsif ($input{'program'} eq "borderm") {

     &OutputHeader('B-Trees Output','tree/BTrees');  # from common.pl
     if (int($input{'n'}) <= 0) {
	&ErrorMessage('<i>n</i> must be greater than 0');
	&EndEntry('tree/BTrees');
	exit(0);
     }
     print "B-trees for <I>n</I> = $input{'n'}";
     if ($input{'m'} eq "") {
         print "<P>Parameter <I>m</I> is needed to be set!<BR>\n";
     } else {
     print " and <I>m</I> = $input{'m'}<P>\n";
     if ($input{'k'}>0) { &ExtraParam('lb',$input{'k'}); }
     if ($input{'u'}>0) { &ExtraParam('ub',$input{'u'}); }
     if (int($input{'n'}) > $MAXn) { &TooBig('n',$MAXn); exit(0); } 
     if (int($input{'m'}) < 3) { &TooSmall('m',3); exit(0); } 
     $input{'output1'} = "true";
     &TreeHeaders('Children Counts','Reverse Children Counts','');
     $retval = system "$BtreesBIN -n $input{'n'} -m $input{'m'} -L $LIMIT -o $out";
     }
     &EndEntry('tree/BTrees');
  } elsif ($input{'program'} eq "freetree") {
     &OutputHeader('Free Trees Output','tree/FreeTrees');  # from common.pl
     if (int($input{'n'}) <= 0) {
	&ErrorMessage('<i>n</i> must be greater than 0');
	&EndEntry('tree/FreeTrees');
	exit(0);
     }
     print "Free trees of <I>n</I> = $input{'n'} nodes";
     if ($input{'m'}) { print " with max degree <I>m</i> = $input{'m'}";}
     else {$input{'m'}=$input{'n'}-1; }
     if ($input{'k'} || $input{'u'}) { print ", <br>diameter"; }
     if ($input{'k'}) { 
	print "at least <I>lb</I> = $input{'k'}";}
     else {$input{'k'}=2; }
     if ($input{'u'}>$input{'n'}-1) { $input{'u'} = $input{'n'}-1; }
     if ($input{'u'}) {
      print " at most <I>ub</i> = $input{'u'}\n";}
     else {$input{'u'} = $input{'n'}-1; }
     print ".<P>";

     if (int($input{'n'}) > $MAXn) { &TooBig('n',$MAXn); exit(0); } 
     if ($input{'m'}>0 && $input{'m'}<2 ) { &TooSmall('m',2); exit(0); } 
     if ($input{'k'}>0 && $input{'k'}<2 ) { &TooSmall('lb',2); exit(0);} 
     &TreeHeaders('Parent Array','Level Sequence','');
     $retval = system 
     "$FtreesBIN -n $input{'n'} -m $input{'m'} -k $input{'k'} -u $input{'u'} -L $LIMIT -o $out"; 
     &EndEntry('tree/FreeTrees');
  } elsif ($input{'program'} eq "PosetBinTree") {
     &OutputHeader('Binary Trees Output','tree/BinaryTrees');  # from common.pl
     if (int($input{'n'}) <= 0) {
	&ErrorMessage('<i>n</i> must be greater than 0');
	&EndEntry('tree/BinaryTrees');
	exit(0);
     }
     print "Binary Trees for <I>n</I> = $input{'n'}";
     print " generated by one or two <I>adjacent</I> transpositions.<BR>\n";
     if ($input{'k'}>0) { &ExtraParam('lb',$input{'k'}); }
     if ($input{'u'}>0) { &ExtraParam('ub',$input{'u'}); }
     if (int($input{'n'}) > $MAXn) { &TooBig('n',$MAXn);  exit(0); } 
     &InitializePosetFILE;
     if ($input{'output1'}) { $oo =  ' 1'; } else { $oo = ' 0'; }
     if ($input{'output2'}) { $oo = $oo.' 1'; } else { $oo = $oo.' 0' }
     if ($input{'output3'} eq "true") {
       print "<P>Sorry, Gray code output not implemented !<P>\n";}
     &TreeHeaders('Parentheses','Nodes','');
     $retval = system "$BinDir/genle -p $PosetFILE | ./BinPoset.pl $oo";
     &EndEntry('tree/BinaryTrees');
  } elsif ($input{'program'} eq "bintree") {
     &OutputHeader('Binary Trees Output','tree/BinaryTrees');  # from common.pl
     if (int($input{'n'}) <= 0) {
	&ErrorMessage('<i>n</i> must be greater than 0');
	&EndEntry('tree/BinaryTrees');
	exit(0);
     }
     print "Binary Trees for <I>n</I> = $input{'n'}";
     if ($input{'m'}>0) { print " and <I>m</I> = $input{'m'}"}
     else { $input{'m'} = 0; }
     print ".<P>\n";
     if ($input{'k'}>0) { &ExtraParam('lb',$input{'k'}); }
     if ($input{'u'}>0) { &ExtraParam('ub',$input{'u'}); }
     if (int($input{'n'}) > $MAXn) { &TooBig('n',$MAXn); exit(0); } 
     &TreeHeaders('Parentheses','Nodes', '','Picture');
     $retval = system 
       "$BintreesBIN -n $input{'n'} -m $input{'m'} -L $LIMIT -o $out -P $$";
     &EndEntry('tree/BinaryTrees');
  } elsif ($input{'program'} eq "graybintree") {
     &OutputHeader('Binary Trees Output','tree/BinaryTrees');  # from common.pl
     if (int($input{'n'}) <= 0) {
	&ErrorMessage('<i>n</i> must be greater than 0');
	&EndEntry('tree/BinaryTrees');
	exit(0);
     }
     print "Binary Trees (Gray code order) for <I>n</I> = $input{'n'}";
     if ($input{'m'}) { print " and <I>m</I> = $input{'m'}"; }
     print ".<P>\n";
     if ($input{'k'}) { &ExtraParam('lb',$input{'k'}); }
     if ($input{'u'}) { &ExtraParam('ub',$input{'u'}); }
     print "<P>\n";
     if (int($input{'n'}) > $MAXn) { &TooBig('n',$MAXn); exit(0); } 
     if ($input{'m'} ne '') { 
        if (int($input{'m'}) < 1) { &TooSmall('m',1); exit(0); }
     } else { $input{'m'} = 0;}
     
     &TreeHeaders('Parentheses','Nodes','Swap','Pictures');
     $retval = system 
        "$GrayBinTreeBIN -n $input{'n'} -m $input{'m'} -L $LIMIT -o $out -P $$"; 
     &EndEntry('tree/BinaryTrees');
  } elsif ($input{'program'} eq "labelled") {
     &OutputHeader('Labelled Free Trees Output','tree/FreeTrees');  # from common.pl
     if (int($input{'n'}) <= 0) {
	&ErrorMessage('<i>n</i> must be greater than 0');
	&EndEntry('tree/FreeTrees');
	exit(0);
     }
     print "Labelled Free Trees (Gray code order) for <I>n</I> = $input{'n'}<BR>";
     if ($input{'m'}) { &ExtraParam('m',$input{'m'}); }
     if ($input{'k'}) { &ExtraParam('lb',$input{'k'}); }
     if ($input{'u'}) { &ExtraParam('ub',$input{'u'}); }
     print "<P>\n";
     if (int($input{'n'}) > $MAXn) { &TooBig('n',$MAXn); exit(0); }
     if ($input{'n'} < 3) {
	&TooSmall('n',3); 
	&EndEntry('tree/FreeTrees');
	exit(0);
     }
     $input{'output1'} = "true";
     &TreeHeaders('Pr&uuml;fer','','');
     $retval = system
          "$LabelledtreeBIN -n $input{'n'} -L $LIMIT -o 1";
     &EndEntry('tree/FreeTrees');

    ### Compact Codes ### 
  } elsif ($input{'program'} eq "compactcodes") {
     &OutputHeader('Compact Codes','tree/CompactCodes');  # from common.pl
     if (int($input{'n'}) <= 0) {
	&ErrorMessage('<i>n</i> must be greater than 0');
	&EndEntry('tree/CompactCodes');
	exit(0);
     }
     print "Compact Codes for <I>n</I> = $input{'n'}<BR>";
     if ($input{'m'}) { &ExtraParam('m',$input{'m'}); }
     if ($input{'k'}) { &ExtraParam('lb',$input{'k'}); }
     if ($input{'u'}) { &ExtraParam('ub',$input{'u'}); }
     print "<P>\n";
     if (int($input{'n'}) > $MAXn) { &TooBig('n',$MAXn); exit(0); }
     $input{'output1'} = "true";
     &TreeHeaders('Compact Codes','','');
     $retval = system
          "$CompactCodesBIN -n $input{'n'} -L $LIMIT -o $out";  # figure out output
     &EndEntry('tree/CompactCodes');
  } elsif ($input{'program'} eq "redblack") {
     &OutputHeader('Red Black Trees Output','tree/RedBlackTree');  # from common.pl
     if (int($input{'n'}) <= 0) {
	&ErrorMessage('<i>n</i> must be greater than 0');
	&EndEntry('tree/RedBlackTree');
	exit(0);
     }
     print "Red-black Trees  for <I>n</I> = $input{'n'}";
     print ".<P>\n";
     if ($input{'m'}) { &ExtraParam('m',$input{'m'}); }
     if ($input{'k'}) { &ExtraParam('lb',$input{'k'}); }
     if ($input{'u'}) { &ExtraParam('ub',$input{'u'}); }
     print "<P>\n";
     if (int($input{'n'}) > $MAXn) { &TooBig('n',$MAXn); exit(0); }
     &TreeHeaders('Preorder','Nodes','','Picture');
     $retval = system "$RedBlackBIN -n $input{'n'} -L $LIMIT -o $out -P $$"; 
     &EndEntry('tree/RedBlackTree');
  } elsif ($input{'program'} eq "rotbintree") {
     &OutputHeader('Binary Trees Output','tree/BinaryTrees');  # from common.pl
     print "Binary Trees by Rotation for <I>n</I> = $input{'n'}.<P>\n";
     if (int($input{'n'}) <= 0) {
	&ErrorMessage('<i>n</i> must be greater than 0');
	&EndEntry('tree/BinaryTrees');
	exit(0);
     }
     if ($input{'m'}) { &ExtraParam('m',$input{'m'}); }
     if ($input{'k'}) { &ExtraParam('lb',$input{'k'}); }
     if ($input{'u'}) { &ExtraParam('ub',$input{'u'}); }
     print "<P>\n";
     if (int($input{'n'}) > $MAXn) { &TooBig('n',$MAXn); exit(0); }
     &TreeHeaders('Parentheses','Nodes','','Picture');
     $retval = system "$BinTreeRotBIN -n $input{'n'} -L $LIMIT -o $out -P $$"; 
     &EndEntry('tree/BinaryTrees');


### Rooted Plane Trees ###

  } elsif ($input{'program'} eq "planerooted") {
     &OutputHeader('Rooted Plane Trees Output','tree/PlaneRootedTree');  # from common.pl
     print "Rooted Plane Trees for <I>n</I> = $input{'n'}.<P>\n";

     # Allow only standard and alternate representations
     if (exists($input{'output3'})) {
	delete($input{'output3'});
	print "<p><b>Gray Code</b> option not available for this object. </p>";
     }
     if (exists($input{'output4'})) {
	delete($input{'output4'});
	print "<p><b>Draw Trees</b> option not available for this object. </p>";
     }
     if ($input{'m'}) { &ExtraParam('m',$input{'m'}); }
     if ($input{'k'}) { &ExtraParam('lb',$input{'k'}); }
     if ($input{'u'}) { &ExtraParam('ub',$input{'u'}); }
     print "<P>\n";
     if (int($input{'n'}) > $MAXn) { &TooBig('n',$MAXn); exit(0); }
     if (int($input{'n'}) < 1) { &TooSmall('n',1); exit(0); }
     &TreeHeaders('Level Sequence','Parent Array','','');
     $retval = system "$PlaneRootedBIN -n $input{'n'} -L $LIMIT -o $out -P $$";
      &EndEntry('tree/PlaneRootedTree');

### Free Plane Trees ###
    } elsif ($input{'program'} eq "planefree") {
     &OutputHeader('Free Plane Trees Output','tree/PlaneFreeTree');  # from common.pl
     print "Free Plane Trees for <I>n</I> = $input{'n'}.<P>\n";

     # Allow only standard and alternate representations
     if (exists($input{'output3'})) {
	    delete($input{'output3'});
	    print "<p><b>Gray Code</b> option not available for this object. </p>";
     }
     if (exists($input{'output4'})) {
	    delete($input{'output4'});
	    print "<p><b>Draw Trees</b> option not available for this object. </p>";
     }
     if ($input{'m'}) { &ExtraParam('m',$input{'m'}); }
     if ($input{'k'}) { &ExtraParam('lb',$input{'k'}); }
     if ($input{'u'}) { &ExtraParam('ub',$input{'u'}); }
     print "<P>\n";
     if (int($input{'n'}) > $MAXn) { &TooBig('n',$MAXn); exit(0); }
     if (int($input{'n'}) < 1) { &TooSmall('n',1); exit(0); }
     &TreeHeaders('Level Sequence','Parent Array','','');
     $retval = system "$PlaneFreeBIN -n $input{'n'} -L $LIMIT -o $out -P $$";
      &EndEntry('tree/PlaneFreeTree');

  } else {
     print "No selection made.\n";
  }
  
  &CloseDocument;
}

















