{Generates contingency tables}
program Generate ( input, output );

const MAXIMUM = 20;

type
    row = array [0..MAXIMUM] of integer;
    col = row;

var
    sum,rows,cols,i : integer;
    t : col;
    k : row;
    table: array[0..MAXIMUM] of row;
    count : integer;

procedure PrintIt;
var i,j:integer;
begin
   count := count + 1;
   write( '[',count:3,'] ' );
   for i := 0 to rows do begin
         for j := 0 to cols do write( table[i,j]:2 );
         writeln;
         write('      ');
      end;
   writeln;
end {of PrintIt};

function max(i,j:integer):integer;
begin
   if i < j then max := j else max := i;
end;

function min(i,j:integer):integer;
begin
   if i > j then min := j else min := i;
end;

procedure Gen(r,c:integer);

   procedure RowGen ( r,k,p,n : integer );
   var temp,i : integer;
   begin
      if k = 0 then Gen(r-1,c)
      else begin
         for i := max(0,k-n+table[0,p]) to min(table[0,p],k) do begin
             table[r,p] := i;
             temp := table[0,p];
             table[0,p] := table[0,p] - i;     {current amount left for column sums}
             RowGen( r, k-i, p-1, n-temp );
             table[0,p] := temp;
             table[r,p] := 0;
          end;
      end;
   end {of RowGen};

begin {Gen}
   if r = 0 then PrintIt
   else begin
      (* sum := sum - k[r+1];*)
      RowGen(r, k[r], c, sum - k[r+1] );
      (* sum := sum + k[r+1]; *)
   end;
end {of Gen};


begin
   readln( rows,cols );
   sum := 0;
   for i := 0 to cols do begin
       read( table[0,i] );
       sum := sum + table[0,i];
   end;
   readln;
   for i := 0 to rows do read( k[i] );  readln;
   k[rows+1] := 0;
   count := 0;
   Gen(rows,cols);
end.



