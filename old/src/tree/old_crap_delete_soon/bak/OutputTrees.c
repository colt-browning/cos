
#include <stdio.h>
#define LIMIT_ERROR -1

int num=0;       /* counter for the number of objects generated */
int outputP=0,   /* output parent array */
    outputG=0,   /* output Gray code    */
    outputL=0,   /* output level sequence */
    outputB=0,   /* output balls   */
    outputF=0;   /* output GIFs    */
int omit = 0;    /* omit the first tree ? */
int shift = 0;   /* number of initial elements to skip(no printout) */

void PrintIt(n,x1,x2)
int n, *x1,*x2;
{ int i;

  num++;
  if (num <= LIMIT) {
    if (num == 1 && omit ) {omit=0; num--;} else {
      printf("<tr>");
      if (outputP) {
                printf("<td align=center>");
                for(i=shift+1;i<=n+shift;i++)printf("%2d",x1[i]);
                printf("  <br></td>");
      }
      if (outputL) {
                printf("<td align=center>");
                for(i=shift+1;i<=n+shift;i++)printf("%2d",x2[i]);
                printf(" <br></td>");
      }
      if (outputB) {
                printf("<td align=center>");
                for(i=shift+1;i<=n+shift;i++) {
		 if (x1[i]==2) 
		     printf("<IMG SRC=../ico/tiny.redball.gif>");
		 if (x1[i]==1) 
		     printf("<IMG SRC=../ico/tiny.blackball.gif>");
		 if (x1[i]==0) 
		     printf("<IMG SRC=../ico/tiny.whiteball.gif>");
                }
                printf(" <br></td>");
      }
      if (outputG) {
                printf("<td align=center>");
		printf("(%2d,%2d)",x2[1],x2[2]);
                printf(" <br></td>");
      }
      if (outputF) {
                printf("<td align=center>");
                printf("N/A");
                printf(" <br></td>");
      }
      if (outputP || outputL || outputB || outputG || outputF) printf("</tr>\n");
    }
  }
  else
  {
    exit(LIMIT_ERROR);
  }
}
