/* Anton An
 * June 1999
 * 
 * This program generates ideals in lexicographic order for a poset, 
 * using Steiner's Variant. 
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_POINTS 50	// max set size
#define STANDARD 0	// display ideals in gray code order
#define GRAY 1	        // highlight change of bit positions

unsigned int table[MAX_POINTS+1];
unsigned int up[MAX_POINTS+1];

//typedef struct PCell *cell;

typedef struct cell{
  unsigned int subset;
  //int index;
  struct cell *next;
}CELL;


int N;			// size of the set
int counter = 0;	// number of ideals
int input_mode;		
CELL *ideal;
int Y;
int two_flag;

unsigned getbit(unsigned int word, int n)
{
  return (word >> n) & 01;
}

unsigned int setbit(unsigned int word, int n, unsigned v)
{
  if (v != 0)
    return word | (01 << n);
  else
    return word & ~(01 << n);
}

/* process the relation table and generate upper/lower 
 * for each element in the poset.
 */
void Process(int* steiner)
{
  int i, j, k, count, bit; 

  // transitive closure
  for(k = 1; k <= N; k++){
    for(i = 1; i <= N; i++){
      for(j= 1; j <= N; j++){
	bit = getbit(table[i], j) | (getbit(table[i], k) 
				     & getbit(table[k], j));
	table[i] = setbit(table[i], j, bit);
      }
    }
  }

  for(i = 1; i <= N; i++){
    up[i] = 0;
    count = 0;
    for(j = 1; j <= N; j++){
      if(getbit(table[i], j)){
	up[i] = setbit(up[i], j, 1);
      }
      if(getbit(table[j], i)){
	count ++;
      }
    }
    steiner[i] = count - 1;
  }
}



// find the minimal element in the poset
int findMinimal(unsigned long poset, int *steiner)
{
  int i, min = 0;
  if(two_flag)
    return Y;
  for(i = 1; i <= N; i++)
    if((getbit(poset, i)) && (!steiner[i])){
      min = i;
      break;
    }
  if(min == 0)
    {
      printf("A cycle has been found. Please check.\n");
      //printf("</TABLE>");
      printf("poset is %d\n", poset);
      printf("steiner is: ");
      for(i = 1; i <= N; i++)
	printf("%d ", steiner[i]);
      printf("\n");
      exit(1);
    }
  return min;
}

int countMinimal(unsigned long poset, int *steiner)
{
  int i, count = 0;
  for(i = 1; i <= N; i++)
    if((getbit(poset, i)) && (!steiner[i]))
      count++;
  return count;
}

// print out the ideals
void printResult(CELL *ideal)
{
  CELL *temp;
  int i, count = 1, flag = 0;
  unsigned int set, trans, last;
  temp = ideal;
  if(input_mode == STANDARD){
    while(temp != NULL){
      if(count % 2 == 0){
	set = temp->subset;
	printf("<TR>\n<TD ALIGN=CENTER>");
	printf("{");
	for(i = 1; i <= N; i++)
	  if(getbit(set, i)){
	    if(flag)
	      printf(" %d", i);
	    else
	      printf("%d", i);
	    if(!flag)
	      flag = 1;
	  }
	printf("}");
	printf("<BR></TD>\n");
	counter ++;
      }
      count ++;
      flag = 0;
      temp = temp->next;
    }
  }
  else{
    while(temp != NULL){
      if(count % 2 == 0)
	{
	  set = temp->subset;
	  if(counter > 0)
	    trans = set ^ last;
	  else trans = 0;
	  printf("<TR>\n<TD ALIGN=CENTER>");
	  printf("{");
	  for(i = 1; i<= N; i++)
	    if(getbit(set, i)){
	      if(getbit(trans, i))
		printf("<FONT COLOR=RED>");
	      if(flag)
		printf(" %d", i);
	      else 
		printf("%d", i);
	       if(getbit(trans, i))
		 printf("</FONT>");
	      if(!flag)
		flag = 1;
	    }
	  printf("}");
	  printf("<BR></TD>\n");
	  counter ++;
	  flag = 0;
	  printf("<TD ALIGN=CENTER>");
	  for(i = 1; i<= N; i++)
	    if(getbit(trans, i) && getbit(set,i))
	      printf("<FONT COLOR=GREEN>%d </FONT>", i);
	    else if(getbit(trans,i) && getbit(last,i))
	      printf("<FONT COLOR=BLUE>%d </FONT>", i);
	  last = set;
	}
      temp = temp->next;
      count++;
    }
  }
}
// update the list for finding minimal element
void Update(int *steiner, int poset, int min)
{
  int i;
  for(i = 1; i <= N; i++)
    if(getbit(poset, i) && getbit(up[min], i))
      steiner[i]--;
}

void setMark(int *steiner, int poset){
  int i;
  for(i = 1; i <= N; i++)
    if((getbit(poset, i)) && (!steiner[i])){
      Y = i;
      break;
    }
}


// generate ideals recursively
void Ideal(unsigned long poset, int *steiner, CELL **list)
{
  int i, j, min, count, num;
  unsigned int poset1, ideal1, x;
  int  steiner1[N+1];
  CELL *temp, *list1, *list2, *list3, *cur;

  if(poset == 0)
    {
      two_flag = 0;
      *list = malloc(sizeof(CELL));
      (*list)->subset = 0;
      (*list)->next = malloc(sizeof(CELL));
      (*list)->next->subset = 0;
      (*list)->next->next = NULL;
      
    }
  else{
    num = countMinimal(poset, steiner);
    min = findMinimal(poset, steiner);
    if(num == 1)
      {
	two_flag = 0;
	//printf("been here \n");
	poset = setbit(poset, min, 0);
	for(i = 1; i<=N; i++)
	  steiner1[i] = steiner[i];
	Update(steiner1, poset, min);
	Ideal(poset, steiner1, list);
	if(list == NULL)
	  printf("here is the problem");
      	temp = *list;
	if(temp == NULL)
	  printf("SHOOT!!!!\n");

	while(temp->next!= NULL)
	  {
	    temp->subset = setbit(temp->subset, min, 1);
	    temp = temp->next;
	  }
	temp->subset = setbit(temp->subset, min, 1);
//	printf("the last elment is %d\n", temp->subset);
	temp->next = malloc(sizeof(CELL));
	temp->next->subset = 0;
	temp->next->next = NULL;
	temp = malloc(sizeof(CELL));
	temp->next = *list;
	temp->subset = 0;
	*list = temp;

	temp = *list;
	/*printf("\n");
	while(temp != NULL)
	  {printf("%d ", temp->subset); temp = temp->next;}*/
      }
    else 
      {
	poset1 = setbit(poset, min, 0);
	two_flag = 1;
	setMark(steiner, poset1);
	for(i = 1; i<=N; i++)
	  steiner1[i] = steiner[i];
	Update(steiner1, poset1, min);
	Ideal(poset1, steiner1, &list1);
	poset = (~up[min]) & poset;
	Ideal(poset, steiner, &list2);
	if(list1 == NULL || list2 == NULL)
	  printf("return a null list");
	temp = list1;
	while(temp!= NULL)
	  {
	    temp->subset = setbit(temp->subset, min, 1);
	    temp = temp->next;
	  }
	cur = list1->next;
	list3 = NULL;
	while(cur != NULL){
	  temp = malloc(sizeof(CELL));
	  temp->subset = cur->subset;
	  temp->next = list3;
	  list3 = temp;
	  cur = cur->next;
	}
	*list = list1;
	(*list)->next = list3;
	temp = list2->next;
	list2->next = *list;
	cur = *list;
	while(cur->next != NULL)
	  cur = cur->next;
	cur->next = temp;
	*list = list2;
      }
  }
    
}

void main(int argc, char* argv[])
{
  FILE *input;
  int i, j, left, right;
  unsigned long poset;
  int steiner[MAX_POINTS+1];
  char *fname;
  char *mode;
  struct cell *ideal, *temp;
  
  if(argc != 3){
    fprintf(stderr, "Usage: genIdeal -flag in_file_name\n");
    exit(1);
  }
  
  mode = argv[1];
  if(!strcmp(mode, "-s"))
    input_mode = STANDARD;
  else 
    input_mode = GRAY;

  printf("Ideals in Gray Code Order<p>");
  printf("<P><TABLE BORDER=1 CELLSPACING=2 CELLPADDING=2><TR>\n");
  if(input_mode == STANDARD)
    printf("<TH COLSPAN=1><FONT SIZE=+1>Ideals");
  else if(input_mode == GRAY){
    printf("<TH COLSPAN=1><FONT SIZE=+1>Ideals");
    printf("<TH COLSPAN=1><FONT SIZE=+1>Changes");
  }
    printf("</FONT><BR></TH>\n");

  fname = argv[2];
  if((input = fopen(fname, "r")) == NULL) {
    fprintf(stderr, "Error: could not read file %s\n", fname);
    exit(1);
  }

  // read number of elements in the set
  fscanf(input, "%d", &N);
  
  for(i = 1; i <= N; i++){
    table[i] = 0;
    table[i] = setbit(table[i], i, 1);
  }

  // store partial orderings
  do {
    fscanf(input,"%d %d", &left, &right);
    table[left] = setbit(table[left], right, 1);
  }while(left != 0);

  // initialize ideal and poset
  ideal = 0;
  poset = 0;
  for(i = 1; i <= N; i++)
    poset = setbit(poset, i, 1);

  Process(steiner);
  two_flag = 0;
  Ideal(poset, steiner, &ideal); 
  if(ideal != NULL)
    printResult(ideal);
  else
    printf("Whoops! Error\n");
  /*temp = ideal;
  if(temp == NULL)
    printf("Shoot");
  while(temp != NULL){
    counter++;
    printf("%d: %d\n", counter, temp->subset);
    temp = temp->next;
  }*/
    
  printf("</TABLE><P>%d ideals in total\n", counter);
  fclose(input);
}

       
