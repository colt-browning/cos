/* Outputs all bitstrings of length n without substring 00, and */
/* all compositions of n+1 with each part 1 or 2.               */
/* Both these objects are counted by the Fibonacci numbers.     */

static long n;
static long a[101];

static void PrintIt()
{
  int i;
  for (i = 1; i <= n; i++) printf("%2ld", a[i]);
  printf(" ... ");
  i = 1;
  while (i <= n + 1) {
    if (a[i] == 0) {
      printf("%2d", 2);  i += 2;
    } else {
      printf("%2d", 1);  i++;
    }
  }
  putchar('\n');
}


static void gen(n)
int n;
{
  if (n == 0) {
    PrintIt(); return;
  }
  if (n == 1) {
    a[1] = 1;
    PrintIt(); return;
  }
  a[n]   = 1;  gen( n-1 );
  a[n-1] = 0;  gen( n-2 );
}


main(argc, argv)
int argc;
char *argv[];
{
  a[0] = 1;
  do {
    printf("Enter n: ");
    scanf("%ld%*[^\n]", &n);
    getchar();
    gen( n+1 );
  } while (n != 0);
  exit(1);
}




/* End. */
