#include <stdio.h>
#define MAXLEN 100
#define EVEN   0
#define even   0
#define ODD    1
#define odd    1
#define true   1
#define TRUE   1
#define false  0
#define FALSE  0
#define less   0
#define greater 1

int n, a[MAXLEN+1], edge[MAXLEN][3], parity[MAXLEN+1];
int finish1;   /* this boolean variable is used as a flag */
int extend_counter;
int expand_counter;
int genIdeal_counter;
int ideal_counter;

void genIdeal();  /* just a prototype for recursive call */

void InitEdges()
{
int i,e1,e2;
printf("please input edges in order of DFS!");
for (i=1; i<n; i++) {
  printf("\nnext edge:");
  scanf("%d",&e1); scanf("%d",&e2);
  if (e1 < e2 ) {
     edge[i][0] = e1;
     edge[i][1] = e2;
     edge[i][2] = less; }
  else {
     edge[i][0] = e2;
     edge[i][1] = e1;
     edge[i][2] = greater; }

  }
  printf("\nEdge input done !\n");
}

void Parity()
{
int i;
printf("\nPlease input parities of all nodes! 0-even 1-odd");
for (i=1; i<=n;i++) {
 printf("\nnode %d:", i); scanf("%d",&parity[i]);
 }
}


void PrintIdeal()
{
int i;
ideal_counter ++;
for(i=1;i<=n;i++) printf("%d ",a[i]);
printf("\n");
}

void expand(neib, i, first, second)
int neib,i,first,second;
{
  a[neib] = first;
  expand_counter++;
  if (i == n-1) {
     PrintIdeal();
     a[neib] = second;
     PrintIdeal();
     finish1 = true;}
  else {
     genIdeal(i+1);
     a[neib] = second;
     genIdeal(i+1);}
}

void extend(neib, i, val)
int neib,i,val;
{
   extend_counter++;
   a[neib] = val;
   if (i == n-1) {
      PrintIdeal();
      finish1 = true;}
   else {
      genIdeal(i+1);}
}


void genIdeal(i)
int i;
{
int relation;
int root,neib;
 genIdeal_counter++;
 root = edge[i][0];
 neib = edge[i][1]; 
 relation = edge[i][2];

 if (a[root] ==  0 ) {
   if (relation == less ) {            /* root < neib     Not expandable */
    extend(neib, i, 0);}
   else  { /* root > neib  */           /* expandable */
     if (finish1 == false) {     /* creating first ideal */
       if (parity[neib] == even) {
	  expand(neib, i, 1, 0); }
       else {  /* Parity[neib-1] is odd */
	  expand(neib, i, 0, 1); }
      }
     else  {                         /* not the first ideal */
       if (a[neib] == 0) {
          expand(neib,i,0,1);}
       else {
          expand(neib,i,1,0);}
     }  /* end of if finish1 = false */ 
   }   /* end of if root > neib */ 
 }      
 else {                               /* if a[root] = 1 */
   if (relation == less) {              /* root < neib i.e.  expandable   */
      if (finish1 == false) {             /* creating first ideal */
         if (parity[neib] == even) {
             expand(neib, i, 0, 1); }
	 else {   /* parity is odd  */
             expand(neib, i, 1, 0); }
       }
      else {                           /* not first ideal  */
         if (a[neib] == 1) { 
            expand(neib, i, 1, 0); }
         else {
            expand(neib, i, 0, 1); }
      }
   }
   else {                              /* not expandable  */
      extend(neib, i, 1);}
 }

return;

} /* genIdeal */



void main()
{
int i;

   printf("Input num of nodes:");
   scanf("%d",&n);
   ideal_counter= 0;
   genIdeal_counter = 0;
   expand_counter = 0;
   extend_counter = 0;

   for(i=1;i<=n;i++) a[i] = 0;  /* init array a[] */
     
   finish1 = false;                 /* if we`ve finished first ideal */
   
   InitEdges();  /* input all edges in DFS order */
   
   Parity();     /* precompute parities of all nodes */

   a[1] = 0;  /* bitstring representing an ideal */
   genIdeal(1);  /* start from the first edge */
   a[1] = 1;
   genIdeal(1);  /* build the second part of first node */ 
   printf("generated %d ideals\n", ideal_counter);
   printf("genIdeal called %d times\n", genIdeal_counter);
   printf("extend called %d times\n", extend_counter);
   printf("expand called %d times\n", expand_counter);
}


