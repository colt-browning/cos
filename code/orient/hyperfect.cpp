/* 
 * Copyright (c) 2022 Ondrej Micka, Arturo Merino and Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cassert>
#include <iostream>
#include <string>
#include <utility>
#include <vector>
#include <algorithm>
#include <cassert>

#include "hyperfect.hpp"
#include "utils.hpp"

Hyperfect::Hyperfect(
  int n, const std::vector<std::string> &vertex_names,
  const std::vector<std::vector<int>> &hyperedge_list
) : n_(n), vertex_names_(vertex_names), hyperfect_checked_(false)
{
  assert((vertex_names.size() == n+1) && "There should be exactly n+1 vertex names");
  build_graph(hyperedge_list);
}

Hyperfect::Hyperedge::Hyperedge(
  int n, const std::vector<int> &vertex_list
) : vertex_list_(vertex_list), char_vector_(n+1, false), ignore_(false)
{
  assert((vertex_list.size() <= n) && "Too many vertices in the edge");
  // Create characteristic vector from vertex list
  for (int i : vertex_list){
    assert((i >= 1) && (i <= n) && "Invalid vertex id");
    assert(!char_vector_[i] && "Vertex already in the edge");
    char_vector_[i] = true;
  }
  // Sort the vertex list
  std::sort(vertex_list_.begin(), vertex_list_.end());
}

void Hyperfect::build_graph(const std::vector<std::vector<int>>& hyperedge_list)
{
  // Transform hyperedge list to our representation and fill incidence list and adjacency matrix
  hyperedge_list_.reserve(hyperedge_list.size()); // !!! This is necessary, otherwise the list would get resized
                                                  // which would invalidated pointers
  incidence_list_.resize(n_+1, std::vector<Hyperedge*>());
  adj_matrix_.resize(n_+1, std::vector<std::vector<Hyperedge*>>(n_+1, std::vector<Hyperedge*>()));

  for (auto & hedge : hyperedge_list){
    // Construct hyperedge and put it to the list
    hyperedge_list_.emplace_back(n_, hedge);
    // And put pointer to the incidence_lists
    for (int i : hedge) {
      incidence_list_[i].push_back(&hyperedge_list_.back());
    }
    // Now add the hyperedge to the adj. matrix. Matrix is symmetric, so we can 
    // just take all ordered pairs of vertices in the hedge
    for (auto i : hedge){
      for (auto j : hedge){
        if (i != j){
          adj_matrix_[i][j].push_back(&hyperedge_list_.back());
        }
      }
    }
  }
}

bool Hyperfect::check_vertex_condition(int i) const {
  // This is a nasty iteration, maybe it would be better to somehow write iterator for cartesian product 
  for (auto A : incidence_list_[i]) if (!A->ignore_) for (auto B : incidence_list_[i]) if (!B->ignore_){
    for (int a : A->vertex_list_) for (int b : B->vertex_list_) if ((a != b) && (a != i) && (b != i)) {
      auto & candidates = adj_matrix_[a][b];
      if (
        std::none_of(candidates.cbegin(), candidates.cend(), [this, i, A, B](Hyperedge *X) {
          return !X->ignore_ && is_certificate(X, i, A, B);
      })){
        return false;
      }
    }
  }
  // We found certificates for all pairs of hyperedges incident to i 
  return true;
}

bool Hyperfect::is_certificate(
      Hyperfect::Hyperedge *X, 
      int i, 
      Hyperfect::Hyperedge *A, 
      Hyperfect::Hyperedge *B
) const {
  if (X->contains(i)){
    return false;
  }
  for (int j : X->vertex_list_){
    if (!A->contains(j) && !B->contains(j)){
      return false;
    }
  }
  return true;
}

void Hyperfect::prune_hyperedges(int i){
  for (auto h : incidence_list_[i]){
    h->ignore_ = true;
  }
}

void Hyperfect::unmark_ignore(){
  for (auto &h : hyperedge_list_){
    h.ignore_ = false;
  }
}

bool Hyperfect::is_hyperfect()
{
  if (!hyperfect_checked_){
    calculate_heo();
    hyperfect_checked_ = true;
  }

  return is_hyperfect_;
}

bool Hyperfect::check_heo(const std::vector<int> &heo){
  // Start by some sanity checks
  if (heo.size() != n_+1){
    return false;
  }

  std::vector<int> oeh(n_+1, 0);
  for (int i = 1; i <= n_; ++i){
    if ((heo[i] > n_+1) || (heo[i] < 1)){
      return false;
    }
    oeh[heo[i]] = i;
  }

  // Check that heo (and thus oeh) are permutations of 1..n
  // If heo wasn't a permutation, then there must be some i in 1..n missing
  for (int i = 1; i <= n_; ++i){
    if (heo[oeh[i]] != i){
      return false;
    }
  }

  // Now check that heo fulfills the conditions
  unmark_ignore();
  for (int i = n_; i > 0; --i){
    int v = heo[i];
    if(!check_vertex_condition(v)){
      return false;
    }
    prune_hyperedges(v);
  }
  return true;
}

void Hyperfect::calculate_heo(){
  unmark_ignore();
  heo_ = std::vector<int>(n_+1, 0);

  // Did we already assigned heo position to the given vertex?
  std::vector<bool> has_heo(n_+1, false);

  // Find i-th vertex in HEO one by one
  for (int i = n_; i > 0; --i){
    // Try all possible candidates
    for (int j = 1; j <= n_; ++j){
      if (has_heo[j]) continue;
      // If the vertex fulfills the HEO condition (1), we can use it as i-th vertex in HEO
      if (check_vertex_condition(j)){
        heo_[i] = j;
        has_heo[j] = true;
        prune_hyperedges(j);
        break;
      }
    }
    // If no candidate fulfils the condition => no HEO exists
    if (heo_[i] == 0){
      is_hyperfect_ = false;
      return;
    }
  }
  // All vertices got their position in HEO
  is_hyperfect_ = true;

  if (DEBUG){
    for (int i = 1; i <= n_; ++i){
      assert(has_heo[i] && ("All vertices should have had assigned HEO position"));
      assert((heo_[i] > 0) && (heo_[i] <= n_) && "Invalid vertex value at HEO");
    }
  }
}

void Hyperfect::print_heo()
{
  if (is_hyperfect()){
    std::cout << "hyperfect elimination ordering: ";
    for (int i=1; i<=n_; i++) {
      if (i>=2) std::cout << " ";
      std::cout << vertex_names_[heo_[i]];
    }
    std::cout << std::endl;
  }
  else{
    std::cout << "no hyperfect elimination order" << std::endl;
  }
}