/* 
 * Copyright (c) 2022 Ondrej Micka, Arturo Merino and Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include<vector>
#include<cassert>
#include<tuple>
#include<algorithm>

#include "hypergraph.hpp"
#include "utils.hpp"

Hyperedge::Hyperedge(int n, std::vector<int> vertices) : char_vector_(n+1, false), head_(0){
  assert((vertices.size() <= n) && "Too many vertices in the edge");
  for (int i : vertices){
    assert((i >= 1) && (i <= n) && "Invalid vertex id");
    assert(!char_vector_[i] && "Vertex already in the edge");
    char_vector_[i] = true;
  }
}


Acyclic_hyperorientation::Acyclic_hyperorientation(
    int n, 
    const std::vector<std::string> &vertex_names, 
    const std::vector<std::vector<int>> &hyperedge_list,
    const std::vector<int> &heo
) : n_(n), vertex_names_(vertex_names), heo_(heo) 
{
  assert((vertex_names.size() == n+1) && "Number of vertex names does not match the number of vertices");
  assert((heo.size() == n+1) && "Length of HEO does not match the number of vertices");

  // Build inverse HEO and do basic sanity checks
  oeh_.resize(n+1, 0);
  for (int i = 1; i <= n_; ++i){
    assert(((heo_[i] >= 1) && (heo_[i] <= n)) && "Invalid HEO value");
    oeh_[heo_[i]] = i;
  }

  if (DEBUG){
    check_heo_consistency();
  }

  init_hypergraph(hyperedge_list);
  init_heads();
  init_alpha();
  init_auxiliary();

  if (DEBUG){
    check_consistency();
  }
}

void Acyclic_hyperorientation::init_hypergraph(const std::vector<std::vector<int>> &hyperedge_list)
{
  // Transform hyperedge list to our representation and fill adjacency matrix
  hyperedge_list_.reserve(hyperedge_list.size()); // !!! This is necessary, otherwise the list would get resized
                                                  // which would invalidated pointers in the matrix   
  adj_matrix_.resize(n_+1, std::vector<std::vector<Hyperedge*>>(n_+1, std::vector<Hyperedge*>()));  // Welcome to template hell :-)
  
  for (auto hedge : hyperedge_list){
    // First rename vertices w.r.t. HEO, so that the vertex 1 is the vertex with heo_[1]
    for (int i = 0; i < hedge.size(); ++i){
      hedge[i] = oeh_[hedge[i]];
    }
    hyperedge_list_.emplace_back(n_, hedge);  // Note that emplace constructs Hyperedge object from `hedge`
    
    // Now add the hyperedge to the adj. matrix. Matrix is symmetric, so we can 
    // just take all ordered pairs of vertices in the hedge
    for (auto i : hedge){
      for (auto j : hedge){
        if (i != j){
          adj_matrix_[i][j].push_back(&hyperedge_list_.back());
        }
      }
    }
  }

  // Create adjacency list
  heo_adj_list_.resize(n_+1, std::vector<int>());
  for (int i = 1; i <= n_; ++i){
    for (int j = 1; j < i; ++j) {
      if (!adj_matrix_[i][j].empty()){
        heo_adj_list_[i].push_back(j);
      }
    }
  }
}

void Acyclic_hyperorientation::init_heads() {
  for (auto & hedge : hyperedge_list_){
    // Set the head to the larges present vertex
    for (int i = n_; i > 0; --i){
      if (hedge.contains(i)){
        hedge.set_head(i);
        break;
      }
    }
  }
}

void Acyclic_hyperorientation::init_alpha(){
  alpha_.resize(n_+1, 0);
  rho_ = 0;

  int last_nonmin = 1;    // first vertex with non-zero alpha_ always has 1 by definition
  for (int i = 1; i <= n_; ++i){

    // vertex is smallest in its component iff its T array is empty
    if (heo_adj_list_[i].size() > 0){
      alpha_[i] = last_nonmin;
      last_nonmin = i;
    }
  }
  rho_ = last_nonmin;
}

void Acyclic_hyperorientation::init_auxiliary() {
  s_.resize(n_+1, 0);
  for (int i = 1; i <= n_; ++i){
    s_[i] = i;
  }

  o_.resize(n_+1, Direction::to_source);
}

bool Acyclic_hyperorientation::next(){
  int i = s_[rho_];
  if (i == 1){
    return false;
  }

  assert((o_[i] != Direction::none) && "Direction in auxiliary data structure should be always specified");

  int j = get_flip_vertex(i, o_[i]);
  if (o_[i] == Direction::to_source){
    flip(i, j);
  }
  else {
    flip(j, i);
  }

  // We need to check if i finished one of its zig-zags, to update auxiliary structures accordingly.
  // To do so, we just try to find a candidate for a next flip -- if there is none, i is finished.
  bool is_end = true;
  for (int k : heo_adj_list_[i]){
    if (o_[i] == pair_orientation(i, k)){
      is_end = false;
      break;
    }
  }

  // Update auxiliary structures
  s_[rho_] = rho_;
  if (is_end){
    o_[i] = reverse_dir(o_[i]);
    int k = alpha_[i];
    s_[i] = s_[k];
    s_[k] = k;
  }

  if (DEBUG){
    check_consistency();
  }

  return true;
}

void Acyclic_hyperorientation::flip(int old_head, int new_head) {
  assert((!adj_matrix_[old_head][new_head].empty()) && "There must be at least one hyperedge to flip");
  
  flipped_hyperedges.clear();

  for (auto h : adj_matrix_[old_head][new_head]) {
    assert((h->get_head() != new_head) && "There is a cycle in the orientation, or invalid flip pair was used");
    if (h->get_head() == old_head) {
      h->set_head(new_head);
      flipped_hyperedges.push_back(h);
    }
  }
  flip_pair = std::pair(old_head,new_head);
}

Acyclic_hyperorientation::Direction Acyclic_hyperorientation::pair_orientation(int from, int to) const {
  for (auto h : adj_matrix_[from][to]){
    int head = h->get_head();
    if (head == from) {
      return Direction::to_source;
    }
    if (head == to){
      return Direction::to_sink;
    }
  }
  return Direction::none;
}

int Acyclic_hyperorientation::get_flip_vertex(int i, Acyclic_hyperorientation::Direction d) const{
  // Only neighbors of i that points/are being pointed to i (depends on d) are possible candidates
  std::vector<int> candidates;
  for (int j : heo_adj_list_[i]){
     if (d == pair_orientation(i,j)){
      candidates.push_back(j);
     }
  }

  assert(!candidates.empty() && "There are no candidates to flip with i");

  // Now we need to find the min/max candidate w.r.t. orientation poset
  // It does not matter whether we use min of max function since we have custom comparator
  int j = *std::min_element(candidates.cbegin(), candidates.cend(),
    [this, d](int a, int b) {
      Acyclic_hyperorientation::Direction res = this->pair_orientation(a,b);
      assert(!(res == Acyclic_hyperorientation::Direction::none && 
               d == Acyclic_hyperorientation::Direction::to_sink)
              && "In to_sink case, every pair of candidates should be comparable");
      return d == res;
    }
  );
  return j;
}

void Acyclic_hyperorientation::print_edge_list(std::ostream &out) const{
  bool start = true;
  for (auto &h : hyperedge_list_){
    if (!start) out << "; ";
    print_hyperedge(out, h);
    start = false;
  }
  out << std::endl;
}

void Acyclic_hyperorientation::print_flip(std::ostream &out) const{
  out << heo_[flip_pair.first] << "," << heo_[flip_pair.second] << ": ";
  bool start = true;
  for (auto h : flipped_hyperedges){
    if (!start) out << "; ";
    print_hyperedge(out, *h);
    start = false;
  }
  out << std::endl;
}

void Acyclic_hyperorientation::print_state() const {
  std::cout << "Vertex names:" << std::endl;
  print_vector(vertex_names_);
  std::cout << "HEO:" << std::endl;
  print_vector(heo_);
  std::cout << "OEH:" << std::endl;
  print_vector(oeh_);
  std::cout << "Hyperedge list:" << std::endl;
  for (int i = 0; i < hyperedge_list_.size(); ++i){
    std::cout << i << ": " << hyperedge_list_[i] << std::endl;
  }
  std::cout << "Adjacency matrix:" << std::endl;
  for (int i = 1; i <= n_; ++i){
    for (int j = 1; j <= n_; ++j){
      std::cout << '[' << i << ',' << j << "]:" << std::endl;
      for (auto h : adj_matrix_[i][j]){
        std::cout << *h << std::endl;
      }
    }
    std::cout << "-------" << std::endl;
  }
 std::cout << "Adjacency list:" << std::endl;
  for (int i = 0; i <= n_; ++i){
    std::cout << "  " << i << ": ";
    print_vector(heo_adj_list_[i]);
  }
  std::cout << "s:" << std::endl;
  print_vector(s_);
  std::cout << "o:" << std::endl;
  print_vector(o_);
  std::cout << "rho: " << rho_ << std::endl;
  std::cout << "alpha: " << std::endl;
  print_vector(alpha_);
}

void Acyclic_hyperorientation::print_hyperedge(std::ostream &out, const Hyperedge &h) const {
  bool start = true;
  for (int i = 1; i <= n_; ++i){
    if (h.contains(i)) {
      if (!start){
        out << ",";
      }
      if (i == h.get_head()){
        out << "[";
      }
      out << heo_[i];
      if (i == h.get_head()){
        out << "]";
      }
      start = false;
    }
  }
}

std::ostream &operator<<( std::ostream &output, const Acyclic_hyperorientation::Direction &d){
  switch (d)
  {
  case Acyclic_hyperorientation::Direction::to_source:
    output << 'v';
    break;
  case Acyclic_hyperorientation::Direction::to_sink:
    output << '^';
    break;
  case Acyclic_hyperorientation::Direction::none:
    output << '-';
    break;
  }
  return output;
}

void Acyclic_hyperorientation::check_consistency() const {
  check_heo_consistency();
  check_adj_matrix_consistency();
  check_heo_adj_list_consistency();
  check_head_consistency();
}

void Acyclic_hyperorientation::check_adj_matrix_consistency() const {
    // Check that each hyperedge in a matrix's entry i,j contains i and j
    for (int i = 1; i <= n_; ++i){
      for (int j = 1; j <= n_; ++j) {
        if (i == j) continue;
        for (auto hedge : adj_matrix_[i][j]){
          assert(hedge->contains(i) && hedge->contains(j) && "Hyperedge at (i,j) should contain i, j");
        }
      }
    } 
}

void Acyclic_hyperorientation::check_heo_adj_list_consistency() const {
    // Check that each vertex's list contain only smaller neighbors
    for (int i = 1; i <= n_; ++i){
        for (int j : heo_adj_list_[i]){
            assert((j < i) && "Adjacency list should contain only HEO-smaller neighbors");
        }
    }

    // Check that all neighbors are backed up by some hyperedge
    for (int i = 1; i <= n_; ++i){
      for (int j : heo_adj_list_[i]){
        assert(!adj_matrix_[i][j].empty() && "Neighbors in the adj. list but not in adj. matrix");
      }
    }
}

void Acyclic_hyperorientation::check_heo_consistency() const{
  for (int i = 1; i <= n_; ++i){
    assert(((heo_[i] >= 1) && (heo_[i] <= n_)) && "Invalid heo value");
    assert(((oeh_[i] >= 1) && (oeh_[i] <= n_)) && "Invalid oeh value");
    assert((heo_[oeh_[i]] == i) && "heo and oeh are not consistent!");
  }
}

void Acyclic_hyperorientation::check_head_consistency() const {
  for (auto & h : hyperedge_list_){
    int head = h.get_head();
    assert((head > 0) && (head <= n_) && "Head must be a valid vertex");
    assert(h.contains(head) && "Head must be inside the hyperedge");
  }
}

std::ostream &operator<<(std::ostream &output, const Hyperedge &h) {
  bool start = true;
  for (int i = 1; i < h.char_vector_.size(); ++i){
    if (h.char_vector_[i]) {
      if (!start){
        output << ", ";
      }
      output << i;
      start = false;
    }
  }
  output << " -> " << h.head_;
  return output;
}