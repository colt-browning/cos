/*===============================================*/
/* Generate degree sequences of connected graphs */
/*===============================================*/


#include <stdio.h>
#include "../ProcessInput.c"
#include "NumPart.h"

#define ALLGRAPHS	0
#define CONNECTED	1
#define TWOCONNECTED	2
/* #define LIMIT_ERROR	-1        (this is now defined in Numpart.h) */
#define CONNECTIVITY_ERROR -2
#define ARGUMENT_ERROR	-3

/* int	p[101], count, LIMIT, N, K;  (this is now defined in Numpart.h */
int	cost, calls;
int	STATE;

void gen(int k)
{
int h, xp, q, s, i, j, hval, FORLIM;

	calls++;
	if (k > N)
	{
		PrintIt(N);
		return;
	}
	h = p[k - 1] + 1;
	hval = p[h];
	xp = h;
	while (p[xp] == hval) xp--;
	q = h;
	while (p[q] == hval) q++;
	if (q > k) q = k;
	cost += q - xp;
	if (p[h] == h)
	{
		if (xp + k - q <= h)
		{
			FORLIM = h - k + q;
			for (i = 1; i <= FORLIM; i++) p[i]++;
			for (j = q; j < k; j++) p[j]++;
			p[k] = h;
			gen(k + 1);
			FORLIM = h - k + q;
			for (i = 1; i <= FORLIM; i++) p[i]--;
			for (j = q; j < k; j++) p[j]--;
			cost += h - q;
		}
	}
	if (p[h] == h - 1)
	{
		s = xp;
		while (p[s] == h) s--;
		if (s < 0) s = 0;
		if (s + k - xp - 1 <= h)
		{
			FORLIM = h - k + xp + 1;
			for (i = 1; i <= FORLIM; i++) p[i]++;
			for (j = xp + 1; j < k; j++) p[j] = h;
			p[k] = h;
			gen(k + 1);
			FORLIM = h - k + xp + 1;
			for (i = 1; i <= FORLIM; i++) p[i]--;
			for (j = xp + 1; j < k; j++) p[j] = h - 1;
			cost += h;
		}
	}
	if (STATE==CONNECTED) FORLIM = 1;
	else if (STATE==TWOCONNECTED) FORLIM = 2;
	else if (STATE==ALLGRAPHS) FORLIM = 0;
	for (h = p[k - 1]; h >= FORLIM; h--)
	{
		hval = p[h];
		xp = h;
		while (p[xp] == hval) xp--;
		q = h;
		while (p[q] == hval) q++;
		if (q > k) q = k;
		cost += q - xp;
		p[k] = h;
		for (i = 1; i <= h; i++) p[i]++;
		gen(k + 1);
		i = h;
		j = q;
		while (i > xp && p[j] == hval - 1 && j < k)
		{
			p[j]++;
			j++;
			p[i]--;
			i--;
			gen(k + 1);
		}
		for (j--; j >= q; j--) p[j]--;
		for (i++; i <= h; i++) p[i]++;
		i = xp;
		j = h + 1;
		while (j < q && p[i] == p[i + 1] + 1)
		{
			p[i]--;
			i--;
			p[j]++;
			j++;
			gen(k + 1);
		}
		for (i++; i <= xp; i++) p[i]++;
		for (j--; j > h; j--) p[j]--;
		for (i = 1; i <= h; i++) p[i]--;
		if (STATE==TWOCONNECTED)
		{
			if (p[1]==2)
			{
				p[k]=2;
				gen(k+1);
			}
		}
	}
}


int main(int argc, char *argv[])
{
    ProcessInput(argc,argv);
    M = 1;  /* force M = 1 in order to print */
    STATE = K;
    
    count = calls = cost = 0;
    switch (STATE) {
    case CONNECTED : { 
	                  K=2;
			  p[1]=0;
			  p[0]=N+1;
			  gen(K);
			  break;
    }
    case TWOCONNECTED : {
	                  K=3;
			  p[1]=2;
			  p[2]=2;
			  p[3]=2;
			  gen(4);
			  break;
    }
    case ALLGRAPHS : {
	                  K=2;
			  p[0]=N+1;
			  p[1]=0;
			  gen(K);
			  break;
    }
    default: {
	                  exit(CONNECTIVITY_ERROR);
			  break;
    }
    }
    printf("</TABLE><P>count = %d", count);
    return(0);
}


