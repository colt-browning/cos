<?php
  if (isset($_GET["n"]) && isset($_GET["a"]) && isset($_GET["fmt"])) {
    $fmt = intval($_GET["fmt"]);
    $algo = intval($_GET["a"]);
    $result = "";
    if (($algo >= 1) && ($algo <= 7)) {
      // de Bruijn successor rules
      $cmd = "code/bruijn/db1 "
                             . min(intval($_GET["n"]), 15)  /* hard-wired maximum bitstring length */
                             . " " . escapeshellarg($_GET["a"])
                             . (($fmt == 2) ? " > file.txt" : "");  /* redirect output to file */
      exec($cmd, $result);
    } else if (($algo >= 8) && ($algo <= 11)) {
      // de Bruijn greedy algorithms
      $k = min(intval($_GET["k"]), 4);  // hard-wired maximum alphabet size
      $n = min($_GET["n"], limit_bruijn($k));  // hard-wired maximum length of substrings, depending on the alphabet size
      $a = intval($_GET["a"]) - 7;  // choice of algorithms is 1,2,3,4
      $cmd = "code/bruijn/db2 " . $k . " " . $n . " " . $a . (($fmt == 2) ? " > file.txt" : "");  /* redirect output to file */
      exec($cmd, $result);
    }
    output($result, $fmt, 0, 0, 0, 0, 0, 0, false, false);
  }
?>

