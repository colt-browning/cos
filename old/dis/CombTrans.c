/*===================================================================*/
/* C program for distribution from the Combinatorial Object Server.  */
/* Generates the k-combinations of [n] by transpositions via a       */
/* direct algorithm (see the book for what "direct" means).          */
/* No input error checking.  Assumes 0 <= k <= n <= MAX.             */
/* Outputs both the bitstring and the transposition (x,y) (meaning   */
/* that x leaves the subset and y enters).                           */
/* Algorithm is CAT (Constant Amortized Time).                       */
/* The program can be modified, translated to other languages, etc., */
/* so long as proper acknowledgement is given (author and source).   */     
/* Programmer: Frank Ruskey, 1995.                                   */
/* The latest version of this program may be found at the site       */
/* http://theory.cs.uvic.ca/inf/comb/CombinationsInfo.html            */
/*===================================================================*/

#define MAX 100   /* maximum value of n */

int b[MAX+1];
int n, k;
int cnt=0;

void PrintIt ( int x, int y ) {
  int i;
  printf( "%8d: ", ++cnt );
  for ( i=1; i<=n; ++i ) printf( "%d ", b[i] );  
  if (x != 0) printf( "     (%d,%d)", x,y ); 
  printf( "\n" );
} /*of PrintIt*/;

void swap ( int x, int y ) {
  int t;
  b[x] = 1;  b[y] = 0;
  PrintIt( x, y ); 
} /* of swap */;

void NEG ( int n, int k );
void GEN ( int n, int k ); 

void GEN ( int n, int k ) {
  if (k > 0 && k < n) {
     GEN( n-1, k );
     if (k == 1) swap( n, n-1 );  else swap( n, k-1 );
     NEG( n-1, k-1 );
  }
} /* of GEN */;

void NEG ( int n, int k ) {
  if (k > 0 && k < n) {
     GEN( n-1, k-1 );
     if (k == 1) swap( n-1, n );  else swap( k-1, n );
     NEG( n-1, k );
  }
} /* of NEG */;

void main () {
  int i;
  printf( "Enter n,k: " );  scanf( "%d %d", &n, &k );
  for ( i=1; i<=k; ++i) b[i] = 1;
  for ( i=k+1; i<=n; ++i) b[i] = 0;
  PrintIt( 0, 0 ); 
  GEN( n, k );
}

