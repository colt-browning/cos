/*===================================================================*/
/* C program for distribution from the Combinatorial Object Server.  */
/* Generate permutations of a multiset in lexicographic order. This  */
/* is the same version used in the book "Combinatorial Generation."  */
/* The program can be modified, translated to other languages, etc., */
/* so long as proper acknowledgement is given (author and source).   */  
/* Programmer: Joe Sawada, 1997.                                     */
/* The latest version of this program may be found at the site       */
/* http://theory.cs.uvic.ca/inf/mult/Multiset.html                    */
/*===================================================================*/

/* This program:  Takes as input: n t n(0) n(1) ... n(t)
		and outputs all permutations in lex order */

#include <stdio.h>

int P[100];	/* max size of perm = 100 */
int a[20]; 	/* represent n(i); */
int N;
int t;

void printP() {
	int i;
	for (i=N-1; i>=0; i--)
		printf("%d ",P[i]+1);
	printf("\n");
}

void GenMult(int n) {

	int j;

	if (a[0] == n) printP();
	else {
		for (j=0; j<=t; j++)  {
			if (a[j] > 0) {
				P[n-1] = j;
				a[j]--;
				GenMult(n-1);
				a[j]++;
				P[n-1] = 0;
			}
		}
	}
}

void main() {
	
	int i;

	printf("Enter n t n(0)..n(t): ");	
	scanf("%d %d",&N, &t);
	for (i=0; i<=t; i++) 
		scanf("%d",&(a[i]));
	printf("\n");
	GenMult(N);
	printf("\n");
}

			
			
