/*
 * Copyright (c) 2021 Joe Sawada, Dennis Wong
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#define MAX 100

int n,den,k,type,prefix[MAX],pn,w[MAX],ZEROS_ONES=1,bit=0,ZEROSONE_ONESZERO=1,substr[MAX];
long long int total=0,capacity,limit=-1;


//==========================================================================================
// Return the longest aperiodic necklace prefix of a given pre-necklace, or 0 if not a
// prenecklace.  Assumes a lex least (max) representative.
//==========================================================================================
int Lyn(int a[]){
int i,p=1;
    
    for (i=2; i<=n; i++) {
        // Lex minimal representatives
        if (bit == 0) {
            if (a[i-p] > a[i]) return 0;
            if (a[i-p] < a[i]) p = i;
        }
        // Lex maximal representatives
        else {
            if (a[i-p] < a[i]) return 0;
            if (a[i-p] > a[i]) p = i;
        }
    }
    return p;
}
//==========================================================================================
int Booth(int a[]){
    int i,j=2,t=2,p=1;
    for (i=1; i<=n; i++) a[n+i] = a[i];
    do {
        t = t + p*((j-t)/p);
        j = t + 1;
        p = 1;
        while (j <= 2*n && (a[j-p]^bit) <= (a[j]^bit)) {
            if ((a[j-p]^bit) < (a[j]^bit)) p = j-t+1;
            j++;
            if (j-t+1==n) return t - 1;
        }
    } while (p*((j-t)/p) < n);
    return t - 1;
}
//==========================================================================================
// MEMBERSHIP TESTERS
//==========================================================================================
//-------------------------------------------------------------------------------
// Necklaces using lex least (max) representative
//-------------------------------------------------------------------------------
int IsNecklace(int a[]){
    
    int p = Lyn(a);
    if (p > 0 && n%p == 0) return 1;
    return 0;
}
//-------------------------------------------------------------------------------
// Lyndon words, or aperiodic necklaces using the lex max representative
//-------------------------------------------------------------------------------
int IsLyndon(int a[]){
    
    if (Lyn(a) == n) return 1;
    return 0;
}
//-------------------------------------------------------------------------------
// Prenecklaces: prefixes of necklaces using lex least (max) representative
//-------------------------------------------------------------------------------
int IsPre(int a[]){
    
    return Lyn(a);
}
//-------------------------------------------------------------------------------
// Pseudonecklaces where blocks of form 0^* 1^* (or 1^* 0^*)
//-------------------------------------------------------------------------------
int IsPseudo(int a[]){
int ones, zeros, s=-1,t,i=1;
    
    // zeros/ones interchange roles with bit = 1 (lex largest representation)
    while (i<=n) {
        ones = zeros = 0;
        while (i<=n && a[i]== bit) { zeros++; i++; }
        while (i<=n && a[i]== 1-bit) { ones++;  i++; }
        if (s == -1) { s = zeros; t = ones; }
        else if (s < zeros || ones == 0 || (s == zeros && t > ones)) return 0;
    }
    return 1;
}
//-------------------------------------------------------------------------------
// Less (or greater) than (or equal) to its reversal
//-------------------------------------------------------------------------------
int IsRev(int a[]){
    
    // Less than reversal
    if (bit == 0) {
        for (int i=1; i<=n; i++) {
            if (a[i] < a[n-i+1]) return 1;
            if (a[i] > a[n-i+1]) return 0;
        }
    }
    // Greater than reversal
    else {
        for (int i=1; i<=n; i++) {
            if (a[i] > a[n-i+1]) return 1;
            if (a[i] < a[n-i+1]) return 0;
        }
    }
    if (type == 6 || type == 46) return 0;  // string equals reversal
    return 1;
}
//-------------------------------------------------------------------------------
// Less (greater) than or equal to a given density (weight) den
//-------------------------------------------------------------------------------
int IsDen(int a[]){
int d = 0;
    
    // Less than or equal to density
    if (bit == 0) {
        for (int i=1; i<=n; i++) if (a[i] == 1) d++;
        if (d <= den) return 1;
    }
    // Greater than or equal to density
    else {
        for (int i=1; i<=n; i++) if (a[i] == 1) d++;
        if (d >= den) return 1;
    }
    return 0;
}
//-------------------------------------------------------------------------------
// Does not have the substring 10^k (or 01^k)
//-------------------------------------------------------------------------------
int IsForb(int a[]){
int z = 0;
    
    // Forbidden 10^k or 01^k depending on bit
    if (a[1] == bit) z = -n;
    for (int i=1; i<=n; i++) {
        if (a[i] == 1-bit) {
            if (z >= k) return 0;
            z = 0;
        }
        else z++;
    }
    if (z >= k) return 0;
    return 1;
}
//-------------------------------------------------------------------------------
// Does not begin with a specified prefix
//-------------------------------------------------------------------------------
int IsForbPrefix(int a[]){
int i;
    
    for (i=1; i<=pn; i++) if (a[i] != prefix[i]) break;
    if (i > pn) return 0;
    return 1;
}
//-------------------------------------------------------------------------------
// Less (or greater) than (or equal) to its complemented reversal
//-------------------------------------------------------------------------------
int IsCompRev(int a[]){
    
    // Less than complemented reversal
    if (bit == 0) {
        for (int i=1; i<=(n+1)/2; i++) {
            if (a[i] == 1 && a[i] ==  a[n-i+1]) return 0;
            if (a[i] == 0 && a[i] ==  a[n-i+1]) return 1;
        }
    }
    // Greater than complemented reversal
    else {
        for (int i=1; i<=(n+1)/2; i++) {
            if (a[i] == 1 && a[i] ==  a[n-i+1]) return 1;
            if (a[i] == 0 && a[i] ==  a[n-i+1]) return 0;
        }
    }
    if (type == 11 || type == 51) return 0;  // string equals complemented reversal
    return 1;
}
//-------------------------------------------------------------------------------
// Less (or greater) than or equal to a given string w[1..n] of the same length
//-------------------------------------------------------------------------------
int IsCompare(int a[]){
    
    // Less than or equal to w[1..n]
    if (bit == 0) {
        for (int i=1; i<=n; i++) {
            if (a[i] < w[i]) return 1;
            if (a[i] > w[i]) return 0;
        }
    }
    // Greater than or equal to w[1..n]
    else {
        for (int i=1; i<=n; i++) {
            if (a[i] < w[i]) return 0;
            if (a[i] > w[i]) return 1;
        }
    }
    return 1;
}
//-------------------------------------------------------------------------------
// The number of 1..0 inversions (or 0..1) is less than or equal to k
//-------------------------------------------------------------------------------
int IsInv(int a[]){
int i, ones=0, inv=0;
    
    // <= k inversions of form 1..0
    if (bit == 0) {
        for (i=1; i<=n; i++) {
            if (a[i] == 1) ones++;
            else inv += ones;
        }
    }
    // <= k inversions of form 0..1
    else {
        for (i=1; i<=n; i++) {
            if (a[i] == 0) ones++;
            else inv += ones;
        }
    }
    if (inv > k) return 0;
    return 1;
}
//-------------------------------------------------------------------------------
// The minimum number of swaps required to convert the word into the form 0^* 1^*
// (or 1^* 0^*) is less than or equal to k
//-------------------------------------------------------------------------------
int IsTrans(int a[]){
    int i, trans=0, d = 0;
    
    // <= k transpositions from 0^* 1^*
    if (bit == 0) {
        for (i=1; i<=n; i++) if (a[i] == 1) d++;
        for (i=1; i<=n-d; i++) if (a[i] == 1) trans++;
    }
    // <= k transpositions from 1^* 0^*
    else {
        for (i=1; i<=n; i++) if (a[i] == 0) d++;
        for (i=1; i<=n-d; i++) if (a[i] == 0) trans++;
    }
    
    if (trans > k) return 0;
    return 1;
}
//-------------------------------------------------------------------------------
// Prefix normal words: every substring of length j has no more 0s (or 1s) as a
// prefix of the same length.
//-------------------------------------------------------------------------------
int IsPrefixNormal(int a[]){
int i,j,f,p[MAX];
    
    // 0-PNW or 1-PNW depending on bit
    p[0] = 0;
    for (i=1; i<=n; i++) p[i] = p[i-1] + (a[i]+bit)%2;
    for (i=2; i<=n; i++) {
        f = 0;
        for (j=i; j<=n; j++) {
            f += (a[j] + bit)%2;
            if (f < p[j-i+1]) return(0);
        }
    }
    return 1;
}
//-------------------------------------------------------------------------------
// Left factors of k-ary Dyck paths.  A 2-ary Dyck path corresponds to a balanced
// parenthesis string and the left factors discussed in https://oeis.org/A001405
//-------------------------------------------------------------------------------
int IsDyck(int a[]){
    int i, d = 0;
    
    // Left factors of parenethese strings (dyck words) starting with "bit" as (
    for (i=1; i<=n; i++) {
        if (a[i] == bit) d++;
        else d -= k-1;
        if (d < 0) return 0;
    }
    return 1;
}
//-------------------------------------------------------------------------------
// Vector solutions to the 0-1 knapsack problem for a given capacity
//-------------------------------------------------------------------------------
int IsKnapsack(int a[]){
    int i, sum = 0;
    
    for (i=1; i<=n; i++) if (a[i] == 1) sum += w[i];
    if (sum <= capacity) return 1;
    return 0;
}
//-------------------------------------------------------------------------------
// Integer partitions discussed in https://oeis.org/A000041
//-------------------------------------------------------------------------------
int IsIntPartition(int a[]) {
    int i, j = n+1, t;
    
    if (a[n]!=1-bit) return 0;
    
    for (i=1; i<=n; i++) {
        if (a[i]==bit) continue;
        
        if (j == n+1) {j = i; t = i;}
        else if (t < i-j) return 0;
        else {t = i - j; j = i;}
    }
    return 1;
}
//-------------------------------------------------------------------------------
int IsUnlabeledNecklace(int a[]) {
    if (!IsNecklace(a)) return 0;
    
    int i, j, t[MAX], c[MAX];
    
    for (i=1; i<=n; i++) c[i] = !a[i];
    j = Booth(c);
    for (i=j+1; i<=n; i++) t[i-j] = c[i];
    for (i=1; i<=j; i++) t[n-j+i] = c[i];
    
    for (i=1; i<=n; i++) {
        if ((a[i]^bit) < (t[i]^bit)) return 1;
        else if ((t[i]^bit) < (a[i]^bit)) return 0;
    }
    
    return 1;
}
//==========================================================================================
int IsCoNecklace(int a[]) {
    int i, b[MAX];
    
    for (i=1; i<=n; i++) b[i] = a[i];
    for (i=n+1; i<=2*n; i++) b[i] = 1-a[i-n];
    
    n = n * 2;
    i = IsNecklace(b);
    n = n / 2;
    return i;
}
//==========================================================================================
int IsForbSubstring(int a[]) {
    int i, j;
    
    for (i=1; i<=n-pn+1; i++) {
        for (j=i; j<i+pn; j++) {
            if (substr[j-i+1]!=a[j]) break;
            if (j==i+pn-1) return 0;
        }
        if (i==n-pn+1) return 1;
    }
    return 1;
}
//-------------------------------------------------------------------------------
// Fibonacci words discussed in http://v.vincent.u-bourgogne.fr/0ABS/0ARTS/gascom_MkT2.pdf
//-------------------------------------------------------------------------------
int IsFibonacci(int a[]) {
    int i, j=0;
    
    for (i=1; i<=n; i++) {
        if (a[i]==1-bit) j++;
        else j = 0;
        
        if (j==k) return 0;
    }
    
    return 1;
}
//-------------------------------------------------------------------------------
// Lucas words discussed in http://v.vincent.u-bourgogne.fr/0ABS/0ARTS/gascom_MkT2.pdf
//-------------------------------------------------------------------------------
int IsLucas(int a[]) {
    int i, j, b[MAX];
    
    for (i=1; i<=n; i++) b[i] = a[i];
    for (i=n+1; i<=2*n; i++) b[i] = a[i-n];
    
    n = n * 2;
    j = IsFibonacci(b);
    n = n / 2;
    return j;
}
//==========================================================================================
void PrintString(int a[], int density, int leftmost) {
    
    if (density == 0 && !ZEROS_ONES) return;
    if (leftmost == n && !ZEROSONE_ONESZERO) return;
    
    if (limit >=0 && total >= limit) {
        printf("output limit reached\n");
        exit(0);
    }
    
    for (int i=1; i<=n; i++) printf("%d", a[i]); printf("\n");
    total++;
}
//==========================================================================================
void Flip(int a[], int t) {
    
    a[t] = 1 - a[t];
}
//==========================================================================================
int MemberTest(int a[], int s, int t) {
int ret_val = 0;
    
    Flip(a,s);
    Flip(a,t);
    switch (type) {
        case  1: ret_val=1; break;
        case  2: if (IsNecklace(a))     ret_val=1; break;
        case  3: if (IsLyndon(a))       ret_val=1; break;
        case  4: if (IsPre(a))          ret_val=1; break;
        case  5: if (IsPseudo(a))       ret_val=1; break;
        case  6: if (IsRev(a))          ret_val=1; break;
        case  7: if (IsRev(a))          ret_val=1; break;
        case  8: if (IsDen(a))          ret_val=1; break;
        case  9: if (IsForb(a))         ret_val=1; break;
        case 10: if (IsForbPrefix(a))   ret_val=1; break;
        case 11: if (IsCompRev(a))      ret_val=1; break;
        case 12: if (IsCompRev(a))      ret_val=1; break;
        case 13: if (IsCompare(a))      ret_val=1; break;
        case 14: if (IsInv(a))          ret_val=1; break;
        case 15: if (IsTrans(a))        ret_val=1; break;
        case 16: if (IsPrefixNormal(a)) ret_val=1; break;
        case 17: if (IsDyck(a))         ret_val=1; break;
        case 18: if (IsKnapsack(a))     ret_val=1; break;
        case 19: if (IsIntPartition(a)) ret_val=1; break;
        case 20: if (IsUnlabeledNecklace(a))    ret_val=1; break;
        case 21: if (IsCoNecklace(a))   ret_val=1; break;
        case 22: if (IsFibonacci(a))    ret_val=1; break;
        case 23: if (IsLucas(a))        ret_val=1; break;
        case 24: if (IsForbSubstring(a))        ret_val=1; break;
            
        case 41: ret_val=1; break;
        case 42: if (IsNecklace(a))     ret_val=1; break;
        case 43: if (IsLyndon(a))       ret_val=1; break;
        case 44: if (IsPre(a))          ret_val=1; break;
        case 45: if (IsPseudo(a))       ret_val=1; break;
        case 46: if (IsRev(a))          ret_val=1; break;
        case 47: if (IsRev(a))          ret_val=1; break;
        case 48: if (IsDen(a))          ret_val=1; break;
        case 49: if (IsForb(a))         ret_val=1; break;
        case 50: if (IsForbPrefix(a))   ret_val=1; break;
        case 51: if (IsCompRev(a))      ret_val=1; break;
        case 52: if (IsCompRev(a))      ret_val=1; break;
        case 53: if (IsCompare(a))      ret_val=1; break;
        case 54: if (IsInv(a))          ret_val=1; break;
        case 55: if (IsTrans(a))        ret_val=1; break;
        case 56: if (IsPrefixNormal(a)) ret_val=1; break;
        case 57: if (IsDyck(a))         ret_val=1; break;
        case 58: if (IsIntPartition(a)) ret_val=1; break;
        case 59: if (IsUnlabeledNecklace(a))    ret_val=1; break;
        case 60: if (IsCoNecklace(a))   ret_val=1; break;
        case 61: if (IsFibonacci(a))    ret_val=1; break;
        case 62: if (IsLucas(a))        ret_val=1; break;
        case 63: if (IsForbSubstring(a))        ret_val=1; break;

    }
    Flip(a,s);
    Flip(a,t);
    return ret_val;
}
//==========================================================================================
// Successor rule for 3-BRGC languages
//==========================================================================================
void f(int a[], int density, int leftmost) {
    int t = 1;
    
    if (density == 1 && (leftmost == n || !MemberTest(a,leftmost,leftmost+1))) Flip(a,leftmost);
    else if (density % 2 == 0) {
        while (t<leftmost && !MemberTest(a,t,0)) t++;
        Flip(a,t);
            
        t--;
        while(t>=1 && !MemberTest(a,t,0)) t--;
        if (t) Flip(a,t);
    }
    else {
        if (MemberTest(a,leftmost+1,0)) Flip(a, leftmost+1);
        else {
            Flip(a,leftmost);
            
            t = leftmost+1;
            while (t<=n && !MemberTest(a,t,0)) t++;
            Flip(a,t);
            
            t--;
            while(t>=1 && !MemberTest(a,t,0)) t--;
            if (t) Flip(a,t);
        }
    }
}
//==========================================================================================
// List the selected 3-BRGC language in Gray code order by repeatedly applying a successor
// rule.  The initial string can be set to any string in the language while updating the
// termination condition.
//==========================================================================================
void BRGC() {
    int a[MAX],i,density=0,leftmost=n+1;
    
    for (i=1; i<=n; i++) a[i] = bit;
    do {
        PrintString(a, density, leftmost);
        f(a, density, leftmost);
        density = 0;
        for (i=n; i>=1; i--) {
            if (a[i] == 1-bit) density++;
            if (a[i] == 1-bit) leftmost = i;
        }
    } while (density != 0);
}
//==========================================================================================
void usage() {
    printf("Usage: brgc [type] [n] [k] [forbid_substring] (1<=type<=24 or 41<=type<=63, n>=1, k>=0)\n");
}
//==========================================================================================
int main(int argc, char **argv) {
int i;
char s[MAX];
    
/*      printf("  ===============================================================================\n");
        printf("                         2-BRGC LANGUAGES \n");
        printf("  Strings starting 0^n                     Strings starting 1^n \n");
        printf("  ===============================================================================\n");
        printf("  1.  All strings                          41.  All strings\n");
        printf("  2.  Necklaces (lex min rep)              42.  Necklaces (lex max rep)  \n");
        printf("  3.  Lyndon words (lex min rep)           43.  Aperiodic necklaces (lex max rep)\n");
        printf("  4.  Prenecklaces (lex min rep)           44.  Prenecklaces (lex max rep)\n");
        printf("  5.  Pseudonecklaces (lex min rep)        45.  Pseudonecklaces (lex max rep)\n");
        printf("  6.  <  their reversals                   46.  > their reversals\n");
        printf("  7.  <= their reversals                   47.  >= their reversals  \n");
        printf("  8.  With maximum density (weight)        48.  With minimum density (weight)\n");
        printf("  9.  With forbidden 10^k                  49.  With forbidden 01^k\n");
        printf(" 10.  With forbidden 1xxx prefix           50.  With forbidden 0xxx prefix \n");
        printf(" 11.  <  their complemented reversals      51.  >  their complemented reversals \n");
        printf(" 12.  <= their complemented reversals      52.  >= their complemented reversals \n");
        printf(" 13.  <= a given string xxx                53.  >= a given string xxx\n");
        printf(" 14.  <= k inversions of form 1..0         54.  <= k inversions of form 0..1\n");
        printf(" 15.  <= k transpositions from 0^* 1^*     55.  <= k transpositions from 1^* 0^*\n");
        printf(" 16.  0-Prefix normal words                56.  1-Prefix normal words\n");
        printf(" 17.  Left factors of k-ary Dyck paths     57.  Left factors of k-ary Dyck paths  \n");
        printf(" 18.  Feasible solutions to 0-1 knapsack   \n");
        printf("  ===============================================================================\n");
        printf("                         3-BRGC LANGUAGES \n");
        printf("  ===============================================================================\n");
        printf(" 19.  Integer partitions                   58. Integer partitions\n");
        printf(" 20.  Unlabeled necklaces (lex min rep)    59. Unlabeled necklaces (lex max rep)\n");
        printf(" 21.  CoNecklaces (lex min rep)            60. CoNecklaces (lex max rep)\n");
        printf(" 22.  Fibonacci words w.r.t 1              61. Fibonacci words w.r.t 0 \n");
        printf(" 23.  Lucas words w.r.t 1                  62. Lucas words w.r.t 0 \n");
        printf(" 24.  With forbidden 1xxx                  63. With forbidden 0xxx; \n");
        printf("  ===============================================================================\n\n");
*/
    
    if (argc < 3) { usage();  return 1; }
    sscanf(argv[1], "%d", &type);
    sscanf(argv[2], "%d", &n);
    sscanf(argv[3], "%d", &k);
    if (argc > 4) sscanf(argv[4], "%lld", &limit);
    if (type != 18 && argc > 5) sscanf(argv[5], "%s", s);
    if (type == 18) {
        sscanf(argv[5], "%lld", &capacity);
        for (i=1; i<=n; i++) {
            sscanf(argv[5+i], "%d", &w[i]);
            if (i > 1 && w[i] > w[i-1]) {
                printf("Weights must be in decreasing order");
                exit(0);
            }
        }
    }

    // DETERMINE WHICH 2-BRGC CASE
    if (type >= 40) bit = 1;
        
    // LANGUAGES WITHOUT THE ROOT OF THE POSET EITHER 0^n OR 1^n
    if (type == 3 || type == 6 || type == 43 || type == 46 || type == 19 || type == 58) ZEROS_ONES = 0;
        
    // EXTRA INPUT DETAILS
    if (type == 8 || type == 48)  den = k;

    if (type == 10 || type == 50) {
        pn = strlen(s);
        for (i=1; i<=pn; i++) prefix[i] = s[i-1] - '0';
        if (prefix[1] != 1-bit) {
            printf("\n INVALID INPUT: prefix must begin with %d\n", 1-bit);
            exit(0);
        }
    }
    if (type == 13 || type == 53) {
        if (strlen(s) != n) {
            printf("\n INVALID INPUT: binary string must have length N\n");
            exit(0);
        }
        for (i=1; i<=n; i++) w[i] = s[i-1] - '0';
    }
    /*
    if (type == 24 || type == 63) {
        printf(" Enter forbidden substring starting with %d: ", 1-bit);  scanf("%s", s);
        pn = strlen(s);
        for (i=1; i<=pn; i++) substr[i] = s[i-1] - '0';
    } */

    BRGC();

}
