/*======================================================================*/
/* C program for distribution from the Combinatorial Object             */
/* Server. Generate B-trees in lexicographic order.                	*/
/* This is the same version used in the upcoming book "Combinatorial    */
/* Generation" by Frank Ruskey.  It is base on the ideas of Peirre      */
/* Kelsen's manuscript "Constant time generation of B-trees.".          */
/* The program can be modified, translated to other languages, etc.     */
/* so long as proper acknowledgement is given (author and source),      */
/* for example, leaving these comments intact. 				*/ 
/* Programmer: Frank Ruskey, 1995.                                      */
/* Programmer: Joe Sawada (translation to C), 1997.                     */
/* The latest version of this program and an explanation of the         */
/* representation of B-trees may be found at the site:          	*/
/* http://www.theory.csc.uvic.ca/inf/tree/BTrees.html.  	        */
/*======================================================================*/

#include <stdio.h>

int a[100];
int n,m,m2,d;

void PrintIt(int p) {
	
	int j;

	for(j=1; j<=p; j++) printf("%d",a[j]);
	printf("\n");
}

int min(int a, int b) {
	
	if (a<b) return(a);
	return(b);
}

void Gen( int s, int d, int p) {
	
	int i;

	if ((s == 1) && (d == 0)) PrintIt(p-1);
	else {
		if ((s>m) && (d>=0)) {
			for (i=m2; i<= min(s-m2,m); i++) {
				a[p] = i;
				Gen(s-i, d+1, p+1);
			}
		} 
		else {
			if (s == m) {
				if (m%2 == 0) {
					a[p] = m2;
					Gen(m2, d+1, p+1);
				}
				a[p] = m;
				Gen(d+1, 0, p+1);
			}	
			else { 
				if ( (s >=m2 ) && (d > 0) ||(s >= 2)) {
					a[p] = s;
					Gen(d+1, 0, p+1);
				}
			}
		}
	}
}
			

void main() {

	int i;	

	printf("Enter n,m: ");
	scanf("%d %d",&n,&m);
	if (m<3) printf("m must be greater than 2.\n\n");
	else { 
		for (i=0; i<=n; i++)  a[i] = 0;
		m2 = (m+1)/2;
		printf("\n");
		Gen(n,0,1);
		printf("\n");
	}
}
			
			
