#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX 32
#define MAX_ITERATIONS 10000
typedef unsigned long ulong;

/* single-word minpoly calc */

int n;
int d;
int type;
int outformat;
int limit;
ulong twonm1;  /* 2^n-1 */
int ir_count = 0, pr_count = 0, density; 
int ir_hist[MAX], pr_hist[MAX];
ulong prim_poly;
int b[MAX];
int iter_limit = 0;

struct pair {
  ulong poly;
  ulong pow2m1;
};

struct pair poly_table[MAX] = {  /* prime factorization of 2^n-1 */
  /*  0 */  {  1,          0 },
  /*  1 */  {  1,          1 },
  /*  2 */  {  3,          3 },
  /*  3 */  {  3,          7 },
  /*  4 */  {  3,         15 },
  /*  5 */  {  5,         31 },
  /*  6 */  {  3,         63 },
  /*  7 */  {  3,        127 },
  /*  8 */  { 29,        255 },
  /*  9 */  { 17,        511 },
  /* 10 */  {  9,       1023 },
  /* 11 */  {  5,       2047 },
  /* 12 */  { 83,       4095 },
  /* 13 */  { 27,       8091 },
  /* 14 */  { 43,      16383 },
  /* 15 */  {  3,      32767 },
  /* 16 */  { 45,      65535 },
  /* 17 */  {  9,     131071 },  /* (131071)     it's prime!          */
  /* 18 */  { 39,     262143 },  /* (3) (3) (7) (19) (73)             */
  /* 19 */  { 39,     524287 },  /* (524287)     it's prime!          */
  /* 20 */  {  9,    1048575 },  /* (3) (5) (5) (11) (31) (41)        */
  /* 21 */  {  5,    2097151 },  /* (7) (7) (127) (337)               */
  /* 22 */  {  3,    4194303 },  /* (3) (23) (89) (683)               */
  /* 23 */  { 33,    8388607 },  /* (47) (178481)                     */
  /* 24 */  { 27,   16777215 },  /* (3) (3) (5) (7) (13) (17) (241)   */
  /* 25 */  {  9,   33554431 },  /* (31) (601) (1801)                 */
  /* 26 */  { 71,   67108863 },  /* (3) (8191) (2731)                 */
  /* 27 */  { 39,  134217727 },  /* (7) (73) (262657)                 */
  /* 28 */  {  9,  268435455 },  /* (3) (5) (29) (43) (113) (127)     */
  /* 29 */  {  5,  536870911 },  /* (233) (1103) (2089)               */
  /* 30 */  { 83, 1073741823 },  /* (3) (3) (7) (11) (31) (151) (331) */
  /* 31 */  {  9, 2147483647 }   /* (2147483647) it's prime!          */
};

void ProcessInput (int argc, char *argv[])
{ 
   if (argc == 6) {
	n = atoi(argv[1]);
	d = atoi(argv[2]);
	type = atoi(argv[3]); /* 0 if both, 1 if only primitive */
	outformat = atoi(argv[4]);
	limit = atoi(argv[5]);
   } 
   else {
	printf("Usage: poly n d type outformat limit\n");
	exit(1);
   }
}

int gcd ( n, m )
int n,m;
{
  if (m == 0) return(n); 
  else return(gcd(m,n%m));
}


void PrintString( int n, ulong a) {
  int i;
  printf("1 ");  
  for ( i=n-1; i>=0; i-- )
    if ( a & (1<<i) ) {
      if (i != 0) printf("1 "); 
    }
    else printf("0 ");
  printf("1\n"); 
}
void PrintPoly( int n, ulong a) {
  int i;
  density = 1;
  printf("x<sup>%d</sup> + ", n );  
  for ( i=n-1; i>=0; i-- )
    if ( a & (1<<i) ) {
      ++density;
      if (i != 0) printf("x<sup>%d</sup> + ", i ); 
   }
  printf("1\n"); 
}

void polyToString( n, a, s )
int n;  ulong a;  char* s;
{
  int i;
  density = 1;
  sprintf( s, "%d", n );  
  for ( i=n-1; i>=0; i-- )
    if ( a & (1<<i) ) {
      ++density;
      sprintf( s+strlen(s), ", %d", i ); 
    }
  ++ir_hist[density];
}


ulong multmod( n, a, b, p )
int n;  ulong a,b,p;
{
  ulong t, rslt;
  ulong top_bit;
  int i;

  rslt = 0;
  t = a; /* t is a*x^i */
  top_bit = 1 << (n-1);

  for ( i=0; i<n; i++ )
  {
     if (b & 1) rslt ^= t;
     if (t & top_bit)
        t = ( (t & ~top_bit) << 1 ) ^ p;
     else
        t = t << 1;
     b = b >> 1;
  }
  return rslt;
}

ulong powmod( n, a, power, p )
int n,power;  ulong a,p;
{
  ulong t = a;
  ulong rslt = 1;
  while ( power != 0 )
  {
    if ( power & 1 ) rslt = multmod( n, t, rslt, p );
    t = multmod( n, t, t, p );
    power = power >> 1;
  }
  return rslt;
}

ulong minpoly( n, necklace, p )
int n, necklace;  ulong p;
{
  ulong root, rslt = 0;
  ulong f[ MAX ];
  int i, j;

  f[0] = 1;
  for (i=1; i<n; i++ ) f[i] = 0;

  root = powmod( n, 2, necklace, p ); /* '2' is monomial x */
  for (i=1; i<=n; i++ )
  {
    if (i != 1)
      root = multmod( n, root, root, p );

    for (j=n-1; j>=1; j--)
      f[j] = f[j-1] ^ multmod( n, f[j], root, p );
    f[0] = multmod( n, f[j], root, p );
  }

  for (i=0; i<n; i++ )
    if (f[i] == 1)
      rslt |= 1 << i;
    else if (f[i] != 0)
      fprintf( stderr, "Ahh!" );

  return rslt;
}

ulong toInt ( )
{
ulong i,x;
  x = 0;
  for (i=1; i<=n; ++i) {
     x = 2*x;
     if (b[i] == 1) ++x;
  }
  return( x );
}
 

void PrintIt( p )
int p;
{
  int necklace;
  ulong m;
  int i;
  char s[100];
  static int count = 0;

  if (p != n) return; 
  if (iter_limit++ > MAX_ITERATIONS) { 
  	printf("</TABLE>\n<BR>");
  	if (type == 0) {
		 printf( "The first %d irreducible polynomials<BR>\n", ir_count);
 		 printf("<FONT COLOR=0000ff>");
  	}
 	printf( "The first %d primitive polynomials\n<br><br>", pr_count );
	printf("<br><b><blink><font color=ff0000>Iteration limit exceeded !!</B><br></blink></font>\n");
	exit(0); 
  }
  if (count > limit) { printf("</TABLE>"); exit(1); }
  necklace = toInt();
  m = minpoly( n, necklace, prim_poly );
  density = 1;
  for ( i=n-1; i>=0; i-- )
    if ( m & (1<<i) ) ++density;
  if (density > d) return;
  count++;
  ++ir_count;
  
     polyToString( n, m, s ); 
     if ( gcd( necklace, twonm1 ) == 1 ) {
  	printf("<TR>");
        ++pr_count;
        if (outformat & 1) {
		printf("<TD>");
		if (type == 0) printf("<FONT COLOR=0000ff>");
 		PrintString(n,m); 
		if (type == 0) printf("</FONT>");
        }
        if (outformat & 2) {
		printf("<TD>");
		if (type == 0) printf("<FONT COLOR=0000ff>");
		printf("%s\n",s);  
		if (type == 0) printf("</FONT>");
	}
        if (outformat & 4) {
		printf("<TD>");
		if (type == 0) printf("<FONT COLOR=0000ff>");
 		PrintPoly(n,m); 
		if (type == 0) printf("</FONT>");
        }
     }
/* print only if type = 0 (irreducible) */
     else if (type == 0) {
  	printf("<TR>");
        if (outformat & 1) {
		printf("<TD>");
 		PrintString(n,m); 
        }
        if (outformat & 2)  printf("<TD>%s\n",s);  
        if (outformat & 4) {
		printf("<TD>");
 		PrintPoly(n,m); 
        }
     }
}


void gen( t, lyn )
int t, lyn;
{
   if (t > n) PrintIt( lyn );
   else {
      b[t] = b[t-lyn];  gen( t+1, lyn );
      if ((b[t-lyn] == 0) && (n>1)) {
         b[t] = 1;  gen( t+1, t );
      }
   }
}

int main (int argc, char *argv[] )
{
  int i;
  ProcessInput( argc, argv );

  printf("<P><TABLE BORDER=1 CELLSPACING=2 CELLPADDING=2><TR>\n");
  if (outformat & 1) {
	printf("<TH COLSPAN=1><FONT SIZE=+1>String</FONT><BR></TH>\n");
  }
  if (outformat & 2) {
	printf("<TH COLSPAN=1><FONT SIZE=+1>Coefficients</FONT><BR></TH>\n");
  }
  if (outformat & 4) {
	printf("<TH COLSPAN=1><FONT SIZE=+1>Polynomials</FONT><BR></TH>\n");
  }

  for(i=0; i<=n; ++i) ir_hist[i] = 0;
  prim_poly = poly_table[n].poly;
  twonm1 = poly_table[n].pow2m1; 
  b[0] = 0;
  gen(1,1);

  printf("</TABLE>\n<BR>");
  if (type == 0) {
	 printf( "The number of irreducible polynomials = %d<BR>\n", ir_count);
 	 printf("<FONT COLOR=0000ff>");
  }
  printf( "The number of primitive polynomials   = %d\n", pr_count );
  return(0);
} 
