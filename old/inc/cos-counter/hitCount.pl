#!/usr/bin/perl
#
# Script to sum the total hit counts from the file cos-access_count

# 100210	TB - changed misformed path from /theory/web... to /theory/www
# 		should fix errors appearing in error log
open (HITS, "/theory/www/act/cos-access_count") || 
		die "Cannot access total hit count\n";
$sum = 0;
while (<HITS>) {
	@stats = split;
	$sum = $sum + $stats[1];
}
print "Content-type: text/html\n\n";
print "(These are hits to this page only; in total there have been ". $sum . 
	" hits on the pages of COS since May 16, 2000.)\n";
close (HITS);

