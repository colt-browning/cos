/****given a value for n,k and ouput mode specified by ProcessInput,
this program prints out subsets of {1,...,n} in bitstrings or lists
in lex order with limit size = k*****/

#include <stdio.h>
#include "ProcessInput.c"

/*********globals*********/
int bitstring = 0;
int list = 0;
int A[MAX];

/************local functions**********/

/*return the number of non-zero elements in the array A*/
int count()
{
   int cnt = 0;
   int i;

   for(i=0;i<N;i++)
      if(A[i])
         cnt++;
   return(cnt);
}

/*printout the generated number according to the output format*/
void printit()
{
   int i;
   int beg = 1;

   if(count()<=K) {
      printf("<TR>");
      if(bitstring) {
         printf("<TD ALIGN=CENTER>");
         for(i=0;i<N;i++)
            printf("%d",A[i]);
         printf("<BR></TD>\n");
      }
      if(list) {
         printf("<TD ALIGN=CENTER>");
         printf("{");
         for(i=N-1;i>=0;i--) {
            if(A[i]) {
               printf("%s%d",beg ? "" : ",",N-i);
               beg = 0;
            }
         }
         printf("}<BR></TD>\n");
      }	
      printf("</TR>");
     LIMIT--; /*count down the number of printable objects*/
  }
}

/*generate bitstrings in A in lex order
this is done recursively
*/
void lex(k)
int k;
{
   if(LIMIT<1)
      return;
   if(k>=N)
      printit();
   else {
      A[k] = 0;
      lex(k+1);
      A[k] = 1;
      lex(k+1);
   }
}

/***********main program********/

int main(int argc, char **argv)
{
   int i;
   int count;

   ProcessInput(argc,argv);
   count = LIMIT;
   if(out_format & 1)
      bitstring = 1;
   if(out_format & 2)
      list = 1;

   lex (0);
   printf("</TABLE><BR>Total number of subsets = %d<BR>\n", count-LIMIT);
   if(LIMIT)
      return(0);
   else
      return(-1);
}
