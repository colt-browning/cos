struct stamp *first;

int count = 0;

void GenStamp(struct stamp *latest, int parity);   /* Recursively generates
						      stamps using a stamp of
						      one size smaller.
						      Parity is the parity of
						      'latest' */
void AddStamp(struct stamp *current, struct stamp *new);

void RemoveStamp(struct stamp *new);

void DisplayStamp();

void main(int argc, char *argv[])
	{
	struct stamp  *temp_stamp;
	struct perforation *temp_perf;
	ProcessInput(argc, argv);

	NN = N;
	/* initialize the data structures */
	temp_stamp = (struct stamp *) malloc(sizeof(struct stamp));
	temp_stamp->value = 1;

	first = (struct stamp *) malloc(sizeof(struct stamp));
	first->value = 0;
	first->prev = temp_stamp;
	first->next = temp_stamp;

	temp_perf = (struct perforation *) malloc(sizeof(struct
perforation));
	temp_perf->left_value  = 0;
	temp_perf->right_value = 0;
	temp_perf->left_link  = first;
	temp_perf->right_link = first;

	first->perf[0] = temp_perf;
	first->perf[1] = temp_perf;

	temp_perf = (struct perforation *) malloc(sizeof(struct
perforation));
	temp_perf->left_value = 0;
	temp_perf->right_value = 1;
	temp_perf->left_link = 0;
	temp_perf->right_link = temp_stamp;

	temp_stamp->perf[0] = 0;
	temp_stamp->perf[1] = temp_perf;
	temp_stamp->prev = first;
	temp_stamp->next =  first;

	GenStamp(first->next, 1);

	free (temp_stamp);
	free (temp_perf);
	free (first->perf[0]);
	free (first->perf[1]);
	free (first);

	printf("\n</TABLE><P> Stamps = %d", count);

	exit(0);
	}

void GenStamp(struct stamp *latest, int parity)
	{
	struct stamp *temp = 0;		/* points to new stamp */
	struct perforation *new_perf = 0;	/* points to new
perforation */
	struct stamp *current = 0;	/* points to a current
stamp */

	/* variables used to adjust the perforation (see below) */
	struct stamp *temp_stamp = 0;
	struct perforation *temp_perf = 0;
	int temp_value;


	if (latest->value == N)
		{
		DisplayStamp();
		return;
		}

	/* make new stamp */
	temp = (struct stamp *) malloc(sizeof(struct stamp));
	temp->value = latest->value + 1;
	temp->prev = 0;
	temp->next = 0;
	temp->perf[0] = 0;
	temp->perf[1] = 0;

	/* make new perforation */
	new_perf = (struct perforation *) malloc(sizeof(struct perforation));
	new_perf->right_value = temp->value;
	new_perf->left_value = 0;

	/* connect stamp to perforation */
	temp->perf[(parity + 1) % 2] = new_perf;
	temp->perf[parity] = 0;
	new_perf->right_link = temp;
	new_perf->left_link = 0;

	/* connect new stamp to peforation of 'latest' */
	(latest->perf[parity])->left_link = temp;
	(latest->perf[parity])->left_value = temp->value;
	temp->perf[parity] = latest->perf[parity];

	/* go left */
	current = latest;
	do
		{
		AddStamp(current, temp);	
		GenStamp(temp, (parity + 1) % 2);
		RemoveStamp(temp);
		current = current->prev;

		if (current == latest) break;

		if (current->perf[parity] != 0)
		   {
		   if ( ((current->perf[parity])->right_value == 
current->value))
		      {
		      current = (current->perf[parity])->left_link;
		      if (current->value == 0) /* adjust per if necessary
*/
		         {
			 temp_perf = latest->perf[parity];
			 /* swap values */
			 temp_value = temp_perf->left_value;
			 temp_perf->left_value = temp_perf->right_value;
			 temp_perf->right_value = temp_value;
			 /* swap links */
		 	 temp_stamp = temp_perf->left_link;
			 temp_perf->left_link = temp_perf->right_link;
			 temp_perf->right_link = temp_stamp;
			 }
		      }
		   else
		      {
		      current = (current->perf[parity])->right_link;
		      /* adjust the perforation */
		      temp_perf = latest->perf[parity];
		      temp_value = temp_perf->left_value;
		      temp_perf->left_value = temp_perf->right_value;
		      temp_perf->right_value = temp_value;
		      temp_stamp = temp_perf->left_link;
		      temp_perf->left_link = temp_perf->right_link;
		      temp_perf->right_link = temp_stamp;
		      }
		   }
		}
	while (current != latest);


	/* remove the extra stamp and extra perforation */
	temp_perf = temp->perf[parity];
	if (temp_perf->left_value == temp->value)
		{
		temp_perf->left_value = 0;
		temp_perf->left_link = 0;
		}
	else
		{
		temp_perf->right_value = temp_perf->left_value;
		temp_perf->right_link = temp_perf->left_link;
		temp_perf->left_value = 0;
		temp_perf->left_link = 0;
		}
	free (temp);
	free (new_perf);
	}


void AddStamp(struct stamp *current, struct stamp *new)
	{
	struct stamp *temp = 0;

	temp = current;
	temp = temp->prev;
	new->next = temp->next;
	new->prev = temp;
	(temp->next)->prev = new;
	temp->next = new;
	}

void RemoveStamp(struct stamp *new)
	{
	struct stamp *before;
	struct stamp *after;

	before = new->prev;
	after = new->next;
	before->next = after;
	after->prev = before;
	new->next = 0;
	new->prev = 0;
	}

void DisplayStamp()
	{
	struct stamp *temp;
	int i;

	Pi[0] = N + 1;
	iP[0] = N + 1;
	temp = first->next;
	for (i = 1; (i <= N) && (temp->value != 0); i++)
		{
		Pi[i] = temp->value;
		iP[temp->value] = i;
		temp = temp->next;
		}

	PrintIt();
	}




