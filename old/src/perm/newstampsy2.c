#include <stdio.h>
#include <stdlib.h>
#include "ProcessInput.c"
#include "commonio.h"
#include "gdstamp.c"

#define SIZE_LIMIT 200

#define TRUE   1
#define FALSE  0
#define odd(x) ( (x)%2==1 ? TRUE : FALSE )
#define LIMIT_ERROR -1

void PrintIt();
void PrintBoard(int perm[], int n);
void init_tab( int );
void insert_tab( int );
void tableaux( int );
void tableaux2 ( int );
int NN;

struct stamp *first;

int stamp_array[SIZE_LIMIT];
int second_array[SIZE_LIMIT];

int count = 0;

void GenStamp(struct stamp *latest);   /* Recursively generates stamps
					  using a stamp of one size
					  smaller. */
void AddStamp(struct stamp *current, struct stamp *new);

void RemoveStamp(struct stamp *new);

void DisplayStamp();

void relabel();

int canonical();

void reverse();


int main(int argc, char *argv[])
	{
	struct stamp  *temp_stamp;
	struct perforation *temp_perf;
	ProcessInput(argc, argv);

	NN = N;
	/* initialize the data structures */
	temp_stamp = (struct stamp *) malloc(sizeof(struct stamp));
	temp_stamp->value = 1;

	first = (struct stamp *) malloc(sizeof(struct stamp));
	first->value = 0;
	first->prev = temp_stamp;
	first->next = temp_stamp;

	temp_perf = (struct perforation *) malloc(sizeof(struct
perforation));
	temp_perf->left_value  = 0;
	temp_perf->right_value = 0;
	temp_perf->left_link  = first;
	temp_perf->right_link = first;

	first->perf[0] = temp_perf;
	first->perf[1] = temp_perf;

	temp_perf = (struct perforation *) malloc(sizeof(struct
perforation));
	temp_perf->left_value = 0;
	temp_perf->right_value = 1;
	temp_perf->left_link = 0;
	temp_perf->right_link = temp_stamp;

	temp_stamp->perf[0] = 0;
	temp_stamp->perf[1] = temp_perf;
	temp_stamp->prev = first;
	temp_stamp->next =  first;

	GenStamp(first->next);

	free (temp_stamp);
	free (temp_perf);
	free (first->perf[0]);
	free (first->perf[1]);
	free (first);

	printf("\n</TABLE><P> Stamps = %d", count);

	exit(0);
	}

void GenStamp(struct stamp *latest)
	{
	struct stamp *temp = 0;		/* points to new stamp */
	struct perforation *new_perf = 0;	/* points to new
perforation */
	struct stamp *current = 0;	/* points to a current
stamp */
	int parity = latest->value % 2;

	/* variables used to adjust the perforation (see below) */
	struct stamp *temp_stamp = 0;
	struct perforation *temp_perf = 0;
	int temp_value;


	if (latest->value == N)
		{
		DisplayStamp();
		return;
		}

	/* make new stamp */
	temp = (struct stamp *) malloc(sizeof(struct stamp));
	temp->value = latest->value + 1;
	temp->prev = 0;
	temp->next = 0;
	temp->perf[0] = 0;
	temp->perf[1] = 0;

	/* make new perforation */
	new_perf = (struct perforation *) malloc(sizeof(struct perforation));
	new_perf->right_value = temp->value;
	new_perf->left_value = 0;

	/* connect stamp to perforation */
	temp->perf[(parity + 1) % 2] = new_perf;
	temp->perf[parity] = 0;
	new_perf->right_link = temp;
	new_perf->left_link = 0;

	/* connect new stamp to peforation of 'latest' */
	(latest->perf[parity])->left_link = temp;
	(latest->perf[parity])->left_value = temp->value;
	temp->perf[parity] = latest->perf[parity];

	/* go left */
	current = latest;
	do
		{
		AddStamp(current, temp);	
		GenStamp(temp);
		RemoveStamp(temp);
		current = current->prev;

		if (current == latest) break;

		if (current->perf[parity] != 0)
		   {
		   if ( ((current->perf[parity])->right_value == 
current->value))
		      {
		      current = (current->perf[parity])->left_link;
		      if (current->value == 0) /* adjust per if necessary
*/
		         {
			 temp_perf = latest->perf[parity];
			 /* swap values */
			 temp_value = temp_perf->left_value;
			 temp_perf->left_value = temp_perf->right_value;
			 temp_perf->right_value = temp_value;
			 /* swap links */
		 	 temp_stamp = temp_perf->left_link;
			 temp_perf->left_link = temp_perf->right_link;
			 temp_perf->right_link = temp_stamp;
			 }
		      }
		   else
		      {
		      current = (current->perf[parity])->right_link;
		      /* adjust the perforation */
		      temp_perf = latest->perf[parity];
		      temp_value = temp_perf->left_value;
		      temp_perf->left_value = temp_perf->right_value;
		      temp_perf->right_value = temp_value;
		      temp_stamp = temp_perf->left_link;
		      temp_perf->left_link = temp_perf->right_link;
		      temp_perf->right_link = temp_stamp;
		      }
		   }
		}
	while (current != latest);


	/* remove the extra stamp and extra perforation */
	temp_perf = temp->perf[parity];
	if (temp_perf->left_value == temp->value)
		{
		temp_perf->left_value = 0;
		temp_perf->left_link = 0;
		}
	else
		{
		temp_perf->right_value = temp_perf->left_value;
		temp_perf->right_link = temp_perf->left_link;
		temp_perf->left_value = 0;
		temp_perf->left_link = 0;
		}
	free (temp);
	free (new_perf);
	}


void AddStamp(struct stamp *current, struct stamp *new)
	{
	struct stamp *temp = 0;

	temp = current;
	temp = temp->prev;
	new->next = temp->next;
	new->prev = temp;
	(temp->next)->prev = new;
	temp->next = new;
	}

void RemoveStamp(struct stamp *new)
	{
	struct stamp *before;
	struct stamp *after;

	before = new->prev;
	after = new->next;
	before->next = after;
	after->prev = before;
	new->next = 0;
	new->prev = 0;
	}

void DisplayStamp()
	{
	struct stamp *temp;
	int position = 0;		/* position of the 1 stamp */
	int i;
	int a, b, c, d;

	temp = first->next;
	a = temp->value;
	temp = temp->next;
	b = temp->value;
	temp = first->prev;
	d = temp->value;
	temp = temp->prev;
	c = temp->value;
	if (a > d)
		return;
	if ( (a > NN + 1 - a) || ((a == NN + 1 - a) && (b > NN + 1 - b)) )
		return;
	if ( (a > NN + 1 - d) || ((a == NN + 1 - d) && (b > NN + 1 - c)) )
		return;

	/* check for canonical representative */
	temp = first->next;
	while (temp != first)
		{
		stamp_array[position] = temp->value;
		second_array[position] = temp->value;
		temp = temp->next;
		position++;
		}

	relabel();
	if (!canonical())
		return;
	reverse();
	if (!canonical())
		return;
	relabel();
	if (!canonical())
		return;


	Pi[0] = N + 1;
	iP[0] = N + 1;
	temp = first->next;
	for (i = 1; (i <= N) && (temp != first); i++)
		{
		Pi[i] = temp->value;
		iP[temp->value] = i;
		temp = temp->next;
		}

	PrintIt();
	}


void relabel()
	{
	int i;

	for (i = 0; i < N; i++)
		{
		second_array[i] = N + 1 - second_array[i];
		}
	}

int canonical()
	{
	int position = 0;

	while ((stamp_array[position] == second_array[position]) &&
		(position < N))
		{
		position++;
		}

	if (position == N)
		return 1;

	if (stamp_array[position] < second_array[position])
		return 1;

	return 0;
	}

void reverse()
	{
	int i;
	int temp;

	for (i = 0; i < N/2; i++)
		{
		temp = second_array[i];
		second_array[i] = second_array[N - 1 - i];
		second_array[N - 1 - i] = temp;
		}
	}




/*  from  perm.c
	   Provide routines for I/O of permutations

           Added KoDeZ to add commas if N > 10 
*/


void PrintIt() {
    int i;
    
    ++count;
    if(count > LIMIT) exit (-1); /* Do this if we exceed max allowable */

    printf("<TR>");
    if (out_format & 1)
	{   printf("<TD ALIGN=CENTER>");
	    for(i=1; i<NN; i++) printf("%d, ",Pi[i]); /* one line */
	    printf("%d",Pi[NN]);
            printf("<BR></TD>");
	}

    if (out_format & 16)
	{   printf("<TD ALIGN=CENTER>");
	    for(i=1; i<=NN; i++) iP[Pi[i]] = i;
	    for(i=1; i<NN; i++) printf("%d, ",iP[i]); /* one line */
	    printf("%d",iP[NN]);
	    printf("<BR></TD>");
	}

    if (out_format & 2) {
	printf("<TD ALIGN=CENTER>");
	PrintCycle();
	printf("<BR></TD>");
    }

    if (out_format & 8) {
	printf("<TD ALIGN=CENTER>");
	PrintBoard(Pi, NN);
	printf("<BR></TD>");
    }

   if(out_format & 64) {
	printf("<TD ALIGN=CENTER>\n");
	printf("<TABLE BORDER=1 CELLPADDING=8 CELLSPACING=1>\n");
	printf("<TR>\n");
	printf("<TD ALIGN=CENTER>\n");
	tableaux(NN);
	printf("</TD>\n");
	printf("<TD ALIGN=CENTER>\n");
	tableaux2(NN);
	printf("</TD></TR></TABLE>\n");
	printf("</TD>\n");
    }
    if (out_format & 128) {
	printf("\n");
	gdStampFolding(first, NN, 0);
    }
    printf("</TR>");

}

void PrintCycle(void) {
    int j,k;
    int a[NN];
    for (k=0; k<=NN;k++) a[k] = 1;

    k=1;

    while( k <= NN) {
	printf("(");
	j=k;
	do {
	    a[j] = 0;
            if (Pi[j] != k) {
	       printf("%d, ",j);
	    } else {
               printf("%d",j);
            }
	    j = Pi[j];
	} while (j != k);
	printf(")");
	while ((!a[k]) && (k<=NN)) k++;
    }
}
	
    

/*
 * PermMatrix
 * ----------
 * 
 * This little set of routines take in a permuation array,
 * A[0]...A[n-1], and display a chessboard corresponding to that
 * permutation.  GIFS are stolen from some chess site.
 *
 */


/*
 * Here are all the GIFs that will used in the generation of this
 * here wonderful chessbored.
 */

const char *blackSquare = "<IMG SRC =ico/red.gif>";
const char *whiteSquare = "<IMG SRC =ico/green.gif>";
const char *blackRook = "<IMG SRC =ico/redRook.gif>";
const char *whiteRook = "<IMG SRC =ico/greenRook.gif>";

/* Here are some nice macros I use.  May be defined somewhere else; I dunno */

#define even(x) !odd(x)

void PrintBoard(int Perm[], int n) {
    int row, col;

    printf("<TABLE BORDER=1 CELLPADDING=0 CELLSPACING=0>\n");
    for(row = 1; row <= n; row++) {
	printf("<TR>\n");
	for(col = 1; col <= n; col++) {
	    printf("<TD>");
	    if (Perm[row] == col) {
		if(odd(row+col))
		    printf(blackRook);
		else
		    printf(whiteRook);
	    } else {
		if(odd(row+col))
                    printf(blackSquare);
                else
                    printf(whiteSquare);
	    }
	    printf("</TD>");
	}
	printf("</TR>\n");
    }
    printf("</TABLE>\n");
}
	
	    
		


#define INFINITY 999

int Tab[MAX][MAX];


void init_tab( int n ) 
{
    int i, j;

    for (i=0; i< n; i++) 
        for(j=0; j< n; j++) Tab[i][j] = INFINITY;

    for (i=0; i< n; i++) 
	Tab[0][i] = 0;

    for (i=0; i< n; i++)
	Tab[i][0] = 0;

}

void tableaux( int n ) {
int i,j;

init_tab(MAX);
for (i=1; i<=n; i++) insert_tab(Pi[i]);

printf("<TABLE BORDER=1 CELLPADDING=8 CELLSPACING=1>\n");

for (i=1; Tab[i][1]!= INFINITY && i<=n; i++) {
    printf("<TR>");
    for (j=1; Tab[i][j] != INFINITY && j <= n; j++)
      printf("<TD>%d</TD>",Tab[i][j]);
    printf("</TR>\n");
}

printf("</TABLE>");

}

void tableaux2( int n ) {
int i,j;

init_tab(MAX);
for(i=1; i<=NN; i++) iP[Pi[i]] = i;
for (i=1; i<=n; i++) insert_tab(iP[i]);

printf("<TABLE BORDER=1 CELLPADDING=8 CELLSPACING=1>\n");

for (i=1; Tab[i][1]!= INFINITY && i<=n; i++) {
    printf("<TR>");
    for (j=1; Tab[i][j] != INFINITY && j <= n; j++)
      printf("<TD>%d</TD>",Tab[i][j]);
    printf("</TR>\n");
}

printf("</TABLE>");

}

void insert_tab(int ex) 
{
   int i = 0; 
   int j;
   int x[MAX];

   x[1] = ex;  

   for (j = 1; j< MAX; j++)    
       if (Tab[1][j] == INFINITY) 
	   break;
	   	 
 do {
      i++;
      while (x[i] < Tab[i][j-1]) j--; 
      x[i+1] = Tab[i][j];             
      Tab[i][j] = x[i];               
  } while (x[i+1] != INFINITY);
  
}

