//
// This program generates permutations with a given left-right
// maxima.  The algorithm is essentially an implementation of
// the Stirling-1 recurrence relation.
//
// 

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <ctype.h>
#include "ProcessInput.c"
#include "commonio.h"


void swap(int, int);
void generate(int, int);

int NN, KK;
int main(int argc, char **argv) {
    int i;

    ProcessInput(argc, argv);
    NN = N; KK = K;
    for(i=0;i<NN;i++) Pi[i] = i;

    calls = count = 0;
    generate(NN-1,KK);
    printf("</TABLE>");
    printf("<p><b>Permutations : %d</b></p>",count);
}

void swap(int i, int j) {
    int temp;
    temp  = Pi[i]; Pi[i] = Pi[j]; Pi[j] = temp;
}


void generate(int n, int k) {
    int i;
    calls++;
    if (n < 0) {
	    PrintIt(); 
    }
    else {
	if (k>0) generate(n-1, k-1);
	if(n>=k)
	    for(i=n+1; i < NN; i++) {
		swap(n, i);
		generate(n-1,k);
		swap(n, i);
	    }
    }
}

	

#include "commonio.cc"
