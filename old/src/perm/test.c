/* Johnson-Trotter Algorithm */

#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <ctype.h>
#include "ProcessInput.c"
#define TRANSP
#include "commonio.h"

void p(int, int);
void l(int, int);
void swap(int, int);
void PrintBoard(int perm[], int n);
void init_tab( int );
void insert_tab( int );
void tableaux( int );
void tableaux2 ( int );


#define TRUE	1
#define FALSE	0
#define odd(x) ( (x)%2==1 ? TRUE : FALSE ) 
#define LIMIT_ERROR  -1

int dir[100];
int NN;

void Move(int x, int d)
{ int j;
  PrintIt(iP[x], iP[x]+d);
  j=iP[x]; 
  Pi[j]=Pi[j+d]; 
  Pi[j+d]=x;
  iP[x]=j+d; 
  iP[Pi[j]]=j;
}

void Perm ( int n )
{ int i;
  if (n<=N) {
     Perm( n+1 );
     for (i=1; i<n; i++) {
        Move(n, dir[n]);
        Perm( n+1 );
     }
     dir[n] = (-1)*dir[n];
  } 
}

int main (int argc, char *argv[])
{ int i;

   ProcessInput(argc, argv);
   count = 0;
   if (N != 0) {	
      for (i=1; i<=N; i++) {
         Pi[i] = i;  iP[i] = i;  dir[i] = -1;
      }
      NN = N;
      Pi[0] = N+1;
      iP[0] = N+1;
      count = 0;
      Perm(1);
      PrintIt(0,0);
      printf("</TABLE><P>Permutations = %d",count);
   } else printf("Sorry.  N equals zero.  Goodbye\n");
   exit( 0 );
}


void PrintIt(int x, int y) {
    int i;
    
    ++count;
    if(count > LIMIT) exit (-1); // Do this if we exceed max allowable
  for(i = 1; i<NN; i++) printf("%d, ",Pi[i]);
	printf("%d", Pi[NN]);
	printf("\n");
/*    printf("<TR>");
    if (out_format & 1)
	{   printf("<TD ALIGN=CENTER>");
	    for(i=1; i<NN; i++) printf("%d, ",Pi[i]); // one line
	    printf("%d",Pi[NN]);
            printf("<BR></TD>");
	}

    if (out_format & 16)
	{   printf("<TD ALIGN=CENTER>");
	    for(i=1; i<=NN; i++) iP[Pi[i]] = i;
	    for(i=1; i<NN; i++) printf("%d, ",iP[i]); // one line
	    printf("%d",iP[NN]);
	    printf("<BR></TD>");
	}

    if (out_format & 2) {
	printf("<TD ALIGN=CENTER>");
	PrintCycle();
	printf("<BR></TD>");
    }

    if (out_format & 8) {
	printf("<TD ALIGN=CENTER>");
	PrintBoard(Pi, NN);
	printf("<BR></TD>");
    }

   if(out_format & 64) {
	printf("<TD ALIGN=CENTER>\n");
	printf("<TABLE BORDER=1 CELLPADDING=8 CELLSPACING=1>\n");
	printf("<TR>\n");
	printf("<TD ALIGN=CENTER>\n");
	tableaux(NN);
	printf("</TD>\n");
	printf("<TD ALIGN=CENTER>\n");
	tableaux2(NN);
	printf("</TD></TR></TABLE>\n");
	printf("</TD>\n");
    }

     if (out_format & 4) {
        if (x != 0) printf("  <TD ALIGN=CENTER>(% %1d)</TD>", x, y);
        else printf("<TD ALIGN=CENTER>.</TD>\n");
     }

    printf("</TR>");

*/}

void PrintCycle(void) {
    int j,k;
    int a[NN];
    for (k=0; k<=NN;k++) a[k] = 1;

    k=1;

    while( k <= NN) {
	printf("(");
	j=k;
	do {
	    a[j] = 0;
            if (Pi[j] != k) {
	       printf("%d, ",j);
	    } else {
               printf("%d",j);
            }
	    j = Pi[j];
	} while (j != k);
	printf(")");
	while ((!a[k]) && (k<=NN)) k++;
    }
}
	
    

/*
 * PermMatrix
 * ----------
 * 
 * This little set of routines take in a permuation array,
 * A[0]...A[n-1], and display a chessboard corresponding to that
 * permutation.  GIFS are stolen from some chess site.
 *
 */


/*
 * Here are all the GIFs that will used in the generation of this
 * here wonderful chessbored.
 */

const char *blackSquare = "<IMG SRC =ico/red.gif>";
const char *whiteSquare = "<IMG SRC =ico/green.gif>";
const char *blackRook = "<IMG SRC =ico/redRook.gif>";
const char *whiteRook = "<IMG SRC =ico/greenRook.gif>";

/* Here are some nice macros I use.  May be defined somewhere else; I dunno */

#define even(x) !odd(x)

void PrintBoard(int Perm[], int n) {
    int row, col;

    printf("<TABLE BORDER=1 CELLPADDING=0 CELLSPACING=0>\n");
    for(row = 1; row <= n; row++) {
	printf("<TR>\n");
	for(col = 1; col <= n; col++) {
	    printf("<TD>");
	    if (Perm[row] == col) {
		if(odd(row+col))
		    printf(blackRook);
		else
		    printf(whiteRook);
	    } else {
		if(odd(row+col))
                    printf(blackSquare);
                else
                    printf(whiteSquare);
	    }
	    printf("</TD>");
	}
	printf("</TR>\n");
    }
    printf("</TABLE>\n");
}
	
	    
		


#define INFINITY 999

int Tab[MAX][MAX];


void init_tab( int n ) 
{
    int i, j;

    for (i=0; i< n; i++) 
        for(j=0; j< n; j++) Tab[i][j] = INFINITY;

    for (i=0; i< n; i++) 
	Tab[0][i] = 0;

    for (i=0; i< n; i++)
	Tab[i][0] = 0;

}

void tableaux( int n ) {
int i,j;

init_tab(MAX);
for (i=1; i<=n; i++) insert_tab(Pi[i]);

printf("<TABLE BORDER=1 CELLPADDING=8 CELLSPACING=1>\n");

for (i=1; Tab[i][1]!= INFINITY && i<=n; i++) {
    printf("<TR>");
    for (j=1; Tab[i][j] != INFINITY && j <= n; j++)
      printf("<TD>%d</TD>",Tab[i][j]);
    printf("</TR>\n");
}

printf("</TABLE>");

}

void tableaux2( int n ) {
int i,j;

init_tab(MAX);
for(i=1; i<=NN; i++) iP[Pi[i]] = i;
for (i=1; i<=n; i++) insert_tab(iP[i]);

printf("<TABLE BORDER=1 CELLPADDING=8 CELLSPACING=1>\n");

for (i=1; Tab[i][1]!= INFINITY && i<=n; i++) {
    printf("<TR>");
    for (j=1; Tab[i][j] != INFINITY && j <= n; j++)
      printf("<TD>%d</TD>",Tab[i][j]);
    printf("</TR>\n");
}

printf("</TABLE>");

}

void insert_tab(int ex) 
{
   int i = 0; 
   int j;
   int x[MAX];

   x[1] = ex;  

   for (j = 1; j< MAX; j++)    
       if (Tab[1][j] == INFINITY) 
	   break;
	   	 
 do {
      i++;
      while (x[i] < Tab[i][j-1]) j--; 
      x[i+1] = Tab[i][j];             
      Tab[i][j] = x[i];               
  } while (x[i+1] != INFINITY);
  
}

