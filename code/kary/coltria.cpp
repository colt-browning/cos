/*
 * Copyright (c) 2024 Rohan Acharya, Torsten Muetze, and Francesco Verciani
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cassert>
#include <iostream>
#include <vector>
#include "brgc.hpp"
#include "brgc3.hpp"
#include "coltria.hpp"

Coltria::Coltria(int n) :
  n_(n),
  even_(n % 2 == 0),
  q_fake_(even_ ? 0 : 1),
  q_((n-2-q_fake_)/2),  // number of points is n=2q+2 or n=2q+3
  tree_(q_+q_fake_, 3, !even_),  // for an odd number of points we need the special trees
  tree_ahead_(q_+q_fake_, 3, !even_)
{
  perm_ = std::vector<int> (q_+q_fake_+1,0);
  mono_edges_ = std::vector<int> (q_+q_fake_+1,0);
  quad_types_ = std::vector<int> (q_+q_fake_+1,0);
  for (int i=1; i<=q_; i++) {
    quad_types_[i] = ((i-1) % 2);  // the initial tree has alternating quadrangle types
  }
  if (!even_) {
    quad_types_[q_+1] = 0;  // the fake quadrangle
    mono_edges_[q_+1] = 1;
  }
  first_next_ = true;
  last_next_  = false;
}

bool Coltria::next()
{
  // prepare next flip of colorful edge (i.e., prepare jump to next hypercube)
  if (first_next_ || (count_mono_flips_ == (1<<q_)-1)) {
    if (last_next_) return false;

    if (!first_next_) {
      int v, dir;
      // flip colorful edge
      tree_.next(v, dir);  // those return values aren't used
      delete brgc_;
    }

    int v, dir;
    bool next;
    next = tree_ahead_.next(v, dir);  // this tree is always one rotation ahead
    if (!next) {
      last_next_ = true;
    }

    // use the information about rotated vertex to compute prescribed bits and update quadrangle types
    int p1, p2, v1, v2;  // positions and values of two bits whose values are prescribed
    if (next) {
      if (dir > 0) {  // up-rotation
        int p = tree_.parent(v);
        assert((p >= 1) && (p <= q_));
        int a = tree_.child_index(p, v);
        assert((a == 1) || (a == 2));
        assert(((a == 1) && ((dir == +1) || (dir == +2))) || ((a == 2) && (dir == +1)));
        p1 = p;
        p2 = v;
        if ((a == 1) && (dir == +1)) {
          v1 = 1 - quad_types_[p];
          v2 = 1 - quad_types_[v];
          quad_types_[v] = 1 - quad_types_[v];
        } else if ((a == 2) && (dir == +1)) {
          v1 = quad_types_[p];
          v2 = 1 - quad_types_[v];
        } else if ((a == 1) && (dir == +2)) {
          v1 = quad_types_[p];
          v2 = quad_types_[v];
          quad_types_[v] = 1 - quad_types_[v];
        }
      } else {  // down-rotation
        int c = tree_.child(v, 0);
        int p = tree_.parent(v);
        assert((c > 0) || ((c == 0) && (p >= 1) && (p <= q_) && (tree_.child_index(p,v) == 2)));
        assert(((c > 0) && ((dir == -1) || (dir == -2))) || ((c == 0) && (dir == -1)));
        p1 = (c > 0) ? c : p;
        p2 = v;
        if ((c > 0) && (dir == -1)) {
          v1 = quad_types_[c];
          v2 = 1 - quad_types_[v];
        } else if ((c == 0) && (dir == -1)) {
          v1 = 1 - quad_types_[p];
          v2 = quad_types_[v];
          quad_types_[v] = 1 - quad_types_[v];
        } else if ((c > 0) && (dir == -2)) {
          v1 = 1 - quad_types_[c];
          v2 = quad_types_[v];
          quad_types_[v] = 1 - quad_types_[v];
        }
      }
    } else {
      // choose them arbitrarily
      p1 = 1;
      p2 = 2;
      v1 = 1 - mono_edges_[p1];
      v2 = mono_edges_[p2];
    }
    assert(p1 < p2);

    int dist = 0;  // Hamming distance between end vertices in small hypercube
    int p3;  // position of bit to fix parity between end vertices of Hamilton path
    // permute positions so that 1s are followed by 0s
    // in first 3 cases we go from 000 to 100
    // and in the last case we go from 000 to 111
    if ((mono_edges_[p1] == v1) && (mono_edges_[p2] == v2)) {
      dist = 1;
      p1 = first_new(p1, p2);
    } else if ((mono_edges_[p1] != v1) && (mono_edges_[p2] == v2)) {
      dist = 1;
    } else if ((mono_edges_[p1] == v1) && (mono_edges_[p2] != v2)) {
      dist = 1;
      p1 = p2;
    } else {
      dist = 3;
      p3 = first_new(p1, p2);      
    }
    assert((dist == 1) || (dist == 3));

    // set permutation
    int i;
    if (dist == 1) {
      perm_[1] = p1;
      i = 2;
    } else {
      perm_[1] = p1;
      perm_[2] = p2;
      perm_[3] = p3;
      i = 4;
    }
    for (int j=1; j<=q_+q_fake_; j++) {
      if (((dist == 1) && (j == p1)) || ((dist == 3) && ((j == p1) || (j == p2) || (j == p3)))) continue;
      perm_[i] = j;
      i++;
    }

    if (dist == 1) {
      brgc_ = new Brgc(q_);  // this Gray code moves from 0000...0 to 1000...0
    } else {
      assert(dist == 3);
      brgc_ = new Brgc3(q_);  // this Gray code moves from 0000...0 to 1110...0
    }

    count_mono_flips_ = 0;

    if (!first_next_) return true;
  }

  // flip one monochromatic edge (i.e., move inside hypercube)
  int p;
  brgc_->next(p);
  int q = perm_[p+1];
  mono_edges_[q] = 1 - mono_edges_[q];
  count_mono_flips_++;

  if (first_next_) {
    first_next_ = false;
  }
  return true;
}

int Coltria::first_new(int a, int b)
{
  int p = (a < b) ? a : b;
  int q = (a < b) ? b : a;
  assert((p >= 1) && (q > p)); 
  if (p > 1) {
    return 1;
  } else if (q > 2) {
    return 2;
  } else {
    return 3;
  }
}
  
void Coltria::print()
{
  tree_.print(false);
  std::cout << "| ";
  for (int i=1; i<=q_+q_fake_; i++) {
     std::cout << mono_edges_[i];
  }
  std::cout << std::endl;
}