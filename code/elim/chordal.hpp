/*
 * Copyright (c) 2021 Jean Cardinal, Arturo Merino and Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>
#include <utility>
#include <vector>

#ifdef NDEBUG
#define DEBUG 0
#else
#define DEBUG 1
#endif

class Elimination_forest {
friend class Tests;

  enum class Direction {up, down, none};

public:
  // last argument of the constructor is set to true if given edge list represents a chordal graph, and to false otherwise
  Elimination_forest(int, std::vector<std::string>&, std::vector<std::pair<int,int>>&, bool&);
  bool next();
  void print();  // print compact representation of elimination forest
  void print_peo();  // print perfect elimination ordering

private:
  int n_;  // number of vertices of the underlying chordal graph and the elimination forest
  std::vector<std::string> vertex_strings_;  // string representations of vertices

  // the underlying chordal graph
  std::vector<std::vector<int>> adjL_;  // adjacency lists
  std::vector<std::vector<int>> adjM_;  // adjacency matrix
  std::vector<int> peo_;  // perfect elimination order
  std::vector<int> oep_;  // inverse permutation
  std::vector<std::vector<int>> earlier_neighbors_;  // for each vertex i, all earlier neighbors of i in the perfect elimination order
  std::vector<std::vector<int>> earlier_neighbors_pos_;  // for each vertex i and every other vertex j, the position of j in the list of earlier neigbors of i, or -1 if j is not an earlier neighbor of i
  std::vector<int> max_earlier_neighbor_;  // the largest earlier neighbor of each vertex in the perfect elimination order

  // the elimination forest
  std::vector<bool> is_root_;  // boolean array that tells whether a vertex is currently root of an elimination tree
  std::vector<int> first_child_;  // the first and last child of each vertex (in no particular order)
  std::vector<int> last_child_;
  std::vector<int> smaller_child_;  // if a vertex has a child that is smaller than itself, then we track it
  std::vector<int> parent_;  // the parent of a vertex in the elimination tree
  std::vector<int> left_sibling_;  // left and right siblings of a vertex
  std::vector<int> right_sibling_;
  std::vector<std::vector<int>> ins_path_;  // each vertex maintains its insertion path into its elimination tree
  std::vector<int> ins_pos_;  // the current position of each vertex on its insertion path

  // bookkeeping for Algorithm J
  int rho_;  // largest rotatable vertex
  std::vector<int> alpha_;  // alpha_[j] tells us the next smaller rotatable vertex
  std::vector<Direction> o_;  // direction array
  std::vector<int> s_;  // rotation vertex selection

  void rotate(int, Direction);  // tree rotation in an elimination tree

  // read in edge list and fill adjacency lists and adjacency matrix
  void build_graph(std::vector<std::pair<int,int>>&);
  // run lexicographic BFS in the graph, recording the ordering of vertices and the corresponding inverse permutation,
  // which is a perfect elimination ordering in the case that the graph is chordal
  void lex_bfs();
  // auxiliary function for the lexicographic BFS
  void split_intervals(int, std::vector<int>&, int, std::vector<int>&);
  // initialize information about earlier neighbors in the perfect elimination order
  void init_earlier_neighbors();
  // check if graph is chordal based on lexicographic BFS ordering
  bool is_chordal();

  // initialization of the elimination forest according to the identity permutation
  void init_elimination_forest();
  // elimination forest recursion making i the root of the current subtree, recording the already removed vertices
  void init_elimination_forest_rec(int i, std::vector<bool> &removed);
  // recursively run DFS from the given starting vertex i, ignoring already removed
  // vertices, recording all vertices reached in this component
  void dfs(int i, std::vector<bool> &removed, std::vector<bool> &reached);
  // Compute the insertion path (from root to leaf) of vertex i in the current elimination tree.
  // The insertion path contains all earlier neighbors of i in the perfect elimination order, plus
  // possibly additional vertices with smaller number.
  void insertion_path(int i, std::vector<int> &path);

  // initialization of the bookkeeping data structures alpha_, o_ and s_
  void init_bookkeeping();

  // insert vertex i as the last child of j in an elimination tree
  void insert_vertex(int i, int j);
  // delete i from the elimination tree
  void delete_vertex(int i);
  // find minimum child (this is not a constant-time operation, and it is only used for debugging)
  int min_child(int i);

  void print_rec(int, bool&);
  void check_consistency();
  void check_consistency_rec(int,int&);
};
