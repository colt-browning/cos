(* The encoding:
  0   8  16  24  32  40  48  56  64  72  80  88  96 104 112 120  
  1   9  17  25  33  41  49  57  65  73  81  89  97
  2  10  18  26  34  42  50
  3  11  19  27  35  43  51
  4  12  20  28  36  44  52
  5  13  21  29  37  45  53
  6  14  22  30  38  46  54
  7  15  23  31  39  47  55
*)

(**************************************************************)
(*                                                            *)
(*   A program to solve ROW by COL pentominoe puzzles.        *)
(*                                                            *)
(**************************************************************)

program  PostitionListGenerator (Input, Output );

const
  
  NUM_PIECES = 12;
  MAX_RC     = 20;  {max allowed row or column size}
  xMAX_RC    = 17;  

  (* INPUT_NAME = 'theory/www/bin/BigPent.temp'; *)
  INPUT_NAME = 'theory/www/bin/misc/BigPent.temp'; 
  (* DATA_NAME = 'theory/www/bin/610PDATA.TEXT'; *)
  DATA_NAME = 'theory/www/bin/misc/610PDATA.TEXT'; 
  RxC       = 143;
 
type
  PieceNumber = 1..NUM_PIECES;
  BoardNumber = 0..RxC;
  Board       = set of BoardNumber;
  ListPtr     = ^ListElement;
  ListElement = record
                  Position : Board;
                  Link     : ListPtr
                end;

var
  ROW : 3..MAX_RC;   {number of rows}
  COL : 3..MAX_RC;   {number of cols}
  xROW, xCOL : integer;  {one more than ROW and COL}
  LIMIT : integer;   {max solutions output}
  minX, maxX : integer;
  minY, maxY : integer;
  PentData  : text;  {contains the initial piece descriptions}
  PentInput : text;  {contains the initial value of ROW, COL, LIMIT}
  List     : array [PieceNumber,BoardNumber] of ListPtr;
  Piece    : array [PieceNumber,1..5] of 
               record  X,Y : -MAX_RC..MAX_RC;
               end;
  n, num, count  : integer;
  j        : 0..6;
  pc       : PieceNumber;
  ii, i, bd    : BoardNumber;
  p        : ListPtr;
  ch       : char;
  PieceAvail : set of PieceNumber;
  Solution : array [PieceNumber] of ListPtr;
  TheBoard : Board;
  WriteOutLists : boolean;
  A        : array [0..xMAX_RC,0..xMAX_RC] of integer;
  TrialPiece : Board;
  r,c, Min_Square : integer;

procedure GeneratePiecePositions;

  procedure Translate ( pc : PieceNumber );
  var
    i,j    : -MAX_RC..MAX_RC;
    xt, yt : integer;
    Part : array [1..5] of BoardNumber;
    ptr  : ListPtr;
    sq   : 1..5;
    min  : BoardNumber;
  begin
    minX :=  9999;  minY :=  9999;
    maxX := -9999;  maxY := -9999;
    for j := 1 to 5 do begin
      if Piece[pc,j].X < minX then minX := Piece[pc,j].X;
      if Piece[pc,j].Y < minY then minY := Piece[pc,j].Y;
      if Piece[pc,j].X > maxX then maxX := Piece[pc,j].X;
      if Piece[pc,j].Y > maxY then maxY := Piece[pc,j].Y;
    end;
    for i :=  - minX  to  COL - maxX - 1  do begin
      for j :=  - minY  to  ROW - maxY - 1  do begin
        for sq := 1 to 5 do begin
          xt := i + Piece[pc,sq].X;
          yt := j + Piece[pc,sq].Y;
          Part[sq] := yt + xt*ROW;
          if (Part[sq] > RxC) or (Part[sq] < 0) then begin
            (* DEBUG *)
            write( 'oops' );
          end;
        end;
        TrialPiece := [Part[1],Part[2],Part[3],Part[4],Part[5]];
        if TrialPiece * TheBoard = [] then begin 
          min := Part[1];
          for sq := 2 to 5 do if Part[sq] < min then min := Part[sq];
          new( ptr );
          num := num + 1;
          with ptr^ do begin
            Position := TrialPiece;
            Link := List[pc,min];
            List[pc,min] := ptr
          end;
        end;
      end;
    end;
  end {of Translate};

  procedure Rotate ( pc : PieceNumber );
  var  j : 0..6;  temp : -MAX_RC..MAX_RC;
  begin
    for j := 1 to 5 do begin
      temp          := Piece[pc,j].X;
      Piece[pc,j].X := Piece[pc,j].Y;
      Piece[pc,j].Y := -temp;
    end;
  end {of Rotate};
  
  procedure Flip ( pc : PieceNumber );
  var  j : 0..6;
  begin
    for j := 1 to 5 do Piece[pc,j].X := -Piece[pc,j].X;
  end {of Flip};
  
  procedure WriteOut ( pc : PieceNumber );
  var bd : integer;
  begin
    n := 0;
    if WriteOutLists then writeln( 'pc = ',pc );
    for bd := 0 to RxC do begin
      if WriteOutLists then writeln( '  bd = ',bd );
      p := List[pc,bd];
      while p <> nil do begin
        if WriteOutLists then begin
          for ii := 0 to RxC do
            if ii in p^.Position then write( ii:4 );
          writeln;
        end;
        n := n + 1;
        p := p^.Link
      end;
    end;
    if WriteOutLists then writeln( 'piece =',pc:3,'      n =',n:4 )
  end {of WriteOut};
  
begin {GeneratePiecePositions}
  var pc : integer;
  for pc := 1 to NUM_PIECES do
     for bd := 0 to RxC do List[pc,bd] := nil;
  Translate(1);
  WriteOut(1);
  for pc := 2 to 12 do begin
    for bd := 0 to RxC do List[pc,bd] := nil;
    Translate(pc);  Rotate(pc);
    Translate(pc);  Rotate(pc);
    if not (pc in [2,4]) then begin
      Translate(pc);  Rotate(pc);
      Translate(pc);  Rotate(pc);
    end;
    if pc in [3,4,8,9,11,12] then begin
      Flip(pc);
      Translate(pc);  Rotate(pc);
      Translate(pc);  Rotate(pc);
      if not (pc in [2,4]) then begin
        Translate(pc);  Rotate(pc);
        Translate(pc);  Rotate(pc)
      end;
      Flip(pc)
    end;
    WriteOut(pc);
  end
end {of GeneratePiecePostions};

#include "backtrack.p"

function Isolated : boolean;
 
  label 4444;
 
  var
    zeroes : 0..60;
    i,j    : 1..MAX_RC;
 
  procedure search ( x, y : integer );
  begin
    zeroes := zeroes + 1;
    A[x,y] := 9999;
    if A[x+1,y] = 0 then search(x+1,y);
    if A[x-1,y] = 0 then search(x-1,y);
    if A[x,y+1] = 0 then search(x,y+1);
    if A[x,y-1] = 0 then search(x,y-1);
  end {of search};
 
begin {Isolated}
  Isolated := TRUE;
  for i := 1 to ROW do
    for j := 1 to COL do begin
      zeroes := 0;
      if A[i,j] = 0 then search(i,j);
      if (zeroes mod 5) <> 0 then goto 4444;
    end;
  Isolated := FALSE;
  4444:
  for i := 1 to ROW do
    for j := 1 to COL do
      if A[i,j] = 9999 then A[i,j] := 0
end {of Isolated};
 
procedure PurgePositionLists;
var p, ptr : ListPtr;  k,pc,bd : integer;
begin
if WriteOutLists then writeln('Starting to purge');
for k := 0 to xROW do begin
   A[k,xCOL] := -1;  A[k,0] := -1
end;
for k := 1 to COL do begin
   A[0,k] := -1;  A[xROW,k] := -1
end;
for pc := 1 to 12 do begin
  if WriteOutLists then writeln( pc:3 ); 
  for bd := 0 to RxC do begin
     p := List[pc,bd];  List[pc,bd] := nil;
     while p <> nil do begin
        for k := 0 to RxC do
           if k in p^.Position then begin
              A[k mod ROW + 1, k div ROW + 1] := pc;
           end;
        if not Isolated then begin
           if WriteOutLists then write('O'); 
           ptr := p^.Link;
           p^.Link := List[pc,bd];
           List[pc,bd] := p;
        end else begin
           if WriteOutLists then write('P'); 
           ptr := p^.Link;
        end;
        for k := 0 to RxC do
           if k in p^.Position then begin
              A[k mod ROW + 1, k div ROW + 1] := 0;
           end;
        p := ptr;
     end {while};
     if WriteOutLists then writeln; 
  end {for all bd};
end {for all pc};
end {of PurgePositionLists}; 

begin {--------------------- PositionListGenerator ----------------------} 

  WriteOutLists := false; 
  if WriteOutLists then writeln('reading input');

(*  assign( PentData, INPUT_NAME); *)
  reset( PentData, INPUT_NAME );

  readln( PentData, ROW, COL, LIMIT );
  writeln( ROW, COL, LIMIT );

  if WriteOutLists then writeln( ROW, COL, LIMIT );
  TheBoard := [0..RxC];
  for r := 1 to ROW do begin
    for c := 1 to COL do begin
       read( PentData, A[r,c] ); 
       if WriteOutLists then write( A[r,c]:3 );
       if A[r,c] = 0 then TheBoard := TheBoard - [ROW*(c-1)+(r-1)];
    end; 
    readln( PentData );  if WriteOutLists then writeln;
  end;
  if WriteOutLists then begin
     write( 'TheBoard = ' );
     for i := 0 to RxC do if i in TheBoard then write( i:0,',' );  writeln;
  end;
  Min_Square := 0;
  while not (Min_Square in TheBoard) do Min_Square := Min_Square + 1;
  xROW := ROW + 1;  xCOL := COL + 1;
  if WriteOutLists then writeln('reading data');
(*
  reset( PentData, DATA_NAME );

  for pc := 1 to 12 do
    for j := 1 to 5 do
      read( PentData, Piece[pc,j].X, Piece[pc,j].Y );
  num := 0;
  if WriteOutLists then writeln('generating position lists');
  GeneratePiecePositions;
  if WriteOutLists then writeln('finding solutions');
  FindSolutions;
*)
end. {-------------------- of PositionListGenerator ----------------------}
