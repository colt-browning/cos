/*
 * Copyright (c) 2020 Arturo Merino, Ondrej Micka, Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef SWITCH_HH
#define SWITCH_HH

#include <vector>

class Switch
{
  public:
    std::vector<bool> get_x(void);
    int get_underlined(void);   // the bit in x to flip to reach y in a switch (x,y,y')
    int get_overlined(void);    // the bit in x to flip to reach y' in a switch (x,y,y')
    int get_f(void);            // the bit flipped when applying f; equals -1 if different from underlined and overlined (not needed in those cases)
    int get_finv(void);         // the bit flipped when applying f^{-1}; always well-defined, even if different from underlined and overlined
    int get_eff_shift(void);    // effective shift of a switch
    Switch(int n, int d);  // make one of the default switches used in our construction
    Switch() = default; // make empty bogus Switch object

    // return x with underlined bit flipped (=y)
    std::vector<bool> get_x_underlined();
    // return x with overlined bit flipped (=y')
    std::vector<bool> get_x_overlined();

  private:
    std::vector<bool> x_;
    int underlined_;
    int overlined_;
    int f_;
    int finv_;
    int eff_shift_;

    void init(const std::vector<bool> &x, int over, int under, int f, int finv, int eff_shift);
};

#endif
