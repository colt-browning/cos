/*This is a C version  of our algorithm to generate rooted trees
  In this version, we implement JUMP, and height restrictions
  so that we can generate trees with n nodes and the height in the
  range of l to u, and each node has at most k children.

  Translated in Aug 16, 1995 by Gang LI.
*/

#include <stdio.h>
#include "ProcessInput.c"
#include "OutputTrees.c"

#define MaxSize   50  /* max size of the tree */

int
 n,                             /* number of nodes in a tree */
 par[MaxSize],                  /* parent position of i */
 L[MaxSize],                    /* level of node i      */
 k,                             /* max number of children */
 chi[MaxSize],                  /* number of children of a node */
 nextp[MaxSize],                /* next good pos to add nodes */
 rChi[MaxSize],                 /* the right most child of node i */
 l=0,                           /* lower bound of the height */
 u=MaxSize;                     /* upper bound of the height */


void Gen(p, s, cL) 
int p,s,cL;
{ int entry,temp;

  if (p > n) PrintIt(N,par,L);
  else {
    if (cL==0 && p <= u+2 ) {par[p] = p-1; L[p] = p-1;}
    else  
      if (par[p-cL] < s) {par[p] = par[s];L[p]=L[s];}
         else {par[p] = cL + par[p-cL]; L[p] = 1+L[par[p]];} 
    
    chi[par[p]] = chi[par[p]] + 1; 
    temp = rChi[par[p]]; rChi[par[p]] = p;
    if (chi[par[p]] <= k) {
       if (chi[par[p]] < k) {nextp[p] = par[p];}
       else {nextp[p] = nextp[par[p]];}
       Gen( p+1, s, cL );
    } 
    chi[par[p]] = chi[par[p]] - 1;
    rChi[par[p]] = temp;
    nextp[p] = nextp[par[p]];
    entry = nextp[p];
    while (entry >= 1) {
       par[p]= entry; L[p] = 1+ L[entry];
       chi[entry] = chi[entry] + 1; 
       temp = rChi[par[p]]; rChi[par[p]] = p;
       if (chi[entry] >= k) nextp[p] = nextp[entry];
       Gen( p+1, temp, p-temp );
       chi[entry] = chi[entry] - 1; 
       rChi[par[p]] = temp;
       entry = nextp[entry];
       nextp[p] = entry;
    }
  }
} 


main(argc, argv)
int argc;
char *argv[];
{
  int i = 0;

  /* first set all the parameters */
  ProcessInput(argc, argv);
  if(out_format&1) outputP = 1;
  if(out_format&2) outputL = 1;
  n = N;
  if (M!=0) k = M; else k=N; /* max number of children */
  if (K!=0) l = K;
  if (U!=0) u = U;
  if (u<n-1 && l < u) omit = 1;
  for(i=1;i<=n;i++) chi[i]=0;

  /* now start the program  */
  if (l > n || n > MaxSize-1) printf("Error: Invalid parameter!\n"); else
  /*
  if (k==1) {printf("<I>m</I> should be greater than 1. <P>\n"); exit(0);}
  */
  if (l < u) {                       /* height at least l */
    for(i=1;i<=l+1;i++) {
      par[i]=i-1; L[i]=i-1;nextp[i]=i-1;
      rChi[i]=i+1; chi[par[i]]=1;}
    rChi[l+1]=0;
    Gen(l+2,0,0);
  } else 
    if (l == u) {
    for(i=1;i<=l+1;i++) {
      par[i]=i-1; L[i]=i-1;nextp[i]=i-1;
      rChi[i]=i+1; chi[par[i]]=1;}
    rChi[l+1]=0;
    Gen(l+2,l+1,1);} else
  printf("lower bound should be less than or equal to upper bound!<BR>\n");
  printf("</table><P>The total number of trees is : %4d<BR>\n",num);
  exit(0);
}
