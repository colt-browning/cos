/* main.c - Coordinator */

/* Copyright 1991, Kenny Wong and Frank Ruskey. All Rights Reserved.
 *
 * This program is free software. You may use this software, and copy,
 * redistribute, and/or modify it under the terms of version 2 of the 
 * GNU General Public License. This and all modified works may not be
 * sold for profit. Please do not remove this notice or the original 
 * copyright notices.
 *
 * This software is provided "as is" and without any express or implied
 * warranties, including, without limitation, any implied warranties
 * of merchantability or fitness for a particular purpose. You bear all
 * risk as to the quality and performance of this software.
 *
 * You should have received a copy of the GNU General Public License
 * with this software (see the file COPYING); if not, write to the
 * Free Software Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
 *
 * Please report bugs, fixes, applications, modifications, and requests to:
 *
 * Kenny Wong or Frank Ruskey
 * Department of Computer Science, University of Victoria
 * PO Box 3055, Victoria, BC, Canada, V8W 3P6
 * Internet: kenw@csr.UVic.CA or fruskey@csr.UVic.CA
 *
 * All correspondence should include your name, address, phone number,
 * and preferably an electronic mail address.
 */

#include <stdio.h>

#define GLOBAL 

#include "genle.h"
#include "except.h"

#include "hooks.h"

#include "pr.h"				/* Pruesse & Ruskey */

/* Control ... */

/* called once to initialize everything */
static void init()
{
	/* initialize modules */
	PruesseRuskeyInit();
	/* add module initializers here */

	/* initialize generic hooks */
	uGenInit();
	uPrintInit();
	/* add hook initializers here */
}

void Quit( x )
int x;
{
	/* shut down hooks */
	uGenQuit();
	uPrintQuit();
	/* add hook terminators here */

	/* shut down generators */
	PruesseRuskeyQuit();
	/* add module terminators here */

	exit( x );
}

static FILE *sFileP;			/* the input file */

static void exceptionHandler( x )
int x;
{
	(void)fclose( sFileP );
}

/* prepare for a new poset; returns 0 if okay */
static int prep()
{
	int i, j, k, u, v, w;

	/* load the poset ... */

	if ((sFileP = fopen( gFilename, "r" )) == (FILE *)0) {
	    RaiseSysException( kRESUME, exceptionHandler );
	    return 1;
	}

	if (fscanf( sFileP, "%d", &w ) != 1) {
	    RaiseAppException( xBADFORMAT, kRESUME, exceptionHandler );
	    return 1;
	}

	if (w > kN) {
	    RaiseAppException( xTOOLARGE, kRESUME, exceptionHandler );
	    return 1;
	}
	if (w <= 0) {
	    RaiseAppException( xTOOSMALL, kRESUME, exceptionHandler );
	    return 1;
	}

	gN = w;
	assert( 0 < gN && gN <= kN );
	(void)fprintf( stderr, "#Elements       : %d\n", gN );

	for (i = 1; i <= gN; i++) {
	    for (j = 1; j <= gN; j++) {
		gCov[i][j] = 0;
	    }
	}

	gE = 0;
	if (fscanf( sFileP, "%d %d", &u, &v ) == 2) {
	    while (u > 0 && u <= gN && v > 0 && v <= gN) {
		gE++;
		gCov[u][v] = 1;
		if (fscanf( sFileP, "%d %d", &u, &v ) != 2) break;
	    }
	}
	(void)fprintf( stderr, "#Cover Relations: %d\n", gE );

	(void)fclose( sFileP );
	/* gN, gE, gCov are now known */

	for (i = 1; i <= gN+1; i++) {
	    gExt[i] = i;
	}

	return 0;
}

/* Options ... */

/* action codes */
#define NOTHING		0
#define PRGEN		11
#define PRPRT		12
#define PRCNT		13
#define PRTBL		14
/* add action codes here */

#define kDEFAULTACTION	PRCNT

typedef struct {
	char name[10];	/* option flag for the action */
	char len;	/* how much of the flag should match */
	int data;	/* action code */
} tAction;

static tAction sActions[] = {
	/* Pruesse & Ruskey */
	"gen",		1, PRGEN,	/* -g */
	"print",	1, PRPRT,	/* -p */
	"count",	1, PRCNT,	/* -c */
	"table",	1, PRTBL,	/* -t */

	"",		0, NOTHING
};

/* given a poset, process it */
static void dispatch( a )
int a;
{
	switch (a) {
	case PRGEN:
	    PruesseRuskeyBegin();
	    PruesseRuskeyGen();
	    PruesseRuskeyEnd();
	    break;
	case PRPRT:
	    PruesseRuskeyBegin();
	    PruesseRuskeyPrint();
	    PruesseRuskeyEnd();
	    break;
	case PRCNT:
	    PruesseRuskeyBegin();
	    PruesseRuskeyCount();
	    PruesseRuskeyEnd();
	    break;
	case PRTBL:
	    PruesseRuskeyBegin();
	    PruesseRuskeyTable();
	    PruesseRuskeyEnd();
	    break;
	default:
	    break;
	}
}
		
int main( argc, argv )
int argc;
char *argv[];
{
	int i, j;
	int action = kDEFAULTACTION;

	(void)strncpy( gProgname, argv[0], kMAXFILENAMELEN );

	init();

	for (i = 1; i < argc; i++) {
	    /* e.g. fence12 */
	    if (argv[i][0] != '-') {
		(void)strncpy( gFilename, argv[i], kMAXFILENAMELEN );
		if (!prep()) {
		    dispatch( action );
		}
		continue;
	    }

	    /* e.g. -c */
	    for (j = 0; sActions[j].data; j++) {
		if (strncmp( argv[i]+1,sActions[j].name,sActions[j].len )==0) {
		    action = sActions[j].data;
		    break;
		}
	    }
	    if (sActions[j].data == NOTHING) {
		(void)fprintf( stderr,
		    "Usage: genle [-c|-g|-p|-t] posetfile ...\n" );
		Quit( xGENERAL );
	    }
	}

	Quit( 0 );
}
