<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<HTML>
<HEAD>
<TITLE>5-ary words of Given Trace and Subtrace</TITLE>
<BASE HREF="http://theory.cs.uvic.ca/">
</HEAD>

<BODY>
<!--#include file="inc/Border.include"-->
<HR>
<center><H1>5-ary words of given trace and subtrace.</H1></center>

<P>   
Here we consider the set <B>S</B>(<I>n</I>;<I>t</I>,<I>s</I>) of length
<I>n</I>
words <I>a</I><sub>1</sub><I>a</I><sub>2</sub>...<I>a<sub>n</sub></I>
over the alphabet consisting of the elements of the field
<i><b>F<sub>5</sub></b></i> that have trace <I>t</I>
and subtrace <I>s</I>.  The <EM>trace</EM> of a word
is the sum of its digits over the field <i><b>F<sub>5</sub></b></i>;
<I>t</I> = a<sub>1</sub>+a<sub>2</sub>+ ... +a<sub><I>n</I></sub>.
The <EM>subtrace</EM> is the sum of the products
of all <I>n</I>(<I>n-</I>1)/2 pairs of digits taken over the field
<i><b>F<sub>5</sub></b></i>;
<I>s</I> = SUM( <I>a<sub>i</sub>a<sub>j</sub></I> :
1 <U>&lt;</U> <I>i</I> &lt; <I>j</I> <U>&lt;</U> <I>n</I> ).

<CENTER>
<TABLE border=1>
<TR>
  <TD>
  <TD ALIGN="center" COLSPAN=15>(trace,subtrace)</TD>
<TR>
<TD align=center><i>n</i>
  <TD bgcolor=yellow align=center><b>(0,0)</b>
  <TD bgcolor=yellow align=center><b>(0,1)</b><br><b>(0,4)</b>
  <TD bgcolor=yellow align=center><b>(0,2)</b><br><b>(0,3)</b><br><b>(1,0)</b><br><b>(4,0)</b>
  <TD bgcolor=yellow align=center><b>(1,1)</b><br><b>(2,4)</b><br><b>(3,4)</b><br><b>(4,1)</b>
  <TD bgcolor=yellow align=center><b>(1,2)</b><br><b>(2,3)</b><br><b>(3,3)</b><br><b>(4,2)</b>
  <TD bgcolor=yellow align=center><b>(1,3)</b><br><b>(2,2)</b><br><b>(3,2)</b><br><b>(4,3)</b>
  <TD bgcolor=yellow align=center><b>(1,4)</b><br><b>(2,1)</b><br><b>(3,1)</b><br><b>(4,4)</b>
  <TD bgcolor=yellow align=center><b>(2,0)</b><br><b>(3,0)</b>
 
<TR><TH align=center bgcolor=yellow>1</TH>
  <TD align=right>1  <TD align=right>0  <TD align=right>0
  <TD align=right>0  <TD align=right>0  <TD align=right>0
  <TD align=right>0  <TD align=right>1  
<TR><TH align=center bgcolor=yellow>2</TH>
  <TD align=right>1  <TD align=right>2  <TD align=right>0
  <TD align=right>0  <TD align=right>0  <TD align=right>2
  <TD align=right>1  <TD align=right>2  
<TR><TH align=center bgcolor=yellow>3</TH>
  <TD align=right>1  <TD align=right>6  <TD align=right>6
  <TD align=right>6  <TD align=right>1  <TD align=right>6
  <TD align=right>6  <TD align=right>6 
<TR><TH align=center bgcolor=yellow>4</TH>
  <TD align=right>25  <TD align=right>20  <TD align=right>30
  <TD align=right>25  <TD align=right>20  <TD align=right>30
  <TD align=right>30  <TD align=right>20  
<TR><TH align=center bgcolor=yellow>5</TH>
  <TD align=right>125  <TD align=right>100  <TD align=right>150
  <TD align=right>125  <TD align=right>125  <TD align=right>125
  <TD align=right>125  <TD align=right>125  
<TR><TH align=center bgcolor=yellow>6</TH>
  <TD align=right>625  <TD align=right>600  <TD align=right>650
  <TD align=right>600  <TD align=right>650  <TD align=right>650
  <TD align=right>600  <TD align=right>625  
<TR><TH align=center bgcolor=yellow>7</TH>
  <TD align=right>3025  <TD align=right>3150  <TD align=right>3150
  <TD align=right>3150  <TD align=right>3150  <TD align=right>3150
  <TD align=right>3025  <TD align=right>3150 
<TR><TH align=center bgcolor=yellow>8</TH>
  <TD align=right>15625  <TD align=right>15750  <TD align=right>15500
  <TD align=right>15750  <TD align=right>15625  <TD align=right>15750
  <TD align=right>15500  <TD align=right>15500 
<TR><TH align=center bgcolor=yellow>9</TH>
  <TD align=right>78625  <TD align=right>78000  <TD align=right>78000
  <TD align=right>78625  <TD align=right>78000  <TD align=right>78000
  <TD align=right>78000  <TD align=right>78000  
<TR><TH align=center bgcolor=yellow>10</TH>
  <TD align=right>393125  <TD align=right>390000  <TD align=right>390000
  <TD align=right>390625  <TD align=right>390625  <TD align=right>390625
  <TD align=right>390625  <TD align=right>390625  
<TR><TH align=center bgcolor=yellow>11</TH>
  <TD align=right>1955625  <TD align=right>1952500  <TD align=right>1952500
  <TD align=right>1952500  <TD align=right>1952500  <TD align=right>1952500
  <TD align=right>1952500  <TD align=right>1955625  
<TR><TH align=center bgcolor=yellow>12</TH>
  <TD align=right>9765625  <TD align=right>9768750  <TD align=right>9762500
  <TD align=right>9762500  <TD align=right>9762500  <TD align=right>9768750
  <TD align=right>9765625  <TD align=right>9768750  
<TR><TH align=center bgcolor=yellow>13</TH>
  <TD align=right>48815625  <TD align=right>48831250  <TD align=right>48831250
  <TD align=right>48831250  <TD align=right>48815625  <TD align=right>48831250
  <TD align=right>48831250  <TD align=right>48831250  
<TR><TH align=center bgcolor=yellow>14</TH>
  <TD align=right>244140625  <TD align=right>244125000  <TD align=right>244156250
  <TD align=right>244140625  <TD align=right>244125000  <TD align=right>244156250
  <TD align=right>244156250  <TD align=right>244125000  
<TR><TH align=center bgcolor=yellow>15</TH>
  <TD align=right>1220703125  <TD align=right>1220625000  <TD align=right>1220781250
  <TD align=right>1220703125  <TD align=right>1220703125  <TD align=right>1220703125
  <TD align=right>1220703125  <TD align=right>1220703125 
</TABLE>
</CENTER>

<H2>Examples:</H2> 
<H2>Further Notes:</H2>


<HR>
<!--#include file="inc/Border.include"-->
<HR>
<FONT SIZE=-1>
<!--#include file="inc/Wizard.include"--><br>
It was last updated <!--#echo var="LAST_MODIFIED" -->.<BR>
<!--#exec cgi="inc/cos-counter/counter-nl"--><BR>
<!--#include file="inc/Copyright.include"-->
</FONT>
                                                   
</BODY>
</HTML>


