/****************************************************************************
* C program to generate necklaces, Lyndon words, and De Bruijn              *
* sequences.  The algorithm is CAT and is described in the book             *
* "Combinatorial Generation."  This program, was obtained from the          *
* (Combinatorial) Object Server, COS, at http://www.theory.csc.uvic.ca/     *
* The inputs are n, the length of the string, k, the arity of the           *
* string, and density, the maximum number of non-0's in the string.         *
* The De Bruijn option doesn't make sense unless density >= n.              *
* The program can be modified, translated to other languages, etc.,         *
* so long as proper acknowledgement is given (author and source).           *
* Programmer: Frank Ruskey (1994), translated to C by Joe Sawada            *
*****************************************************************************/
#include <stdio.h>

int a[101];
int n,k,option,density;

void Print(int p) {

	int j;

	if ( ( option == 0) || ((n%p == 0)&&(option == 1)) ||
		((option == 2) && (p == n)) ) {
		for(j=1; j<=n; j++) 
			printf("%d ",a[j]);
		printf("\n");
	}
	else if ((option == 3) && (n%p == 0)) {
		for(j=1; j<=p; j++) 
			printf("%d ",a[j]);
		printf("\n");
		
	}
}

void Gen(int t, int p, int ones) {
	
	int j;
	if (ones <= density) {
		if(t>n) Print(p);
		else {
			a[t] = a[t-p]; 
			if (a[t] > 0)
				Gen(t+1,p,ones+1);
			else
				Gen(t+1,p,ones);
			for(j=a[t-p]+1; j<=k-1; j++) {
				a[t] = j; 
				Gen(t+1,t,ones+1);
			}
		}
	}
}
void main() {

	printf("Enter n k max-density:");
	scanf("%d %d %d",&n,&k,&density);
	printf("pre-necklace = 0, necklace = 1, lyndon = 2, DeBruijn = 3:");
	scanf("%d",&option);
	a[0] = 0;
	Gen(1,1,0); 
}

