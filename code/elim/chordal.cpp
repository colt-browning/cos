/*
 * Copyright (c) 2021 Jean Cardinal, Arturo Merino and Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cassert>
#include <iostream>
#include <string>
#include <utility>
#include <vector>
#include "chordal.hpp"

Elimination_forest::Elimination_forest(int n, std::vector<std::string> &vertex_strings, std::vector<std::pair<int,int>> &edge_list, bool &chordal) : n_(n), vertex_strings_(vertex_strings)
{
  build_graph(edge_list);

  lex_bfs();
  init_earlier_neighbors();
  chordal = is_chordal();
  if (!chordal) {
    return;
  }

  init_elimination_forest();
  init_bookkeeping();

  if (DEBUG) {
    check_consistency();
  }
}

void Elimination_forest::build_graph(std::vector<std::pair<int,int>> &edge_list)
{
  adjL_.resize(n_+1, std::vector<int> ());
  adjM_.resize(n_+1, std::vector<int> (n_+1,0));
  for (auto e : edge_list) {
    int i = e.first;
    int j = e.second;
    if ((i<1) || (i>n_) || (j<1) || (j>n_) || (adjM_[i][j] == 1)) {
      // ignore invalid edge
      continue;
    }
    adjL_[i].push_back(j);
    adjL_[j].push_back(i);
    adjM_[i][j] = 1;
    adjM_[j][i] = 1;
  }
}

void Elimination_forest::lex_bfs()
{
  // list of vertices of the graph to be processed from start to end
  std::vector<int> vertices(n_+1,0);
  for (int v=1; v<=n_; ++v) {
    vertices[v] = v;
  }
  // intervals[i] < 0 means that the index i is in the middle of an interval
  // intervals[i] = r >= i means that the index i is the left endpoint of an interval with right endpoint r
  // intervals[i] = l <= i means that the index i is the right endpoint of an interval with left endpoint l
  std::vector<int> intervals(n_+1,-1);
  intervals[1] = n_;
  intervals[n_] = 1;

  peo_.resize(n_+1, 0);
  oep_.resize(n_+1, 0);

  for (int i=1; i<=n_; ++i) {
    // remove first vertex
    int v = vertices[i];
    peo_[i] = v;
    oep_[v] = i;
    if (i < n_) {
      // split remaining sets of vertices into neighbors and non-neighbors of v
      int l, r;
      if (intervals[i] == i) {
        // interval of size 1, ignore it and start splitting with the next interval
        l = i+1;
        r = intervals[l];
      } else {
        // interval of size >= 2, might need to be split
        assert(intervals[i] > i);
        l = i+1;
        r = intervals[i];
        intervals[l] = r;
        intervals[r] = l;
      }
      while (l <= n_) {
        split_intervals(v, intervals, l, vertices);
        l = r+1;
        if (l <= n_) {
          r = intervals[l];
        }
      }
    }
  }
}

// split the vertices in the interval starting at l0 into neighbors and non-neighbors of v
void Elimination_forest::split_intervals(int v, std::vector<int> &intervals, int l0, std::vector<int> &vertices)
{
  int r0 = intervals[l0];  // right endpoint of this interval
  assert((l0>=1) && (r0>=l0));
  int l = l0;
  int r = r0;
  bool swapped = false;
  do {
    // move pointers from both sides until they cross
    while ((l <= r) && (adjM_[v][vertices[l]] == 1)) l++;
    while ((l <= r) && (adjM_[v][vertices[r]] == 0)) r--;
    if (l < r) {
      // swap neighbor with non-neighbor under the two pointers
      int t = vertices[l];
      vertices[l] = vertices[r];
      vertices[r] = t;
      swapped = true;
    }
  } while (l < r);
  assert(r == l-1);
  if ((r >= l0) && (l <= r0)) {
    assert((adjM_[v][vertices[r]] == 1) && (adjM_[v][vertices[l]] == 0));
    // split interval into two
    intervals[l0] = r;
    intervals[r] = l0;
    intervals[l] = r0;
    intervals[r0] = l;
  } else {
    assert(!swapped);
  }
}

void Elimination_forest::init_earlier_neighbors()
{
  earlier_neighbors_.resize(n_+1, std::vector<int>());
  earlier_neighbors_pos_.resize(n_+1, std::vector<int>());
  max_earlier_neighbor_.resize(n_+1, 0);
  for (int i=1; i<=n_; ++i) {
    int v = peo_[i];
    // determine earlier neighbors of v, and keep track of largest one
    int j = 0;
    earlier_neighbors_pos_[i].resize(n_+1, -1);
    for (auto w : adjL_[v]) {
      int k = oep_[w];
      if (k >= i) continue;
      earlier_neighbors_pos_[i][k] = earlier_neighbors_[i].size();
      earlier_neighbors_[i].push_back(k);
      j = k > j ? k : j;
    }
    max_earlier_neighbor_[i] = j;
  }
}

bool Elimination_forest::is_chordal()
{
  for (int i=1; i<=n_; ++i) {
    int v = peo_[i];
    int j = max_earlier_neighbor_[i];
    if (j == 0) continue;  // no earlier neighbor
    int u = peo_[j];
    // chordality: all earlier neighbors of v must be neighbors of u as well
    for (auto w : adjL_[v]) {
      int k = oep_[w];
      if ((k > i) || (k == j)) continue;
      assert(k < j);
      if (adjM_[u][w] == 0) return false;  // violation found
    }
  }

  return true;  // no violation found
}

void Elimination_forest::init_elimination_forest()
{
  // initialize elimination tree based on perfect elimination order of underlying tree
  is_root_.resize(n_+1, false);
  first_child_.resize(n_+1, 0);
  last_child_.resize(n_+1, 0);
  smaller_child_.resize(n_+1, 0);
  parent_.resize(n_+1, 0);
  left_sibling_.resize(n_+1, 0);
  right_sibling_.resize(n_+1, 0);
  ins_path_.resize(n_+1, std::vector<int>());
  ins_pos_.resize(n_+1, 0);
  std::vector<bool> removed(n_+1, false);  // vertices that are already in the elimination forest
  for (int i=1; i<=n_; ++i) {
    if (removed[i]) continue;
    // process the next connected component
    is_root_[i] = true;
    removed[i] = true;
    init_elimination_forest_rec(i, removed);
  }
}

void Elimination_forest::init_elimination_forest_rec(int i, std::vector<bool> &removed)
{
  // run graph search from i to find all interesting vertices reachable from i
  std::vector<bool> reached(n_+1, false);
  dfs(i, removed, reached);

  for (int j=1; j<=n_; ++j) {
    if ((j == i) || removed[j] || !reached[j]) continue;
    // recursively add children j of i
    insert_vertex(j, i);
    removed[j] = true;
    init_elimination_forest_rec(j, removed);
  }
}

void Elimination_forest::dfs(int i, std::vector<bool> &removed, std::vector<bool> &reached)
{
  reached[i] = true;
  int v = peo_[i];
  for (auto w : adjL_[v]) {
    int j = oep_[w];
    if (removed[j] || reached[j]) continue;
    dfs(j, removed, reached);
  }
}

void Elimination_forest::insertion_path(int i, std::vector<int> &path)
{
  // this should only happen when a vertex starts to move down
  assert(o_[i] == Direction::down);
  assert(((parent_[i] == 0) || (parent_[i] > i)) && (first_child_[i] != 0));
  // iterate through all earlier neighbors of i in the perfect elimination order,
  // and from each of them move upwards in the elimination tree until i is encountered
  // or a previously seen earlier neighbor
  path.clear();
  int u = peo_[i];
  int s = earlier_neighbors_[i].size();
  std::vector<bool> seen(s, false);
  for (int t = 0; t < s; t++) {  // go through earlier neighbors
    int j = earlier_neighbors_[i][t];
    if (seen[t]) continue;  // have started path computation from this vertex before
    seen[t] = true;
    std::vector<int> subpath;
    do {  // go up from j until we reach i or a previously seen earlier neighbor
      subpath.push_back(j);  // record the subpath from bottom to top
      int v = peo_[j];
      if (adjM_[u][v] == 1) {  // this is a neighbor of i
        int r = earlier_neighbors_pos_[i][j];
        assert((r >= 0) && (r < earlier_neighbors_[i].size()));
        if (r < t) {  // a previous search has started from this neighbor upwards
          assert(seen[r]);
          subpath.pop_back();  // neighbor already included in earlier path, so remove it
          break;
        } else {
          seen[r] = true;
        }
      }
      j = parent_[j];  // move one step up
      assert(j <= i);
    } while (j != i);
    // append the reversed subpath, so that it gets recorded from top to bottom
    path.insert(path.end(), subpath.rbegin(), subpath.rend());
  }
}

void Elimination_forest::insert_vertex(int i, int j)
{
  assert((i != 0) && (j != 0));
  int k = last_child_[j];
  // modify parent/child relations
  parent_[i] = j;
  last_child_[j] = i;
  if (first_child_[j] == 0) {
    first_child_[j] = i;
  }
  // modify sibling relations
  right_sibling_[k] = i;
  left_sibling_[i] = k;
  right_sibling_[i] = 0;
  // update smaller child
  if (i < j) {
    smaller_child_[j] = i;
  }
}

void Elimination_forest::delete_vertex(int i)
{
  assert(i != 0);
  // modify parent/child relations
  int j = parent_[i];
  int l = left_sibling_[i];
  int r = right_sibling_[i];
  if (first_child_[j] == i) {
    first_child_[j] = r;
  }
  if (last_child_[j] == i) {
    last_child_[j] = l;
  }
  // modify sibling relations
  right_sibling_[l] = r;
  left_sibling_[r] = l;
  // update smaller child
  if (i == smaller_child_[j]) {
    smaller_child_[j] = 0;
  }
}

int Elimination_forest::min_child(int i)
{
  assert((first_child_[i] !=0) && (last_child_[i] != 0));
  int j = first_child_[i];
  int min = j;
  while (j != 0) {
    min = (j < min) ? j : min;
    j = right_sibling_[j];
  }
  return min;
}

void Elimination_forest::init_bookkeeping()
{
  alpha_.resize(n_+1, 0);

  int prev = 1;  // remember previous vertex that is rotating
  for (int i=1; i<=n_; ++i) {
    int j = max_earlier_neighbor_[i];
    if (j > 0) {
      // there is an earlier neighbor
      alpha_[i] = prev;
      prev = i;
    }
  }
  rho_ = prev;

  o_.resize(n_+1, Direction::up);
  s_.resize(n_+1, 0);
  for (int j=1; j<=n_; ++j) {
    s_[j] = j;
  }
}

bool Elimination_forest::next()
{
  int j = s_[rho_];
  if (j == 1) {
    return false;
  }
  // rotate vertex j in direction o_[j]
  rotate(j, o_[j]);
  if (DEBUG) {
    check_consistency();
  }

  s_[rho_] = rho_;
  if (o_[j] == Direction::up && (is_root_[j] || (parent_[j] > j)))
  {
    o_[j] = Direction::down;
    int k = alpha_[j];
    s_[j] = s_[k];
    s_[k] = k;
    ins_pos_[j] = 0;  // reset position on insertion path, so that it gets recomputed
  }
  if ((o_[j] == Direction::down) && (smaller_child_[j] == 0))
  {
    o_[j] = Direction::up;
    int k = alpha_[j];
    s_[j] = s_[k];
    s_[k] = k;
  }
  return true;
}

void Elimination_forest::rotate(int j, Direction d)
{
  if (d == Direction::down) {
    // rotate j down by rotating its smaller child up
    int i = smaller_child_[j];
    assert((i != 0) && (i < j) && (i == min_child(j)));
    rotate(i, Direction::up);
  } else if (d == Direction::up) {
    int i = parent_[j];
    int p = parent_[i];
    if ((i > j) && (ins_pos_[i] == 0)) {
      assert((p == 0) || (p > i));
      // beginning of i's movement downward on its insertion path,
      // compute its insertion path once
      insertion_path(i, ins_path_[i]);
      ins_pos_[i] = 0;
    }
    delete_vertex(j);
    int k = first_child_[j];
    while (k != 0) {
      int l = k;
      k = right_sibling_[k];
      int u = peo_[i];
      int v = peo_[l];
      // the following slightly technical conditions determine whether a child l of j changes parent
      bool ins = (ins_pos_[i]+1 < ins_path_[i].size()) && (l == ins_path_[i][ins_pos_[i]+1]);  // l is on insertion path of i
      if (((i > j) && (((l < i) && ins) || ((l > i) && (adjM_[u][v] == 1) && (adjM_[u][v] == 1)))) ||
          ((i < j) && ( (l < j)         || ((l > j) && (adjM_[u][v] == 1) && (adjM_[u][v] == 1))))) {
        // The first condition applies when i>j moves down, the second condition applies when j>i moves up.
        // Then there are two cases depending on whether the moving element is larger or smaller than l.
        delete_vertex(l);  // the vertex l changes parent from j to i
        insert_vertex(l, i);
      }
    }
    if (p == 0) {
      assert(is_root_[i]);
      is_root_[i] = false;
      is_root_[j] = true;
      parent_[j] = 0;
      left_sibling_[j] = 0;
      right_sibling_[j] = 0;
    } else {
      delete_vertex(i);
      insert_vertex(j, p);
    }
    insert_vertex(i, j);
    // increment position on insertion path when moving down
    // (the position is irrelevant when moving up)
    if (i > j) {
      ins_pos_[i]++;
    }
  }
}

void Elimination_forest::print()
{
  bool first = true;
  for (int i=1; i<=n_; ++i) {
    if (is_root_[i]) print_rec(i, first);
  }
  std::cout << std::endl;
}

void Elimination_forest::print_rec(int i, bool &first)
{
  if (!first) std::cout << " ";
  first = false;
  std::cout << i;
  int j = first_child_[i];
  while (j != 0) {
    std::cout << ".";  // print one dot for each child of the current vertex
    j = right_sibling_[j];
  }
  j = first_child_[i];
  while (j != 0) {
    print_rec(j, first);
    j = right_sibling_[j];
  }
}

void Elimination_forest::print_peo()
{
  std::cout << "perfect elimination ordering: ";
  for (int i=1; i<=n_; i++) {
    if (i>=2) std::cout << " ";
    std::cout << vertex_strings_[peo_[i]];
  }
  std::cout << std::endl;
}

void Elimination_forest::check_consistency()
{
  // check root conditions
  int num_roots = 0;
  int count = 0;
  for (int i=0; i<=n_; ++i) {
    if (is_root_[i]) {
      assert((parent_[i] == 0) && (left_sibling_[i] == 0) && (right_sibling_[i] == 0));
      // check children recursively
      check_consistency_rec(i, count);
      num_roots++;
    }
  }
  assert(num_roots > 0);
  // check overall vertex count
  assert(n_ == count);
}

void Elimination_forest::check_consistency_rec(int i, int &count)
{
  count++;
  std::vector<int> child_to(n_+1, 0);
  if (first_child_[i] != 0) {
    int m = min_child(i);
    assert(((m > i) && (smaller_child_[i] == 0)) || ((m < i) && (smaller_child_[i] == m)));
  }
  // go through children from left to right
  int j = first_child_[i];
  int r = 0;
  while (j != 0) {
    r++;
    assert(parent_[j] == i);
    j = right_sibling_[j];
  }
  // go through children from right to left
  j = last_child_[i];
  int l = 0;
  while (j != 0) {
    l++;
    assert(parent_[j] == i);
    j = left_sibling_[j];
  }
  assert(r == l);
  // check children recursively
  j = first_child_[i];
  while (j != 0) {
    check_consistency_rec(j, count);
    j = right_sibling_[j];
  }
}