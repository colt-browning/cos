/* This program:  Takes as input: n t n(0) n(1) ... n(t)
		and outputs all permutations in lex order */

/* Written by:  Joe Sawada, Jan 97 */

#include <stdio.h>

int P[100];	/* max size of perm = 100 */
int a[20]; 	/* represent n(i); */
int N;
int t;
int count=0;
int flag=0;

void printP() {
	int i;

	count++;
	if( count > 200 )
		flag = 1;
	for (i=N-1; i>=0; i--)
		printf("%d ",P[i]+1);
	printf("\n");
}

void GenMult(int n) {

	int j;

	if( flag )
		return;
	if (n == 0) printP();
	else {
		for (j=0; j<=t; j++)  {
			if (a[j] > 0) {
				P[n-1] = j;
				a[j]--;
				GenMult(n-1);
				a[j]++;
				P[n-1] = 0;
			}
		}
	}
}

int main() {
	
	int i;

	scanf("%d",&N);
	scanf("%d",&t);
	++t;
	for (i=0; i<t; i++) 
		scanf("%d",&(a[i]));
	GenMult(N);
}

			
			




