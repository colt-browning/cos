#!/usr/bin/perl

# This file called "e_fibo.pl.cgi"
# Written and copyrighted by Frank Ruskey and Scott Lausch, March 1998
# Some code taken from COS, but virtually everything re-written for ECOS/AMOF;
# in particular, there are no calls to C programs, everything done
# in PERL.
# Not to be distributed without permission.
# "Ownership": Frank Ruskey

require "e_common.pl";

@p = ();

sub Reflections
	{
	local ($KK) = @_;
	local ($i, $location);  # $location is 3, 2, 1 or 0.

	printf "<TABLE BORDER='0' CELLPADDING='0' CELLSPACING='0'><TR><TD>\n";
	$location = 0;
	for ($i = 1; $i <= $KK; $i++)
		{
		if ($p[$i] == 0)
			{
			if ($location == 0)
				{
				print "<IMG SRC=Ico/rdown.gif width=18 height=30>";
				printf "<IMG SRC=Ico/rblank.gif width=18 height=30>";
				$location = 3;
				}
			elsif ($location == 3)
				{
				print "<IMG SRC=Ico/rblank.gif width=18 height=30>";
				printf "<IMG SRC=Ico/rup.gif width=18 height=30>";
				$location = 0;
				}
			elsif ($location == 2)
				{
				printf "<IMG SRC=Ico/rblank.gif width=18 height=30>";
				$location = 3;
				}
			elsif ($location == 1)
				{
				printf "<IMG SRC=Ico/rup.gif width=18 height=30>";
				$location = 0;
				}
			else
				{}
			}
		else
			{
			if ($location == 0)
				{
				printf "<IMG SRC=Ico/rdown.gif width=18 height=30>";
				$location = 1;
				}
			elsif ($location == 3)
				{
				printf "<IMG SRC=Ico/rblank.gif width=18 height=30>";
				$location = 2;
				}
			elsif ($location == 2)
				{
				printf "<IMG SRC=Ico/rblank.gif width=18 height=30>";
				$location = 3;
				}
			elsif ($location == 1)
				{
				printf "<IMG SRC=Ico/rup.gif width=18 height=30>";
				$location = 0;
				}
			else
				{}
			}
		}
	if ($location == 0)
		{
		printf "<IMG SRC=Ico/rdown.gif width=18 height=30><IMG SRC=Ico/rblank.gif width=18 height=30>";
		}
	elsif ($location == 1)
		{
		printf "<IMG SRC=Ico/rup.gif width=18 height=30>";
		}
	elsif ($location == 2)
		{
		printf "<IMG SRC=Ico/rblank.gif width=18 height=30>";
		}
	elsif ($location == 3)
		{
		printf "<IMG SRC=Ico/rblank.gif width=18 height=30><IMG SRC=Ico/rup.gif width=18 height=30>";
		}
	print "</TR><TR><TD>";
	$location = 0;
	for ($i = 1; $i <= $KK; $i++)
		{
		if ($p[$i] == 0)
			{
			if ($location == 0)
				{
				print "<IMG SRC=Ico/rblank.gif width=18 height=30>";
				printf "<IMG SRC=Ico/rdown.gif width=18 height=30>";
				$location = 3;
				}
			elsif ($location == 3)
				{
				print "<IMG SRC=Ico/rup.gif width=18 height=30>";
				printf "<IMG SRC=Ico/rblank.gif width=18 height=30>";
				$location = 0;
				}
			elsif ($location == 2)
				{
				printf "<IMG SRC=Ico/rdown.gif width=18 height=30>";
				$location = 3;
				}
			elsif ($location == 1)
				{
				printf "<IMG SRC=Ico/rblank.gif width=18 height=30>";
				$location = 0;
				}
			}
		else
			{
			if ($location == 0)
				{
				printf "<IMG SRC=Ico/rblank.gif width=18 height=30>";
				$location = 1;
				}
			elsif ($location == 3)
				{
				printf "<IMG SRC=Ico/rup.gif width=18 height=30>";
				$location = 2;
				}
			elsif ($location == 2)
				{
				printf "<IMG SRC=Ico/rdown.gif width=18 height=30>";
				$location = 3;
				}
			elsif ($location == 1)
				{
				printf "<IMG SRC=Ico/rblank.gif width=18 height=30>";
				$location = 0;
				}
			}
		}
	if ($location == 0)
		{
		printf "<IMG SRC=Ico/rblank.gif width=18 height=30><IMG SRC=Ico/rdown.gif width=18 height=30>";
		}
	elsif ($location == 1)
		{
		printf "<IMG SRC=Ico/rblank.gif width=18 height=30>";
		}
	elsif ($location == 2)
		{
		printf "<IMG SRC=Ico/rdown.gif width=18 height=30>";
		}
	elsif ($location == 3)
		{
		printf "<IMG SRC=Ico/rup.gif width=18 height=30><IMG SRC=Ico/rblank.gif width=18 height=30>";
		}

	printf "</TR></TABLE>\n";
	}

sub PrintIt
	{
	local ($KK) = @_;
	local($i, $j);

	if ($count == $LIMIT) { $retval = $LIMITerror; return; }
	++$count;
	printf "<TR>\n";
	if ($outformat & 1)
		{
		printf "<TD ALIGN=CENTER>";
		for ($i = 1; $i <= $KK; $i++)
			{
			printf (" %2d ", $p[$i]);
			}
		printf "<BR></TD>\n";
		}
	if ($outformat & 2)
		{
		printf "<TD ALIGN=CENTER>";
		$j = 0;
		for ($i = 1; $i <= $KK; $i++)
			{
			if ($p[$i] == 0)
				{
				print " 1 "; $j++;
				}
			else
				{
				print " 2 "; $j += 2;
				if ($i < $KK)
					{
					$i++;                   
					}
				}
			}
		if ($j < $KK + 1)
			{
			print " 1 ";
			}
		printf "<BR></TD>\n";
		}
	if ($outformat & 4)
		{
		print "<TD>";
		&Reflections($KK);
		print "</TD>";
		}
	}

sub Fibonacci
	{
	local ($N, $val, $i)  = @_; 

	if ($retval == $LIMITerror) {return;}
	
	$p[$i] = $val;
	if ($N == 1) { &PrintIt($i); return; }

	if ($val == 0)
		{
		&Fibonacci($N - 1, 0, $i + 1);
		&Fibonacci($N - 1, 1, $i + 1);          
		}
	else
		{
		&Fibonacci($N - 1, 0, $i + 1);
		}
	}

sub GenFib
	{
	local ($N) = @_;
	local ($i);
	
	if ($N != 0)
		{
		$p[0] = 0;
		&Fibonacci($N, 0, 1);
		&Fibonacci($N, 1, 1);
		}
	else
		{
		print "Sorry.  N equals zero.  Goodbye";
		}
	}


MAIN: 
{
# Read in all the variables set by the form
   if (&ReadParse(*input)) {
      &ProcessForm;
   }
   &CloseDocument( 'fibo' );
}

sub PrintTableHeaders {
   &TableSetup;
   if ($input{'output1'}) { &ColumnLabel( "Bitstring" ); }
   if ($input{'output2'}) { &ColumnLabel( "Stair-stepping" ); }
   if ($input{'output3'}) { &ColumnLabel( "Pane Reflections" ); }
   print "</TR><BR>";
}


sub StdOutput {
   &TableSetup;
   if ($input{'output1'}) 
	{
	print "<TH COLSPAN=1><FONT SIZE=+1>$_[0]<BR></FONT>"; 
	}
   if ($input{'output2'}) { &ColumnLabel( $_[1] ); }
   if ($input{'output3'}) { &ColumnLabel( $_[2] ); }
   print "</TR>";
}



sub nError
	{
	&ErrorMessage('An <I>n</I> parameter must be specified');
	}

sub ProcessForm
	{
	&OutputHeader ('Fibonacci Output', 'fibo' ); # from common.pl

	$outformat = 0;
	if ($input{'output1'}) { $outformat = 1; }
	if ($input{'output2'}) { $outformat = $outformat | 2; }
	if ($input{'output3'}) { $outformat = $outformat | 4; }
	if ($outformat == 0) { &OutError; return; }

	if (!($input{'n'}))
		{ 
		&ErrorMessage( "Parameter <I>n</I> must be specified!" );
		return;
		}
	$N = $input{'n'};
	if (int($input{'n'}) eq 0) {&nError; return;}   
	if ($N > 11) { &TooBig('n', 11); return; }
	if ($N < 1) { &TooSmall('n', 0); return; }
	$retval = $count = 0;

	print"<P>Output in lexicographic order.<P>";
	&StdOutput('Bitstrings', 'Stair-steps', 'Pane Reflections');
	&GenFib($N, $K, $M);
	if ($retval) { &LimitError; }
	print "</TABLE><P>Fibonacci objects generated = $count\n";
	}




