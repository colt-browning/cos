/*
 * Copyright (c) 2019 Ondrej Micka, Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <iostream>
#include <unistd.h>
#include <vector>
#include "chains.hpp"

// print a vector of integers without separation characters (omit first entry)
inline std::ostream& operator<<(std::ostream& os, const std::vector<int>& c) {
  for (int i = 1; i < c.size(); ++i) {
    char d;
    if (c[i] == 0) {
      d = '0';
    } else if (c[i] == 1) {
      d = '1';
    } else {
      d = '*';
    }
    os << d;
  }
  os;
  return os;
}

void visit(const std::vector<int> &x) {
  std::cout << x << std::endl;
}

// display help
void help() {
  std::cout << "./chains [options]  compute Gray code for Greene-Kleitman chains in the n-cube from [Gregor,Micka,Muetze]" << std::endl;
  std::cout << "-h                  display this help" << std::endl;
  std::cout << "-n{1,2,...}         dimension of the cube" << std::endl;
  std::cout << "-c{0,1}             print bitstrings instead of chains (*0=no, 1=yes)" << std::endl;
  std::cout << "-l{-1,0,1,2,...}    number of chains/bitstrings to generate; -1 for all" << std::endl;  
  std::cout << "examples:  ./chains -n5" << std::endl;
  std::cout << "           ./chains -n5 -c1" << std::endl;
}

void opt_n_missing() {
  std::cerr << "option -n is mandatory" << std::endl;
}

int main(int argc, char *argv[]) {
  int n;
  bool n_set = false;  // flag whether option -n is present
  bool full_cycle = false;  // by default, we do not compute the full Hamilton cycle
  int limit = -1;
  
  // process command line options
  int c;
  while ((c = getopt(argc, argv, ":hn:c:l:")) != -1) {
    switch (c) {
      case 'h':
        help();
        return 0;
      case 'n':
        n = atoi(optarg);
        if (n < 1) {
          std::cerr << "option -n must be followed by an integer from {1,2,...}" << std::endl;
          return 1;
        }
        n_set = true;
        break;
      case 'c':
        {
        int arg = atoi(optarg);
        if ((arg < 0) || (arg > 1)) {
          std::cerr << "option -c must be followed by 0 or 1" << std::endl;
          return 1;
        }
        full_cycle = (bool) arg;
        break;
        }
      case 'l':
        limit = atoi(optarg);
        if (limit < -1) {
          std::cerr << "option -l must be followed by an integer from {-1,0,1,2,...}" << std::endl;
          return 1;
        }
        break;        
      case ':':
        std::cerr << "option -" << (char) optopt << " requires an operand" << std::endl;
        return 1;
      case '?':
        std::cerr << "unrecognized option -" << (char) optopt << std::endl;
        return 1;
    }
  }
  if (!n_set) {
    opt_n_missing();
    help();
    return 1;
  }

  Chains scd(n, full_cycle, visit, limit);

  return 0;
}

