<?php
  if (isset($_GET["n"]) && isset($_GET["p"]) && isset($_GET["fmt"]) && isset($_GET["num"]) && isset($_GET["gfx"])) {
    $fmt = intval($_GET["fmt"]);
    $cmd = "code/btree/btree"
                             . " -n" . min(intval($_GET["n"]), 20)  /* hard-wired maximum number of vertices */
                             . " -p" . escapeshellarg(substr(str_replace(array("\r","\n"),"",$_GET["p"]), 0, 500))  /* hard-wired maximum pattern length (also remove line breaks from input) */
                             . " -l" . strval(limit($fmt))  /* hard-wired maximum number of binary trees */
                             . " 2>&1"  /* this command redirects stderr to stdout */
                             . (($fmt == 2) ? " > file.txt" : "");  /* redirect output to file */
    exec($cmd, $result);
    $num = filter_var($_GET["num"], FILTER_VALIDATE_BOOLEAN);
    $gfx = filter_var($_GET["gfx"], FILTER_VALIDATE_BOOLEAN);
    output($result, $fmt, "string_to_list", 0, "svg_bracketstr", 0, 0, 0, $num, $gfx);
  }
?>
