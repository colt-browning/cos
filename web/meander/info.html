Consider a strip of $n$ stamps numbered $1,2,\ldots,n$.
Such a strip may be folded in a variety of ways along the perforations to create a flat pile of $n$ stamps.
In creating this folding we assume that the perforations are perfectly elastic and may be stretched around any number of other stamps.
Any such way of folding the stamps to a flat pile is called a <span class="def">stamp folding</span>, where we always orient the pile horizontally with the perforations facing up and down, so that the perforation between stamps 1 and 2 is at the bottom.
We can represent such a folding as a permutation $\pi=(\pi(1),\pi(2),\ldots,\pi(n))$, where $\pi(i)$ is the stamp at position $i$ in the pile when considering it from left to right.

<p>
By ignoring the labeling of the stamps (stamp $i$ is identified with stamp $n+1-i$) and the orientation of the pile (we may swap left and right) we obtain <span class="def">unlabeled stamp foldings</span>.
A <span class="def">semi-meander</span> is a stamp folding in which stamp 1 can be seen from above.
A <span class="def">symmetric semi-meander</span> is a semi-meander in which stamp 2 is left of stamp 1.
An <span class="def">open meander</span> is a stamp folding in which stamp 1 can be seen from above left, and stamp $n$ can be seen from above right if $n$ is even and from the bottom (right) if $n$ is odd.
Meanders count the number of ways that a river flowing from west to east, starting in the north-west and ending in the north-east if $n$ is even and the south-east if $n$ is odd, crosses a straight line.
<span class="def">Symmetric meanders</span> are obtained by considering open meanders modulo east-west symmetry.
The following figure shows all the different variants for $n=4$, where stamp 1 is marked by a little dot, and the gray horizontal line shows the left-to-right order in which we consider the pile of stamps.

<p>
<table style="border-spacing:15px 5px">
<tr>
<td></td>
<td>permu-<br>tations</td>
<td>stamp<br>foldings</td>
<td>unlabeled<br>stamp<br>foldings</td>
<td>semi-<br>meanders</td>
<td>symmetric<br>semi-<br>meanders</td>
<td>open<br>meanders</td>
<td>symmetric<br>meanders</td>
</tr>
<style>.l{stroke:lightgray;stroke-width:1.5}.c{stroke-width:0;fill:black}.p{fill:none;stroke:black;stroke-width:1.5}</style>
<tr>
<td>1</td>
<td>4321</td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='36' cy='12' r='3'/><polyline class='p' points='36,12 36,24 24,24 24,12'/><polyline class='p' points='24,12 24,0 12,0 12,12'/><polyline class='p' points='12,12 12,24 0,24 0,12'/></svg></td>
<td>=16</td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='36' cy='12' r='3'/><polyline class='p' points='36,12 36,24 24,24 24,12'/><polyline class='p' points='24,12 24,0 12,0 12,12'/><polyline class='p' points='12,12 12,24 0,24 0,12'/></svg></td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='36' cy='12' r='3'/><polyline class='p' points='36,12 36,24 24,24 24,12'/><polyline class='p' points='24,12 24,0 12,0 12,12'/><polyline class='p' points='12,12 12,24 0,24 0,12'/></svg></td>
<td></td>
<td></td>
</tr>
<tr>
<td>2</td>
<td>3421</td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='36' cy='12' r='3'/><polyline class='p' points='36,12 36,24 24,24 24,12'/><polyline class='p' points='24,12 24,0 0,0 0,12'/><polyline class='p' points='0,12 0,24 12,24 12,12'/></svg></td>
<td>=15</td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='36' cy='12' r='3'/><polyline class='p' points='36,12 36,24 24,24 24,12'/><polyline class='p' points='24,12 24,0 0,0 0,12'/><polyline class='p' points='0,12 0,24 12,24 12,12'/></svg></td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='36' cy='12' r='3'/><polyline class='p' points='36,12 36,24 24,24 24,12'/><polyline class='p' points='24,12 24,0 0,0 0,12'/><polyline class='p' points='0,12 0,24 12,24 12,12'/></svg></td>
<td></td>
<td></td>
</tr>
<tr>
<td>3</td>
<td>3214</td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='24' cy='12' r='3'/><polyline class='p' points='24,12 24,18 12,18 12,12'/><polyline class='p' points='12,12 12,0 0,0 0,12'/><polyline class='p' points='0,12 0,24 36,24 36,12'/></svg></td>
<td>=12</td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='24' cy='12' r='3'/><polyline class='p' points='24,12 24,18 12,18 12,12'/><polyline class='p' points='12,12 12,0 0,0 0,12'/><polyline class='p' points='0,12 0,24 36,24 36,12'/></svg></td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='24' cy='12' r='3'/><polyline class='p' points='24,12 24,18 12,18 12,12'/><polyline class='p' points='12,12 12,0 0,0 0,12'/><polyline class='p' points='0,12 0,24 36,24 36,12'/></svg></td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='24' cy='12' r='3'/><polyline class='p' points='24,12 24,18 12,18 12,12'/><polyline class='p' points='12,12 12,0 0,0 0,12'/><polyline class='p' points='0,12 0,24 36,24 36,12'/></svg></td>
<td>=12</td>
</tr>
<tr>
<td>4</td>
<td>2431</td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='36' cy='12' r='3'/><polyline class='p' points='36,12 36,24 0,24 0,12'/><polyline class='p' points='0,12 0,0 24,0 24,12'/><polyline class='p' points='24,12 24,18 12,18 12,12'/></svg></td>
<td>=13</td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='36' cy='12' r='3'/><polyline class='p' points='36,12 36,24 0,24 0,12'/><polyline class='p' points='0,12 0,0 24,0 24,12'/><polyline class='p' points='24,12 24,18 12,18 12,12'/></svg></td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='36' cy='12' r='3'/><polyline class='p' points='36,12 36,24 0,24 0,12'/><polyline class='p' points='0,12 0,0 24,0 24,12'/><polyline class='p' points='24,12 24,18 12,18 12,12'/></svg></td>
<td></td>
<td></td>
</tr>
<tr>
<td>5</td>
<td>2341</td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='36' cy='12' r='3'/><polyline class='p' points='36,12 36,24 0,24 0,12'/><polyline class='p' points='0,12 0,0 12,0 12,12'/><polyline class='p' points='12,12 12,18 24,18 24,12'/></svg></td>
<td>=12</td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='36' cy='12' r='3'/><polyline class='p' points='36,12 36,24 0,24 0,12'/><polyline class='p' points='0,12 0,0 12,0 12,12'/><polyline class='p' points='12,12 12,18 24,18 24,12'/></svg></td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='36' cy='12' r='3'/><polyline class='p' points='36,12 36,24 0,24 0,12'/><polyline class='p' points='0,12 0,0 12,0 12,12'/><polyline class='p' points='12,12 12,18 24,18 24,12'/></svg></td>
<td></td>
<td></td>
</tr>
<tr>
<td>6</td>
<td>4213</td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='24' cy='12' r='3'/><polyline class='p' points='24,12 24,18 12,18 12,12'/><polyline class='p' points='12,12 12,0 36,0 36,12'/><polyline class='p' points='36,12 36,24 0,24 0,12'/></svg></td>
<td>=13</td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td>7</td>
<td>2143</td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='12' cy='12' r='3'/><polyline class='p' points='12,12 12,24 0,24 0,12'/><polyline class='p' points='0,12 0,0 36,0 36,12'/><polyline class='p' points='36,12 36,24 24,24 24,12'/></svg></td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='12' cy='12' r='3'/><polyline class='p' points='12,12 12,24 0,24 0,12'/><polyline class='p' points='0,12 0,0 36,0 36,12'/><polyline class='p' points='36,12 36,24 24,24 24,12'/></svg></td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td>8</td>
<td>2134</td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='12' cy='12' r='3'/><polyline class='p' points='12,12 12,24 0,24 0,12'/><polyline class='p' points='0,12 0,0 24,0 24,12'/><polyline class='p' points='24,12 24,24 36,24 36,12'/></svg></td>
<td>=15</td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td>9</td>
<td>4312</td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='24' cy='12' r='3'/><polyline class='p' points='24,12 24,24 36,24 36,12'/><polyline class='p' points='36,12 36,0 12,0 12,12'/><polyline class='p' points='12,12 12,24 0,24 0,12'/></svg></td>
<td>=15</td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td>10</td>
<td>3412</td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='24' cy='12' r='3'/><polyline class='p' points='24,12 24,24 36,24 36,12'/><polyline class='p' points='36,12 36,0 0,0 0,12'/><polyline class='p' points='0,12 0,24 12,24 12,12'/></svg></td>
<td>=7</td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td>11</td>
<td>3124</td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='12' cy='12' r='3'/><polyline class='p' points='12,12 12,18 24,18 24,12'/><polyline class='p' points='24,12 24,0 0,0 0,12'/><polyline class='p' points='0,12 0,24 36,24 36,12'/></svg></td>
<td>=13</td>
<td></td>
<td></td>
<td></td>
<td></td>
</tr>
<tr>
<td>12</td>
<td>1432</td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='0' cy='12' r='3'/><polyline class='p' points='0,12 0,24 36,24 36,12'/><polyline class='p' points='36,12 36,0 24,0 24,12'/><polyline class='p' points='24,12 24,18 12,18 12,12'/></svg></td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='0' cy='12' r='3'/><polyline class='p' points='0,12 0,24 36,24 36,12'/><polyline class='p' points='36,12 36,0 24,0 24,12'/><polyline class='p' points='24,12 24,18 12,18 12,12'/></svg></td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='0' cy='12' r='3'/><polyline class='p' points='0,12 0,24 36,24 36,12'/><polyline class='p' points='36,12 36,0 24,0 24,12'/><polyline class='p' points='24,12 24,18 12,18 12,12'/></svg></td>
<td>=5</td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='0' cy='12' r='3'/><polyline class='p' points='0,12 0,24 36,24 36,12'/><polyline class='p' points='36,12 36,0 24,0 24,12'/><polyline class='p' points='24,12 24,18 12,18 12,12'/></svg></td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='0' cy='12' r='3'/><polyline class='p' points='0,12 0,24 36,24 36,12'/><polyline class='p' points='36,12 36,0 24,0 24,12'/><polyline class='p' points='24,12 24,18 12,18 12,12'/></svg></td>
</tr>
<tr>
<td>13</td>
<td>1342</td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='0' cy='12' r='3'/><polyline class='p' points='0,12 0,24 36,24 36,12'/><polyline class='p' points='36,12 36,0 12,0 12,12'/><polyline class='p' points='12,12 12,18 24,18 24,12'/></svg></td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='0' cy='12' r='3'/><polyline class='p' points='0,12 0,24 36,24 36,12'/><polyline class='p' points='36,12 36,0 12,0 12,12'/><polyline class='p' points='12,12 12,18 24,18 24,12'/></svg></td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='0' cy='12' r='3'/><polyline class='p' points='0,12 0,24 36,24 36,12'/><polyline class='p' points='36,12 36,0 12,0 12,12'/><polyline class='p' points='12,12 12,18 24,18 24,12'/></svg></td>
<td>=4</td>
<td></td>
<td></td>
</tr>
<tr>
<td>14</td>
<td>4123</td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='12' cy='12' r='3'/><polyline class='p' points='12,12 12,18 24,18 24,12'/><polyline class='p' points='24,12 24,0 36,0 36,12'/><polyline class='p' points='36,12 36,24 0,24 0,12'/></svg></td>
<td>=12</td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='12' cy='12' r='3'/><polyline class='p' points='12,12 12,18 24,18 24,12'/><polyline class='p' points='24,12 24,0 36,0 36,12'/><polyline class='p' points='36,12 36,24 0,24 0,12'/></svg></td>
<td>=3</td>
<td></td>
<td></td>
</tr>
<tr>
<td>15</td>
<td>1243</td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='0' cy='12' r='3'/><polyline class='p' points='0,12 0,24 12,24 12,12'/><polyline class='p' points='12,12 12,0 36,0 36,12'/><polyline class='p' points='36,12 36,24 24,24 24,12'/></svg></td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='0' cy='12' r='3'/><polyline class='p' points='0,12 0,24 12,24 12,12'/><polyline class='p' points='12,12 12,0 36,0 36,12'/><polyline class='p' points='36,12 36,24 24,24 24,12'/></svg></td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='0' cy='12' r='3'/><polyline class='p' points='0,12 0,24 12,24 12,12'/><polyline class='p' points='12,12 12,0 36,0 36,12'/><polyline class='p' points='36,12 36,24 24,24 24,12'/></svg></td>
<td>=2</td>
<td></td>
<td></td>
</tr>
<tr>
<td>16</td>
<td>1234</td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='0' cy='12' r='3'/><polyline class='p' points='0,12 0,24 12,24 12,12'/><polyline class='p' points='12,12 12,0 24,0 24,12'/><polyline class='p' points='24,12 24,24 36,24 36,12'/></svg></td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='0' cy='12' r='3'/><polyline class='p' points='0,12 0,24 12,24 12,12'/><polyline class='p' points='12,12 12,0 24,0 24,12'/><polyline class='p' points='24,12 24,24 36,24 36,12'/></svg></td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='0' cy='12' r='3'/><polyline class='p' points='0,12 0,24 12,24 12,12'/><polyline class='p' points='12,12 12,0 24,0 24,12'/><polyline class='p' points='24,12 24,24 36,24 36,12'/></svg></td>
<td>=1</td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='0' cy='12' r='3'/><polyline class='p' points='0,12 0,24 12,24 12,12'/><polyline class='p' points='12,12 12,0 24,0 24,12'/><polyline class='p' points='24,12 24,24 36,24 36,12'/></svg></td>
<td><svg width='42' height='25.5' viewBox='-0.75 -0.75 37.5 25.5'><line class='l' x1='0' y1='12' x2='36' y2='12'/><circle class='c' cx='0' cy='12' r='3'/><polyline class='p' points='0,12 0,24 12,24 12,12'/><polyline class='p' points='12,12 12,0 24,0 24,12'/><polyline class='p' points='24,12 24,24 36,24 36,12'/></svg></td>
</tr>
</table>

<p>
The algorithms running on this website are described in Sawada and Li's paper [SL12].
