/* 
 * Copyright (c) 2022 Ondrej Micka, Arturo Merino and Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cassert>
#include <iostream>
#include <string>
#include <utility>
#include <vector>
#include "chordal.hpp"
#include "utils.hpp"

Chordal::Chordal(
  int n, const std::vector<std::string> &vertex_strings,
  const std::vector<std::pair<int,int>> &edge_list
) : n_(n), vertex_strings_(vertex_strings), chordality_checked_(false)
{
  assert((vertex_strings.size() == n+1) && "There should be exactly n+1 vertex names");
  build_graph(edge_list);
}

void Chordal::build_graph(const std::vector<std::pair<int,int>> &edge_list)
{
  adjL_.resize(n_+1, std::vector<int> ());
  adjM_.resize(n_+1, std::vector<int> (n_+1,0));
  for (auto e : edge_list) {
    int i = e.first;
    int j = e.second;
    // Do some sanity checks
    assert(((i >= 1) && (i <= n_)) && "Invalid vertex");
    assert(((j >= 1) && (j <= n_)) && "Invalid vertex");
    assert((!adjM_[i][j] && !adjM_[j][i]) && "Edge already exists!");

    adjL_[i].push_back(j);
    adjL_[j].push_back(i);
    adjM_[i][j] = 1;
    adjM_[j][i] = 1;
  }
}

void Chordal::lex_bfs()
{
  // list of vertices of the graph to be processed from start to end
  std::vector<int> vertices(n_+1,0);
  for (int v=1; v<=n_; ++v) {
    vertices[v] = v;
  }
  // intervals[i] < 0 means that the index i is in the middle of an interval
  // intervals[i] = r >= i means that the index i is the left endpoint of an interval with right endpoint r
  // intervals[i] = l <= i means that the index i is the right endpoint of an interval with left endpoint l
  std::vector<int> intervals(n_+1,-1);
  intervals[1] = n_;
  intervals[n_] = 1;

  peo_.resize(n_+1, 0);
  oep_.resize(n_+1, 0);

  for (int i=1; i<=n_; ++i) {
    // remove first vertex
    int v = vertices[i];
    peo_[i] = v;
    oep_[v] = i;
    if (i < n_) {
      // split remaining sets of vertices into neighbors and non-neighbors of v
      int l, r;
      if (intervals[i] == i) {
        // interval of size 1, ignore it and start splitting with the next interval
        l = i+1;
        r = intervals[l];
      } else {
        // interval of size >= 2, might need to be split
        assert(intervals[i] > i);
        l = i+1;
        r = intervals[i];
        intervals[l] = r;
        intervals[r] = l;
      }
      while (l <= n_) {
        split_intervals(v, intervals, l, vertices);
        l = r+1;
        if (l <= n_) {
          r = intervals[l];
        }
      }
    }
  }
}

// split the vertices in the interval starting at l0 into neighbors and non-neighbors of v
void Chordal::split_intervals(int v, std::vector<int> &intervals, int l0, std::vector<int> &vertices)
{
  int r0 = intervals[l0];  // right endpoint of this interval
  assert((l0>=1) && (r0>=l0));
  int l = l0;
  int r = r0;
  bool swapped = false;
  do {
    // move pointers from both sides until they cross
    while ((l <= r) && (adjM_[v][vertices[l]] == 1)) l++;
    while ((l <= r) && (adjM_[v][vertices[r]] == 0)) r--;
    if (l < r) {
      // swap neighbor with non-neighbor under the two pointers
      int t = vertices[l];
      vertices[l] = vertices[r];
      vertices[r] = t;
      swapped = true;
    }
  } while (l < r);
  assert(r == l-1);
  if ((r >= l0) && (l <= r0)) {
    assert((adjM_[v][vertices[r]] == 1) && (adjM_[v][vertices[l]] == 0));
    // split interval into two
    intervals[l0] = r;
    intervals[r] = l0;
    intervals[l] = r0;
    intervals[r0] = l;
  } else {
    assert(!swapped);
  }
}

void Chordal::calculate_max_neighbor(const std::vector<int> &peo, const std::vector<int> &oep, std::vector<int> &max_neighbor)
{
  max_neighbor.resize(n_+1, 0);
  for (int i=1; i<=n_; ++i) {
    int v = peo[i];
    // determine largest earlier neighbor of v
    int j = 0;
    for (auto w : adjL_[v]) {
      int k = oep[w];
      if (k >= i) continue;
      j = k > j ? k : j;
    }
    max_neighbor[i] = j;
  }
}

bool Chordal::is_chordal()
{
  if (!chordality_checked_){
    // We need to calculate PEO by lex DFS and then check that it is valid
    lex_bfs();
    is_chordal_ = check_peo();
    chordality_checked_ = true;
  }

  return is_chordal_;
}

bool Chordal::check_peo(const std::vector<int> &peo){
  // Calculate oep, check that peo is a permutation of 1..n and delegate to respective method
  if (peo.size() != n_+1){
    return false;
  }

  std::vector<int> oep(n_+1, 0);
  for (int i = 1; i <= n_; ++i){
    if ((peo[i] > n_+1) || (peo[i] < 1)){
      return false;
    }
    oep[peo[i]] = i;
  }

  // Check that peo (and thus oep) are permutations of 1..n
  // If peo wasn't a permutation, then there must be some i in 1..n missing
  for (int i = 1; i <= n_; ++i){
    if (peo[oep[i]] != i){
      return false;
    }
  }

  return check_peo(peo, oep);
}

bool Chordal::check_peo(const std::vector<int> &peo, const std::vector<int> &oep){
  assert((peo.size() == n_+1) && "Invalid size of peo");
  assert((oep.size() == n_+1) && "Invalid size of oep");

  std::vector<int> max_neighbor;
  calculate_max_neighbor(peo, oep, max_neighbor);

  for (int i=1; i<=n_; ++i) {
    int v = peo[i];
    int j = max_neighbor[i];
    if (j == 0) continue;  // no earlier neighbor
    int u = peo[j];
    // chordality: all earlier neighbors of v must be neighbors of u as well
    for (auto w : adjL_[v]) {
      int k = oep[w];
      if ((k > i) || (k == j)) continue;
      assert(k < j);
      if (adjM_[u][w] == 0) return false;  // violation found
    }
  }

  return true;  // no violation found

}

void Chordal::print_peo()
{
  if (is_chordal()){
    std::cout << "perfect elimination ordering: ";
    for (int i=1; i<=n_; i++) {
      if (i>=2) std::cout << " ";
      std::cout << vertex_strings_[peo_[i]];
    }
    std::cout << std::endl;
  }
  else{
    std::cout << "not chordal" << std::endl;
  }
}