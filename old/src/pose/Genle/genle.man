


User Commands                                            GENLE(1)



NNNNAAAAMMMMEEEE
     genle - generate linear extensions of partially ordered sets



SSSSYYYYNNNNOOOOPPPPSSSSIIIISSSS
     ggggeeeennnnlllleeee [ -cccc | -gggg | -pppp | -tttt ] _p_o_s_e_t_f_i_l_e ...



DDDDEEEESSSSCCCCRRRRIIIIPPPPTTTTIIIIOOOONNNN
     For each _p_o_s_e_t_f_i_l_e, ggggeeeennnnlllleeee generates all  the  linear  exten-
     sions  of the partially ordered set (poset) described by the
     file. Each  linear  extension  is  generated  exactly  once.
     (Linear extensions are also known as topological sortings.)

     If the poset contains no cover relations (all N elements are
     unrelated),  then  the  set  of all linear extensions is the
     same as the set of N! permutations of N elements.

     The ggggeeeennnnlllleeee implementation is based on an  algorithm  by  Gara
     Pruesse  and  Frank  Ruskey  which  generates all the linear
     extensions of a poset in constant amortized time.  That  is,
     the  total  running  time  is  some constant times the total
     number of linear extensions (plus a small amount of  prepro-
     cessing).   The  next  linear extension is produced by using
     only adjacent transpositions on the elements of the  current
     linear  extension.   Moreover,  successive linear extensions
     differ by at most two adjacent transpositions.

     The ggggeeeennnnlllleeee program can  count  the  total  number  of  linear
     extensions.   Counting is often faster than generating since
     not all linear extensions need to be produced to be included
     in the final count.  Counting is the default option.

     Generation normally does not  produce  output;  there  is  a
     separate option for printing the linear extensions.

     The ggggeeeennnnlllleeee program can produce for each ordered pair of  ele-
     ments  (x,y),  the  count  of  linear  extensions in which x
     appears before y. The results are presented in a table.  The
     computation of this table is also done in constant amortized
     time, with a small amount of postprocessing.

     Timings of the  main  processing  are  always  provided  and
     directed to the standard error stream.

     It is more efficient to specify a series of poset files  for
     processing instead of running the ggggeeeennnnlllleeee program repeatedly.






SunOS 5.7        Last change: 15 September 1991                 1






User Commands                                            GENLE(1)



OOOOPPPPTTTTIIIIOOOONNNNSSSS
     -cccc   Count the total number of linear extensions.

     -gggg   Generate all  the  linear  extensions  without  output.
          This  is  only  useful  for evaluating the speed of the
          implementation.

     -pppp   Print all the linear extensions as they are generated.

     -tttt   Tabulate, in matrix form, the number of  linear  exten-
          sions  in  which  one  element  appears before another.
          Write the total number of linear  extensions  and  also
          the  best  pair  (x,y)  which comes closest to having x
          before y in half the linear extensions (along with  the
          actual ratio).

     Options cannot be combined (e.g., -cccc -pppp ))));;;; the  most  recent
     one  specified overrides earlier ones.  However, a different
     option can be given for each _p_o_s_e_t_f_i_l_e specified.



UUUUSSSSAAAAGGGGEEEE
     A _p_o_s_e_t_f_i_l_e is an ASCII text file  and  specifies  a  single
     poset.   The  poset must be labelled in breadth-first-search
     order starting from the minimal elements.  The first line in
     the  file  contains  the number of elements; following lines
     contain pairs of integers specifying  the  cover  relations.
     (Cover  relations  correspond  to  the  edges  of  the Hasse
     diagram of the poset; additional relations in the transitive
     closure do not affect the algorithm.)  A line which contains
     a pair of zeroes marks the  end  of  the  relations.   After
     this, the file may contain anything (e.g., a note describing
     the poset).

     Consider the following  poset,  which  has  been  given  the
     proper breadth-first-search numbering.

     7  8  9 10 11 12
     | /| /| /| /| /|
     |/ |/ |/ |/ |/ |
     1  2  3  4  5  6

     This poset is described by a file containing:

     12
     1 7
     1 8
     2 8
     2 9
     3 9
     3 10



SunOS 5.7        Last change: 15 September 1991                 2






User Commands                                            GENLE(1)



     4 10
     4 11
     5 11
     5 12
     6 12
     0 0
     -- fence poset on 12 elements and 11 cover relations



DDDDIIIIAAAAGGGGNNNNOOOOSSSSTTTTIIIICCCCSSSS
     The exit status is normally 0.  System exceptions  return  a
     positive  exit  status (e.g., could not open the _p_o_s_e_t_f_i_l_e).
     Program exceptions return a negative exit status.

     UUUUssssaaaaggggeeee:::: ggggeeeennnnlllleeee [[[[-cccc||||-gggg||||-pppp||||-tttt]]]] _p_o_s_e_t_f_i_l_e...
          Invalid options were specified  on  the  command  line.
          Exit status becomes -1.

     BBBBaaaadddd ffffoooorrrrmmmmaaaatttt
          The _p_o_s_e_t_f_i_l_e is corrupted  or  in  the  wrong  format.
          Exit status becomes -2.

     _p_o_s_e_t_f_i_l_e:::: TTTToooooooo mmmmaaaannnnyyyy eeeelllleeeemmmmeeeennnnttttssss
          The number of elements in the poset exceeds 254.   Exit
          status becomes -3.

     _p_o_s_e_t_f_i_l_e:::: TTTToooooooo ffffeeeewwww eeeelllleeeemmmmeeeennnnttttssss
          The number of elements in the  poset  is  non-positive.
          Exit status becomes -4.



SSSSEEEEEEEE AAAALLLLSSSSOOOO
     Please report bugs, fixes, applications, modifications,  and
     requests to:

     Kenny Wong or Frank Ruskey
     Department of Computer Science
     University of Victoria
     PO Box 3055, Victoria, BC, Canada, V8W 3P6
     Internet: kenw@csr.UVic.CA or fruskey@csr.UVic.CA

     All correspondence should include your name, address,  phone
     number, and preferably an electronic mail address.



BBBBUUUUGGGGSSSS
     A signed integer overflow  may  occur  if  too  many  linear
     extensions are counted.




SunOS 5.7        Last change: 15 September 1991                 3



