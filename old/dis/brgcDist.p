(*=================================================================*)
(* Pascal program for distribution.                                *)
(* Generates the binary reflected Gray code (BRGC).                *)
(* No input error checking.  Assumes 0 <= NN <= MAX.               *)
(* Outputs both the transition sequence and the BRGC list.         *)
(* Algorithm is CAT (Constant Amortized Time).                     *)
(* Written by Frank Ruskey (fruskey@csr.uvic.ca).                  *)
(*=================================================================*)

program Subsets ( input, output );

const MAX = 50;  {maximum value of NN}

var NN : integer;                    {generate all subsets of [NN]}
    g  : array [1..MAX] of integer;  {bitstring representation of subset}
    
procedure PrintIt( n : integer );
var i : integer;
begin
  write( n:2,' : ' );                   {transition sequence}
  for i := 1 to NN do write( g[i]:2 );  {bitstring}
  writeln;
end {of PrintIt};

procedure flip( n : integer );
begin
  PrintIt( n );
  g[n] := 1 - g[n];
end {of flip};

procedure BRGC ( n : integer );
begin
  if n > 0 then begin
     BRGC( n-1 );  flip( n );  BRGC( n-1 );
  end;
end {of BRGC};

procedure Initialize;
var i : integer;
begin
  for i := 1 to NN do g[i] := 0;  {but any other bitstring would work}
end {of Initialize};

{-------------------------- MAIN ----------------------------}
begin
  write( 'Enter n: ' );  readln( NN );
  Initialize;
  BRGC( NN );
  PrintIt( NN );
end.


