<?php
  if (isset($_GET["n"]) && isset($_GET["k"]) && isset($_GET["s"]) && isset($_GET["fmt"]) && isset($_GET["num"]) && isset($_GET["gfx"])) {
    $n = min(intval($_GET["n"]), 20);  // hard-wired maximum combination length
    $fmt = intval($_GET["fmt"]);
    $cmd = "code/schrijver/schrijver"
                             . " -n" . strval($n)
                             . " -k" . escapeshellarg($_GET["k"])
                             . " -s" . escapeshellarg($_GET["s"])
                             . " -l" . strval(limit($fmt))  /* hard-wired maximum number of combinations */
                             . " 2>&1"  /* this command redirects stderr to stdout */
                             . (($fmt == 2) ? " > file.txt" : "");  /* redirect output to file */
    exec($cmd, $result);
    $num = filter_var($_GET["num"], FILTER_VALIDATE_BOOLEAN);
    $gfx = filter_var($_GET["gfx"], FILTER_VALIDATE_BOOLEAN);
    output($result, $fmt, "string_to_bits", 0, "svg_bits", 0, "svg_wheel", "bits_col", $num, $gfx);
  }
?>
