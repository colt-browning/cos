#pragma once

#include <iostream>
#include <vector>

// a class to represent a vertex of a Schrijver graph, or more generally, s-stable Kneser graph
class Vertex {
public:
  // the bitstring representation of the vertex
  std::vector<int> bits_;
  int s_;  // the parameter s of the s-stable Kneser graph; s=2 for Schrijver graphs

public:
  explicit Vertex(const std::vector<int>& x, int s);
  const std::vector<int>& get_bits() const { return bits_; }

  void right_shift();
  void left_shift();

  // connector checks and edges
  bool is_x_vertex_connector();
  bool is_y_vertex_connector();
  bool is_sigma_x_vertex_connector();
  bool is_sigma_y_vertex_connector();

  void edge_x_y();  // edge x-->y
  void edge_y_x();  // edge y-->x
  void edge_sigma_x_sigma_y();  // edge sigma(x)-->sigma(y)
  void edge_sigma_y_sigma_x();  // edge sigma(y)-->sigma(x)

  void print();

private:
  // various auxiliary functions
  int find_first_bit(const std::vector<int>& x, int b);  // finds position of first occurrence of given bit
  void match_unmatch(std::vector<int>& m);  // converts all matched bits to 1s and unmatched bits to 0s
  int align(std::vector<int>& m);  // align m at a boundary of matched and unmatched bits; return value is number of left shifts that were applied
  void block_gap_lengths(std::vector<int>& m, std::vector<int>&p);  // calculate the vector of block and gap lengths obtained from reading the string backwards from right to left (block lengths receive a negative sign)

  // Booth's algorithm to compute lexicographically smallest rotation of the given string/array.
  // Return value is the index where the lexicographically smallest string (viewed cyclically)
  // starts. The running time is linear in the length of the input array.
  // The input array x should be allocated on the stack for speed reasons. The reference to the
  // original paper is:
  // [K. Booth, Lexicographically least circular substrings, Inf. Proc. Letters, 10 (4-5): 240–242]
  int min_string_rotation(int* x, int length);

  // modulus function that returns non-negative result in the range {0,...,b-1}
  int mod(int a, int b) { return (a % b + b) % b; }
};
