#include <algorithm>
#include <cstring>
#include <vector>

#include "vertex.hpp"

Vertex::Vertex(const std::vector<int>& x, int s) : bits_(x), s_(s)
{
}

void Vertex::right_shift()
{
  std::rotate(bits_.begin(), bits_.begin()+bits_.size()-1, bits_.end());
  return;
}

void Vertex::left_shift()
{
  std::rotate(bits_.begin(), bits_.begin()+1, bits_.end());
  return;
}

int Vertex::find_first_bit(const std::vector<int>& x, int b)
{
  int n = bits_.size();
  for (int i = 0; i < n; i++) {
    if (x[i] == b) return i;
  }
  return -1;
}

void Vertex::match_unmatch(std::vector<int>& m)
{
  int n = bits_.size();
  m.resize(n, 0);
  for (int i = 0; i < n; i++) {
    if (bits_[i] == 1) {
      m[i] = 1;
      for (int j = 1; j < s_; j++) {  // each 1-bit is matched to the next s-1 many 0s
        m[mod ((i+j), n)] = 1;
      }
      i += s_-1;
    }
  }
}

int Vertex::align(std::vector<int>& m)
{
  int n = m.size();
  if (m[0] != m[n-1]) return 0;
  // shift the string m until a block or gap starts at the first position
  int p = find_first_bit(m, 1 - m[0]);  // find the first transition 0-->1 or 1-->0
  std::rotate(m.begin(), m.begin()+p, m.end());
  return p;
}

void Vertex::block_gap_lengths(std::vector<int>&m, std::vector<int>&p)
{
  int n = m.size();
  // read m from right to left
  int j = n-1;
  while (j >= 0) {
    int w = 1;
    // new block or gap
    while ((j >= 1) and (m[j] == m[j-1])) {
      w++;
      j--;
    }
    if (m[j] == 1) {
      p.push_back(-w);  // block length receives a negative sign
    } else {
      p.push_back(w);
    }
    j--;
  }
}

bool Vertex::is_x_vertex_connector()
{
  std::vector<int> m;
  match_unmatch(m);
  int n = bits_.size();

  // The special position p in the paper is taken to be p=n-1, i.e., a connector has the form
  //             x = 0-  ...    1
  //      sigma(x) = 10-  ...
  // sigma^{-1}(y) = 10  ...    -
  //            y  = -10  ...
  //      sigma(y) =  -10  ...
  // This is for the case s=2; for general s the pattern is 10^{s-1}-

  // check 1 and -
  if (!((bits_[n-1] == 1) and (bits_[s_-1] == 0) and (m[s_-1] == 0))) {
    return false;
  }
  // check 0^{s-1}
  for (int i = 0 ; i < s_-1; i++) {
	  if (!((bits_[i] == 0) and (m[i] == 1))) {
	    return false;
	  }
  }

  int l = align(m);
  std::vector<int> p;
  block_gap_lengths(m, p);

  int np = p.size();

  if (np == 2) {  // two blocks and gaps
    return false;
  }

  // copy p onto the stack for running Booth's algorithm
  int ps[np];
  std::copy(p.begin(), p.end(), ps);
  // compute the index from where the minimum rotation in p begins
  int min_index = min_string_rotation(ps, np);

  // compute the vector p right rotated starting from the index min_index+2
  std::vector<int> p_rot(p);
  std::rotate(p_rot.begin(), p_rot.begin()+mod(min_index+2, np), p_rot.end());

  if ((p_rot == p) and (l == s_-1)) {
    return true;
  } else {
    return false;
  }
}

bool Vertex::is_y_vertex_connector()
{
  std::vector<int> m;
  match_unmatch(m); // identify the matched and unmatched bits

  // check 1 and -
  if (!((bits_[1] == 1) and (bits_[0] == 0) and (m[0] == 0))) {
    return false;
  }
  // check 0^{s-1}
  for (int i = 2 ; i < s_+1; i++) {
    if (!((bits_[i] == 0) and (m[i] == 1))) {
 	    return false;
 	  }
  }

  // change y to sigma(x)
  bits_[0] = 1;
  bits_[1] = 0;

  bool res = is_sigma_x_vertex_connector();
  bits_[0] = 0;  // restore original state
  bits_[1] = 1;
  return res;
}

bool Vertex::is_sigma_x_vertex_connector()
{
  left_shift();  // left-shift to get x
  bool res = is_x_vertex_connector();
  right_shift();  // restore original state
  return res;
}

bool Vertex::is_sigma_y_vertex_connector()
{
  left_shift();  // left-shift to get y
  bool res = is_y_vertex_connector();
  right_shift();  // restore original state
  return res;
}

void Vertex::edge_x_y()
{
  // right-shift x to get sigma(x)
  right_shift();
  // change the first three bits of sigma(x) to get y
  bits_[0] = 0;
  bits_[1] = 1;
}

void Vertex::edge_y_x()
{
  // change the first three bits of y to get sigma(x)
  bits_[0] = 1;
  bits_[1] = 0;
  // left-shift sigma(x) to get x
  left_shift();
}

void Vertex::edge_sigma_x_sigma_y()
{
  // change the first three bits of sigma(x) to get y
  bits_[0] = 0;
  bits_[1] = 1;
  // right-shift y to get sigma(y)
  right_shift();
}

void Vertex::edge_sigma_y_sigma_x()
{
  // left-shift sigma(y) to get y
  left_shift();
  // change the first three bits of y to get sigma(x)
  bits_[0] = 1;
  bits_[1] = 0;
}

void Vertex::print()
{
  for (int i = 0; i < bits_.size(); i++) {
    std::cout << bits_[i];
  }
  std::cout << std::endl;
}

// The source code for this implementation of Booth's
// algorithm was copied verbatim from the following Wikipedia
// site: "Lexicographically minimal string rotation"
int Vertex::min_string_rotation(int* x, int length) {
  // concatenate array with itself to avoid modular arithmetic
  int xx[2*length];
  std::memcpy(xx, x, sizeof(int) * length);
  std::memcpy(xx + length, x, sizeof(int) * length);
  // failure function
  std::vector<int> fail(2*length, -1);
  int k = 0;  // lexicographically smallest starting position found so far
  for (int j = 1; j < 2*length; ++j) {
    int xj = xx[j];
    int i = fail[j - k - 1];
    while ((i != -1) && (xj != xx[k + i + 1])) {
      if (xj < xx[k + i + 1]) {
        k = j - i - 1;
      }
      i = fail[i];
    }
    if (xj != xx[k + i + 1]) {
      if (xj < xx[k]) {
        k = j;
      }
      fail[j - k] = -1;
    } else {
      fail[j - k] = i + 1;
    }
  }
  return k;
}
