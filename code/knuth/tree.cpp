/*
 * Copyright (c) 2020 Arturo Merino, Ondrej Micka, Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <algorithm>
#include <cassert>
#include <fstream>
#include <iostream>
#include <iterator>
#include <list>
#include <queue>
#include <vector>
#include "tree.hpp"
#include "utils.hpp"

Tree::Tree(std::vector<bool> &dyck_word) : id_to_vtx_(), centroids_(nullptr, nullptr), potential_(-1) {
  assert((dyck_word.size() % 2 == 0) && "Dyck word must have even length");

  root_ = new Vertex(nullptr, 0);
  num_vertices_ = 1;
  id_to_vtx_.push_back(root_);
  Vertex *current = root_;
  for (bool b : dyck_word) {
    // b == 1 means creation of a new child and moving to this child in the tree
    if (b) {
      Vertex *v = new Vertex(current, num_vertices_);
      num_vertices_++;
      id_to_vtx_.push_back(v);   // register id of the new vertex
      current->children_.push_back(v);
      v->parent_list_pos_ = std::prev(current->children_.end());  // set position in parent's list of children
      current = v;
    }
    // b == 0 means moving up to the parent, without creating any vertices
    else {
      assert(current->parent_ && "Dyck word says to move to the parent of the root");
      current = current->parent_;
    }
  }
  assert((current == root_) && "tree construction must end in the root");

  num_edges_ = num_vertices_-1;
}

Tree::~Tree() {
  for (Vertex *v : id_to_vtx_) {
    delete v;
  }
}

void Tree::rotate() {
  Vertex *v = root_;
  Vertex *u = v->get_left_child();
  assert((u->parent_ == v) && "root is not parent of root's left child");
  // goal: make u new root and turn v into right child of u
  root_ = u;

  u->children_.push_back(v);
  v->children_.pop_front();

  u->set_parent(nullptr, std::list<Vertex *>::iterator());
  v->set_parent(u, std::prev(u->children_.end()));
}

void Tree::rotate_inverse() {
  Vertex *u = root_;
  Vertex *v = u->get_right_child();
  assert((v->parent_ == u) && "root is not parent of root's right child");
  // goal: make v new root and turn u into left child of v
  root_ = v;

  v->children_.push_front(u);
  u->children_.pop_back();

  v->set_parent(nullptr, std::list<Vertex *>::iterator());
  u->set_parent(v, v->children_.begin());
}

void Tree::pull_to_root(Vertex *leaf) {
  assert(leaf && "cannot pull nullptr");
  assert(leaf->is_leaf() && "can only pull leaves");
  assert(leaf->parent_ && "pulled leaf must not be the root");
  assert(leaf->is_left_child() && "pulled leaf must be left child");
  assert((leaf->parent_->parent_ || !leaf->is_right_child()) && "if leaf's parent is the root, then leaf must have a sibling");

  Vertex *p = leaf->parent_;
  Vertex *t = p->parent_;
  if (t == nullptr) {
    // if leaf's parent is the root, then leaf will become right child of the current root's right child
    t = p->get_right_child();
  }

  p->children_.erase(leaf->parent_list_pos_);

  // insert leaf before p into list of t's children, then update leaf's parent info
  // if p is the root, then we insert it as a right child of t
  auto leaf_pos = t->children_.insert(
    p == root_ ? t->children_.end() : p->parent_list_pos_,
    leaf
  );
  leaf->set_parent(t, leaf_pos);

  assert(check_consistency_parent_list_pos() && "inconsistent at the end of pull_to_root");
}

void Tree::pull_from_root(Vertex *leaf) {
  assert(leaf && "cannot pull nullptr");
  assert(leaf->is_leaf() && "can only pull leaves");
  assert(leaf->parent_ && "pulled leaf must not be the root");
  assert(!leaf->is_left_child() && "pulled leaf must not be left child");

  Vertex *p = leaf->parent_;
  Vertex *t = leaf->get_left_sibling();

  p->children_.erase(leaf->parent_list_pos_);

  // insert leaf as right child of t, then update leaf's parent info
  auto leaf_pos = t->children_.insert(t->children_.end(), leaf);
  leaf->set_parent(t, leaf_pos);

  assert(check_consistency_parent_list_pos() && "inconsistent at the end of pull_from_root");
}

void Tree::push_from_root(Vertex *leaf) {
  assert(leaf && "cannot push nullptr");
  assert(leaf->is_leaf() && "can only push leaves");
  assert(leaf->parent_ && "pushed leaf must not be the root");
  assert(!leaf->is_right_child() && "pushed leaf must not be right child");

  Vertex *p = leaf->parent_;
  Vertex *t = leaf->get_right_sibling();

  p->children_.erase(leaf->parent_list_pos_);

  // insert leaf as left child of t, then update leaf's parent info
  auto leaf_pos = t->children_.insert(t->children_.begin(), leaf);
  leaf->set_parent(t, leaf_pos);

  assert(check_consistency_parent_list_pos() && "inconsistent at the end of push_from_root");
}

void Tree::push_to_root(Vertex *leaf) {
  assert(leaf && "cannot push nullptr");
  assert(leaf->is_leaf() && "can only push leaves");
  assert(leaf->parent_ && "pushed leaf must not be the root");
  assert(leaf->is_right_child() && "pushed leaf must be right child");
  assert((leaf->parent_->parent_ || !leaf->is_left_child()) && "if leaf's parent is the root, then leaf must have a sibling");

  Vertex *p = leaf->parent_;
  Vertex *t = p->parent_;
  if (t == nullptr) {
    // if leaf's parent is the root, then leaf will become left child of the current root's left child
    t = p->get_left_child();
  }

  p->children_.erase(leaf->parent_list_pos_);

  // insert leaf after p into list of t's children, then update leaf's parent info
  auto leaf_pos = t->children_.insert(
    p == root_ ? t->children_.begin() : std::next(p->parent_list_pos_),
    leaf
  );
  leaf->set_parent(t, leaf_pos);

  assert(check_consistency_parent_list_pos() && "inconsistent at the end of push_to_root");
}

void Tree::reroot(Vertex *v) {
  assert(v && "cannot root tree in nullptr");

  if (v == root_) return;

  assert(check_consistency_parent_list_pos() && "inconsistent at the start of reroot");

  root_ = v;

  // we need to walk path from v to the previous root and "reverse" the edges
  Vertex *new_parent = nullptr;
  std::list<Vertex *>::iterator new_parent_pos;
  std::list<Vertex *>::iterator new_parent_list_pos;

  while (v != nullptr) {
    // update the information about v's parent
    Vertex *parent = v->parent_;
    v->parent_ = new_parent;

    // insert original parent into list of children
    if (parent != nullptr) {
      v->children_.push_front(parent);
    }
    auto tmp_new_parent_pos = v->children_.begin();   // remember position of original parent in the list
                                                      // irrelevant when parent is nullptr

    // now we need to rotate the list of children so that the new parent is at the beginning
    if (new_parent != nullptr) {
      // i.e., move everything from the start to the new_parent_pos at the end of the list (works in O(1))
      v->children_.splice(
        v->children_.end(),
        v->children_,
        v->children_.begin(),
        new_parent_pos
      );
      // remove the new parent from the list of children
      v->children_.erase(v->children_.begin());
    }

    new_parent_pos = v->parent_list_pos_;
    v->parent_list_pos_ = new_parent_list_pos;
    new_parent_list_pos = tmp_new_parent_pos;
    new_parent = v;
    v = parent;
  }

  assert(check_consistency_parent_list_pos() && "inconsistent at the end of reroot");
}

void Tree::make_first_subtree(Vertex *v) {
  assert(v->parent_ == root_ && "make_first_subtree: v has to be root's child");

  auto v_pos = v->parent_list_pos_;
  if (v_pos == root_->children_.begin()) return;  // v is already first

  // we take everything before v and put it at the end of the list of children (works in O(1))
  root_->children_.splice(
    root_->children_.end(),
    root_->children_,
    root_->children_.begin(),
    v_pos
  );

  assert(check_consistency_parent_list_pos() && "inconsistent at the end of make_first_subtree");
}

void Tree::centroids_and_potential() {
  // Here is the idea of the algorithm:
  // First, we compute the potential of vertices, bottom-up in each subtree.
  // This gives the correct global potential of the root. We then compute, top-down,
  // the global potential of the parent to compute the global potential of its children.
  std::vector<int> size(num_vertices_, 0);  // number of vertices in subtree rooted at given vertex
  std::vector<int> potential(num_vertices_, 0);  // potential of vertex with respect to its subtree
  std::vector<int> global_potential(num_vertices_, 0);  // actual global potential with respect to entire tree

  // compute subtree potentials first
  subtree_potential(root_, potential, size);

  // now we can calculate the potential of the root
  global_potential[root_->id_] = potential[root_->id_];
  // from potential of the root we calculate potential of the children top-down
  std::queue<Vertex *> q;
  for (Vertex *c : root_->children_) {
    q.push(c);
  }

  while (!q.empty()) {
    Vertex *v = q.front();
    q.pop();
    int vid = v->id_;
    // Lemma 3 from the paper, or simple calculation of parent contribution and subtree contribution
    global_potential[vid] = global_potential[v->parent_->id_] + num_vertices_ - 2*size[vid];
    for (Vertex *c : v->children_) {
      q.push(c);
    }
  }

  // potential computation finished, now we can find the centroid(s)
  // we do the hard work with a library function
  auto first_min_it = std::min_element(global_potential.begin(), global_potential.end());
  auto second_min_it = std::min_element(first_min_it+1, global_potential.end());   // min_element() returns first minimum, so we can continue the search
  centroids_.first = get_vtx_by_id(first_min_it - global_potential.begin());
  potential_ = global_potential[centroids_.first->id_];
  if (second_min_it == global_potential.end()) {
    centroids_.second = nullptr;
  }
  else {
    centroids_.second = get_vtx_by_id(second_min_it - global_potential.begin());
    // if the second minimum is different from the first one, we have only one centroid
    if (global_potential[centroids_.second->id_] != global_potential[centroids_.first->id_]) {
      centroids_.second = nullptr;
    }
  }
}

void Tree::subtree_potential(Vertex *v, std::vector<int> &potential, std::vector<int> &size) {
  int id = v->id_;
  potential[id] = 0;
  size[id] = 1;

  for (Vertex *c : v->children_) {
    subtree_potential(c, potential, size);
    size[id] += size[c->id_];
    // for any w in c-subtree of v, we have d(v,w) = d(c,w) + 1, potential is just the sum of these over all w's
    potential[id] += potential[c->id_] + size[c->id_];
  }
}

void Tree::get_subtree_bitstring(Vertex *v, std::list<bool> &bs) {
  if (v != root_) bs.push_back(1);  // include edge going to this subtree
  for (Vertex *c : v->children_) {  // recursively insert bitstrings from subtrees
    get_subtree_bitstring(c, bs);
  }
  if (v != root_) bs.push_back(0);
}

int Tree::get_subtree_vertices(Vertex *v) {
  int n = 1;
  for (Vertex *c : v->children_) {
    n += get_subtree_vertices(c);
  }
  return n;
}

void Tree::normalize() {
  // reroot the tree to first of the centroids
  centroids_and_potential();
  reroot(centroids_.first);

  if (has_two_centroids()) {
    normalize_two_centroids();
  }
  else {
    normalize_one_centroid();
  }
}

void Tree::normalize_one_centroid() {
  // if the tree is a star there is nothing to do
  if (is_star())  return;

  // otherwise we need to choose the correct order of subtrees
  std::list<Subtree *> subtrees;
  make_subtree_list(subtrees);

  std::vector<bool> tree_bitstring;
  // we also need to mark the subtree boundaries
  std::vector<bool> is_subtree_start(2*num_vertices_-2, 0);
  int last_subtree = 0;
  for (Subtree *s : subtrees) {
    is_subtree_start[last_subtree] = 1;
    tree_bitstring.insert(tree_bitstring.cend(), s->bitstring_.cbegin(), s->bitstring_.cend());
    last_subtree += 2*s->num_edges_;
  }
  assert(last_subtree == is_subtree_start.size());
  // run Booth's algorithm to find lexicographically smallest rotation
  int smallest_rot_idx = lex_smallest_rotation(tree_bitstring, is_subtree_start);
  // find the corresponding subtree in the list
  last_subtree = 0;
  auto it = subtrees.cbegin();
  for (; last_subtree != smallest_rot_idx; last_subtree += 2*(*it)->num_edges_, ++it) {}

  make_first_subtree((*it)->get_root());

  // free memory
  for (auto it = subtrees.cbegin(); it != subtrees.cend(); ++it) {
    delete *it;
  }
}

void Tree::normalize_two_centroids() {
  // make the second centroid a first child of the root
  make_first_subtree(centroids_.second);

  // if the tree is dumbbell we are done
  if (is_dumbbell())  return;

  // We need to root the tree in the correct centroid.
  // We start with easy case: one of the centroids has only leaves as children.
  // We use the fact that if we split the tree between centroids, we get two trees of the same size.
  if (centroids_.second->children_.size() == num_vertices_/2 - 1) {
    // second centroid has only leaves attached to it => we are done
    return;
  }
  if (centroids_.first->children_.size() == num_vertices_/2) {
    // first centroid has only leaves attached to it => we need to swap centroids
    reroot(centroids_.second);
    make_first_subtree(centroids_.first);
    std::swap(centroids_.first, centroids_.second);
    return;
  }

  // Now we know both centroids have non-leaf subtrees attached to them.
  // We have to choose the centroid whose subtrees (without the subtree that contains the other centroid)
  // forms the lexicographically smaller bitstring.

  // build bitstring for the first centroid
  std::list<bool> bitlist;
  for (
    auto it = std::next(root_->children_.cbegin());   // first child is the other centroid so we ignore it
    it != root_->children_.cend(); ++it) {
    get_subtree_bitstring(*it, bitlist);
  }
  std::vector<bool> string1(bitlist.cbegin(), bitlist.cend());

  // reroot the tree to the second centroid
  reroot(centroids_.second);
  make_first_subtree(centroids_.first);

  // build bitstring for the second centroid
  bitlist = std::list<bool>();   // clear the list
  for (
    auto it = std::next(root_->children_.cbegin());   // first child is the other centroid so we ignore it
    it != root_->children_.cend(); ++it) {
    get_subtree_bitstring(*it, bitlist);
  }
  std::vector<bool> string2(bitlist.cbegin(), bitlist.cend());

  if (!lex_less(string1, string2)) {
    // if the second centroid's string is lexicographically smaller or equal to the first
    // we just swap centroids, since we are already rooted at the second one
    std::swap(centroids_.first, centroids_.second);
  }
  else {
    // otherwise, we need to reroot back to the first centroid
    reroot(centroids_.first);
    make_first_subtree(centroids_.second);
  }
}

void Tree::make_subtree_list(std::list<Subtree *> &subtrees) {
  subtrees.clear();

  for (Vertex *c : root_->children_) {
    // ALLOCATED MEMORY HAS TO BE FREED BY THE CALLER AFTER USAGE
    subtrees.push_back(new Subtree(this, c));
  }
}

int Tree::lex_smallest_rotation(const std::vector<bool> &bitstring, const std::vector<bool> &is_start) {
  // We use Booth's algorithm as on Wikipedia (https://en.wikipedia.org/wiki/Lexicographically_minimal_string_rotation).
  // To ensure that only rotations to subtree boundaries are returned, we insert -1s at those positions to force
  // the algorithm to return such a position.
  // It can be shown that this gives the lexicographically smallest ordering of subtrees, but for this
  // to work it is crucial that the subtree strings are Dyck words.
  assert((bitstring.size() == is_start.size()) && "admissible rotation bitmap must have same size as the bitstring");

  // first, insert -1s between subtree boundaries
  std::vector<int> S;
  for (int i = 0; i < bitstring.size(); ++i) {
    if (is_start[i]) {
      S.push_back(-1);
    }
    S.push_back((int)bitstring[i]);
  }

  const int n = S.size();

  std::vector<int> f(2*n, -1);  // failure function
  int k = 0;  // index of smallest rotation so far

  for (int j = 1; j < 2*n; j++) {
    int sj = S[j % n];
    int i = f[j-k-1];

    while((i != -1) && (sj != S[(k + i + 1) % n])) {
      if (sj < S[(k + i + 1) % n]) {
        k = j - i - 1;
      }
      i = f[i];
    }

    if (sj != S[(k + i + 1) % n]) {
      if (sj < S[k % n]) {
        k = j;
      }
      f[j - k] = -1;
    }
    else {
      f[j - k] = i + 1;
    }
  }

  k %= n;
  int minus_ones = 0;   // we cannot directly change k since we use it in the loop condition below
  // subtract number of -1s included in the count k
  for (int i = 0; i < k; ++i) {
    if (S[i] == -1) {
      ++minus_ones;
    }
  }
  k -= minus_ones;

  assert(is_start[k] && "returned rotation is not admissible");
  return k;
}

void Tree::get_leaves(Vertex *v, std::list<Vertex *> &leaves) {
  if (v->is_leaf()) {
    leaves.push_back(v);
  }
  else {
    for (Vertex *c : v->children_) {
      get_leaves(c, leaves);
    }
  }
}

int Tree::get_symmetry() {
  if (has_two_centroids()) {
    return get_symmetry_two_centroids();
  }
  else {
    return get_symmetry_one_centroid();
  }
}

int Tree::get_symmetry_two_centroids() {
  assert(root_ == centroids_.first && "tree has to be rooted in the first centroid");
  assert(root_->get_left_child() == centroids_.second && "left child of the root has to be the second centroid");
  // we check whether cutting the edge between the centroids gives us two isomorphic trees
  // second centroid is easy since it is child of the first centroid
  std::list<bool> second_bitlist;
  get_subtree_bitstring(centroids_.second, second_bitlist);
  // first centroid is a bit more complicated -- we need to ignore the first child (second centroid)
  std::list<bool> first_bitlist;
  for (Vertex *c : root_->children_) {
    if (c != centroids_.second) {
      get_subtree_bitstring(c, first_bitlist);
    }
  }
  // add the edge between the centroids
  first_bitlist.push_front(1);
  first_bitlist.push_back(0);

  if (first_bitlist == second_bitlist) {
    return 1;
  }
  else {
    return 0;
  }
}

int Tree::get_symmetry_one_centroid() {
  assert(root_ == centroids_.first && "tree has to be rooted in the first centroid");
  // The plan is as follows: The current order of subtrees of the root is the lexicographically smallest one.
  // We rotate by one subtree, and recompute the smallest rotation. Iff it occurs after one full rotation
  // of all subtrees, then there is no symmetry.
  int first_subtree_size = get_subtree_vertices(root_->get_left_child());
  std::list<bool> tmp;
  get_subtree_bitstring(root_, tmp);
  std::vector<bool> rotated_bitstring;  // bitstring representation of the tree with root's subtrees rotated by one
  std::list<bool>::const_iterator second_subtree_start = std::next(tmp.cbegin(), 2*first_subtree_size);
  rotated_bitstring.insert(rotated_bitstring.cend(), second_subtree_start, tmp.cend());
  rotated_bitstring.insert(rotated_bitstring.cend(), tmp.cbegin(), second_subtree_start);

  std::vector<bool> rotated_subtree_start(2*num_edges_, 0);
  int last_subtree = 2*num_edges_ - 2*first_subtree_size;
  for (Vertex *c : root_->children_) {
    rotated_subtree_start[last_subtree] = 1;
    int size = get_subtree_vertices(c);
    last_subtree += 2*size;
    last_subtree %= 2*num_edges_;
  }

  int smallest_rot = lex_smallest_rotation(rotated_bitstring, rotated_subtree_start);

  // if we got the same rotation as the tree has => no symmetry
  if (smallest_rot == (2*num_edges_ - 2*first_subtree_size)) {
    return 0;
  }

  // there is a symmetry
  // count the number of subtrees we need to rotate
  int out = 1;
  for (int i = 0; i < smallest_rot; ++i) {
    if (rotated_subtree_start[i]) {
      ++out;
    }
  }
  return out;
}

#ifndef NDEBUG
void Tree::draw(const std::string &filename, bool show_centroid) {
  std::ofstream f(filename);
  f << "digraph G{" << std::endl;
  // label
  f << "file[label=\"" << filename << "\"];" << std::endl;
  f << root_->id_ << "[shape=box];" << std::endl;
  // draw centroids
  if (show_centroid) {
    Vertex *v = centroids_.first;
    if(v != nullptr) {
      f << v->id_ << "[color=green];" << std::endl;
    }
    if ((v = centroids_.second) != nullptr) {
      f << v->id_ << "[color=green];" << std::endl;
    }
  }
  draw_subtree(f, root_);
  f << "}" << std::endl;
  f.close();
}

void Tree::draw_subtree(std::ofstream &file, Vertex *v) {
  // give children the same rank
  file << "{rank=\"same\"; ";
  for (Vertex *c : v->children_) {
    file << c->id_ << "; ";
  }
  file << "}";

  for (Vertex *c : v->children_) {
    file << v->id_ << " -> " << c->id_;
    // highlight first child edges
    if (v->get_left_child() == c) {
      file << "[color=red]";
    }
    file << ';' << std::endl;
    draw_subtree(file, c);
  }
  // edge to next sibling
  for (Vertex *c : v->children_) {
    auto sibling_it = std::next(c->parent_list_pos_);
    if (sibling_it != v->children_.end()) {
      file << c->id_ << " -> " << (*sibling_it)->id_
        << "[style=dotted, arrowsize=0.5];" << std::endl;
    }
  }
}
#endif

bool Tree::check_consistency_parent_list_pos() {
  bool out = true;
  for (Vertex *v : id_to_vtx_) {
    if ((v != root_) && (*(v->parent_list_pos_) != v)) {
      std::cout << "inconsistent: " << v->id_ << std::endl;
      std::cout << "points to " << (*(v->parent_list_pos_))->id_ << std::endl;
      out = false;
    }
  }
  return out;
}

Tree::Subtree::Subtree(Tree *tree, Vertex *subtree_root) :
  tree_(tree), root_(subtree_root), bitstring_(), leaves_() {
  num_edges_ = tree_->get_subtree_vertices(root_);  // one extra edge leading to the centroid
  tree->get_subtree_bitstring(root_, bitstring_);
  tree->get_leaves(root_, leaves_);

  // identify initial path in the subtree
  initial_path_end_ = root_;
  initial_path_length_ = 1;

  while (initial_path_end_->children_.size() == 1) {
    initial_path_length_++;
    initial_path_end_ = initial_path_end_->get_left_child(); // first and only child (glory to Tanith!)
  }
}

// initialize special set of trees q_i defined in the paper
const std::vector<std::vector<bool>> Tree::Subtree::Q_TREES = {
  {1,0},                  // q_0   we often use the fact that a subtree equals q0 iff the root of the subtree is a leaf
  {1,1,0,0},              // q_1
  {1,1,0,1,0,0},          // q_2
  {1,1,1,0,0,1,0,0},      // q_3
  {1,1,0,1,0,1,0,0},      // q_4
  {1,1,1,0,1,0,0,1,0,0},  // q_5
  {1,1,1,0,0,1,0,1,0,0},  // q_6
  {1,1,1,0,0,1,1,0,0,0},  // q_7
  {1,1,0,1,0,1,1,0,0,0},  // q_8
  {1,1,0,1,0,1,0,1,0,0},  // q_9
};

// compute number of edges of the largest special tree
// we use lambda as an anonymous function in initialization
const int Tree::Subtree::Q_TREE_MAX_SIZE = []() -> int {
  size_t m = 0;
  for (auto &q : Tree::Subtree::Q_TREES) {
    m = std::max(m, q.size());
  }
  return m/2;
}();

bool lex_less(const std::vector<bool> &x1, const std::vector<bool> &x2) {
  return std::lexicographical_compare(x1.cbegin(), x1.cend(), x2.cbegin(), x2.cend());
}