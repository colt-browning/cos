#!/usr/bin/perl

require "../common.pl";

$BinPath = "../../bin/setp";
$poseDir = "../../bin/pose";    

$KnuthBIN = "$BinPath/KnuthSetPart";
$PosetFILE = "./poset.temp";

MAIN:
{
# Read in all the variables set by the form
  if (&ReadParse(*input)) {
    &ProcessForm;
  }
}

sub InitializePosetFILE {

#if entry blank then set to 0.
if ($input{'n0'}) {$n0 =  $input{'n0'};} else {$n0 = 0;} 
if ($input{'n1'}) {$n1 =  $input{'n1'};} else {$n1 = 0;} 
if ($input{'n2'}) {$n2 =  $input{'n2'};} else {$n2 = 0;} 
if ($input{'n3'}) {$n3 =  $input{'n3'};} else {$n3 = 0;} 
if ($input{'n4'}) {$n4 =  $input{'n4'};} else {$n4 = 0;} 

@size = ($n0,$n1,$n2,$n3,$n4);
     open( FILE, ">$PosetFILE" ) || die "Can't open $PosetFILE:$!\n";
     $N =($input{'n'});
     $count = (1* $size[0])+(2*$size[1])+(3*$size[2])+(4*$size[3])+(5*$size[4]);
     if ($count != $N) {
	&ErrorMessage('Block sizes do not match N');
	&Clean;  
     } 

# Create poset.temp
     $pointer = 1;
     print FILE "$N\n";
     for ($i=0;$i<=5;++$i) {
	for($j=0; $j<$size[$i]; $j++) {
		$temp = $pointer;
		for($k=1; $k <=$i; $k++) {
			print FILE "$pointer ";
			$pointer++;
			print FILE "$pointer\n";
		}
		if (($size[$i] > 1) && ($j <$size[$i]-1)) {
			print FILE "$temp ";
			$temp = $temp+$i+1;
			print FILE "$temp\n";
		}
	$pointer++;
	}
     }
     print FILE "0\n";
     close( FILE );
}
 

sub TableHeaders {
   if ($outformat == 0 ) { &OutError; }
   &TableSetup;
   $xx = ''; # output parameter string
   if ($input{'output1'}) { $xx = $xx.' -R '; &ColumnLabel( "RG function" ); }
   if ($input{'output2'}) { $xx = $xx.' -S '; &ColumnLabel( "Subsets" ); }
   if ($input{'output3'}) { $xx = $xx.' -G '; &ColumnLabel( "(Element, Block)" ); }
   if ($input{'output4'}) { $xx = $xx.' -C '; &ColumnLabel( "Chessboard" ); }
   print "</TR>";
}

sub ExtraNs {
  for ($i=1; $i<=6; $i++) {
    if ($nval[$i]) {
      &ExtraParam("n($i)",$nval[$i]); } }
  }

sub kError {
  &ErrorMessage('A k parameter must be specified');
}

sub OutError {
  &ErrorMessage('The output has not been specified or is not applicable');
}

sub Clean {
  &Cleanup('setp/SetPartitions');
}

sub ProcessForm {

  &OutputHeader('Set Partitions Output','setp/SetPartitions');  # from common.pl

  $outformat = 0;
  if ($input{'output1'}) { $outformat = 1;}
  if ($input{'output2'}) { $outformat = $outformat | 2;}
  if ($input{'output3'}) { $outformat = $outformat | 4;}
  if ($input{'output4'}) { $outformat = $outformat | 8;}

  $nval[1] = $input{'n1'};
  $nval[2] = $input{'n2'};
  $nval[3] = $input{'n3'};
  $nval[4] = $input{'n4'};
  $nval[5] = $input{'n5'};
  $nval[6] = $input{'n6'};

  if ($input{'program'} eq "Gray") {
     print "Set partitions of [<I>n</I>] for <I>n</I> = $input{'n'}";
     print " in Gray code order.<BR>";
     &ExtraNs;
     if ($input{'k'}) { &ExtraParam('k',$input{'k'}); }
     if (int($input{'n'}) > 50) { &TooBig('n',50); &Clean; }
     if (int($input{'n'}) < 1) { &TooSmall('n',1); &Clean; }
     &TableHeaders;
     $retval = system "$KnuthBIN -n $input{'n'} -g $xx -l $LIMIT"; 
     print "</TABLE>\n";
     if ($retval) { &LimitError; }
  
  } elsif ($input{'program'} eq "lex") {
     print "Set partitions of [<I>n</I>] for <I>n</I> = $input{'n'}";
     if ($input{'k'}) { print " into at most <I>k</I> = $input{'k'} blocks"; } 
     else { $input{'k'} = $input{'n'}; }
     print " in lex order.<BR>\n";
     &ExtraNs;
     if (int($input{'n'}) > 50) { &TooBig('n',50); &Clean; }
     if (int($input{'n'}) < 1) { &TooSmall('n',1); &Clean; }
     if ($input{'output3'}) { &OutputIgnored('Gray Code Changes');
       $input{'output3'} = '0'; }
     $outformat = $outformat & 11;
     &TableHeaders;
     $retval = system "$KnuthBIN -n $input{'n'} -k $input{'k'} $xx -l $LIMIT"; 
     print "</TABLE>\n";
     if ($retval) { &LimitError; }
  
  } elsif ($input{'program'} eq "kblockslex") {
     print "Set partitions of [<I>n</I>] for <I>n</I> = $input{'n'}";
     print " into <I>k</I> = $input{'k'} blocks in Pseudo-colex order.<BR>\n";    

     &ExtraNs;

     if (int ($input{'n'}) > 50) { &TooBig('n',50); &Clean; }
     if (int($input{'n'}) < 1) { &TooSmall('n',1); &Clean; }
     if ($input{'output3'}) { &OutputIgnored('Gray Code Changes');
       $input{'output3'} = '0'; }

     $outformat = $outformat & 11;

     if (int($input{'k'}) eq 0 ){ &kError; }

     &TableHeaders;

     $retval = system "$BinPath/kcolex -n $input{'n'} -k $input{'k'} -o $outformat -L $LIMIT";
     print "</TABLE>\n";
     if ($retval) { &LimitError; }

  } elsif ($input{'program'} eq "kblocksGray") {
     print "Set partitions of [<I>n</I>] for <I>n</I> = $input{'n'}";
     print " into <I>k</I> = $input{'k'} blocks in Gray code order.<BR>\n";
     &ExtraNs;
     if (int($input{'n'}) > 50) { &TooBig('n',50); &Clean; }
     if (int($input{'n'}) < 1) { &TooSmall('n',1); &Clean; }
     if (int($input{'k'}) eq 0) { &kError; }
     &TableHeaders;
     $retval = system "$BinPath/kgray -n $input{'n'} -k $input{'k'} -o $outformat -L $LIMIT";
     if ($retval) { &LimitError; }
  
  } elsif ($input{'program'} eq "blocksgiven") {

     print "Set partitions of [<I>n</I>] for <I>n</I> = $input{'n'}";
     print " with block sizes n(1) = $input{'n0'}, n(2) = $input{'n1'}, ";
     print "n(3) = $input{'n2'}, n(4) = $input{'n3'}, ";
     print "n(5) = $input{'n4'}";

     if (int($input{'n'}) > 50) { &TooBig('n',50); &Clean; }
     if (int($input{'n'}) < 1) { &TooSmall('n',1); &Clean; }
     if ($outformat == 0 ) { &OutError; &Clean; }
     $rg=0; $std=0; $gray=0; $rook=0;
     if ($input{'output1'}) { $rg = 1;}
     if ($input{'output2'}) { $std=1;}
     if ($input{'output3'}) { $gray=1;}
     if ($input{'output4'}) { $rook=1;}

     &InitializePosetFILE;
     system "$poseDir/filt";
     $retval = system "$poseDir/genle -p data.new | /usr/bin/perl inverse.pl $size[0] $size[1] $size[2] $size[3] $size[4] $rg $std $gray $rook";
     if ($retval) {&LimitError;} 
 
  }
  
  &WorkingMoreInfo('setp/SetPartitions');
  print "</body></html>\n";
}











