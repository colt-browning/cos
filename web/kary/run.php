<?php
  if (isset($_GET["t"]) && isset($_GET["n"]) && isset($_GET["k"]) && isset($_GET["N"]) && isset($_GET["fmt"]) && isset($_GET["num"]) && isset($_GET["gfx"])) {
    $t = $_GET["t"];
    $fmt = intval($_GET["fmt"]);
    $num = filter_var($_GET["num"], FILTER_VALIDATE_BOOLEAN);
    $gfx = filter_var($_GET["gfx"], FILTER_VALIDATE_BOOLEAN);
    if ($t == 1) {
      $n = min(intval($_GET["n"]), 10);  // hard-wired maximum number of vertices
      $k = min(intval($_GET["k"]), 5);  // hard-wired maximum arity
      $cmd = "code/kary/kary"
                             . " -n" . strval($n)
                             . " -k" . strval($k)
                             . " -l" . strval(limit($fmt))  /* hard-wired maximum number of trees */
                             . " 2>&1"  /* this command redirects stderr to stdout */
                             . (($fmt == 2) ? " > file.txt" : "");  /* redirect output to file */
      exec($cmd, $result);
      output($result, $fmt, "string_to_list", 0, "svg_kary", array(false, 0, false, 0), "animate_kary", array(false, 0), $num, $gfx);
    } else {
      $N = min(intval($_GET["N"]), 20);  // hard-wired maximum number of points
      $cmd = "code/kary/ctria"
                             . " -n" . strval($N)
                             . " -l" . strval(limit($fmt))  /* hard-wired maximum number of trees */
                             . " 2>&1"  /* this command redirects stderr to stdout */
                             . (($fmt == 2) ? " > file.txt" : "");  /* redirect output to file */
      exec($cmd, $result);
      output($result, $fmt, "string_to_coltree", 0, "svg_kary", array(true, $N, false, 0), "animate_kary", array(true, $N), $num, $gfx);
    }
  }
?>

