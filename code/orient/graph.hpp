/* 
 * Copyright (c) 2022 Ondrej Micka, Arturo Merino and Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include<vector>
#include<string>
#include<ostream>

#include "interfaces.hpp"

class Acyclic_orientation : public Algorithm{

  enum class Direction {to_source, to_sink};

public:
  // Init algorithm on the graph given by list of edges and with given perfect elimination order
  // Algorithm assumes PEO is correct and does not check its correctness
  Acyclic_orientation(
    int n, 
    const std::vector<std::string> &vertex_names, 
    const std::vector<std::pair<int,int>> &edge_list,
    const std::vector<int> &peo
  );
  virtual ~Acyclic_orientation() = default;

  virtual bool next();
  friend std::ostream &operator<<( std::ostream &output, const Direction &d );   // allow easy printing of enum
  
  virtual void print_adj_matrix(std::ostream &out) const;
  virtual void print_edge_list(std::ostream &out) const;
  virtual void print_flip(std::ostream &out) const;   // print last flipped edge
  virtual void print_state() const; // Print debug info.
  void print_dot(std::ostream &out) const; // Print orientation using graphviz format

private:
  int n_;   // number of vertices of the underlying graph
  const std::vector<std::string> vertex_names_;   // vertex labels/names given by the user
  std::pair<int,int> flipped_edge;    // last flipped edge; order of the vertices is not significant

  // Underlying graph and its orientation (we don't really distinguish between these two)
  std::vector<std::vector<bool>> adj_matrix_; // adjacency matrix
                        // adj_matrix_[i][j] == true means there is an edge i->j
  const std::vector<std::pair<int,int>> edge_list_;
  std::vector<int> peo_;  // perfect elimination order
  std::vector<int> oep_;  // inverse of peo_ permutation

  // Auxiliary data structures for the algorithm
  // Note: "largest" and "smaller" usually means with respect to the peo_
  std::vector<std::vector<int>> T_arrays_;  // vector of arrays T_i from the algorithm
                        // arrays T_i are 0-indexed, because extra value would mess up the sorting
                        // doubles as in-adjacency list, where only smaller neighbors are included
  std::vector<int> alpha_;  // auxiliary array used to deal with disconnected graphs
                // alpha_[i] is the largest vertex smaller than i such that it is not the smallest vertex in its connected component
                // alpha_[i] == oep_[1] iff no such vertex exists
                // alpha_[i] is zero iff the vertex is smallest in its connected component
  int rho_;         // largest vertex with non-zero alpha
  std::vector<int> s_;        // vertex selection array
  std::vector<Direction> o_;  // zig-zag direction array
  std::vector<int> t_;        // arc selection array

  void flip(int i, int j);  // flip arc between i and j; sets flipped_edge

  // Data structure initialization
  // Needs to be called in this order
  void init_graph();     // init adjacency matrix etc.
  void init_T_arrays();  // build unsorted T arrays
  void init_alpha();     // build array alpha_ and set rho_
  void init_auxiliary();  // initialize s_, o_, t_


  // Utilities
  std::pair<int,int> peo_sort(int i, int j) const ;  // return i,j ordered such that peo_[out.first] > peo_[out.second]
  Direction reverse_dir(Direction d) const {return d == Direction::to_source ? Direction::to_sink : Direction::to_source;};

  // Debug
  void check_consistency() const;   // Check various invariants that should hold throughout the algorithm
  void check_arc_consistency() const; // Check consistency of adj_matrix and T_arrays
  void check_peo_consistency() const; // Check whether peo_ and oep_ are permutations and inverse to each other
                                      // Does not check whether peo_ is really PEO
};
