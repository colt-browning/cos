/* 
 * Copyright (c) 2022 Ondrej Micka, Arturo Merino and Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>
#include <utility>
#include <vector>
#include "interfaces.hpp"
#include "utils.hpp"

// Tools for checking whether a graph is chordal, a sequence is a valid PEO and possibly other related stuff.
class Chordal : public Checker {
  friend class Tests;
public:
  Chordal(int, const std::vector<std::string>&, const std::vector<std::pair<int,int>>&);
  virtual ~Chordal() = default;

  // check if graph is chordal based on lexicographic BFS ordering
  bool is_chordal();
  // check whether the given peo is valid for the graph
  // !!! peo vector must be 1-indexed
  // that is, it must have size n_+1 and peo[0] is dummy element that is ignored
  bool check_peo(const std::vector<int> &peo);

  inline std::vector<int> get_peo() {return is_chordal() ? peo_ : std::vector<int>();}
  void print_peo();  // print perfect elimination ordering

  virtual bool is() {return is_chordal();}
  virtual bool check(const std::vector<int> &peo) {return check_peo(peo);};
  virtual std::vector<int> get() {return get_peo();};
  virtual void print() {print_peo();};

private:
  int n_;  // number of vertices of the graph
  std::vector<std::string> vertex_strings_;  // string representations of vertices

  bool is_chordal_;               // cached value of is_chordal()
  bool chordality_checked_;       // was the chordality of the graph already checked?
                                  // if true, is_chordal() just returns the cached value,
                                  // otherwise it runs the time-consuming calculation

  // the graph
  std::vector<std::vector<int>> adjL_;  // adjacency lists
  std::vector<std::vector<int>> adjM_;  // adjacency matrix
  std::vector<int> peo_;  // perfect elimination order
  std::vector<int> oep_;  // inverse permutation

  // read in edge list and fill adjacency lists and adjacency matrix
  void build_graph(const std::vector<std::pair<int,int>>&);
  // run lexicographic BFS in the graph, recording the ordering of vertices and the corresponding inverse permutation,
  // which is a perfect elimination ordering in the case that the graph is chordal
  void lex_bfs();
  // auxiliary function for the lexicographic BFS
  void split_intervals(int, std::vector<int>&, int, std::vector<int>&);
  // initialize information about largest earlier neighbors in the perfect elimination order
  // max neighbor is the largest earlier neighbor of each vertex in the perfect elimination order
  void calculate_max_neighbor(const std::vector<int> &peo, const std::vector<int> &oep, std::vector<int> &max_neighbor);
  // auxiliary function, same as check_peo, but inverse peo is already given
  bool check_peo(const std::vector<int> &peo, const std::vector<int> &oep);
  // check whether peo calculated for this graph is valid
  inline bool check_peo() {return check_peo(peo_, oep_);};

};
