/****given a value for n and an output mode specified by ProcessInput,
as well as the number in each subset, k:
this program prints out the subsets in order of transpositions*****/

#include <stdio.h>
#include "ProcessInput.c"

/*********globals*********/
int bitstring = 0;
int bitchange = 0;
int list = 0;
int A[MAX];

/************local functions**********/

/*prints out the generated number according to the output format*/
/*if b1 = -1, then no bits were changed*/
void printit(b1,b2)
int b1, b2;
{
  int i,j;
  int beg = 1;

  if(!LIMIT)
    return;
  printf("<TR>");
  if(bitstring) {
  printf("<TD ALIGN=CENTER>");
    for(i=0;i<N;i++)
      printf("%d",A[i]);
    printf("<BR></TD>\n");
  }
  if(list) {
    printf("<TD ALIGN=CENTER>{");
    for(i=N-1;i>=0;i--) {
      if(A[i]) {
	printf("%s%d",beg ? "" : ",",N-i);
	beg = 0;
      }
    }
    printf("}<BR></TD>\n");
  }
  if(bitchange) {
    if(b1!=-1) 
      printf("<TD ALIGN=CENTER>%d,%d<BR></TD>\n",N-b1+1,N-b2+1);
    else
      printf("<TD ALIGN=CENTER><BR></TD>\n");
  }
  LIMIT--; /*count down the number of printable objects*/
}

/*flips the bits that are specified 
and calls the print function*/
void swap(int a,int b)
{
  printit(a,b);
  A[a-1] = (1-A[a-1])%2;
  A[b-1] = (1-A[b-1])%2;
}

/*the following 2 functions call each other
to generate the transposition combinations
*/
void neg(int n, int k);

void trans(int n, int k)
{
   if((0<k)&&(k<n)&&(LIMIT)) {
      trans(n-1,k);
      if(k==1)
	 swap(n,n-1);
      else
	 swap(n,k-1);
      neg(n-1,k-1);
   }
}
void neg(int n, int k)
{
   if((0<k)&&(k<n)&&(LIMIT)) {
      trans(n-1,k-1);
      if(k==1)
	 swap(n,n-1);
      else 
	 swap(n,k-1);
      neg(n-1,k);
   }
}

/***********main program********/

int main(int argc, char **argv)
{
  int i;
  int count;

  ProcessInput(argc, argv);
  count = LIMIT;

  if(out_format & 1)
    bitstring = 1;
  if(out_format & 4)
    bitchange = 1;
  if(out_format & 2)
    list = 1;

  /*initialize the array A*/
  for(i=0;i<K;i++)
    A[i] = 1;
  for(i=K;i<N;i++)
    A[i] = 0;
   
  trans(N,K);
  printit(-1);
  printf("</TABLE><BR>Total number of combinations = %d<BR>\n",count-LIMIT);
  if(LIMIT)
    return(0);
  else
    return(-1);
}
