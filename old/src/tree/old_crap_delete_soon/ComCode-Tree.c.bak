/********************************
 *  Program:  ComCode-Tree.c
 *  Author:  Chris Deugau
 *
 *  Description:  Using paper in IEEE Transactions on Info Theory
 *                from Oct 1967, using the recursive T(2s, q+s) to
 *                recursively generate the tree from the bottom to the top.
 *
 *  Started:  May 9th, 2005
 *
 *  Update History:
 *     May 9th - started  (CJD)
 *     May 11th - added handling for height and keys
 ********************************/

#include <stdio.h>
#include <stdlib.h>
#include "OutputTrees.c"

#define PRECOMPUTE_SIZE 100
#define MAX_N 50
#define MAXKIDS 2

int a1[PRECOMPUTE_SIZE+1];
int p1[PRECOMPUTE_SIZE+1];
int height;
int testingOutput;
int methodCalls;

int outputArray[MAX_N+1];
int tmpOutputArray[MAX_N+1];
int outputArrayCurr;

/*************** STRUCTS FOR TREE **************/
Node *makeNode( void );
void levelize(Node *root, Node *x, int y);
/*********************************************/

/*********** STRUCTURES FOR LIST *************/
typedef struct listhead {
  int height;
  int currArray[MAX_N+1];
  int size;

  struct listptr *head;
  struct listptr *tail;
} List;

typedef struct listptr {
  Node *node;

  struct listptr* next;
} ListNode;

List *makeList( void );
ListNode *makeListNode( void );
void insert(List *lst, Node *nodePar);
/********************************************/

// precompute precomputes the a1(n) and p1(n) arrays for testing what the
// first q column a one occurs for every p.
void precompute();

// recursive call to generate trees with p bottom level nodes, and q leaves
// currList will contain a list of the trees created so far
void T(int pPar, int qPar, List *currList);

/**********************/
void levelize(Node *root, Node *x, int level) {
  int i;

  if (x == NULL) {
    return;
  }

  if (level > 0) x->father = root;

  x->height = level;
  levelize(x, x->son[0], level+1);
  levelize(x, x->son[1], level+1);
}

/**********************/
void precompute() {
  int i;

  /* base cases for a1(n) */
  a1[1] = 1; a1[2] = 1; a1[3] = 2;
  p1[1] = 0; p1[1*2] = 1+1; p1[2*2] = 3+1;

  for (i=4; i <= PRECOMPUTE_SIZE; i++) {
    a1[i] = a1[i - 1 - a1[i-1]] + a1[i - 2 - a1[i-2]];

    // if a1[i-1] != a1[i], then we can update p1[a1[i]] = i
    if (a1[i-1] != a1[i]) {
      // i+1 gives us the first column q with a 1.
      p1[a1[i] * 2] = i+1;
    }
  }
}
/**********************/
void T(int pPar, int qPar, List *currList) {
  int i;
  int s,q,p;
  int oldOutputArray;

  List *newList;
  Node *newNode;
  ListNode *currListItr, *currListNext;
  DimStruct dim;

  methodCalls++;

  /***** BASE CASES *****/
  if ((pPar == 1) && (qPar == 1)) {
    // combine the left and right tree into the root node, 
    // and print out the finished tree
    currListItr = currList->head;
    newNode = makeNode();
    newNode->key = 1;
    newNode->father = NULL;

    newNode->son[1] = currListItr->node;
    currListItr = currListItr->next;
    newNode->son[0] = currListItr->node;
    currListItr = currListItr->next;

    levelize(newNode,newNode,0);
    for (i=N; i >= 1; i--) { tmpOutputArray[i] = height - outputArray[i]; }

    if (testingOutput) {
      num++; 
      //      for (i=N; i >= 1; i--) { printf("%d ", tmpOutputArray[i]); }
      // printf(" x\n");
    }
    else {
      PrintIt(N, tmpOutputArray, (int *) newNode);
    }

    free (newNode);

    return;
  }

  if ((pPar % 2) != 0) {
    // no trees with an odd number of blns
    return;
  }

  if (qPar < p1[pPar]) {
    // since p1[pPar] = first column q with a 1 in it, if qPar is
    // less than p1[pPar], there are no trees
    return;
  }
  /***** END OF BASE CASES *****/


  newList = makeList();
  currListItr = currList->head;

  for (i=0; i < pPar; i++) {
    newNode = makeNode();
    newNode->height = height;

    if (currListItr == NULL) {
      newNode->key = 0;
      newNode->son[0] = NULL;
      newNode->son[1] = NULL;
      insert(newList, newNode);
      outputArrayCurr++;
      outputArray[outputArrayCurr] = height;
    }
    else {
      newNode->key = 1;
      newNode->son[1] = currListItr->node;
      currListItr = currListItr->next;

      if (currListItr != NULL) {
	newNode->son[0] = currListItr->node;
	currListItr = currListItr->next;
      }

      insert(newList, newNode);
    }
  }

  s = pPar / 2;
  q = qPar - s;

  oldOutputArray = outputArrayCurr;

  for (p=s; p <= q; p++) {
    if ((p % 2 == 0) || ((p == 1) && (q == 1))) {
      outputArrayCurr = oldOutputArray;
      height++;
      T(p, q, newList);
      height--;
    }
  }

  currListItr = newList->head;

  while (currListItr != NULL) {
    currListNext = currListItr->next;
    free(currListItr);
    currListItr = currListNext;
  }

  free(newList);
  free(newNode);
}

/*********************************************/
List *makeList( void ) {
  List *newList;

  newList = (List *) malloc(sizeof(List));
  if (!newList) {
    printf("Error Allocating List.  Wah wah.  sniffle.\n");
    exit(1);
  }

  newList->height = 0;
  newList->size = 0;
  newList->head = NULL;
  newList->tail = NULL;
}

ListNode *makeListNode( void ) {
  ListNode *newListNode;

  newListNode = (ListNode *) malloc(sizeof(ListNode));
  if (!newListNode) {
    printf("Error Allocating ListNode.  Wah wah.  sniffle.\n");
    exit(1);
  }

  newListNode->next = NULL;

  newListNode->node = NULL;
}

void insert(List *lst, Node *nodePar) {
  ListNode *newListNode = makeListNode();

  newListNode->node = nodePar;

  if (lst->size == 0) {
    lst->size++;
    lst->head = newListNode;
    lst->tail = newListNode;
    newListNode->next = newListNode;
  }
  else {
    lst->size++;
    lst->tail->next = newListNode;
    lst->tail = newListNode;
  }
}
/*****************************************/


int main(int argv, char **argc) {
  int i;
  List *lst;

  ProcessInput(argv, argc);

  if (out_format == 101) testingOutput = 1;
  if (out_format & 1) outputCC = 1;
  if (out_format & 8) outCCGif = 1;

  lst = makeList();
  precompute();

  methodCalls = 0;

  for (i=2; i <= N; i=i+2) {
    outputArrayCurr = 0;
    height = 0;
    T(i,N, lst);
  }

  if (out_format != 101) {
    printf("</table>");
    printf("<P>The total number of trees is:  %d\n", num);
  }

  if (out_format == 101) {
    printf("Total Number of Trees:  %d\n", num);
    printf("The total number of calls:  %d\n", methodCalls);
  }

  return 0;
}











