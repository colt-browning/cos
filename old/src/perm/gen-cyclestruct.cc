//
// This program generates all permuatations of [n] with cycle 
// structure specified in lambda array  
//
// Program modified to accept command line arguments

#include <stdio.h>
#include <string.h>
#include <ctype.h>
#include <stdlib.h>
#include "ProcessInput.c"
#include "commonio.h"

void swap(int, int);
void cycle(int);
void xchg(int, int);
int process_list(char *);

int NN, KK;
int main(int argc, char *argv[])
{
	int i;
	int sum;
	int input;
	
	// Initialize the lambda array
	for(i=0;i<MAX;i++) lambda[i] = 0;

	// See if we got any command line arguments
	ProcessInput(argc, argv);
	NN = N; KK= K;
	// Now do a quick check to make sure that the numbers add
	
	calls = 0;
	for(i=0; i < NN; i++) { iP[i] = i; Pi[i] = i; }
	cycle(0);
	printf("</TABLE>");	
	printf("<P><B> Permutations = %d </B></P>",count);
	
}


void Swap(int i, int j) {
	int temp;
	temp  = Pi[i]; Pi[i] = Pi[j]; Pi[j] = temp;
	iP[Pi[j]] = j;
	iP[Pi[i]] = i;
}

void xchg(int i, int j) {
	Swap(iP[i], iP[j]);
	Swap(i, j);
}

	
void cycle(int n)
{
	int k, length,i;
	int counter[MAX];
	int firstime = 1;
	int done;

	calls++;
	if (n >= NN) { 
		PrintIt();
		}
	else {
	    for(length = 1; length <= NN; length++) {
		if ((length == 1) && (lambda[1] > 0)) {lambda[1]--; cycle(n+1); lambda[1]++;}  // This is for 1 cycles
		else if((length == 2) && (NN-n >= 2) && (lambda[2] > 0)) {
		    // 2 Cycle stuff here
		        lambda[2]--;
			Swap(n, n+1);
			cycle(n+2);
			// Loop through and replace ....
			for(i=n-1;i >=0; i--) {
				if (Pi[i]==i) {	// We're swapping out a singleton
					Swap(i, n); Swap (n, n+1);
					cycle(n+2);
					Swap(n, n+1); Swap(i, n);
				 }
				 else 
					{
					// We're swapping 2-cycles
					Swap(Pi[i], n); Swap(i, n+1);
					cycle(n+2);
					Swap(i, n+1); Swap(Pi[i],n);
				    }
			}
			Swap(n, n+1);
			lambda[2]++;
		}
		else  // Now worry about cycles of length >2 
		    if ((NN-n >=length) && (lambda[length] > 0)) {
			lambda[length]--;
			firstime = 1;
			// Make a generic (length)-cycle
			for(k=1; k < length; k++) Swap(n+(k-1), n+k);
			
			// Now do the shuffle
			for(k=0; k< length; k++) counter[k] = n+k; // Initialize
			done = 0; // Initialize termination flag
			while (!done) {  // This should emulate the basic nested for-loop
				k = 0;
				while(counter[k] <= 0)
					{
					  xchg(n+k, 0); 
					  counter[k] = n+k;
					  k++;
					}
				if (k> length-2) {
				    done = 1; // Done if finished processing
				} else {
				   if (!firstime) {
					xchg(n+k, counter[k]);
					counter[k]--;
					if (counter[k] >= 0) {
						xchg(n+k, counter[k]);
						cycle(n+length);
						}
					}
			        	else {
					  firstime = 0;
					  cycle(n+length);
					}
				}
			}
			lambda[length]++;
			for(k=(length-1); k>=1; k--) Swap(n+(k-1), n+k); // Restore
		    }
	    }

	}
}

#include "commonio.cc"





