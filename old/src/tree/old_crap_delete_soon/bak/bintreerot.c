#include "ProcessInput.c"
#include "OutputTrees.c"

#define NULL 0
#define false 0
#define true  1

typedef struct cell {
  long data;
  struct cell *left, *right, *parent;
  int dtype;
} cell;


cell *treelist[30];
long dir[30];
cell *tree;
long n, c;
int L[50], count;

void preorder(tree)
cell *tree;
{
  if (tree == NULL) {
    L[count++] = 0;
    return;
  }
  L[count++] = 1;
  preorder(tree->left);
  preorder(tree->right);
}


void writetree()
{
  c++;
  count = 1;
  preorder(tree);
  PrintIt(2*N, L, L);
}


int extreme(i)
long i;
{
  if (dir[i - 1] == -1)
    return (treelist[i - 1]->left == NULL);
  else
    return (treelist[i - 1]->dtype || treelist[i - 1]->parent == NULL);
}

void rotateleft(i)
long i;
{
  if (treelist[i - 1]->parent->parent == NULL)
    tree = treelist[i - 1];
  else if (treelist[i - 1]->parent->dtype)
    treelist[i - 1]->parent->parent->left = treelist[i - 1];
  else
    treelist[i - 1]->parent->parent->right = treelist[i - 1];
  treelist[i - 1]->dtype = treelist[i - 1]->parent->dtype;
  treelist[i - 1]->parent->right = treelist[i - 1]->left;
  if (treelist[i - 1]->parent->right != NULL) {
    treelist[i - 1]->parent->right->parent = treelist[i - 1]->parent;
    treelist[i - 1]->parent->right->dtype = false;
  }
  treelist[i - 1]->left = treelist[i - 1]->parent;
  treelist[i - 1]->parent = treelist[i - 1]->parent->parent;
  treelist[i - 1]->left->parent = treelist[i - 1];
  treelist[i - 1]->left->dtype = true;

}

void rotateright(i)
long i;
{
  cell *s;

  if (treelist[i - 1]->parent == NULL)
    tree = treelist[i - 1]->left;
  else if (treelist[i - 1]->dtype)
    treelist[i - 1]->parent->left = treelist[i - 1]->left;
  else
    treelist[i - 1]->parent->right = treelist[i - 1]->left;
  treelist[i - 1]->left->dtype = treelist[i - 1]->dtype;
  treelist[i - 1]->left->parent = treelist[i - 1]->parent;
  treelist[i - 1]->dtype = false;
  treelist[i - 1]->parent = treelist[i - 1]->left;
  s = treelist[i - 1]->left->right;
  if (s != NULL) {
    s->parent = treelist[i - 1];
    s->dtype = true;
  }
  treelist[i - 1]->left->right = treelist[i - 1];
  treelist[i - 1]->left = s;
}


void next(pos)
long pos;
{
  /*of next*/
  if (pos > n) {
    writetree();
    return;
  }
  next(pos + 1);
  while (!extreme(pos)) {
    if (dir[pos - 1] == -1)
      rotateright(pos);
    else
      rotateleft(pos);
    next(pos + 1);
  }
  dir[pos - 1] = -dir[pos - 1];
}


void initialize()
{
  long i;
  cell *s;
  long FORLIM;

  tree = (cell *)malloc(sizeof(cell));
  treelist[0] = tree;
  tree->parent = NULL;
  tree->left = NULL;
  tree->data = 1;
  tree->dtype = true;
  s = tree;
  dir[0] = 1;
  FORLIM = n;
  for (i = 2; i <= FORLIM; i++) {
    dir[i - 1] = 1;
    s->right = (cell *)malloc(sizeof(cell));
    s->right->parent = s;
    s = s->right;
    s->dtype = false;
    s->left = NULL;
    treelist[i - 1] = s;
    s->data = i;
  }
  s->right = NULL;
  c = 0;
}


main(argc, argv)
int argc;
char *argv[];
{
  ProcessInput(argc, argv);
  n = N;
  if (out_format&1) outputL = 1;
  if (out_format&2) outputB = 1;
  initialize();
  next(1L);
  printf("</table><P>The Total number of Trees is :%12ld<BR>\n", c);
  return(0);
}

