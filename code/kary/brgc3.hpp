/*
 * Copyright (c) 2024 Rohan Acharya, Torsten Muetze, and Francesco Verciani
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <vector>
#include "brgc3.hpp"
#include "graycode.hpp"

// a Hamilton path in the n-dimensional hypercube from 0^n to 1110^{n-3}
// obtained by gluing together two BRGCs in dimension n-1 and cyclically shifting them
class Brgc3 : public Graycode {
public:
  Brgc3(int n);
  bool next(int &pos);  // pos stores the bit position flipped
  void print();  // print bitstring
  
private:
  int n_;  // length of bitstring
  std::vector<int> x_;  // bitstring
  Brgc brgc0_, brgc1_;
};
