/*
 * Copyright (c) 2018 Petr Gregor, Torsten Muetze, Jerri Nummenpalo
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <algorithm>
#include <cassert>
#include <iostream>
#include <vector>
#include "mlc_hamcycle.hpp"
#include "mlc_tree.hpp"
#include "mlc_vertex.hpp"

// ####################
// The functions in this file are taken from our previous paper
// [Muetze, Nummenpalo, "A constant-time algorithm for middle levels Gray codes"]
// with some straightforward adaptions.
// ####################

HamCycle::HamCycle(int k, long long limit, long long skip_start, AbstractCycle<std::vector<int> >* caller, bool visit) :
  AbstractCycle(2*k+1, limit, 0), skip_start_(skip_start), caller_(caller), visit_(visit) {
  // ############################
  // ### Initialization phase ###
  // ############################

  // initialize the starting/current vertex in Q_{2k}(k,k+1)\circ 0
  x0_ = Vertex(std::vector<int> (n_, 0));
  for (int i = 0; i < k; ++i) {
    x0_[i] = 1;
  }
  x_ = x0_;

  // maintain adjacency list representation of the tree
  // that corresponds to the current vertex x_ throughout the algorithm
  Tree x_tree(x_);

  length_ = 0;

  // ##################################
  // ### Hamilton cycle computation ###
  // ##################################
  std::vector<int> seq;
  std::vector<int> seq01;
  seq01.push_back(2*k);  // flip sequence that flips only the last bit
  while (true) {
    // #################################################
    // follow the path in the graph Q_{2k}(k,k+1)\circ 0
    // #################################################
    bool flip = x_tree.flip_tree();  // tau() or tau_inverse() is applied inside the function call
    x_tree.rotate();

    // compute flip sequence
    x_.compute_flip_seq_0(seq, flip);

    // apply flip sequence
    assert(x_.is_first_vertex());
    if (flip_seq(seq)) {
      break;
    }
    assert(x_.is_last_vertex());

    // flip last bit to jump to the graph Q_{2k}(k-1,k)\circ 1
    if (flip_seq(seq01)) {
      break;
    }
    assert(x_[2*n] == 1);

    // #################################################
    // follow the path in the graph Q_{2k}(k-1,k)\circ 1
    // #################################################
    // compute transformed flip sequence directly
    x_.compute_flip_seq_1(seq);

    // apply flip sequence
    assert(x_.is_last_vertex());
    if (flip_seq(seq)) {
      break;
    }
    assert(x_.is_first_vertex());

    // flip last bit to jump to the graph Q_{2k}(k,k+1)\circ 0
    if (flip_seq(seq01)) {
      break;
    }
    assert(x_[2*k] == 0);
  }
}

bool HamCycle::flip_seq(const std::vector<int>& seq) {
  if ((length_ + seq.size() > limit_) || ((skip_start_ > 0) && (seq.size() > skip_start_))) {
    // apply only part of the flip sequence
    for (int j = 0; j < seq.size(); ++j) {
      int i = seq[j];
      x_[i] = 1 - x_[i];
      ++length_;
      if (skip_start_ > 0) {
        --skip_start_;
      } else {
        if (visit_) {
          caller_->visit_f_visit(i);
        } else {
          caller_->visit_f_log(i);
        }
      }
      if (length_ == limit_) {
        return true;  // terminate Hamilton cycle computation prematurely
      }
    }
  } else {
    // highspeed loop without case distinctions
    // apply the entire flip sequence
    for (int j = 0; j < seq.size(); ++j) {
      int i = seq[j];
      x_[i] = 1 - x_[i];
    }
    length_ += seq.size();
    if (skip_start_ > 0) {
      skip_start_ -= seq.size();
    } else {
      for (int j = 0; j < seq.size(); ++j) {
        int i =  seq[j];
        if (visit_) {
          caller_->visit_f_visit(i);
        } else {
          caller_->visit_f_log(i);
        }
      }
    }
    if (length_ == limit_) {
      return true;  // terminate Hamilton cycle computation prematurely
    }
  }
  return false;  // continue Hamilton cycle computation
}
