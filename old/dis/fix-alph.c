/*-----------------------------------------------------------
This is a simple algorithm for generating necklaces where the
number of each alphabet symbol is fixed. The necklaces
are produced in reverse lex order.  If you want to generate
Lyndon words, then replace the comparison (n%p)==0 with n==p.

This algorithm is described in the paper "A fast algorithm to 
generate necklaces with fixed contenet" by Joe Sawada.

This program, was obtained from the (Combinatorial) Object Server,
at http://www.theory.cs.uvic.ca.
The program can be modified, translated to other languages, etc., 
so long as proper acknowledgement is given (author and source).   

Developed by: Joe Sawada 2001-2002.
/*-----------------------------------------------------------*/
#include <stdio.h>

typedef struct cell {
        int next,prev;
} cell;

cell avail[50];
int num[50];
int a[50];
int run[50];
int n,k,total,head;

void Remove(int i) {

        int p,n;
	
	if (i == head) head = avail[i].next;
        p = avail[i].prev;
        n = avail[i].next;
        avail[p].next = n;
        avail[n].prev = p;
}

void Add(int i) {

        int p,n;

        p = avail[i].prev;
        n = avail[i].next;
        avail[n].prev = i;
        avail[p].next = i;
	if (avail[i].prev == k+1) head = i;
}

/*-----------------------------------------------------------*/
void Print() {

	int j;
	total++;
	for(j=1; j<=n; j++) printf("%d ",a[j]);
	printf("\n");         
}

/*-----------------------------------------------------------*/
void Gen(int t, int p, int s) {
	
	int j, s2;

	if (num[k] == n-t+1) {
		if ((num[k] == run[t-p]) && (n%p == 0)) Print();	
		else if  (num[k] > run[t-p]) Print();	
	}
	else if (num[1] != n-t+1) { 
		j = head;
		s2 = s;
		while( j >= a[t-p]) {

			run[s] = t-s;  
			a[t] = j; 

			num[j]--;
			if (num[j] == 0) Remove(j);

			if (j != k)  s2 = t+1;
			if (j == a[t-p]) Gen(t+1,p,s2);
			else Gen(t+1,t,s2); 

			if (num[j] == 0) Add(j);
			num[j]++;

			j = avail[j].next;
		}
		a[t] = k;
}	}

/*-----------------------------------------------------------*/
int main() {

	int j;

	printf("enter n k: "); scanf("%d %d", &n, &k); 
	for (j=1; j<=k; j++) {
                printf("  enter # of %d's: ", j);
                scanf("%d", &num[j]);
        } 

	for (j=k+1; j>=0; j--) {
		avail[j].next = j-1;
		avail[j].prev = j+1;
	}
	head = k;

	for (j=1; j<=n; j++) {
		a[j] = k;
		run[j] = 0;
	}

        total = 0;
	a[1] = 1;
	num[1]--;
	if (num[1] == 0) Remove(1);
	Gen(2,1,2);

	printf("Total = %d\n", total);
}
