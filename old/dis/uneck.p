(**********************************************************************)
(* Pascal program to generate UNLABELED necklaces, prenecklaces and   *)
(* Lyndon words . This program, was obtained from the (Combinatorial) *)
(* Object Server at http://theory.cs.uvic.ca  The inputs are n,    *)
(* the length of the string and density, the maximum number of        *)
(* non-0's in the string.                                             *)
(* The program can be modified, translated to other languages, etc.,  *)
(* so long as proper acknowledgement is given (author and source).    *)
(* The algorithm is CAT and was developed by Joe Sawada, by modifying *)
(* Frank Ruskeys recursive necklace algorithm.			      *)
(* Programmer: Joe Sawada, 1997.                                      *)
(**********************************************************************)

program necklace ( input, output );

var a : array [0..100] of integer;
    n : integer;
    option,density : integer;

  procedure PrintIt( p : integer );
  var i : integer;
  begin
     if (option = 0) or ((option = 1) and (n mod p = 0)) or
        ((option = 2) and (p = n)) then begin
        for i := 1 to n do write( a[i]:2 );  writeln;
     end else
     if ((option = 3) and (n mod p = 0)) then begin
        for i := 1 to p do write( a[i]:2 );  writeln;
     end;
  end;

  procedure gen ( t,p,c,ones : integer );
  begin
     if ones <= density then 
       if t > n then PrintIt( p )
       else begin
	 if a[t-c] = 0 then begin
           if a[t-p] = 0 then begin
             a[t] := 0;
             gen(t+1, p, t, ones);
           end;
           a[t] := 1;
           if a[t-p] = 1 then gen(t+1, p, c, ones+1)
           else gen(t+1, t, c, ones+1);
         end
         else begin
           a[t] := 0;
           gen(t+1, p, c, ones);
         end;
       end;
  end {of gen};

begin
  write( 'Enter n, density: ' );  readln( n,density );
  write( 'pre-necklace = 0, necklace = 1, lyndon = 2: ' );
  readln( option );
  a[0] := 0; a[1] := 0;
  gen( 2,1,1,0 );
end.


