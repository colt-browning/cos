#include <cassert>

#include "hamcycle.hpp"
#include "vertex.hpp"

HamCycle::HamCycle(const Vertex& x, long long limit, bool quiet, long long& length) : x0_(x), x_(x), limit_(limit), quiet_(quiet)
{
  // initialize
  length = 0;
  x_ = x0_;
  Direction dir = Direction::right;

  do {
    if ((limit_ >= 0) and (length == limit_)) {
      std::cout << "output limit reached" << std::endl;
      break;
    }

    if (!quiet_) {
      // visit current vertex
      x_.print();
    } else if (length % quiet_dot == 0) {
      std::cout << "." << std::flush;
    }

    length++;

    if (dir == Direction::right) {
      // special connector steps
      if (x_.is_x_vertex_connector()) {
        x_.edge_x_y();
        dir = Direction::left;
      } else if (x_.is_y_vertex_connector()) {
        x_.edge_y_x();
        dir = Direction::left;
      // moving along the cycle factor
      } else {
        x_.right_shift();
      }
    } else {
      assert(dir == Direction::left);
      // special connector steps
      if (x_.is_sigma_x_vertex_connector()) {
        x_.edge_sigma_x_sigma_y();
        dir = Direction::right;
      } else if (x_.is_sigma_y_vertex_connector()) {
        x_.edge_sigma_y_sigma_x();
        dir = Direction::right;
      // moving along the cycle factor
      } else {
        x_.left_shift();
      }
    }
  } while (x_.get_bits() != x0_.get_bits());
}
