
/*===================================================================*/
/* C program for distribution from the Combinatorial Object Server.  */
/* Generate subsets in colex order. This                             */
/* is the same version used in the book "Combinatorial Generation."  */
/* The program can be modified, translated to other languages, etc., */
/* so long as proper acknowledgement is given (author and source).   */  
/* Programmer: Joe Sawada, 1997.                                     */
/* The latest version of this program may be found at the site       */
/* http://theory.cs.uvic.ca/inf/comb/SubsetInfo.html                    */
/*===================================================================*/

#include <stdio.h>

int n;
int b[50];
    
void PrintIt() {

	int i, first;

	for (i=1; i<=n; i++) printf("%d ",b[i]);
	printf("  {"); 
	first = 1;
	for (i=1; i<=n; i++) {
		if ( b[i] ==1) {
			if (first == 0) printf(",");
			first = 0;
			printf("%d",i);
		}
	}
	printf("}\n");
}

void SubLex(int N) {	

	if (N == 0) PrintIt();
  	else { 
     		b[N] = 0;  SubLex( N-1 );  
     		b[N] = 1;  SubLex( N-1 );  
  	} 
}


void main() {

  	printf( "Enter n: " );  scanf("%d", &n );
  	SubLex( n );
}

