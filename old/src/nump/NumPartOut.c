/* Generating Numerical Partitions */

#include <stdio.h>
#include <string.h>
#include "ProcessInput.c"

extern int out_format; /* */
extern int LIMIT;      /* max objects allowed */

#define TRUE	1
#define FALSE	0
#define max(x,y) ( x<y ? y : x )
#define min(x,y) ( x<y ? x : y )
#define LIMIT_ERROR  -1

#define nat_out    1
#define mult_out   2
#define gray_out   4
#define ferrer_out 8

int	p[100];
int	count;

void PrintFerrer ( int KK )
{ int i,j;
  
  printf("<TABLE BORDER=1 CELLPADDING=0 CELLSPACING=0>\n");
  for (i=(M==0?2:1); i<=KK; ++i){
     printf("<TR>");
     for (j=1; j<=p[i]; ++j) 
        printf("<TD><IMG SRC=ico/redball.gif></TD>");
     printf("</TR>\n");
  }
  printf("</TABLE>\n");
}

void PrintIt ( int KK )
{ int i, mult;

  ++count;
  if (count <= LIMIT) {
     printf("<TR>\n");
     if (out_format & nat_out) {
        printf("<TD ALIGN=CENTER>");
        for (i=(M==0?2:1); i<=KK; printf(" %2d ", p[i++]));
        printf("<BR></TD>\n");
     }
     if (out_format & mult_out) {
        printf("<TD ALIGN=CENTER>");
        printf("[%d,",p[M==0?2:1]);  mult = 1;
        for (i=(M==0?3:2); i<=KK; ++i) {
           if (p[i] == p[i-1]) ++mult;
           else { printf("%d][%d,",mult,p[i]);  mult = 1; }
        }   
        printf("%d]",mult);
        printf("<BR></TD>\n");
     }
     if (out_format & gray_out) {
        printf("<TD ALIGN=CENTER>");
        printf("not yet");
        printf("<BR></TD>\n");
     }
     if (out_format & ferrer_out) {
        printf("<TD ALIGN=CENTER>");
        PrintFerrer( KK );
        printf("</TD>\n");
     }
     printf("</TR>\n");
  } else { printf("</TABLE>"); exit(LIMIT_ERROR); }
}


/* don't need this now, but will keep it in comment in case
   something goes wriong.

void ProcessInput (int argc, char *argv[])
{  int i;
   nat_out = mult_out = ferrer_out = gray_out = FALSE;
   N = 0;  M = 0;  K = 0;
   for (i=1; i<argc; i++) {
       if (strcmp(argv[i], "-n") == 0) {
          if (i+1 < argc) { N = atoi(argv[++i]); }
       }
       else if (strcmp(argv[i], "-k") == 0) {
          if (i+1 < argc) { K = atoi(argv[++i]); }
       }
       else if (strcmp(argv[i], "-m") == 0) {
          if (i+1 < argc) { M = atoi(argv[++i]); }
       }
       else if (strcmp(argv[i], "-a") == 0) nat_out = TRUE;
       else if (strcmp(argv[i], "-u") == 0) mult_out = TRUE;
       else if (strcmp(argv[i], "-y") == 0) gray_out = TRUE;
       else if (strcmp(argv[i], "-e") == 0) ferrer_out = TRUE;
       else if (strcmp(argv[i], "-l") == 0) {
          if (i+1 < argc) { LIMIT = atoi(argv[++i]); }
       }
       else printf("Spurious input data: %s\n", argv[i]);
   }
}

*/
