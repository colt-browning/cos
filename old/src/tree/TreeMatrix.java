/*Code by Michael Miller*/
/*
 * Takes in a level representation of a rooted tree and returns a matrix which positions
 * the elements of the tree into their relative positions
 * ex 0 1 2 2 1 2 returns
 * 1 0 0 0 0 0
 * 2 5 0 0 0 0
 * 3 4 6 0 0 0
 * 0 0 0 0 0 0
 * 0 0 0 0 0 0
 * 0 0 0 0 0 0
 * From here we can see that each non-zero value in the array corresponds to a node in the tree and its parent is the
 * value lower and above the current value
 */
import java.util.Scanner;

public class TreeMatrix {

	public static int[][] treeArray(String levelIn) {
		int debug = 0;
		int[] values = new int[50];
		int[] iterators = new int[50];
		int x = 0;
		int maxlevel = 0;
		int rightmost = 0;
		Scanner sc = new Scanner(levelIn);

//As we scan through we are going to find the max level and the max width
		while(sc.hasNext())
		{
			values[x++] = sc.nextInt();

			if(values[x-1]>maxlevel)
				maxlevel = values[x-1];
		}

		//this is the array which will represent the tree, right now I am marking with ints, maybe that just mark the parent? do I need 2?
		if(x<4)
		{
			x = 4;
		}
		
		int[][] treeArray = new int[x][x];


		if(debug ==1)
			System.out.println(x+"\n"+levelIn+"\n");

		treeArray[1][0] = x;
		//iterate through the data we bring in
		for(int z = 0; z<x; z++)
		{
			//currentLevel = values[z];
			treeArray[ iterators[ values[z] ]++ ] [ values[ z ] ] = z+1;

			//we adjust lower rows to make sure that the iterators are moved to a more representative position
			for(int q = 1; q<50; q++)
			{
				if(iterators[q-1]>iterators[q])
					iterators[q]= iterators[q-1]-1;
			}
		}

		for(int z = 0; z<x; z++)
		{
			for(int q = 0; q<x; q++)
			{
				if(treeArray[q][z]!=0)
				{
					if(q > rightmost)
						rightmost = q;
				}
			}
		}
		treeArray[2][0] = maxlevel;
		treeArray[3][0] = rightmost;

		if(debug == 1)
		{
			for(int z = 0; z<x; z++)
			{
				for(int q = 0; q<x; q++)
				{
					System.out.print(treeArray[q][z]+ " ");
				}
				System.out.println();
			}
		}
		if(debug == 1)
			System.out.println("Testing Output");
		return treeArray;
	}

	/*
	public static void main(String[] args){
		Scanner sc = new Scanner(System.in);
		String y = sc.nextLine();
		int[][] testSet = treeArray(y);
	}
*/
}
