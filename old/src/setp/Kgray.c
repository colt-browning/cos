/****given a value for n and an output mode specified by ProcessInput,
as well as the number of blocks, k:
this program prints out the set partitions in Gray code order.*****/

#include <stdio.h>
#include "../ProcessInput.c"

#define ICON printf("ico/");
#define kodd k%2
#define keven !(k%2)

/*********globals*********/
int bitstring = 0;
int bitchange = 0;
int list = 0;
int chess = 0;
int A[MAX];

/************local functions**********/

/*prints out the generated number according to the output format*/
/*if b1 = -1, then no bits were changed*/
void printit(b1,b2)
int b1, b2;
{
  static int gray;	/* the bit that changes  in bitsting */
  static int flag;  
  int i,j;
  int beg, pos;

  if(!LIMIT)
    return;
  printf("<TR>");
  if(bitstring) {
    printf("<TD ALIGN=CENTER>");
    for(i=0;i<N;i++) {
      if ( (flag == 1) && (i == gray)) {
        printf("<FONT COLOR=00FF00>");
      }
      printf("%d",A[i]);
      if ( (flag == 1) && (i==gray)) {
        printf("</FONT>");
      } 
    }
    printf("<BR></TD>\n");
  }

  if(list) {
    printf("<TD ALIGN=CENTER>{");
    for (j=0; j<N; j++) {
      beg = 1;
      for(i=0; i<N; i++) {
        if(A[i]==j) {
	  printf("%s%d",beg&&j ? "},{" : "",i+1);
	  beg = 0;
        }
      }
    }
    printf("}<BR></TD>\n");
  }
  if(bitchange) {
    if(b1!=-1) 
      printf("<TD ALIGN=CENTER>(%d,%d)<BR></TD>\n",b1+1,b2);
    else
      printf("<TD ALIGN=CENTER><BR></TD>\n");
  }

  if(chess) {
    printf("<TD ALIGN=CENTER>");
    printf("<TABLE BORDER=1 CELLPADDING=0 CELLSPACING=0>\n");
    for (j=0; j<N; j++) {
      printf("<TR>");
      pos = -1;
      for (i=j-1; i>=0 && pos==-1; i--)
	if (A[i]==A[j]) pos = i;
      for (i=0; i<j; i++) {
	printf("<TD><IMG SRC="); ICON
	if (i==pos) printf("greenRook.gif></TD>\n");
	else printf("green.gif></TD>\n");
      }
      printf("<TD><IMG SRC="); ICON printf("red.gif></TD></TR>\n");
    }
    printf("</TABLE></TD>\n");
  }

if(b1>=0)
  A[b1] = b2;
  LIMIT--;
  gray = b1;
  flag = 1;

}

/*the following 2 functions call each other
to generate the set partitions 
*/
void rvgen(int n, int k, int b);

void gen(int n, int k, int b)
{
  int i;
 
  if(!LIMIT) return;
  if (k==1 || k==n) return;

  if ((!b && kodd) || (b && keven)) {
    gen(n-1,k-1,1);
    printit(k-1,k-1); rvgen(n-1,k,1);
  } else { /*b=0,keven or b=1,kodd*/
    gen(n-1,k-1,0);
    printit(n-2,k-1); gen(n-1,k,1);
  }
  if(keven) {
    printit(n-1,k-2);
    i = k-3;
    if (!b) rvgen(n-1,k,1);
    else gen(n-1,k,1);
  }else /*k odd*/
    i = k-2;
  if (!b) { /*b=0*/
    for (i; i>0; i=i-2) {
      printit(n-1,i); gen(n-1,k,1);
      printit(n-1,i-1); rvgen(n-1,k,1);
    }
  } else { /*b=1*/
    for (i; i>0; i=i-2) {
      printit(n-1,i); rvgen(n-1,k,1);
      printit(n-1,i-1); gen(n-1,k,1);
    }
  }
}

void rvgen(int n, int k, int b)
{
  int i;

  if(!LIMIT) return;
  if (k==1 || k==n) return;

  if (!b) { /*b=0*/
    gen(n-1,k,1);
    for (i=1; i<k-1; i=i+2) {
      printit(n-1,i); rvgen(n-1,k,1);
      printit(n-1,i+1); gen(n-1,k,1);
    }
    if (keven) { 
      printit(n-1,k-1); rvgen(n-1,k,1);
      printit(n-2,0); rvgen(n-1,k-1,0);
    } else {
      printit(k-1,0); rvgen(n-1,k-1,1);
    }
  } else { /*b=1*/
    rvgen(n-1,k,1);
    for (i=1; i<k-1; i=i+2) {
      printit(n-1,i); gen(n-1,k,1);
      printit(n-1,i+1); rvgen(n-1,k,1);
    }
    if (keven) { 
      printit(n-1,k-1); gen(n-1,k,1);
      printit(k-1,0); rvgen(n-1,k-1,1);
    } else {
      printit(n-2,0); rvgen(n-1,k-1,0);
    }
  }
}

/***********main program********/

int main(int argc, char **argv)
{
  int i;
  int count;

  ProcessInput(argc, argv);
  count = LIMIT;

  if(out_format & 1)
    bitstring = 1;
  if(out_format & 4)
    bitchange = 1;
  if(out_format & 2)
    list = 1;
  if(out_format & 8)
    chess = 1;

  /*initialize the array A*/
  for(i=0;i<=N-K;i++)
    A[i] = 0;
  for(i=1;i<N;i++)
    A[N-K+i] = i;
   
  gen(N,K,1);

  printit(-1);

  printf("</TABLE><BR>Total number of combinations = %d<BR>\n",count-LIMIT );

  if(LIMIT)
    return(0);
  else
    return(-1);
}


















