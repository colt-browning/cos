/* 
 * Copyright (c) 2022 Ondrej Micka, Arturo Merino and Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <string>
#include <utility>
#include <vector>
#include <cassert>

#include "interfaces.hpp"
#include "utils.hpp"

// Tools for checking whether a graph has a hyperfect elimination order (HEO),
// a sequence is a valid HEO and possibly other related stuff.
class Hyperfect : public Checker{
  friend class Tests;
public:
  Hyperfect(int n, const std::vector<std::string>& vertex_names,
            const std::vector<std::vector<int>>& hyperedge_list);
  virtual ~Hyperfect() = default;

  // check if graph is chordal based on lexicographic BFS ordering
  bool is_hyperfect();
  // check whether the given heo is valid for the graph
  // !!! heo vector must be 1-indexed
  // that is, it must have size n_+1 and heo[0] is dummy element that is ignored
  bool check_heo(const std::vector<int> &heo);

  inline std::vector<int> get_heo() {return is_hyperfect() ? heo_ : std::vector<int>();}
  void print_heo();  // print hyperfect elimination ordering

  virtual bool is() {return is_hyperfect();}
  virtual bool check(const std::vector<int> &peo) {return check_heo(peo);};
  virtual std::vector<int> get() {return get_heo();};
  virtual void print() {print_heo();};
  
private:
  struct Hyperedge {
      Hyperedge(int n, const std::vector<int> &vertex_list);
      inline bool contains(int i) const {
        assert((i >= 0) && (i < char_vector_.size()) && "Invalid vertex id");
        return char_vector_[i];
      };
      // Sorted list of vertices
      std::vector<int> vertex_list_;
      // Characteristic vector of the hyperedge. Position 0 is dummy because of 1-indexing
      std::vector<bool> char_vector_;
      // Flag to mark hyperedges that should be ignored 
      bool ignore_;
  };

  int n_;  // number of vertices of the hypergraph
  std::vector<std::string> vertex_names_;  // string representations of vertices

  bool is_hyperfect_;             // cached value of is_hyperfect()
  bool hyperfect_checked_;        // was the hyperfectness of the graph already checked?
                                  // if true, is_hyperfect() just returns the cached value,
                                  // otherwise it runs the time-consuming calculation
  
  // The hypergraph
  std::vector<Hyperedge> hyperedge_list_; // Should be constant, since we use pointers to this list, 
                                          // instead of directly working with Hyperedge instances
  std::vector<std::vector<Hyperedge*>> incidence_list_; // For each vertex, a list of incident hyperedges
  std::vector<std::vector<std::vector<Hyperedge *>>> adj_matrix_; // adjacency matrix
                                        // adj_matrix_[i][j] is a list of all hyperedges containing both i and j
  std::vector<int> heo_;  // perfect elimination order

  // Build hypergraph related data structures from user-supplied hyperedge list
  void build_graph(const std::vector<std::vector<int>>& hyperedge_list);

  // Calculate and fill heo_. Sets is_hyperfect_ and hyperfect_checked_. 
  void calculate_heo();

  // Check HEO condition (1) for the given vertex
  bool check_vertex_condition(int i) const;
  // Helper for check_vertex_condition
  bool is_certificate(Hyperedge *X, int i, Hyperedge *A, Hyperedge *B) const;

  // Mark all hyperedges containing i to ignore
  void prune_hyperedges(int i);
  // Remove the ignore flag from all hyperedges
  void unmark_ignore();
};
