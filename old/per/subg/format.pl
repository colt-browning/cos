#!/usr/bin/perl
#
# This script prints a table for a list of spannning trees in HTML format

require "../common.pl";
if ($#ARGV == -1) {exit(0);}
else { 
       $output = $ARGV[0]; 
       $n = $ARGV[1]; 
}
$#ARGV = -1;

open (STDIN);

print "<P><TABLE BORDER=1 CELLSPACING=2 CELLPADDING=2><TR>\n";
if ($output & 1) {
	print '<TH COLSPAN=1><FONT SIZE="+1">Edges</FONT><BR></TH>';
	print "\n";
}
if ($output & 2) {
	print '<TH COLSPAN=1><FONT SIZE="+1">Gray Code</FONT><BR></TH>';
	print "\n";
}

$count = 0;
$returnval = 0;
$flag = 0;
while (<>) {
    $count++;
    @tree = split;
    $gray = -1;
    print"<TR>";
    for ($i=0; $i< 2*($n-1); $i = $i+2) {
        if ($count > 1 && 
		($prev[$i] != $tree[$i] || $prev[$i+1] != $tree[$i+1] )){
  	  $gray = $i;
        }
	if ($output & 1) {
		if ($i == 0) { print "<TD>"; }
        	if ($gray == $i){ print '<font color="#00ff00">';}
		print $tree[$i];
 		print "-";
		print $tree[$i+1];
        	if ($gray == $i){ print "</font>";}
        	if ($i+2 < 2*($n-1)) { print ", "; }
	}
    }
    if ($output & 2) {
	print "<TD align=center>";
	if ($count == 1) {
    		for ($i=0; $i< 2*($n-1); $i = $i+2) {
			print $tree[$i];
 			print "-";
			print $tree[$i+1];
        		if ($i+2 < 2*($n-1)) { print ", "; }
		}
	}
	else {
		print " In: " . $tree[$gray] . "-" . $tree[$gray+1];
		print " &nbsp Out: " . $prev[$gray] . "-" . $prev[$gray+1];
	}
    }
    @prev = @tree;
}

print "</TABLE>";
print "<P>Spanning tree output = $count";
close (STDIN);
$returnval; # return value  
