/* Generate Necklaces, Lyndon words, de Bruijn cycles */
/* -n num    the number of beads              */
/* -k num    the number of colors             */
/* -N        generate necklaces               */
/* -P        generate prenecklaces            */
/* -L        generate lyndon words            */
/* -D        generate de Bruijn sequence      */
/* -B        generate bracelets               */
/* -b 	     generate lie basis               */
/* -U        generate unlabelled necklaces    */
/* -UL       generate unlabelled lyndon words    */
/* -F        generate necklace with forbidden 0's */
/* -FL       generate lyndon words with forbidden 0's */
/* -m        maximum # of non-zero elements   */
/* -d        fixed density of string   */
/* -f        number of 0's in forbidden sequence   */
/* -l num    max number of objects generated  */
/* programmer: Frank Ruskey, Sept. 12, 1995   */
/* updated: Joe Sawada, June. 25, 1997   */

#include <stdio.h>
#include <math.h>

#define LIMIT_ERROR     -1
#define TRUE             1
#define FALSE            0

int  a[100];
int  b[200];
/* m and pre for lie basis */
int  m[50][50]; 	/* If a[i]..a[j] is a Lyndon word then the value
			of m[i][j] is the starting position of the
			longest proper right factor of a[i]..a[j] */
int  pre[50][50];/* Stores the p value for each propoer right factor */
int  count, N, K, M, Z;
int  D = -1;
int  pre_out, deb_out, lyn_out, neck_out, brace_out, lie_out, unlabelled_out, 
	unlabelledL_out, forbidL_out,forbid_out, LIMIT;

void ProcessInput (int argc, char *argv[])
{  int i;
   neck_out = lyn_out = deb_out = pre_out = brace_out = FALSE;
   N = 0;  M = 100;  K = 0; Z = 100;
   for (i=1; i<argc; i++) {
       if (strcmp(argv[i], "-n") == 0) {
          if (i+1 < argc) { N = atoi(argv[++i]); }
       }
       else if (strcmp(argv[i], "-k") == 0) {
          if (i+1 < argc) { K = atoi(argv[++i]); }
       }
       else if (strcmp(argv[i], "-m") == 0) {
          if (i+1 < argc) { M = atoi(argv[++i]); }
       }
       else if (strcmp(argv[i], "-d") == 0) {
          if (i+1 < argc) { D = atoi(argv[++i]); }
       }
       else if (strcmp(argv[i], "-z") == 0) {
          if (i+1 < argc) { Z = atoi(argv[++i]); }
       }
       else if (strcmp(argv[i], "-N") == 0) neck_out = TRUE;
       else if (strcmp(argv[i], "-L") == 0) lyn_out = TRUE;
       else if (strcmp(argv[i], "-P") == 0) pre_out = TRUE;
       else if (strcmp(argv[i], "-D") == 0) deb_out = TRUE;
       else if (strcmp(argv[i], "-B") == 0) brace_out = TRUE;
       else if (strcmp(argv[i], "-b") == 0) lie_out = TRUE;
       else if (strcmp(argv[i], "-U") == 0) unlabelled_out = TRUE;
       else if (strcmp(argv[i], "-UL") == 0) unlabelledL_out = TRUE;
       else if (strcmp(argv[i], "-F") == 0) forbid_out = TRUE;
       else if (strcmp(argv[i], "-FL") == 0) forbidL_out = TRUE;
       else if (strcmp(argv[i], "-l") == 0) {
          if (i+1 < argc) { LIMIT = atoi(argv[++i]); }
       }
       else printf("Spurious input data: %s\n", argv[i]);
   }
}

/* A naive test for bracelets */
int Bracelet( int p) {

	int i,j,k,n;

	for (i=1; i<=N; i++) b[(2*N) -i+1] = b[N-i+1] = a[i];

	k = 0;
	b[2*N+1] = -1;
	while(k<2*N) {
		n=k+2;
		p=1;
		while (b[n-p]<=b[n]) {
			if (b[n-p] < b[n]) p=n-k;
			n++;
		}
		if (n-k > N) break; 
		k = k+p;
		while (k<n-p) k = k+p; 
	}
	j = 1;
	for (i=k+1; i<=N+k; i++) {
		if (b[i] < a[j]) return(0);	
		if (b[i] > a[j++]) return(1);	
	}
	return(1);
}

void PrintIt ( int p )
{ int i;
  if (count <= LIMIT) {
     if (pre_out) {
        ++count;
        for (i=1;i<=N;++i) printf("%d ",a[i] );  
        printf("\n");
     }
     if ((neck_out || unlabelled_out || forbid_out) && N%p == 0) {
        ++count;
        for (i=1;i<=N;++i) printf("%d ",a[i] );  
        printf("\n");
     }
     if (brace_out && N%p == 0) {
	if (Bracelet(p)) {
        	++count;
        	for (i=1;i<=N;++i) printf("%d ",a[i] );  
        	printf("\n");
	}
     }
     if ((lyn_out || unlabelledL_out || forbidL_out) && p == N) {
        ++count;
        for (i=1;i<=N;++i) printf("%d ",a[i] ); 
        printf("\n");
     }
     /* special case: must also add the length of the sequence */
     if (deb_out && N%p == 0) {
        ++count;
	printf("%d ",p);
        for (i=1;i<=p;++i) printf("%d ",a[i] );
        printf("\n");
     }
  } else exit(LIMIT_ERROR);
}

void gen( int t, int p, int non_zero )
{
int j;
  if (non_zero <=M) {
   if (t > N) PrintIt( p );
   else {
      a[t] = a[t-p];  
      if (a[t] > 0) gen( t+1, p, non_zero+1);
      else gen(t+1, p, non_zero);
      for (j = a[t-p]+1; j <= K-1; ++j) {
         a[t] = j;  gen( t+1, t,non_zero+1);
      }
   }
  }
}


void forbid_gen(int t, int p, int non_zero, int all_zeros) {
	
int j;	
  	if (non_zero >M) return; 

	if(t>N) PrintIt(p);
	else {
		a[t] = a[t-p]; 
		if (a[t] != 0) forbid_gen(t+1, p, non_zero+1, FALSE); 
		else if ((all_zeros == FALSE) || (t<Z)) forbid_gen(t+1,p,non_zero, all_zeros);
      		for (j = a[t-p]+1; j <= K-1; ++j) {
         		a[t] = j;  forbid_gen( t+1, t,non_zero+1,FALSE);
		}
	}
}


void unlabelled_gen(int t, int p, int non_zero, int r) {
	
  	if (non_zero >M) return; 

	if (t>N) PrintIt(p);
	else {
		a[t] = a[t-p]; 

		if (a[t] == 1) {
			if (r == 0) r = t;
			else if (a[t-r+1] == a[t]) return;
			unlabelled_gen(t+1,p,non_zero+1,r);
		}
		else {
			if ((r > 0) &&(a[t-r+1] == a[t])) unlabelled_gen(t+1,p,non_zero,0);
			else unlabelled_gen(t+1,p,non_zero,r);
			a[t] = 1; 
			if (r == 0) r = t;
			else if (a[t-r+1] == a[t])  return;
			if (N>1) unlabelled_gen(t+1,t,non_zero+1, r);
		}
	}
}

/* Fixed Density stuff */

void FixedPrint(int p) {

	int i,j,next,end,min;

	/* Determine minimum position for next bit */
	next =  (D/p)*a[p] + a[D%p];	
	if (next < N) return;

	/* Determine last bit */
	min = 1;	
	if (pre_out) min = K-1;
	else if ((next == N) && (D%p != 0)) {
		min =  b[D%p]+1;
		p = D;
	}
	else if ((next == N) && (D%p == 0)) {
		min = b[p];
	}

	/* Determine length of String */
	end = N;
	if (pre_out) end = N-1; 

	for( b[D]=min; b[D]<K; b[D]++ ) {	
		i = 1;
		/* Test for lyndon words */
		if ( (lyn_out) && (N%a[p] == 0) && (a[p] != N)) {}
		else {	
			for(j=1; j<=end; j++) {
				if (a[i] == j) {
						printf("%d ",b[i]);
						i++;
					}
					else printf("0 ");  
				}
				printf("\n"); 
				count++;
		}
		p = D;
	}     
}

void FixedGen(int t,int p) {

	int i,j,max,tail;

	if (t >= D-1) FixedPrint(p);
	else {
		tail = N - (D - t) + 1;
		max = ((t+1)/p)*a[p] + a[(t+1)%p];
		if (max <=tail) {
			a[t+1] = max;
			if ((t+1)%p == 0) b[t+1] = b[p];
			else b[t+1] = b[(t+1)%p];

			FixedGen(t+1,p);
			for (i=b[t+1] +1; i<K; i++) {
				b[t+1] = i;
				FixedGen(t+1,t+1);
			}
			tail = max-1;
		}
		for(j=tail; j>=a[t]+1; j--) {
			a[t+1] =  j;
			for (i=1; i<K; i++) {
				b[t+1] =  i;
				FixedGen(t+1,t+1);
			}
		} 
	}
} 

void Fixed() {

	int i,j;


	if (D == 0) {
		if (neck_out || pre_out) {
			for (j=1; j<=N; j++) printf("0 ");
			printf("\n");
			count = 1; 
		}
	} 
	else {
		/* Increment N and D for prenecklaces */
		if (pre_out) { N++; D++; }

		/* initialize string */
		for(j=0; j<=D; j++) a[j] = 0;

		a[0] = 0;
		a[D] = N; 	
		for(j=N-D+1; j>=(N-1)/D + 1; j--) {
			a[1] = j;
			for (i=1; i<K; i++) {
				b[1] = i;
				FixedGen(1,1);		
			}
		} 
	}
}



/*----------------------------------------------------------------------*/
/* A recursive function which prints out the Lie bracket for the 
	generated Lyndon word */
/*----------------------------------------------------------------------*/
void PrintBracket(int start, int end) {

	int split;

	if (start == end) { printf("%d", a[start]); return; }
	if (end-start == 1) { printf("[ %d , %d ]",a[start], a[end]); return; }

	split = m[start][end];
	printf("[ ");
	PrintBracket(start,split-1);
	printf(" , ");
	PrintBracket(split,end);
	printf(" ]");
}

void GenLie(int t, int p) {
	
	int h,i,j,last;

	if(t>N) {
		if (N == p) { 
			count++;
			if (count > LIMIT) exit(LIMIT_ERROR);
			PrintBracket(1,N); printf("\n");  }
	}
	else {
		a[t] = a[t-p]; 
		last = 1;
		for(i=2; i<=t-1; i++) {
			pre[i][t] = pre[i][t-1];
			if (pre[i][t] != 0) { 
				if (a[t] < a[ t-pre[i][t] ]) pre[i][t] = 0;
				if (a[t] > a[ t-pre[i][t] ]) {
					for(j=last; j<i; j++) m[j][t] = i;
					last = i;
					pre[i][t] = t-i+1;
				}
			}
		}
		for(j=last; j<t; j++) m[j][t] = t;
		GenLie(t+1,p);
	
                for(h=a[t-p]+1; h<=K-1; h++) {
			a[t] = h;
			last = 1;
			for (i=2; i<=t-1; i++) {
				pre[i][t] = pre[i][t-1];
				if (( pre[i][t] != 0) && 
				            (a[t] > a[ t-pre[i][t] ])) {
					for(j=last; j<i; j++) m[j][t] = i;
					last = i;
					pre[i][t] = t-i+1;
				}
			}
			for(j=last; j<t; j++) m[j][t] = t;
			GenLie(t+1, t);
		}
	}
}


int main (int argc, char *argv[] )
{
  int i;

  ProcessInput( argc, argv );
  a[0] = count = 0;
  if ( D >=0 ) Fixed();
  else if (unlabelled_out || unlabelledL_out) unlabelled_gen(1,1,0,0);
  else if (forbid_out || forbidL_out) forbid_gen(1,1,0,TRUE);
  else if (lie_out) {
	for (i=1; i<=N; i++)  pre[i][i] = 1; 
	GenLie(1,1);
  }
  else gen( 1, 1 ,0);
  return (0);
}

