/*
 * Copyright (c) 2023 Torsten Muetze and Namrata
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "tree.hpp"
#include <algorithm>
#include <getopt.h>
#include <iostream>
#include <sstream>

void help() {
  std::cout << "./btree [options] compute binary trees that avoid given tree patterns" << std::endl;
  std::cout << "-h                display this help" << std::endl;
  std::cout << "-n{1,2,3,...}     generate binary trees with n vertices" << std::endl;
  std::cout << "-p                forbidden tree patterns; each pattern is a pair of preorder" << std::endl;
  std::cout << "                  permutation and corresponding edge type list, separated by" << std::endl;
  std::cout << "                  comma; multiple patterns are separated by semicolon" << std::endl;
  std::cout << "-l{-1,0,1,2,...}  number of binary trees to generate; -1 for all" << std::endl;
  std::cout << "-q                quiet output" << std::endl;
  std::cout << "-c                output number of binary trees" << std::endl;
  std::cout << "examples: ./btree -n5 -c" << std::endl;
  std::cout << "          ./btree -n6 -p'132,10' -c" << std::endl;
  std::cout << "          ./btree -n8 -p'2134,000; 2143,111' -l75" << std::endl;
  std::cout << "          ./btree -n11 -p'2134,000; 15234,1010' -q -c" << std::endl;
}

bool read_patterns(std::string s, std::vector<Pattern> &patterns)
{
  std::vector<std::string> patterns_str;  // input pattern string
  std::string tmp;

  s.erase (std::remove (s.begin(), s.end(), ' '), s.end());  // remove whitespaces in s

  if (s.find_first_not_of(",;0123456789") != std::string::npos) {
    std::cerr << "invalid characters in the pattern specification" << std::endl;
    return false;
  }
  std::stringstream ss(s);
  while (std::getline(ss, tmp, ';')) {
    // split the pattern string specification
    patterns_str.push_back(tmp);
  }
  for (int i = 0; i < patterns_str.size(); i++) {
    std::string::difference_type comma = std::count(patterns_str[i].begin(), patterns_str[i].end(), ',');
    // check if there is an exactly one comma between a preorder and an edge type specification
    if (comma != 1) {
      std::cerr << "invalid separation characters in pattern specification" << std::endl;
      return false;
    }
    std::string pat;
    std::string e_str;
    std::vector<int> pat_pre;  // vector of preorder of a pattern
    std::vector<int> pat_e;  // vector of edge type of a pattern
    size_t found;
    if ((found = patterns_str[i].find(",")) != std::string::npos) {
      // split the pattern into a preorder and an edgetype specification
      pat =  patterns_str[i].substr(0,found);
      e_str = patterns_str[i].substr(found+1, std::string::npos);
    }
    if (pat.empty()) {
      std::cerr << "preorder specification for pattern must not be empty" << std::endl;
      return false;
    }
    std::string pat_sort = pat;
    sort(pat_sort.begin(), pat_sort.end());  // sort the preorder of the pattern
    for (int i = 0; i < pat_sort.size(); ++i) {
      if (pat_sort[i] - '0' != i+1) {
        std::cerr << "preorder specification for pattern must be a permutation" << std::endl;
        return false;
      }
    }
    for (int i = 0; i < pat.size(); ++i) {  // convert the char into an int and inserts it into pat_pre
      pat_pre.push_back(pat[i] - '0');
    }

    if (!e_str.empty()) {  // if edge type exists in a given pattern
      for (int k = 0; k < e_str.size(); k++) {
        if ((e_str[k] != '0') and (e_str[k] != '1')  and (e_str[k] != '2')) {
          std::cerr << "edge type specification is invalid" << std::endl;
          return false;
        }
      }
      if (e_str.size()!= pat.size() - 1) {
        std::cerr << "edge type specification must be one shorter than preorder list" << std::endl;
        return false;
      }
      pat_e.push_back(0);
      for (int i = 0; i < e_str.size(); ++i) {  // convert the char into an int and inserts it into pat_e
        pat_e.push_back(e_str[i] - '0');
      }
    }
    if (e_str.empty() and pat.size() != 1) {  // when preorder is 1, no binary tree is generated
      std::cerr << "edge type specification for pattern is missing" << std::endl;
      return false;
    }

    Pattern p(pat_pre, pat_e);  // create a pattern object
    if (!(p.avoids_231())) {
      std::cerr << "invalid preorder specification of pattern" << std::endl;
      return false;
    }
    patterns.push_back(p);  // create a vector of pattern objects
  }
  return true;
}

int main(int argc, char* argv[])
{
  int c, n;
  std::vector<Pattern> patterns;
  int num_trees = 0;  // count number of binary trees that avoid the given forbidden patterns
  bool n_set = false;  // flag whether the option -n is present
  long long steps = -1;  // compute all binary trees by default
  bool quiet = false;  // print output by default
  bool output_counts = false;  // omit counts by default
  int quiet_dot = 10000000;  // print one dot every 10^7 binary trees in quiet output mode

  while ((c = getopt (argc, argv, "hn:p:l:qc")) != -1) {
    switch (c) {
      case 'h': {
        help();
        return 0;
      }
      case 'n': {
        n = std::stoi(optarg);
        if (n <= 0) {
          std::cerr << "n must be greater than 0" << std::endl;
          return 1;
        }
        n_set = true;
        break;
      }
      case 'q': {
        quiet = true;
        break;
      }
      case 'c': {
        output_counts = true;
        break;
      }
      case 'p': {
        std::string s = optarg;
        if (!read_patterns(s, patterns)) {
          return 1;
        }
        break;
      }
      case 'l': {
        steps = atoi(optarg);
        if (steps < -1) {
          std::cerr << "option -l must be followed by an integer from {-1,0,1,2,...}" << std::endl;
          return 1;
        }
        break;
      }
    }
  }

  if (steps == 0) {
    std::cout << "output limit reached" << std::endl;
    if (output_counts) {
      std::cout << "number of binary trees: 0" << std::endl;
    }
    return 0;
  }

  if (!n_set) {
    std::cerr << "option -n is mandatory" << std::endl;
    help();
    return 1;
  }

  bool friendly_patterns = true;
  for (int i = 0; i < patterns.size(); i++) {
    if (!patterns[i].is_friendly()) {
      friendly_patterns = false;
      break;
    }
  }

  if (friendly_patterns) {
    // use Algorithm S if all patterns are friendly
    Tree t(n, patterns);  // initialize a tree object with an all-right tree
    bool next;
    do {
      num_trees++;
      if (!quiet) {
        t.print();
        std::cout << std::endl;
      } else if (num_trees % quiet_dot == 0) {
        std::cout << "." << std::flush;
      }
      next = t.next();
      if (next && (steps >= 0) && (num_trees >= steps)) {
        std::cout << "output limit reached" << std::endl;
        break;
      }
    } while (next);
  } else {
    // use brute-force generation

    #ifdef COS
    if (n > 10) {
      std::cerr << "number of vertices must be <=10 for non-friendly patterns" << std::endl;
      return 1;
    }
    #endif  // COS

    std::vector<Tree> all_trees = Tree::construct_all_trees(n, patterns);
    std::vector<Tree> avoiding_trees;
    for (int i = 0; i < all_trees.size(); i++) {
      if (all_trees[i].avoids()) {
        avoiding_trees.push_back(all_trees[i]);
      }
    }
    for (int i = 0; i < avoiding_trees.size(); i++) {
      if (!quiet) {
        avoiding_trees[i].print();
        std::cout << std::endl;
      }
      num_trees++;
      if (quiet && (num_trees % quiet_dot == 0)) {
        std::cout << "." << std::flush;
      }
      if ((steps >= 0) && (num_trees >= steps) && (num_trees < avoiding_trees.size())) {
        std::cout << "output limit reached" << std::endl;
        break;
      }
    }
  }
  if (output_counts) {
    if (quiet && num_trees >= quiet_dot) {
      std::cout << std::endl;
    }
    std::cout << "number of binary trees: " << num_trees << std::endl;
  }
  return 0;
}
