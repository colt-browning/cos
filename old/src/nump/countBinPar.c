#include <stdio.h>

int count(int n)
{
  if(n == 0)
    return 1;
  else if(n % 2 == 1)
    return count(n-1);
  else
    return (count(n-1) + count(n/2));
}

int main(){
int n;
printf("Enter n: ");
scanf("%d", &n);
printf("number of binary partition is %d\n", count(n));
}
