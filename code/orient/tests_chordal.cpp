/* 
 * Copyright (c) 2022 Ondrej Micka, Arturo Merino and Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cassert>
#include <vector>
#include <tuple>
#include <utility>
#include <iostream>
#include "tests_chordal.hpp"
#include "chordal.hpp"

struct Tests{
  using edgelist_type = std::vector<std::pair<int, int>>; // We're going to use this type a lot

  static inline void expect(bool condition, const std::string &message) {
    if (!condition) {
      std::cout << "FAILED TEST" << std::endl;
      std::cout << message << std::endl;
      exit(-1);
    }
  }

  /* Section of utility functions */

  // Create vector of strings "0", "1", ..., "n" to be used as vertex names
  static std::vector<std::string> make_names(int n) {
    assert((n >= 0) && "n must be non-negative");
    auto out = std::vector<std::string>(n+1, "0");
    for (int i = 1; i <= n; ++i){
      out[i] = std::to_string(i);
    }
    return out;
  }

  // Create a vector with sequence of length n
  static std::vector<int> make_seq(int n, int start=1, int inc=1){
    assert((n >= 0) && "n must be non-negative");
    auto out = std::vector<int>(n, 0);
    for (int i = 0; i < n; ++i){
      out[i] = start + i*inc;
    }
    return out;
  }

  // Create edge list of P_n with vertices start, start+1, ... start+n-1
  static edgelist_type make_path(int n, int start = 1){
    assert((n >= 1) && "Path needs to have at least one vertex!");
    edgelist_type out;
    for (int i = 0; i < n-1; ++i){
      out.push_back(std::make_pair(start+i, start+i+1));
    }
    return out;
  }

  // Create edge list of K_n with vertices start, start+1, ... start+n-1
  static edgelist_type make_clique(int n, int start = 1){
    assert((n >= 1) && "Clique needs to have at least one vertex!");
    edgelist_type out;
    for (int i = start; i < start + n; ++i){
      for (int j = i+1; j < start + n; j++){
        out.push_back(std::make_pair(i, j));
      }
    }
    return out;
  }

  // Create edge list of m disjoint edges (start, start+1), ..., (start+2i, start+2i+1), ...
  static edgelist_type make_matching(int m, int start = 1){
    assert((m >= 1) && "Matching needs to have at least one edge!");
    edgelist_type out;
    for (int i = 0; i < m; ++i){
      out.push_back(std::make_pair(start+2*i, start+2*i + 1));
    }
    return out;
  }

  /* Section of tests */

  static void check_chordal(int n, const edgelist_type &edge_list, const std::vector<int> &answer_peo, const std::vector<int> &answer_oep, bool answer_chordal) {
    auto strings = make_names(n);
    Chordal f = Chordal(n, strings, edge_list);
    bool is_chordal = f.is_chordal();
    expect(answer_chordal == is_chordal, "wrong chordality checking");
    for (int i=1; i <= f.n_; ++i) {
      // Minus one, because there is a leading zero in f
      expect((answer_peo[i-1] == f.peo_[i]) && (answer_oep[i-1] == f.oep_[i]), "wrong PEO"); 
    }
  }
  
  static void test_hyperfect() {
    std::vector<std::tuple<int, edgelist_type, std::vector<int>, std::vector<int>, bool>> tests = {
      {5,{{1,2},{1,3},{1,4},{1,5}},{1,2,3,4,5},{1,2,3,4,5},true},
      {7,{{2,1},{4,1},{3,2},{6,5},{5,2},{4,7}},{1,2,4,3,5,7,6},{1,2,4,3,5,7,6},true},
      {7,{{6,5},{3,5},{1,5},{5,2},{2,4},{2,7}},{1,5,3,6,2,4,7},{1,5,3,6,2,4,7},true},
      {8,{{2,3},{2,1},{3,1},{1,8},{1,6},{1,7},{7,8},{7,6},{6,8},{4,6},{4,5}},{1,2,3,8,7,6,4,5},{1,2,3,7,8,6,5,4},true},
      {8,{{2,3},{2,1},{3,1},{1,8},{1,6},{1,7},{7,6},{6,8},{4,6},{4,5}},{1,2,3,8,6,7,4,5},{1,2,3,7,8,5,6,4},true},
      {8,{{2,3},{2,1},{3,1},{1,8},{1,7},{7,6},{6,8},{4,6},{4,5}},{1,2,3,8,7,6,4,5},{1,2,3,7,8,6,5,4},false},
      {6,{{1,2},{2,3},{3,4},{4,5},{5,6},{1,6}},{1,2,6,3,5,4},{1,2,4,6,5,3},false},
      {11,{{1,2},{1,7},{1,9},{2,9},{3,10},{3,8},{3,4},{8,10},{8,4},{4,10},{4,6},{6,10},{11,6},{11,5},{11,4},{5,6},{5,4}},{1,2,9,7,5,6,4,11,10,3,8},{1,2,10,7,5,6,4,11,3,9,8},true},
      {8,{{1,2},{1,7},{1,8},{2,7},{2,8},{7,8},{3,4},{3,6},{4,5},{5,6}},{1,2,8,7,5,6,4,3},{1,2,8,7,5,6,4,3},false},
      {5,{{3,4}},{1,2,3,4,5},{1,2,3,4,5},true},
      {5,{{1,2},{4,5}},{1,2,3,4,5},{1,2,3,4,5},true},
    };
    std::cout << "testing lex BFS and chordal graph checking" << std::endl;
    for (auto &test_case : tests) {
      // C++17 FTW!
      std::apply(check_chordal, test_case);
      //check_chordal(std::get<0>(test_case), std::get<1>(test_case), std::get<2>(test_case), std::get<3>(test_case), std::get<4>(test_case));
    }
    std::cout << "OK" << std::endl;
  }

  static void check_heo(int n, const edgelist_type &edge_list, const std::vector<int> &peo, bool answer_peo){
    auto strings = make_names(n);
    // add leading zero required by the tested function
    auto peo_copy = peo;
    peo_copy.insert(peo_copy.begin(), 0);
    Chordal f = Chordal(n, strings, edge_list);
    bool is_peo = f.check_peo(peo_copy);
    expect(answer_peo == is_peo, "wrong PEO check!");
  }

  static void test_heo(){
    std::vector<std::tuple<int, edgelist_type, std::vector<int>, bool>> tests = {
      {2, make_path(2), make_seq(2), true},
      {5, make_path(5), make_seq(5), true},
      {5, make_path(5), make_seq(5, 5, -1), true},
      {5, make_path(5), {3, 2, 4, 5, 1}, true},
      {5, make_path(5), {4, 3, 2, 5, 1}, true},
      {5, make_path(5), {1, 5, 4, 2, 3}, false},
      {5, make_path(5), {4, 2, 3, 5, 1}, false},
      {5, make_path(5), {4, 2, 3, 1}, false},
      {5, make_path(5), make_seq(6), false},
      {5, make_path(5), {4, 3, 2, 5, 3}, false},
      {8, make_matching(4), make_seq(8), true},
      {8, make_matching(4), {2, 8, 1, 7, 6, 4, 5, 3}, true},
      {8, make_matching(4), {2, 8, 1, 7, 6, 6, 5, 3}, false},
      {6, make_clique(6), make_seq(6), true},
      {6, make_clique(6), {4, 5, 3, 1, 6, 2}, true},
      {6, make_clique(6), {4, 5, 3, 1, 2}, false},
      {11,{{1,2},{1,7},{1,9},{2,9},{3,10},{3,8},{3,4},{8,10},{8,4},{4,10},{4,6},{6,10},{11,6},{11,5},{11,4},{5,6},{5,4}},{1,2,9,7,5,6,4,11,10,3,8}, true},
      {11,{{1,2},{1,7},{1,9},{2,9},{3,10},{3,8},{3,4},{8,10},{8,4},{4,10},{4,6},{6,10},{11,6},{11,5},{11,4},{5,6},{5,4}},{2,1,5,7,6,9,4,11,10,8,3}, true},
      {11,{{1,2},{1,7},{1,9},{2,9},{3,10},{3,8},{3,4},{8,10},{8,4},{4,10},{4,6},{6,10},{11,6},{11,5},{11,4},{5,6},{5,4}},{1,2,9,7,5,6,3,4,11,10,8}, false}
    };
    std::cout << "testing PEO checking" << std::endl;
    for (auto &test_case : tests) {
      // C++17 FTW!
      std::apply(check_heo, test_case);
    }
    std::cout << "OK" << std::endl;
  }

};

void run_tests_chordal() {
  std::cout << "running tests for chordality algorithm" << std::endl;
  Tests::test_hyperfect();
  Tests::test_heo();
  std::cout << "testing finished OK" << std::endl;
}