#!/usr/bin/perl

# perl cgi script for handling set partitions in the Object Server.
# Written by Frank Ruskey, 1995.

require "../common.pl";

$GraphPartBIN 	= "../../bin/nump/GraphicalPartition";
$ScoreBIN 	= "../../bin/nump/Score";
$DegSeqBIN 	= "../../bin/nump/DegreeSequences";
$PartitionsBIN 	= "../../bin/nump/NumericalPartition";
$OddPartBIN 	= "../../bin/nump/NumPartOdd";
$GrayPartBIN	= "../../bin/nump/GrayPartition";
$BinPartBIN	= "../../bin/nump/BinPart";
$DistPartBIN    = "../../bin/nump/NumParDist";
$DistOddPartBIN  = "../../bin/nump/NumDistOdd";

MAIN:
{
# Read in all the variables set by the form
  if (&ReadParse(*input)) { &ProcessForm; }
}

sub PrintTableHeaders {
   print "<P>\n";
   &TableSetup;  # from common.pl
   if ($input{'output1'}) { 
	     &ColumnLabel( "Natural" ); }
   if ($input{'output2'}) { &ColumnLabel( "Multip." ); }
   if ($input{'output3'}){ 
     if( $input{'program'} eq "PartitionsGrayCode") 
     { &ColumnLabel( "Gray Code<BR>Change<BR> <FONT SIZE=-1>(+1, -1)</FONT> " ); }
     else
     { print "<FONT SIZE=-1>Gray Code output mode is not supported for this type.</FONT>";}
   }
   if ($input{'output4'}) { &ColumnLabel( "Ferrer" ); }
   if ($input{'output5'}) { &ColumnLabel( "Conjugate");}
   print "</TR>";
}

sub ProcessForm {

  &MakeNumeric( $input{'n'} );
  &MakeNumeric( $input{'m'} );
  &MakeNumeric( $input{'k'} );

  $outformat = 0;
  $inputflag = 0;
  if ($input{'output1'}) { $outformat = 1; }
  if ($input{'output2'}) { $outformat = $outformat | 2; }
  if (($input{'output3'}) && ($input{'program'} eq "PartitionsGrayCode")) 
    	{ $outformat = $outformat | 4; }
  if ($input{'output4'}) { $outformat = $outformat | 8; }
  if ($input{'output5'}) { $outformat = $outformat | 16;}
  if ($outformat eq 0) { $outformat = 1; }
  $params = "-n $input{'n'} -L $LIMIT -o $outformat";
  
  if(!$input{'program'}){
    $input{'program'} = "AllPartitions";
    $inputflag = 1;
  }
  if ($input{'program'} eq "Graphical") {
     &OutputHeader('Graphical Partitions Output','nump/GraphicalPartition');  # from common.pl
     print "Graphical partitions for <I>n</I> = $input{'n'}";
     if ($input{'k'}) { print " and <I>k</I> = $input{'k'}.<P>\n"; }
     else { print ".<P>\n"; }
     if ($input{'m'}) { &ExtraParam('m',$input{'m'}); }
     &PrintTableHeaders;
     if (int($input{'n'})%2 == 1) { &MustBeEven( 'n' ); exit(0); }
     if (int($input{'n'}) > 50) { &TooBig('n',50); exit(0); }
     if ($input{'k'}) {
	 $retval = system "$GraphPartBIN $params -k $input{'k'}";
     } else { 
	 $retval = system "$GraphPartBIN $params -k $input{'n'}";
     }
     print "</TABLE>\n";
     if ($retval) { &LimitError; }
     &MoreInfo('nump/GraphicalPartition');

  } elsif ($input{'program'} eq "AllPartitions") {
     &OutputHeader('Numerical Partitions Output','nump/NumPartition');  # from common.pl
     if($inputflag){
       print "<p>No input mode was selected, set to default: All Partitions</p>";
     }
     print "Partitions of <I>n</I> = $input{'n'}";
     if ($input{'k'}) { print " into <I>k</I> = $input{'k'} parts\n"; }
     if ($input{'m'}) { print 
	     " with largest part <I>m</I> = $input{'m'}\n"; }
     print ".\n";
     if (int($input{'n'}) > 50) { &TooBig('n',50);  exit(0); }
     if (int($input{'n'}) < 1)  { &TooSmall('n',1); exit(0); }
     &PrintTableHeaders;
     if (!$input{'m'}) { $input{'m'} = '0'; }
     if (!$input{'k'}) { $input{'k'} = '0'; }
     $retval = system "$PartitionsBIN $params -k $input{'k'} -m $input{'m'}";
     print "</TABLE>\n";
     if ($retval) { &LimitError; }
     &WorkingMoreInfo('nump/NumPartition');
  
  }elsif ($input{'program'} eq "Binary partitions") {
     &OutputHeader('Binary Partitions Output','nump/NumPartition');  # from common.pl
     print "Binary Partitions of <I>n</I> = $input{'n'}";
     if ($input{'k'}) { print " with the largest coefficient <I>k</I> = $input{'k'}\n"; }
	 else {print ".<P>\n";}
	 if (int($input{'n'}) > 50) { &TooBig('n',50); exit(0); }
     if (int($input{'n'}) < 1) { &TooSmall('n',1); exit(0); }
	 if (!$input{'k'}) { $input{'k'} = $input{'n'}; }
	   &PrintTableHeaders;
     $retval = system "$BinPartBIN $params -k $input{'k'}";
     print "</TABLE>\n";
	 &WorkingMoreInfo('nump/NumPartition');
  } elsif ($input{'program'} eq "PartitionsOdd") {
     &OutputHeader('Numerical Partitions Output','nump/NumPartition');  # from common.pl
     print "Partitions of <I>n</I> = $input{'n'} into odd parts";
     if ($input{'m'}) { 
        print " with largest part <I>m</I> = $input{'m'}.<P>\n"; 
        if (int($input{'m'})%2 == 0) { &MustBeOdd( 'm' ); exit(0); }
     } else { print ".<P>\n"; }
     if ($input{'k'}) { &ExtraParam('k',$input{'k'}); }
     if (int($input{'n'}) > 50) { &TooBig('n',50); exit(0); }
     if (int($input{'n'}) < 1) { &TooSmall('n',1); exit(0); }
     &PrintTableHeaders;
     if (!$input{'m'}) { $input{'m'} = '0'; }
     $retval = system "$OddPartBIN $params -m $input{'m'}";
     print "</TABLE>\n";
     if ($retval) { &LimitError; }
     &WorkingMoreInfo('nump/NumPartition');
  
  }elsif ($input{'program'} eq "PartitionsDistinct") {
     &OutputHeader('Numerical Partitions Output','nump/NumPartition');  # from common.pl
     print "Partitions of <I>n</I> = $input{'n'} into disinct parts";
     if ($input{'m'}) { 
        print " with largest part <I>m</I> = $input{'m'}.<P>\n"; 
     } else { print ".<P>\n"; }
     if ($input{'k'}) { &ExtraParam('k',$input{'k'}); }
     
     if (int($input{'n'}) > 50) { print "too big";&TooBig('n',50); exit(0); }
     if (int($input{'n'}) < 1) { print "too small";&TooSmall('n',1); exit(0); }
     &PrintTableHeaders;
     if (!$input{'m'}) { $input{'m'} = '0'; }
     $retval = system "$DistPartBIN $params -m $input{'m'}";
     print "</TABLE>\n";
     if ($retval) { &LimitError; }
     &WorkingMoreInfo('nump/NumPartition');
  }
  
  elsif ($input{'program'} eq "PartitionsOddDistinct") {
     &OutputHeader('Numerical Partitions Output','nump/NumPartition');  # from common.pl
     print "Partitions of <I>n</I> = $input{'n'} into disinct odd parts";
     if ($input{'m'}) { 
        print " with largest part <I>m</I> = $input{'m'}.<P>\n"; 
     } else { print ".<P>\n"; }
     if ($input{'k'}) { &ExtraParam('k',$input{'k'}); }
     
     if (int($input{'n'}) > 50) { print "too big";&TooBig('n',50); exit(0); }
     if (int($input{'n'}) < 1) { print "too small";&TooSmall('n',1); exit(0); }
     &PrintTableHeaders;
     if (!$input{'m'}) { $input{'m'} = '0'; }
     $retval = system "$DistOddPartBIN $params -m $input{'m'}";
     print "</TABLE>\n";
     if ($retval) { &LimitError; }
     &WorkingMoreInfo('nump/NumPartition');
  }

  

 elsif ($input{'program'} eq "Score") {
     &OutputHeader('Score Sequences Output','nump/ScoreSequence');  # from common.pl
     print "Score sequences for <I>n</I> = $input{'n'}.<BR>";
     if ($input{'k'}) { &ExtraParam('k',$input{'k'}); }
     if ($input{'m'}) { &ExtraParam('m',$input{'m'}); }
     &PrintTableHeaders;
     if (int($input{'n'}) > 50) { &TooBig('n',50); exit(0); }
     $retval = system "$ScoreBIN $params"; 
     print "</TABLE>\n";
     if ($retval) { &LimitError; }
     &MoreInfo('nump/ScoreSequence');

  } elsif ($input{'program'} eq "DegreeSequences") {
     &OutputHeader('Degree Sequences Output','nump/DegreeSequences');  # from common.pl
     print "Degree sequences for <I>n</I> = $input{'n'}";
     if ($input{'k'}) { print " and <I>k</I> = $input{'k'}"; }
     print ".<BR>";		# 
     if ($input{'m'}) { &ExtraParam('m',$input{'m'}); }
     if (!$input{'m'}) { $input{'m'} = '0'; }
     if (!$input{'k'}) { $input{'k'} = '0'; }
     if (int($input{'n'}) > 50) { &TooBig('n',50); exit(0); }
     &PrintTableHeaders;
     if (!$input{'k'}) {
        $retval = system "$DegSeqBIN $params -k 0";
	&LimitError if ($retval); 
     } elsif ((int($input{'k'}) >= 0) && (int($input{'k'}) <= 2)) {
	$retval = system "$DegSeqBIN $params -k $input{'k'}";
	&LimitError if ($retval);
     } else {
        print "<BR><BLINK>Error:</BLINK> Connectivity must be 0,1, or 2.<BR>\n";
     }
     print "</TABLE>\n";
     &MoreInfo('nump/DegreeSequences');

  } 
   elsif ($input{'program'} eq "PartitionsGrayCode"){
    &OutputHeader('Partitions in Gray Code', 'nump/DegreeSequences');
    print "Partitions in Gray Code for <I>n</I> = $input{'n'}";
    if ($input{'m'}){
      print " with largest part <I>m</I> = $input{'m'}.<p>\n";}
    else{
      print ".<P>\n";}
    if ($input{'k'}){ 
      &ExtraParam('k', $input{'k'});}
    if (int($input{'n'}) > 50 ) 
    {
      &TooBig('n', 50); 
      exit(0);
    }
    if (int($input{'n'}) < 1)
    {
      &TooSmall('n', 1);
      exit(0);
    }
    &PrintTableHeaders;
    if (!$input{'m'}) {
      $input{'m'} = '0';}
    $retval = system "$GrayPartBIN $params -m $input{'m'}";
    print "</TABLE>\n";
    &WorkingMoreInfo('nump/NumPartition');
  }
	
  &CloseDocument;
}





