/************************************
 *  compactcodes2.c
 *  Started:  April 26th, 2005
 * 
 *  this compact codes problem is based off a paper by Emily Norwood,
 *  which looks at a way to count the number of different possible 
 *  compact codes, using the recursive function:
 *
 *  |T(2s, q+s)| = | U   T(p,q)|
 *                 p>=s
 *
 *  Where T(p,q) is the number of nontrivally different trees with q 
 *  terminal nodes, and p of those terminal nodes being at the bottom level
 ************************************/

#include <stdio.h>  // stderr
#include <stdlib.h>

void T(int p, int q);
void arrayFill(int a[], int fill, int x);
void printIndent();

#define MAX_N 51
#define MAX_OUTPUTS 1000001

int current;
int methodCalls;
int indent;

int currArray[MAX_OUTPUTS][MAX_N];

void arrayFill(int a[], int fillNum, int x)
{
  int i;

  for (i=1; i <= x; i++) {
    a[i] = fillNum;
  }
}

// int T(int p, int q) counts the number of nontrivally different trees
// with q terminal nodes, and p of those terminal nodes are at the
// bottom level of the tree.
//
// NOTE:  p should be even

void T(int pPar, int qPar)
{
  int s, q, p;
  int count1, count2;
  int bln;
  int oldCurr;

  // used for counting # of method calls
  methodCalls++;


  // if we've already printed out MAX_OUTPUTS arrays, we can stop
  if (current > MAX_OUTPUTS) {
    fprintf(stderr, "ERROR:  You requested more than %d\n", MAX_OUTPUTS);
    exit(1);
  }

  // since pPar should be even, and there can't be any trees with
  // an odd number of bottom level nodes, if pPar % 2 != 0, there
  // are 0 trees
  if (pPar % 2 != 0) {
    return;
  }

  if ((pPar == 2) && (qPar == 2)) {
    current++;
    arrayFill(currArray[current], 1, 2);
    return;
  }
  else if ((pPar == 4) && (qPar == 4)) {
    current++;
    arrayFill(currArray[current], 2, 4);
    return;
  }
  else if ((pPar == 8) && (qPar == 8)) {
    current++;
    arrayFill(currArray[current], 3, 8);
    return;
  }
  else if ((pPar == 16) && (qPar == 16)) {
    current++;
    arrayFill(currArray[current], 4, 16);
    return;
  }
  else if ((pPar == 32) && (qPar == 32)) {
    current++;
    arrayFill(currArray[current], 5, 32);
    return;
  }
  else if ((pPar == 64) && (qPar == 64)) {
    current++;
    arrayFill(currArray[current], 6, 64);
    return;
  }

  // ASSUMPTION:  No powers of 2 over 64 being used
  if (pPar == qPar) {
    return;
  }

  // since we're trying to find T(2s, q+s), s = pPar/2 ...
  s = pPar / 2;

  // ... and q = qPar - s
  q = qPar-s;

  // since we only want to check p's that are even, we need to make sure
  // p is even initally...if not, add one to it
  if (s % 2 == 1) {
    p = s+1;
  }
  else {
    p = s;
  }

  // increment p=p+2, so that we only test the p's which are even
  for (; p <= q; p=p+2) {
    oldCurr = current;

    //  this will print out what p and q is currently being looked at
    indent++;
    printIndent();
    printf("p: %d q: %d\n", p, q);
    T(p, q);
    indent--;

    // if oldCurr != current, then we have found new codes in our
    // recursive call, and need to split these new codes
    if (oldCurr != current) {
      for (count1 = oldCurr+1; count1 <= current; count1++) {
	bln = currArray[count1][q];

	for (count2 = q-s+1; count2 <= q+s; count2++) {
	  currArray[count1][count2] = bln+1;
	}
      }
      // we need to split s of the blns
    }
  }
}

void printIndent() {
  int i;
  for (i=1; i <= indent; i++) {
    printf("  ");
  }
}

int main(int argv, char **argc) {
  int P, Q;
  int N;
  int i,j;
  current = 0;
  methodCalls = 0;
  indent = 0;

  if (argv != 2) {
    fprintf(stderr, "Usage:  [program] n\n\n");
    exit(1);
  }

  //  P = atoi(argc[1]);
  //  Q = atoi(argc[2]);

  N = atoi(argc[1]);

  for (i=2; i <= N; i=i+2) {
    printf("***************** T %d,%d ****************\n", i, N);
    T(i, N);
  }

  /**********************************
  for (i=1; i <= current; i++) {
    for (j=1; j <= Q; j++) {
      printf("%d ", currArray[i][j]);
    }
    printf("\n");
  }
  ***********************************/

  printf("Method Calls: \t\t%d\n", methodCalls);
  printf("Number of trees:  \t%d\n", current);

  return 0;
}






