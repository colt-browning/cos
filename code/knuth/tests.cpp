/*
 * Copyright (c) 2020 Arturo Merino, Ondrej Micka, Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cassert>
#include <vector>
#include <utility>
#include <tuple>
#include <string>
#include <iostream>
#include "knuth_gray_code.hpp"
#include "periodic_path.hpp"
#include "tests.hpp"
#include "tree.hpp"
#include "utils.hpp"

struct Tests{
  static inline void expect(bool condition, const std::string &message) {
    if (!condition) {
      std::cout << "FAILED TEST" << std::endl;
      std::cout << message << std::endl;
      exit(-1);
    }
  }

  // utility function that turns {0,1}-string to vector of bools
  static std::vector<bool> bitstring_to_vector(const std::string &bitstring) {
    std::vector<bool> out(bitstring.size(), false);
    for (int i = 0; i < bitstring.size(); ++i) {
      assert((bitstring[i] == '0' || bitstring[i] == '1') && "01 string is expected as input");
      out[i] = (bitstring[i] == '1');
    }
    return out;
  }

  static void check_gcd(int a, int b, int answer_gcd, int answer_ca, int answer_cb) {
    int ca, cb;
    int gcd = ext_gcd(a,b,ca,cb);
    expect(
      (answer_gcd == gcd) && (answer_ca == ca) && (answer_cb == cb),
      "wrong GCD computation for integers " + std::to_string(a) + "," + std::to_string(b) + ": " + std::to_string(gcd) + "," + std::to_string(ca) + "," + std::to_string(cb)
    );
  }

  static void test_gcd() {
    std::vector<std::tuple<int, int, int, int, int>> tests = {
      {10,5,5,0,1},
      {656,124,4,7,-37},
      {6765,4181,1,1597,-2584},
      {157,180,1,-47,41},
      {4182,16760,2,1571,-392},
      {23,0,23,1,0},
      {0,13,13,0,1},
      {25,25,25,0,1}
    };
    std::cout << "testing GCD computation" << std::endl;
    for (auto test_case : tests) {
      check_gcd(std::get<0>(test_case), std::get<1>(test_case), std::get<2>(test_case), std::get<3>(test_case), std::get<4>(test_case));
    }
    std::cout << "OK" << std::endl;
  }

  static void check_inv(int a, int b, int answer_inv) {
    int inv = inverse(a,b);
    expect(
      answer_inv == inv,
      "wrong multiplicative inverse computation for integers " + std::to_string(a) + "," + std::to_string(b)
    );
  }

  static void test_inv() {
    std::vector<std::tuple<int,int,int>> tests = {
      {3,26,9},
      {2,13,7},
      {2,3,2},
      {9,100,89},
      {11,2,1}
    };
    std::cout << "testing multiplicative inverse computation" << std::endl;
    for (auto test_case : tests) {
      check_inv(std::get<0>(test_case), std::get<1>(test_case), std::get<2>(test_case));
    }
    std::cout << "OK" << std::endl;
  }

  static void check_factoring(int n, const std::vector<int> &answer_factors, const std::vector<int> &answer_mult) {
    std::vector<int> factors;
    std::vector<int> mult;
    factor(n, factors, mult);
    expect(
      (answer_factors.size() == factors.size()) && (answer_mult.size() == mult.size()),
      "wrong number of factors or multiplicities for integer " + std::to_string(n)
    );
    for (int i = 0; i < answer_factors.size(); ++i) {
      expect(
        answer_factors[i] == factors[i],
        "wrong factor for integer " + std::to_string(n)
      );
    }
    for (int i = 0; i < answer_mult.size(); ++i) {
      expect(
        answer_mult[i] == mult[i],
        "wrong multiplicities for integer " + std::to_string(n)
      );
    }
  }

  static void test_factoring() {
    std::vector<std::tuple<int, std::vector<int>, std::vector<int>>> tests = {
      {1, {}, {}},
      {2, {2}, {1}},
      {8, {2}, {3}},
      {27, {3}, {3}},
      {525, {3,5,7}, {1,2,1}},
      {362505, {3,5,11,13}, {1,1,1,3}},
      {7919, {7919}, {1}},
      {62710561, {7919}, {2}}
    };
    std::cout << "testing factoring" << std::endl;
    for (auto test_case : tests) {
      check_factoring(std::get<0>(test_case), std::get<1>(test_case), std::get<2>(test_case));
    }
    std::cout << "OK" << std::endl;
  }

  static void check_switches(int n, int d, const std::vector<bool> &answer_x, int answer_over, int answer_under, int answer_f, int answer_finv, int answer_eff_shift) {
    Switch swi(n, d);
    std::vector<bool> x = swi.get_x();
    expect(
      answer_x.size() == x.size(),
      "wrong switch length for parameters " + std::to_string(n) + "," + std::to_string(d)
    );
    for (int i = 0; i < x.size(); ++i) {
      expect(
        answer_x[i] == x[i],
        "wrong switch for parameters " + std::to_string(n) + "," + std::to_string(d)
      );
    }
    expect(
      (answer_over == swi.get_overlined()) && (answer_under == swi.get_underlined()) && (answer_f == swi.get_f()) && (answer_finv == swi.get_finv()) && (answer_eff_shift == swi.get_eff_shift()),
      "wrong switch for parameters " + std::to_string(n) + "," + std::to_string(d)
    );
  }

  static void test_switches() {
    std::vector<std::tuple<int, int, std::vector<bool>, int, int, int, int, int>> tests = {
      {1,1,{1,0,0},1,2,-1,2,1},
      {3,1,{1,1,1,0,0,0,0},3,6,-1,6,1},
      {6,1,{1,1,1,1,1,1,0,0,0,0,0,0,0},6,12,-1,12,1},
      {2,2,{1,0,1,0,0},4,3,-1,4,2},
      {5,2,{1,0,1,0,1,0,1,0,1,0,0},10,9,-1,10,2},
      {6,2,{1,0,1,0,1,0,1,0,1,0,1,0,0},12,11,-1,12,2},
      {4,3,{1,1,0,0,1,0,0,1,0},3,6,-1,6,-3},
      {7,3,{1,1,0,1,1,0,0,1,0,0,1,0,0,1,0},6,12,-1,12,-3},
      {7,5,{1,1,1,0,0,0,1,1,0,0,0,1,1,0,0},5,10,-1,10,-5},
      {10,3,{1,1,0,1,1,0,1,1,0,0,1,0,0,1,0,0,1,0,0,1,0},9,18,-1,18,-3},
      {10,7,{1,1,1,1,0,0,0,0,1,1,1,0,0,0,0,1,1,1,0,0,0},7,14,-1,14,-7},
    };
    std::cout << "testing switch generation" << std::endl;
    for (auto test_case : tests) {
      check_switches(std::get<0>(test_case), std::get<1>(test_case), std::get<2>(test_case), std::get<3>(test_case), std::get<4>(test_case), std::get<5>(test_case), std::get<6>(test_case), std::get<7>(test_case));
    }
    std::cout << "OK" << std::endl;
  }

  static void check_dyck_shift(const std::string &bitstring, const int answer) {
    assert((bitstring.length() % 2 == 1) && "input string must be of odd length");
    int n = (bitstring.length() - 1) / 2;
    std::vector<bool> x = bitstring_to_vector(bitstring);
    int ones, zeros;
    std::tie(ones, zeros) = Periodic_path::count_bits(x);
    Periodic_path p(n, 0);  // create dummy periodic path (shift value is irrelevant)
    p.has_more_ones_ = (ones > zeros);  // fake the only thing about the bitstring that the path needs to know

    assert(
      ((p.has_more_ones_ && (ones == n+1)) || (!p.has_more_ones_ && (zeros == n+1))) &&
      "input string must have exactly one more 0s than 1s or vice versa"
    );

    expect(
      p.dyck_shift(x) == answer,
      "wrong Dyck shift for input " + bitstring
    );
  }

  static void test_dyck_shift() {
    std::vector<std::pair<std::string, int>> tests = {
      {"010011001", 4},
      {"101010100", 0},
      {"011001100", 1},
      {"100", 0},
      {"10001", 4},
      {"110", 1},
      {"10110", 3},
      {"100101101", 6},
      {"110110100", 1},
    };
    std::cout << "testing calculation of Dyck shift" << std::endl;
    for (auto test_case : tests) {
      check_dyck_shift(test_case.first, test_case.second);
    }
    std::cout << "OK" << std::endl;
  }

  static void check_tree_potential(const std::string &tree_string, const int answer) {
    std::vector<bool> x = bitstring_to_vector(tree_string);
    Tree t(x);
    t.centroids_and_potential();
    expect(t.potential_ == answer, "wrong potential of tree " + tree_string);
  }

  static void test_tree_potential() {
    std::vector<std::pair<std::string, int>> tests = {
      // n = #vertices
      // stars have potential n-1
      {"10", 1},
      {"1100", 2},
      {"1010", 2},
      {"110101010100", 6},
      {"101010101010", 6},
      // odd paths have potential (n^2-1)/4
      {"111111000000", 12},
      {"11111111110000000000", 30},
      // even paths have potential n^2/4
      {"111000", 4},
      {"111111111000000000", 25},
      // some trees from the big figures with the spanning tree T_n in the paper
      {"11101000", 5},
      {"10110010", 5},
      {"1101101000", 7},
      {"1010110100", 7},
      {"1111010000", 8},
      {"1010111000", 8},
      {"101100110100", 9},
      {"111101010000", 9},
      {"110110011000", 8},
      {"11010110101000", 10},
      {"11001011101000", 13},
      {"11101100110000", 10},
      {"11010011110000", 15},
      {"11010010111000", 12},
      {"11011001100100", 9}
    };
    std::cout << "testing calculation of tree potential" << std::endl;
    for (auto test_case : tests) {
      check_tree_potential(test_case.first, test_case.second);
    }
    std::cout << "OK" << std::endl;
  }

  static void check_subtree_leaves(const std::string &tree_string, const int subtree_root_id, const std::vector<int> &answer_leaves_id) {
    std::vector<bool> x = bitstring_to_vector(tree_string);
    Tree t(x);
    Tree::Subtree s(&t, t.get_vtx_by_id(subtree_root_id));
    expect(
      s.leaves_.size() == answer_leaves_id.size(),
      "wrong number of leaves in subtree " + std::to_string(subtree_root_id) + " of tree " + tree_string
    );
    auto it = s.leaves_.cbegin();
    for (int i = 0; i < answer_leaves_id.size(); ++i, ++it) {
      expect(
        answer_leaves_id[i] == (*it)->get_id(),
        "wrong leaf in subtree " + std::to_string(subtree_root_id) + " of tree " + tree_string
      );
    }
  }

  static void test_subtree_leaves() {
    std::vector<std::tuple<std::string, int, std::vector<int>>> tests = {
      // tree, subtree, answer
      // stars
      {"10101010", 1, {1}},
      {"10101010", 2, {2}},
      {"11010100", 1, {2,3,4}},
      {"11010100", 2, {2}},
      {"110101010100", 1, {2,3,4,5,6}},
      // paths
      {"11001100", 1, {2}},
      {"11001100", 3, {4}},
      {"11110000", 1, {4}},
      // various trees mostly from the paper or modifications of them
      {"1110010100", 1, {3,4,5}},
      {"11100100", 1, {3,4}},
      {"10110110100100", 2, {3,5,6,7}}
    };
    std::cout << "testing leaves in the subtree" << std::endl;
    for (auto test_case : tests) {
      check_subtree_leaves(std::get<0>(test_case), std::get<1>(test_case), std::get<2>(test_case));
    }
    std::cout << "OK" << std::endl;
  }

  static void check_subtree_initial_path(const std::string &tree_string, int subtree_root_id, int answer) {
    std::vector<bool> x = bitstring_to_vector(tree_string);
    Tree t(x);
    Tree::Subtree s(&t, t.get_vtx_by_id(subtree_root_id));
    expect(
      s.initial_path_length_ == answer,
      "wrong initial path length in subtree " + std::to_string(subtree_root_id) + " of tree " + tree_string
    );
  }

  static void test_subtree_initial_path() {
    std::vector<std::tuple<std::string, int, int>> tests = {
      // tree, subtree, answer
      // stars
      {"10101010", 1, 1},
      {"10101010", 2, 1},
      {"11010100", 1, 1},
      {"11010100", 2, 1},
      {"110101010100", 1, 1},
      // paths
      {"11001100", 1, 2},
      {"11001100", 3, 2},
      {"11110000", 1, 4},
      // various trees mostly from the paper or modifications
      {"1110010100", 1, 1},
      {"11100100", 1, 1},
      {"1110100010", 1, 2},
      {"10110110100100", 2, 1},
      {"101111011010010000", 2, 3},
      {"101011110000", 3, 4}
    };
    std::cout << "testing initial path length in the subtree" << std::endl;
    for (auto test_case : tests) {
      check_subtree_initial_path(std::get<0>(test_case), std::get<1>(test_case), std::get<2>(test_case));
    }
    std::cout << "OK" << std::endl;
  }

  // The subtree string has to include the edge into the subtree's root.
  // The leaf id is with respect to the tree created from the subtree string.
  static void check_select_leaf(const std::string &subtree_string, int potential, int answer_leaf_id, Periodic_path::Operation answer_op) {
    // make a dummy Periodic_path object
    Periodic_path p(1, 0);
    p.init("100");

    // create a tree that contains only the given subtree as the root's child
    std::vector<bool> x = bitstring_to_vector(subtree_string);
    Tree *t = new Tree(x);   // tree on the stack would be deallocated twice, due to p
    // tweak the tree so that it looks like it is rooted in the centroid
    t->centroids_.first = t->root_;
    t->centroids_.second = nullptr;  // this makes sure it will not look like a dumbbell
    // fake the potential
    t->potential_ = potential;
    // and give it to the path as a centroid tree
    p.centroid_tree_ = t;

    // now make the subtree
    Tree::Subtree s(t, t->root_->get_left_child());

    auto selected = p.select_leaf(&s);
    expect(
      answer_leaf_id == selected.first->get_id(),
      "incorrect leaf selected in subtree " + subtree_string
    );
    expect(
      answer_op == selected.second,
      "incorrect operation selected in subtree " + subtree_string
    );

    delete t;
  }

  static void test_select_leaf() {
    typedef Periodic_path::Operation Op;  // to make test cases more readable
    std::vector<std::tuple<std::string, int, int, Op>> tests = {
      // subtree, potential, answer leaf, answer operation
      // rule (q137) -> pull leftmost leaf
      // q_1
      {"1100", 0, 2, Op::PULL},
      {"111000", 0, 3, Op::PULL},
      {"11111111110000000000", 0, 10, Op::PULL},
      // q_3
      {"11100100", 0, 3, Op::PULL},
      {"1111111001000000", 0, 7, Op::PULL},
      // q_7
      {"1110011000", 0, 3, Op::PULL},
      {"1111110011000000", 0, 6, Op::PULL},
      // rule (q24) -> push rightmost leaf
      // q_2
      {"110100", 0, 3, Op::PUSH},
      {"11101000", 0, 4, Op::PUSH},
      // q_4
      {"11010100", 0, 4, Op::PUSH},
      {"111111101010000000", 0, 9, Op::PUSH},
      // rule (q5) -> push middle leaf
      {"1110100100", 0, 4, Op::PUSH},
      {"11111010010000", 0, 6, Op::PUSH},
      // rule (q8) -> pull rightmost leaf
      {"1101011000", 0, 5, Op::PULL},
      {"11110101100000", 0, 7, Op::PULL},
      // rule (e) -> pull leftmost leaf
      {"1110010100", 2, 3, Op::PULL},
      {"1111110010100000", 2, 6, Op::PULL},
      {"110110100100", 2, 2, Op::PULL},
      {"1101010100", 2, 2, Op::PULL},
      // rule (o1) -> pull rightmost
      {"111001011000", 3, 6, Op::PULL},
      {"1111100101100000", 3, 8, Op::PULL},
      {"110110110000", 3, 6, Op::PULL},
      // rule (o2) -> push rightmost
      {"1110010100", 3, 5, Op::PUSH},
      {"1111110010100000", 3, 8, Op::PUSH},
      {"110110100100", 3, 6, Op::PUSH},
      {"1111011010010000", 3, 8, Op::PUSH},
      {"1101010100", 3, 5, Op::PUSH},
    };
    std::cout << "testing leaf selection" << std::endl;
    for (auto test_case : tests) {
      check_select_leaf(std::get<0>(test_case), std::get<1>(test_case), std::get<2>(test_case), std::get<3>(test_case));
    }
    std::cout << "OK" << std::endl;
  }

  static void check_symmetry(const std::string &tree_string, int answer) {
    std::vector<bool> x = bitstring_to_vector(tree_string);
    Tree t(x);
    t.normalize();
    expect(t.get_symmetry() == answer, "wrong symmetry of tree " + tree_string);
  }

  static void test_symmetry() {
    std::vector<std::pair<std::string, int>> tests = {
      // tree, answer leaf
      // two centroids
      {"111000", 1},
      {"110010", 1},
      {"11111110000000", 1},
      {"10101011010100", 1},
      {"101100101101011000", 0},
      {"101011001101011000", 1},
      {"110010101101011000", 0},
      {"11001011100100", 1},
      // one centroid
      {"1010101010", 1},
      {"101010101100", 0},
      {"101100101100101100", 2},
      {"110100110010110100110010", 3},
      {"110100110010110100101100", 0},
      {"11001100", 1},
      {"10110010", 0},
      {"11001011100010", 0},
    };
    std::cout << "testing symmetry calculation" << std::endl;
    for (auto test_case : tests) {
      check_symmetry(test_case.first, test_case.second);
    }
    std::cout << "OK" << std::endl;
  }

  static void check_booth(const std::string &bitstring, const std::string &possible_rots, int answer) {
    std::vector<bool> S = bitstring_to_vector(bitstring);
    std::vector<bool> P = bitstring_to_vector(possible_rots);
    expect(
      Tree::lex_smallest_rotation(S, P) == answer,
      "incorrect smallest rotation for string " + bitstring + " with possible rotations " + possible_rots
    );
  }

  static void test_booth() {
    std::vector<std::tuple<std::string, std::string, int>> tests = {
      // bitstring, possible rotations, answer
      {"10101010", "10101010", 0},
      {"11001010", "10001010", 4},
      {"10110010", "10100010", 6},
      {
        "110100110010110100101100",
        "100000100010100000101000", 18
      },{
        "1101001010101010",
        "1000001010101010", 6
      },{
        "1010101010110010",
        "1010101010100010", 14
      }
    };
    std::cout << "testing Booth" << std::endl;
    for (auto test_case : tests) {
      check_booth(std::get<0>(test_case), std::get<1>(test_case), std::get<2>(test_case));
    }
    std::cout << "OK" << std::endl;
  }

  // check that the shift of one block of the whole Gray code equals the Catalan number modulo 2*n+1
  static void check_catalan_shift(const std::string &bitstring) {
    std::vector<bool> x = bitstring_to_vector(bitstring);
    assert((x.size() % 2 == 1) && "input string needs to be of odd length");
    int n = (x.size() - 1)/2;
    Periodic_path p(n, 0);
    p.init(x);
    int initial_ds = p.dyck_shift_;

    int dummy;
    do {
      p.next();
    } while (!p.is_rotation_of(x, dummy));  // block ends with cyclic rotation of initial bitstring

    int final_shift = (p.dyck_shift_ - initial_ds + 2*n + 1) % (2*n + 1);
    expect((final_shift == catalan(n)), "incorrect final shift for starting string " + bitstring);
  }

  static void test_catalan_shift() {
    std::vector<std::string> tests = {
      "101010100",
      "110011000",
      "001110011",
      "11100011000",
      "11100011001",
      "10101011001",
      "0101010101010",
      "1110000111000",
      "111100001110000",
      "111110000111000"
    };
    std::cout << "testing Catalan shift" << std::endl;
    for (auto test_case : tests) {
      check_catalan_shift(test_case);
    }
    std::cout << "OK" << std::endl;
  }

  static void check_init_switches(int n, int s, int s_scaled, int answer_s_scaled, int answer_fac, int answer_fac_inv) {
    std::function<void (int, const std::vector<bool> &)> visit;
    std::vector<bool> x(2*n+2,0);
    for (int i=0; i<=n; ++i) {
      x[i] = 1;
    }
    Knuth_gray_code gc(n,s,x,0,visit);
    gc.init_switches(s_scaled);
    expect(
      (answer_s_scaled == gc.s_scaled_) && (answer_fac == gc.fac_) && (answer_fac_inv == gc.fac_inv_),
      "wrong scaling for parameters " + std::to_string(n) + "," + std::to_string(s) + ": " + std::to_string(s_scaled)
    );
  }

  static void test_init_switches() {
    std::vector<std::tuple<int, int, int, int, int, int>> tests = {
      {4,2,0,1,5,2},
      {4,2,1,1,5,2},
      {4,4,3,4,1,1},
      {4,4,8,8,2,5},
      {7,1,9,11,11,11},
      {10,2,6,8,4,16},
      {24,1,0,1,1,1},  // 48=7^2
      {577,4,15,1093,562,298},  // 1155=3*5*7*11, 15=3*5, 15-7*11=1093
      {16978,33956,7,33931,26,32651},  // 33957=3^2*7^3*11, 7=7, 7-3*11=33931
      {16978,1,231,151,151,9445},  // 33957=3^2*7^3*11, 231=3*7*11, 231-3=228=2^2*3*19, 228-7*11=151
      {16978,4,0,33877,33937,22072},  // 33957=3^2*7^3*11, 0-3=33954, 33954=2*3*5659, 33954-7*11=33877
    };
    std::cout << "testing switch/scaling initialization" << std::endl;
    for (auto test_case : tests) {
      check_init_switches(std::get<0>(test_case), std::get<1>(test_case), std::get<2>(test_case), std::get<3>(test_case), std::get<4>(test_case), std::get<5>(test_case));
    }
    std::cout << "OK" << std::endl;
  }
};

void run_tests() {
  std::cout << "running tests" << std::endl;
  Tests::test_gcd();
  Tests::test_inv();
  Tests::test_factoring();
  Tests::test_switches();
  Tests::test_dyck_shift();
  Tests::test_tree_potential();
  Tests::test_subtree_leaves();
  Tests::test_subtree_initial_path();
  Tests::test_select_leaf();
  Tests::test_booth();
  Tests::test_symmetry();
  Tests::test_catalan_shift();
  Tests::test_init_switches();
  std::cout << "testing finished OK" << std::endl;
}