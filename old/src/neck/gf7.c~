/* Program to enumerate all irreducible and/or primitive polynomials over GF(7) 
   Gilbert Lee
   Sept 08, 2006
*/

#include <stdio.h>
#include <stdlib.h>

/* Type definitions */
typedef unsigned long long ulong;

int encoding[8] = {0,2,4,5,6,3,7};
int decoding[8] = {0,-1,1,5,2,3,4,6};

typedef struct{
  /* Each polynomial is expressed in three words. 
     Coefficient: 0 1 2 3 4 5 6 */
  ulong x1;    /* 0 0 0 1 0 1 1 */
  ulong x2;    /* 0 1 0 0 1 1 1 */
  ulong x3;    /* 0 0 1 1 1 0 1 */
} Poly_GF7; 

/*========================================================================*/
/* Global Constants                                                       */
/*========================================================================*/
ulong ONE = (ulong)1;
ulong TWO = (ulong)2;
ulong iter_limit = 0;

#define MAX 64
/* MAX is the size of unsigned long long */
#define MAX_ITERATIONS 10000
#define HTML_OUTPUT 1

#define value(a,i) (decoding[(((a).x3&(ONE<<(i)))?4:0) + (((a).x2&(ONE<<(i)))?2:0) + (((a).x1&(ONE<<(i)))?1:0)])
#define val(a,i)    ((((a).x3&(ONE<<(i)))?4:0) + (((a).x2&(ONE<<(i)))?2:0) + (((a).x1&(ONE<<(i)))?1:0))
Poly_GF7 poly_table[MAX+1] = { 
  /* A list of primitive polynomials                  */        
  /* poly_table[i] contains what x^i is equivalent to */
  /* E.g. given primitive polynomial x^3 + 3x + 2:    */
  /* x^3 ~= 4x + 5 --> {011,110} = {1,3,2}            */
  /*   x1: 0 1   -->  1                              */ 
  /*   x2: 1 1   -->  3                              */
  /*   x3: 1 0   -->  2                              */

  /* 00  */ {(ulong)0ULL, (ulong)0ULL, (ulong)1ULL}, 
  /* 01  */ {(ulong)0ULL, (ulong)0ULL, (ulong)1ULL},

  /* 02  */ {(ulong)  2ULL, (ulong)  3ULL, (ulong)  3ULL}, /* x^2+x+3 */
  /* 03  */ {(ulong)  1ULL, (ulong)  3ULL, (ulong)  2ULL}, /* x^3+3*x+2 */
  /* 04  */ {(ulong)  4ULL, (ulong)  6ULL, (ulong)  7ULL}, /* x^4+x^2+3*x+5 */
  /* 05  */ {(ulong)  3ULL, (ulong)  2ULL, (ulong)  3ULL}, /* x^5+x+4 */
  /* 06  */ {(ulong)  2ULL, (ulong)  6ULL, (ulong)  7ULL}, /* x^6+3*x^2+x+5 */
  /* 07  */ {(ulong)  1ULL, (ulong)  3ULL, (ulong)  0ULL}, /* x^7+6*x+2 */
  /* 08  */ {(ulong)  2ULL, (ulong)  3ULL, (ulong)  3ULL}, /* x^8+x+3 */
  /* 09  */ {(ulong)  7ULL, (ulong)  7ULL, (ulong)  6ULL}, /* x^9+x^2+x+2 */
  /* 10  */ {(ulong)  2ULL, (ulong)  2ULL, (ulong)  7ULL}, /* x^10+5*x^2+x+5 */
  /* 11  */ {(ulong)  3ULL, (ulong)  2ULL, (ulong)  3ULL}, /* x^11+x+4 */
  /* 12  */ {(ulong)  2ULL, (ulong)  7ULL, (ulong)  5ULL}, /* x^12+3*x^2+2*x+3 */
  /* 13  */ {(ulong)  7ULL, (ulong)  7ULL, (ulong)  6ULL}, /* x^13+x^2+x+2 */
  /* 14  */ {(ulong)  6ULL, (ulong)  7ULL, (ulong)  1ULL}, /* x^14+2*x^2+2*x+3 */
  /* 15  */ {(ulong)  1ULL, (ulong)  6ULL, (ulong)  7ULL}, /* x^15+3*x^2+3*x+4 */
  /* 16  */ {(ulong)  2ULL, (ulong)  3ULL, (ulong)  1ULL}, /* x^16+2*x+3 */
  /* 17  */ {(ulong)  3ULL, (ulong)  2ULL, (ulong)  3ULL}, /* x^17+x+4 */
  /* 18  */ {(ulong)  2ULL, (ulong)  6ULL, (ulong)  7ULL}, /* x^18+3*x^2+x+5 */
  /* 19  */ {(ulong)  7ULL, (ulong)  6ULL, (ulong)  1ULL}, /* x^19+2*x^2+2*x+4 */
  /* 20  */ {(ulong)  6ULL, (ulong)  2ULL, (ulong)  7ULL}, /* x^20+4*x^2+x+5 */
  /* 21  */ {(ulong)  1ULL, (ulong)  6ULL, (ulong)  7ULL}, /* x^21+3*x^2+3*x+4 */
  /* 22  */ {(ulong)  8ULL, (ulong)  9ULL, (ulong)  9ULL}, /* x^22+x^3+3 */
  /* 23  */ {(ulong)  7ULL, (ulong)  3ULL, (ulong)  6ULL}, /* x^23+4*x^2+x+2 */
  /* 24  */ {(ulong)  8ULL, (ulong) 15ULL, (ulong) 15ULL}, /* x^24+x^3+3*x^2+3*x+3 */
  /* 25  */ {(ulong) 11ULL, (ulong)  9ULL, (ulong) 10ULL}, /* x^25+x^3+4*x+2 */
  /* 26  */ {(ulong)  6ULL, (ulong)  7ULL, (ulong)  7ULL}, /* x^26+x^2+x+3 */
  /* 27  */ {(ulong)  7ULL, (ulong)  6ULL, (ulong)  5ULL}, /* x^27+x^2+2*x+4 */
  /* 28  */ {(ulong)  2ULL, (ulong)  2ULL, (ulong)  5ULL}, /* x^28+5*x^2+2*x+5 */
  /* 29  */ {(ulong)  1ULL, (ulong)  1ULL, (ulong)  2ULL}, /* x^29+5*x+2 */
  /* 30  */ {(ulong)  6ULL, (ulong)  6ULL, (ulong)  7ULL}, /* x^30+x^2+x+5 */
  /* 31  */ {(ulong)  7ULL, (ulong)  5ULL, (ulong)  6ULL}, /* x^31+x^2+4*x+2 */
  /* 32  */ {(ulong)  8ULL, (ulong) 10ULL, (ulong)  9ULL} /* x^32+x^3+6*x+5 */
#if (MAX > 32)
  ,
  /* 33  */ {(ulong)  5ULL, (ulong)  4ULL, (ulong)  7ULL}, /* x^33+x^2+5*x+4 */
  /* 34  */ {(ulong)  2ULL, (ulong)  3ULL, (ulong)  1ULL}, /* x^34+2*x+3 */
  /* 35  */ {(ulong)  3ULL, (ulong)  2ULL, (ulong)  5ULL}, /* x^35+5*x^2+2*x+4 */
  /* 36  */ {(ulong) 14ULL, (ulong) 12ULL, (ulong) 15ULL}, /* x^36+x^3+x^2+4*x+5 */
  /* 37  */ {(ulong)  1ULL, (ulong)  3ULL, (ulong)  0ULL}, /* x^37+6*x+2 */
  /* 38  */ {(ulong)  6ULL, (ulong)  7ULL, (ulong)  7ULL}, /* x^38+x^2+x+3 */
  /* 39  */ {(ulong)  7ULL, (ulong)  7ULL, (ulong)  6ULL}, /* x^39+x^2+x+2 */
  /* 40  */ {(ulong)  2ULL, (ulong)  3ULL, (ulong)  1ULL}, /* x^40+2*x+3 */
  /* 41  */ {(ulong) 13ULL, (ulong) 13ULL, (ulong) 10ULL}, /* x^41+x^3+2*x^2+5*x+2 */
  /* 42  */ {(ulong) 12ULL, (ulong) 15ULL, (ulong)  5ULL}, /* x^42+2*x^3+x^2+6*x+3 */
  /* 43  */ {(ulong)  3ULL, (ulong)  7ULL, (ulong)  4ULL}, /* x^43+3*x^2+2*x+2 */
  /* 44  */ {(ulong)  8ULL, (ulong) 11ULL, (ulong)  9ULL}, /* x^44+x^3+6*x+3 */
  /* 45  */ {(ulong) 15ULL, (ulong) 13ULL, (ulong)  6ULL}, /* x^45+2*x^3+x^2+4*x+2 */
  /* 46  */ {(ulong)  4ULL, (ulong)  6ULL, (ulong)  7ULL}, /* x^46+x^2+3*x+5 */
  /* 47  */ {(ulong)  5ULL, (ulong)  6ULL, (ulong)  3ULL}, /* x^47+2*x^2+3*x+4 */
  /* 48  */ {(ulong) 24ULL, (ulong) 24ULL, (ulong) 31ULL}, /* x^48+x^4+x^3+5*x^2+5*x+5 */
  /* 49  */ {(ulong) 11ULL, (ulong)  9ULL, (ulong) 10ULL}, /* x^49+x^3+4*x+2 */
  /* 50  */ {(ulong)  8ULL, (ulong) 10ULL, (ulong)  9ULL}, /* x^50+x^3+6*x+5 */
  /* 51  */ {(ulong)  7ULL, (ulong)  6ULL, (ulong)  5ULL}, /* x^51+x^2+2*x+4 */
  /* 52  */ {(ulong)  2ULL, (ulong)  2ULL, (ulong)  7ULL}, /* x^52+5*x^2+x+5 */
  /* 53  */ {(ulong)  1ULL, (ulong)  1ULL, (ulong)  2ULL}, /* x^53+5*x+2 */
  /* 54  */ {(ulong) 10ULL, (ulong) 15ULL, (ulong)  7ULL}, /* x^54+2*x^3+3*x^2+x+3 */
  /* 55  */ {(ulong)  3ULL, (ulong)  6ULL, (ulong)  7ULL}, /* x^55+3*x^2+x+4 */
  /* 56  */ {(ulong)  6ULL, (ulong)  6ULL, (ulong)  7ULL}, /* x^56+x^2+x+5 */
  /* 57  */ {(ulong)  1ULL, (ulong)  3ULL, (ulong)  2ULL}, /* x^57+3*x+2 */
  /* 58  */ {(ulong) 16ULL, (ulong) 23ULL, (ulong) 23ULL}, /* x^58+x^4+3*x^2+3*x+3 */
  /* 59  */ {(ulong) 15ULL, (ulong) 14ULL, (ulong) 13ULL}, /* x^59+x^3+x^2+2*x+4 */
  /* 60  */ {(ulong) 12ULL, (ulong) 14ULL, (ulong)  7ULL}, /* x^60+2*x^3+x^2+3*x+5 */
  /* 61  */ {(ulong)  1ULL, (ulong)  1ULL, (ulong)  6ULL}, /* x^61+5*x^2+5*x+2 */
  /* 62  */ {(ulong)  8ULL, (ulong)  8ULL, (ulong) 15ULL}, /* x^62+x^3+5*x^2+5*x+5 */
  /* 63  */ {(ulong) 21ULL, (ulong) 21ULL, (ulong) 16ULL}, /* x^63+x^4+2*x^2+2 */
  /* 64  */ {(ulong)  2ULL, (ulong)  3ULL, (ulong)  1ULL}  /* x^64+2*x+3 */
#endif
};

/*========================================================================*/
/* Global Variables                                                       */
/*========================================================================*/
int N;                 /* Degree of the desired polynomials               */
int D;                 /* Density of desired polynomials                  */
int outformat = 1;     /* Output format of the polynomials                
                          0 = string, 1 = coefficients, 2 = expanded      */
int type = 0;          /* Type of polynomials to find: 0=all, 1=primitive */
ulong maxNumber = 0;   /* Maximum # of polynomials to output, 0 = all     */
ulong ir_count = 0;    /* Count # of irreducible polynomials found        */
ulong pr_count = 0;    /* Count # of primitive polynomials found          */
ulong printed = 0;     /* Current number of polynomials found             */
int doneFlag = 0;      /* Flag to mark when maxNumber of polys found      */
Poly_GF7 sevenN_1;     /* Represents 7^N-1                                */
int a[MAX+1] = {0};    /* Used to store the unlabelled necklace           */


/*========================================================================*/
/* Printing routines                                                      */
/*========================================================================*/
void PrintPoly(Poly_GF7 a){
  int i, x;
  if(HTML_OUTPUT){
    printf("x<sup>%d</sup> + ", N);
  } else {
    printf("x^%d + ", N);
  }
  for(i = N-1; i > 0; i--){
    x  = value(a,i);
    if(x){
      if(x > 1) printf("%d", x);
      if(i == 1) printf("x + ");
      else printf("x^%d + ", i);
    }
  }
  x = value(a,0);
  printf("%d\n", x);
}

void PrintBit(Poly_GF7 a){
  int i;
  for(i = N-1; i >= 0; i--) printf("%d", a.x3&(ONE<<i) ? 1 : 0); printf("\n");
  for(i = N-1; i >= 0; i--) printf("%d", a.x2&(ONE<<i) ? 1 : 0); printf("\n");
  for(i = N-1; i >= 0; i--) printf("%d", a.x1&(ONE<<i) ? 1 : 0); printf("\n");
}

void PrintString(Poly_GF7 a){
  int i;
  printf("1");
  for(i = N-1; i >= 0; i--){
    printf(" %d", value(a,i));
  }
  printf("\n");
}

void PrintCoeff(Poly_GF7 a){
  int i;
  if(HTML_OUTPUT){
    printf("%d<SUB>1</SUB>", N);
  } else {
    printf("1");
  }
  for(i = N-1; i >= 0; i--){
    if(value(a,i)){
      if(HTML_OUTPUT){
	printf(" %d<SUB>%d</SUB>", i, value(a,i));
      } else {
	printf("%d", value(a,i));
      }
    }
  }
  printf("\n");
}

void PrintBase(Poly_GF7 a){
  long long r = 0;
  int i;
  for(i = N-1; i >= 0; i--){
    r = r*7 + value(a,i);
  }
  printf("%lld\n", r);
}

int bitcount(ulong x){
  int c = 0;
  while(x){
    c++;
    x &= x-1;
  }
  return c;
}

int density(Poly_GF7 a){
  return bitcount(a.x1|a.x2|a.x3);
}

/*========================================================================*/
/* Number Operations                                              */
/*========================================================================*/

/* Returns -1 if a < b, 0 if a == b, +1 if a > b */
int cmp(Poly_GF7 a, Poly_GF7 b){
  int i, x, y;
  for(i = N-1; i >= 0; i--){
    x = value(a,i);
    y = value(b,i);
    if(x < y) return -1;
    if(x > y) return 1;
  }
  
  return 0;
}

/* Returns a - b, (assuming a > b and a,b are ternary numbers) */
Poly_GF7 subtract(Poly_GF7 a, Poly_GF7 b){
  int i, borrow = 0, x, y;
  Poly_GF7 r = {0,0,0};
  for(i = 0; i < N; i++){
    x = value(a,i) - borrow;
    y = value(b,i);
    if(x < y){
      borrow = 1;
      x += 7;
    } else {
      borrow = 0;
    }
    switch(x-y){
    case 1:                   r.x2 |= (ONE<<i);                   break;
    case 2: r.x3 |= (ONE<<i);                                     break;
    case 3: r.x3 |= (ONE<<i);                   r.x1 |= (ONE<<i); break;
    case 4: r.x3 |= (ONE<<i); r.x2 |= (ONE<<i);                   break;
    case 5:                   r.x2 |= (ONE<<i); r.x1 |= (ONE<<i); break;
    case 6: r.x3 |= (ONE<<i); r.x2 |= (ONE<<i); r.x1 |= (ONE<<i); break;
    }
  }
  return r;
}

/* Returns a % b (a and b treated as ternary numbers) */
Poly_GF7 mod(Poly_GF7 a, Poly_GF7 b){
  Poly_GF7 r = {0,0,0};
  int i;

  for(i = N-1; i >= 0; i--){
    r.x1 <<= 1; r.x2 <<= 1; r.x3 <<= 1;
    if(a.x1 & (ONE<<i)) r.x1++;
    if(a.x2 & (ONE<<i)) r.x2++;
    if(a.x3 & (ONE<<i)) r.x3++;
    while(cmp(b,r) <= 0){
      r = subtract(r, b);
    }
  }
  return r;
}

/* Returns the gcd of a and b (a and b treated as ternary numbers */
Poly_GF7 gcd(Poly_GF7 a, Poly_GF7 b){
  if(!b.x1 && !b.x2 && !b.x3) return a;
  return gcd(b, mod(a, b));
}

/*========================================================================*/
/* Polynomial Operations                                                  */
/*========================================================================*/

/* Returns a + b */
Poly_GF7 add(Poly_GF7 a, Poly_GF7 b){
  Poly_GF7 res;

  ulong x07 =  a.x3 &  b.x2;
  ulong x08 =  a.x2 &  b.x2;
  ulong x09 =  a.x2 & ~b.x1;
  ulong x10 =  a.x2 &  b.x3;
  ulong x11 =  a.x3 & ~b.x1;
  ulong x12 =  a.x3 &  b.x3;
  ulong x13 =  a.x1 |  b.x1;
  ulong x14 =  a.x3 ^  a.x1;
  ulong x15 = ~a.x1 &  b.x3;
  ulong x16 =   x07 ^   x10;
  ulong x17 =  b.x2 &  ~x14;
  ulong x18 =   x08 ^   x11;
  ulong x19 =   x09 ^   x10;
  ulong x20 =   x12 ^   x15;
  ulong x21 =   x12 ^   x17;
  res.x3    =   x18 ^   x20; 
  res.x2    =   x19 ^   x21; 
  res.x1    =   x13 ^   x16; 
  return res;
}

/* Returns (a * b) mod p (where p = poly_table[N]) */
Poly_GF7 multmod(Poly_GF7 a, Poly_GF7 b){
  Poly_GF7 t = a, result = {0,0,0}, t2;
  ulong top_bit = ONE << (N-1);
  int x;
  int i;

  for(i = 0; i < N; i++){
    x = value(b,0);
    switch(x){
    case 1: result = add(result, t); break;
    case 2: t2 = add(t,t); result = add(result,t2); break;
    case 3: t2 = add(t,t); result = add(result,t2); result = add(result,t); break;
    case 4: t2 = add(t,t); result = add(result,t2); result = add(result,t2); break;
    case 5: t2 = add(t,t); result = add(result,t2); result = add(result,t2); result = add(result,t); break;
    case 6: t2 = add(t,t); result = add(result,t2); result = add(result,t2); result = add(result,t2); break;
    }
    x = value(t,N-1);
    t.x1 = (t.x1 & ~top_bit) << 1;
    t.x2 = (t.x2 & ~top_bit) << 1;
    t.x3 = (t.x3 & ~top_bit) << 1;
    switch(x){
    case 1: t = add(t,poly_table[N]); break;
    case 2: t2 = add(poly_table[N],poly_table[N]); t = add(t,t2); break;
    case 3: t2 = add(poly_table[N],poly_table[N]); t = add(t,t2); t = add(t,poly_table[N]); break;
    case 4: t2 = add(poly_table[N],poly_table[N]); t = add(t,t2); t = add(t,t2); break;
    case 5: t2 = add(poly_table[N],poly_table[N]); t = add(t,t2); t = add(t,t2); t = add(t,poly_table[N]); break;
    case 6: t2 = add(poly_table[N],poly_table[N]); t = add(t,t2); t = add(t,t2); t = add(t,t2); break;
    }
    b.x1 >>= 1;
    b.x2 >>= 1;
    b.x3 >>= 1;
  }
  return result;
}

/* Returns (a^power) mod p (where p = poly_table[N]) */
Poly_GF7 powmod(Poly_GF7 a, Poly_GF7 power){
  Poly_GF7 t = a, result = {0,ONE,0}, t2, t4, t6;
  int x;

  while(power.x1 || power.x2 || power.x3){
    x = value(power,0);
    t2 = multmod(t,t);
    t4 = multmod(t2,t2);
    t6 = multmod(t4,t2);
    switch(x){
    case 1: result = multmod(result, t); break;
    case 2: result = multmod(result, t2); break;
    case 3: result = multmod(result, multmod(t2,t));break;
    case 4: result = multmod(result, t4); break;
    case 5: result = multmod(result, multmod(t4,t)); break;
    case 6: result = multmod(result, t6); break;
    }
    t = multmod(t,t6);
    power.x1 >>= 1;
    power.x2 >>= 1;
    power.x3 >>= 1;
  }
  return result;
}

Poly_GF7 negate(Poly_GF7 a){
  Poly_GF7 r = {0,0,0};
  int i;
  for(i = 0;  i < N; i++){
    switch(value(a,i)){
    case 6:                   r.x2 |= (ONE<<i);                   break;
    case 5: r.x3 |= (ONE<<i);                                     break;
    case 4: r.x3 |= (ONE<<i);                   r.x1 |= (ONE<<i); break;
    case 3: r.x3 |= (ONE<<i); r.x2 |= (ONE<<i);                   break;
    case 2:                   r.x2 |= (ONE<<i); r.x1 |= (ONE<<i); break;
    case 1: r.x3 |= (ONE<<i); r.x2 |= (ONE<<i); r.x1 |= (ONE<<i); break;
    }
  }
  return r;
}

/* Calculates the minimum polynomial, given a necklace */
Poly_GF7 minpoly(Poly_GF7 necklace){
  Poly_GF7 root = {0,TWO,0}, neg_root, result = {0,0,0}, root2, root4;
  Poly_GF7 f[MAX]; /* f[i] contains the polynomials listing the
		      coefficient of x^i in terms of the primitive root */
  int i, j;

  f[0].x1 = 0;
  f[0].x2 = ONE;
  f[0].x3 = 0;
  for(i = 1; i < N; i++) f[i].x1 = f[i].x2 = f[i].x3 = 0;
  root = powmod(root, necklace); 

  for(i = 0; i < N; i++){
    if(i){
      root2 = multmod(root,root);
      root4 = multmod(root2,root2);
      root  = multmod(root, multmod(root2, root4));
    }
    neg_root = negate(root);

    for(j = N-1; j >= 1; j--)
      f[j] = add(f[j-1], multmod(f[j], neg_root));
    f[0] = multmod(f[0], neg_root);
  }

  for(i = N-1; i >= 0; i--){
    j = value(f[i],0);
    switch(j){
    case 0: break;
    case 1:                        result.x2 |= (ONE<<i);                        break;
    case 2: result.x3 |= (ONE<<i);                                               break;
    case 3: result.x3 |= (ONE<<i);                        result.x1 |= (ONE<<i); break;
    case 4: result.x3 |= (ONE<<i); result.x2 |= (ONE<<i);                        break;
    case 5:                        result.x2 |= (ONE<<i); result.x1 |= (ONE<<i); break;
    case 6: result.x3 |= (ONE<<i); result.x2 |= (ONE<<i); result.x1 |= (ONE<<i); break;
    default : 
      fprintf(stderr, "TROUBLE\n");
    }
  }
  /*  printf("\n");*/
  return result;
}

void OutputPoly(Poly_GF7 poly, int isPrimitive){
  printed++;
  
  if(HTML_OUTPUT){
    if(isPrimitive){
      printf("<TR>");
      if(outformat & 1){
	printf("<TD>");
	if(type == 0) printf("<FONT COLOR=0000ff>");
	PrintString(poly);
	if(type == 0) printf("</FONT>");
      }
      if(outformat & 2){
	printf("<TD>");
	if(type == 0) printf("<FONT COLOR=0000ff>");
	PrintCoeff(poly);
	if(type == 0) printf("</FONT>");
      }
      if(outformat & 4){
	printf("<TD>");
	if(type == 0) printf("<FONT COLOR=0000ff>");
	PrintPoly(poly);
	if(type == 0) printf("</FONT>");
      }
    } else {
      printf("<TR>");
      if(outformat & 1){
	printf("<TD>");
	PrintString(poly);
      }
      if(outformat & 2){
	printf("<TD>");
	PrintCoeff(poly);
      }
      if(outformat & 4){
	printf("<TD>");
	PrintPoly(poly);
      }
    }
    if(maxNumber && printed >= maxNumber){
      printf("</TABLE>");
      exit(1);
    }
  } else {
    if(isPrimitive){
      printf("P ");
    } else {
      printf("I ");
    }
    switch(outformat){
    case 0: PrintBit(poly); break;
    case 1: PrintCoeff(poly); break;
    case 2: PrintPoly(poly); break;
    }
  }
}

void PrintIt(){
  int i;
  Poly_GF7 necklace = {0,0,0};
  Poly_GF7 gcdResult;
  Poly_GF7 mpoly;

  if(++iter_limit > MAX_ITERATIONS){
    printf("</TABLE>\n<BR>");
    if(type == 0){
      printf("The first %llu irreducible polynomials<BR>\n", ir_count);
      printf("<FONT COLOR=0000ff");
    }
    printf("The first %llu primitive polynomials\n<BR><BR>", pr_count);
    printf("<BR><B><BLINK><FONT COLOR=FF0000>Iteration limit exceeded !!</B><BR></BLINK></FONT>\n");
    exit(0);
  }

  for(i = 1; i <= N; i++){
    necklace.x1 <<= 1;
    necklace.x2 <<= 1;
    necklace.x3 <<= 1;
    switch(a[i]){
    case 1:                necklace.x2++;                break;
    case 2: necklace.x3++;                               break;
    case 3: necklace.x3++;                necklace.x1++; break;
    case 4: necklace.x3++; necklace.x2++;                break;
    case 5:                necklace.x2++; necklace.x1++; break;
    case 6: necklace.x3++; necklace.x2++; necklace.x1++; break;
    }
  }

  mpoly = minpoly(necklace);
  if(density(mpoly)+1 > D) return;

  ir_count++;

  gcdResult = gcd(necklace, sevenN_1);

  if(gcdResult.x3 == 0 && gcdResult.x2 == ONE && gcdResult.x1 == 0){
    ++pr_count;
    OutputPoly(mpoly, 1); 
  } else if(type == 0){
    OutputPoly(mpoly, 0); 
  }
  
  if(maxNumber && printed >= maxNumber){
    if(HTML_OUTPUT){
      printf("</TABLE>");
      exit(1);
    } else {
      printf("Print limit exceeded\n");
      doneFlag = 1;
    }
  }
}

void Gen(int t, int p){
  int j;
  if(doneFlag) return;
  if(t > N){
    if(p == N) PrintIt();
  } else {
    a[t] = a[t-p]; 
    Gen(t+1,p);
    for(j=a[t-p]+1; j<=6; j++) {
      a[t] = j; 
      Gen(t+1,t);
    }
  }
}

void ProcessInput(int argc, char *argv[])
{
  int i;
  if (argc > 1) N = atoi(argv[1]);
  if (argc > 2) D = atoi(argv[2]); else D = N+1;
  if (argc > 3) outformat = atoi(argv[3]);
  if (argc > 4) type = atoi(argv[4]); 
  if (argc > 5) maxNumber = atoi(argv[5]);
  if (argc > 6 || N < 2 || N > MAX){
    fprintf(stderr,"Usage: gf7 n d format type number\n");
    fprintf(stderr,"         All params except 'n' are optional\n");
    fprintf(stderr,"         n = degree (2-%d)\n", MAX);
    fprintf(stderr,"         d = density (2-%d)\n", MAX);
    fprintf(stderr,"         format = 0,1,2 = string,coeffs,poly\n");
    fprintf(stderr,"         type = 0/1 = all/primitive only\n");
    fprintf(stderr,"         num = max to find (0 for all)\n");
    exit( 1 );
  }

  for(i = 0; i < N; i++)
    sevenN_1.x1 |= (ONE<<i);
  sevenN_1.x3 = sevenN_1.x2 = sevenN_1.x1;
}

int main(int argc, char **argv)
{

  ProcessInput(argc, argv);
  
  if(HTML_OUTPUT){
    printf("N = %d. Maximum density = %d\n", N, D);
    printf("<P><TABLE BORDER=1 CELLSPACING=2 CELLPADDING=2><TR>\n");
    if (outformat & 1) {
      printf("<TH COLSPAN=1><FONT SIZE=+1>String</FONT><BR></TH>\n");
    }
    if (outformat & 2) {
      printf("<TH COLSPAN=1><FONT SIZE=+1>Coefficients</FONT><BR></TH>\n");
    }
    if (outformat & 4) {
      printf("<TH COLSPAN=1><FONT SIZE=+1>Polynomials</FONT><BR></TH>\n");
    }
  }

  Gen(1,1);
  if(HTML_OUTPUT){
    printf("</TABLE>\n<BR>");
  }

  /* Display final counts */
  if(type == 0){
    printf("The number of irreducible polynomials = %llu\n", ir_count);
    if(HTML_OUTPUT) printf("<BR><FONT COLOR=0000ff>");
  }
  printf("The number of primitive polynomials   = %llu\n", pr_count);
  return 0;
}

