#include <stdio.h>

#define TRUE 1
#define FALSE 0
int a[50];
int n,k;
long total,calls;
int forbid;

void Print(int p,int t) {

	int j,count;

	if (n%p == 0) {
		total++;
/*		for(j=1; j<=n; j++) 
			printf("%d ",a[j]);
		printf("\n");    */
	}

}

void Gen(int t, int p, int all_zeros) {
	
	int j;
	
	calls++;

	if(t>n) Print(p,t);
	else {
		a[t] = a[t-p]; 
		if (a[t] == 1) Gen(t+1, p, FALSE); 
		else if ((all_zeros == FALSE) || (t<forbid)) Gen(t+1,p,all_zeros);
		if (a[t] == 0) {
			a[t] = 1; 
			Gen(t+1,t,FALSE);
		}
	}
}

void main() {

	int i,j;
	double ratio;

	k = 2;
	forbid = 10;
	for (n=4; n <=30; n++) {
		calls = 0;
		total = 0;
		Gen(1,1,TRUE); 
		ratio = (double) calls/total;
		printf("N = %d, Tot = %d, Calls = %d,  Ratio = %lf\n", n,total,calls, ratio); 
	}
}
