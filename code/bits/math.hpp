/*
 * Copyright (c) 2016 Petr Gregor, Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef MATH_HPP
#define MATH_HPP

// compute the binomial cofficient \binom{n}{k}
long long binom(int n, int k);
// compute the number of vertices of the graph Q_{n,[k,l]}
long long num_vertices_Qnkl(int n, int k, int l);
// compute the difference between the larger and the smaller partition class of the graph Q_{n,[k,l]}
long long delta_Qnkl(int n, int k, int l);

#endif