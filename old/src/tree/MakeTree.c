/*  MakeTree.c
 *  
 *     program based on algorithm MakeTree by Frank Ruskey.
 *
 *  NOTE:  Method calls should be to MakeTree, MDFS should only
 *         be called from MDFS
 *
 *  Written:  April 12th, 2005
 *  Written By:  Chris Deugau
 *
 *  Last Update:
 *   x  April 12th 2005
 *************************/

#ifndef MAKETREE_C 
#define MAKETREE_C


#include <assert.h>
#include <stdlib.h>


void MDFS(int v);
void MakeTree(int *arrayPar, int NPar, int *leftPar, int *rightPar);

// variables for MakeTree.c
static int CurrentDepth = 0;
static int Root = 0;
static int j = 0;
static int NextInternalNode = 0;
static int EN = 0;
static int* a_different = NULL;
int* LeftKids = NULL;
int* RightKids = NULL;

void MDFS(int v) {
  CurrentDepth++;

  if (CurrentDepth == a_different[j]) {
    LeftKids[v] = j;
    j++;
  }
  else {
    NextInternalNode--;
    LeftKids[v] = NextInternalNode;
    MDFS(NextInternalNode);
  }

  if (CurrentDepth == a_different[j]) {
    RightKids[v] = j;
    j++;
  }
  else {
    NextInternalNode--;
    RightKids[v] = NextInternalNode;
    MDFS(NextInternalNode);
  }

  CurrentDepth--;
}

void MakeTree(int *arrayPar, int NPar, int *leftPar, int *rightPar) {
  // setting up array variables, not described in algorithm
  a_different = arrayPar;
  LeftKids = leftPar;
  RightKids = rightPar;
  EN = NPar;
  // end of array variable setup

  for (j=EN; j >= 1; j--) {
    LeftKids[j] = 0;
    RightKids[j] = 0;
  }
  j = 1;

  Root = (2 * EN) - 1;
  NextInternalNode = (2 * EN) - 1;

  CurrentDepth = 0;
  MDFS(Root);
}


#endif
