/****given a value for n and an output mode specified by ProcessInput,
this program prints out the subsets in BRGC order*****/

#include <stdio.h>
#include "../ProcessInput.c"
#include "TowerPic.c"

/*********globals*********/
int bitstring = 0;
int bitchange = 0;
int list = 0;
int towers = 0;
int A[MAX];
int pos[MAX]; /*used for Towers of Hanoi positions*/
int inc;

/************local functions**********/

/*prints out the generated number according to the output format*/
/*if bit = -1, then no bits were changed*/
void printit(bit)
int bit;
{
  int i,j;
  int beg = 1;

  if(!LIMIT)
    return;
  printf("<TR>");
  if(bitstring) {
  printf("<TD ALIGN=CENTER>");
    for(i=0;i<N;i++)
      if (N-bit==i) printf("<B><FONT COLOR=008800>%d</FONT></B>",A[i]);
      else printf("%d",A[i]);
    printf("<BR></TD>\n");
  }
  if(list) {
    printf("<TD ALIGN=CENTER>{");
    for(i=N-1;i>=0;i--) {
      if(A[i]) {
	printf("%s%d",beg ? "" : ",",N-i);
	beg = 0;
      }
    }
    printf("}<BR></TD>\n");
  }
  if(bitchange) {
    if(bit!=-1) 
      printf("<TD ALIGN=CENTER>%d<BR></TD>\n",bit);
    else
      printf("<TD ALIGN=CENTER><BR></TD>\n");
  }
  if(towers) {
    printf("<TD ALIGN=CENTER>");
    Arrange(&pos,N);
    if(bit==1) {
      pos[0] = (pos[0]+inc)%3;
    }
    else {
      if((pos[bit-1] = (pos[bit-1]+1)%3)==pos[0])
        pos[bit-1] = (pos[bit-1]+1)%3;
    }
  }
  printf("</TR>");
  LIMIT--; /*count down the number of printable objects*/
}

/*recursively generates the array A*/
void gray(n)
int n;
{
  if((n>0)&&(LIMIT>0)) {
    gray(n-1);
    printit(n);
    A[N-n] = (1-A[N-n])%2;
    gray(n-1);
  }
}

/***********main program********/

int main(int argc, char **argv)
{
  int i;
  int count;

  ProcessInput(argc, argv);
  count = LIMIT;
  if (N/2*2==N) {
    inc = 1;
  }
  else
    inc = 2;

  if(out_format & 1)
      bitstring = 1;
  if(out_format & 4)
      bitchange = 1;
  if(out_format & 2)
      list = 1;
  if(out_format & 8) {
      if (N<9) /*this is glen's requirement*/
      towers = 1;
    }

  /*initialize the arrays*/
  for(i=0;i<N;i++)
    A[i] = 0;
  for(i=0;i<N;i++)
    pos[i] = 0; 

  gray(N);
  printit(-1);
  printf("</TABLE><BR>Total number of subsets = %d<BR>\n",count-LIMIT);
  if(LIMIT)
    return(0);
  else
    return(-1);
}
