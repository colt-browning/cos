#!/usr/bin/perl

require "cgi-lib.pl";

select((select(STDOUT), $| = 1)[$[]);
select((select(STDERR), $| = 1)[$[]);

$RootedBIN = "/home/fruskey/.www/cgi-bin/rooted";
$BtreesBIN = "/home/fruskey/.www/cgi-bin/Btrees";


 MAIN: 
{
# Read in all the variables set by the form
  if (&ReadParse(*input)) {
    &ProcessForm;
  }
}

sub ProcessForm {
# Print the header
  print &PrintHeader;
  print "<html><head>\n";
  print "<title>Trees Output</title>\n";
  print "</head>\n<body>\n";
  print "<h1>Trees Output</h1>\n";

  if ($input{'program'} eq "rooted") {
  		print "<table cellspacing=5><th>";
  		if ($input{'output1'} eq "true") {
			print "<th>Parent Array ";
		}
  		if ($input{'output2'} eq "true") {
			print "<th>Level Sequences\n"
  		}
		if ($input{'output1'} eq "true" && $input{'output2'} eq "true") {
			if ($input{'k'}) {
				$retval = system "$RootedBIN -n $input{'n'} -k $input{'k'} -P -L";
			} else {
				$retval = system "$RootedBIN -n $input{'n'} -P -L";
			}
		} elsif ($input{'output1'} eq "true") {
			if ($input{'k'}) {
				$retval = system "$RootedBIN -n $input{'n'} -k $input{'k'} -P";
			} else {
				$retval = system "$RootedBIN -n $input{'n'} -P";
			}
		} else {
			if ($input{'k'}) {
				$retval = system "$RootedBIN -n $input{'n'} -k $input{'k'} -L";
			} else {
				$retval = system "$RootedBIN -n $input{'n'} -L";
			}
		}
	}
  else if ($input{'program'} eq "rooted") {
  		print "<table cellspacing=5><th>";
		$retval = system "$BtreesBIN -n $input{'n'} -m $input{'k'}";
       }




# Close the document cleanly.
  print "</table></body></html>\n";
}
