/*  This program finds the k-permutations of a multiset in
 *  Gray code order, where each permutation is different
 *  from the one before it in exactly one position (although
 *  two numbers may be different by more than one.  For example,
 *  122 -> 124 is valid)
 *
 *  Written by:  Chris Deugau, 2005 */

int P[100];   // max permutation size is 100
int dir[100]; // direction of each spot

int a[21];    // represent n(i)
int N;  // N = total of all n(i)'s
int k;  // k permutations
int t;  // number of n(i)'s
int count;
int flag;

void printP() {
	int i;

	count++;
	if( count > 200 )
		flag = 1;
	for (i=0; i<k; i++)
		printf("%d ",P[i]);
	printf("\n");
}

void makeP() {
  int i;
  int x=k-1;
  int newPX;
  int jumpBottom=0;
  int runLoop = 1;

  while (1) {
    printP();

    newPX = P[x] + dir[x];

    if ((newPX > 0) && (newPX < t) && (a[newPX] > 0)) {
      a[P[x]]++;
      P[x] = newPX;
      a[P[x]]--;

      continue;
    }

    runLoop = 1;
    while (runLoop == 1) {
      while ((newPX > 0) && (newPX < t)) {
	if (a[newPX] > 0) {
	  runLoop = 0;

	  a[P[x]]++;
	  P[x] = newPX;
	  a[P[x]]--;

	  x = k-1;
	  break;
	}

	newPX = newPX + dir[x];
      }

      if (runLoop == 1) {
	dir[x] = dir[x] * (-1);

	x = x - 1;

	if (x == -1)
	  return;

	newPX = P[x] + dir[x];
      }
    }
  }
}

// will create first instance of P, which will be the smallest
// lexigraphically k-permutation of the multiset
void initP() {
  int i;
  int j;
  int curr=1;

  // i is the index in P
  for (i=0; i < k; i++) {
    dir[i] = 1;  //  all directions initally set to +1

    // find the next a[i] which has an extra instance left
    while (a[curr] == 0) {
      curr++;
    }

    P[i] = curr;  // update P[i] to be curr
    a[curr]--;    // subtract one instance of a[curr]
  }
}

int main() {
  int i;

  count=0;   flag=0;

  scanf("%d",&N);
  scanf("%d",&k);
  scanf("%d",&t);

  ++t;  // increment t by one so loops only need to check for < t
  a[0] = 0;

  for (i=1; i < t; i++) {
    scanf("%d",&(a[i]));
  }

  initP();

  makeP();
}








