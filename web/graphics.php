<?php
  function perm_col($perm, $i) {
    // 20 standard colors
    $pal = array("red", "orange", "gold", "yellow", "greenyellow",
      "springgreen", "limegreen", "forestgreen", "olive", "darkturquoise",
      "aquamarine", "skyblue", "dodgerblue", "royalblue", "darkblue",
      "violet", "mediumpurple", "blueviolet", "purple", "sienna");
    if (($i < 0) || ($i > sizeof($perm)) || ($perm[$i] == 0)) {
      return "dimgray";  // special color for out-of-bounds requests
    }
    $m = max($perm);
    $col = 0;
    if ($m <= 7) {
      $col = 3*($perm[$i] - 1);
    } else if ($m <= 10) {
      $col = 2*($perm[$i] - 1);
    } else {
      $col = ($perm[$i] - 1) % sizeof($pal);
    }
    return $pal[$col];
  }

  // create svg of given permutation
  function svg_perm($perm, $dummy) {
    // each entry of the permutation is drawn as a box with these dimensions
    $box = 12;
    $sw = 1;  // stroke width
    $colbox = "white";

    $n = sizeof($perm);
    echo "<svg width='" . strval($box*$n + $sw) . "' height='" . strval($box + $sw)
       . "' viewBox='" . strval(-0.5*$sw) . " " . strval(-0.5*$sw) . " " . strval($box*$n + $sw) . " " . strval($box + $sw). "'>";
    echo "<style>.r{stroke:" . $colbox . ";stroke-width:" . strval($sw) . "}</style>";
    for ($i = 0; $i < $n; $i++) {
      echo "<rect class='r' x='" . strval($box*$i) . "' y='0' width='" . strval($box) . "' height='" . strval($box) . "' style='fill:" . perm_col($perm, $i) . "'/>";
    }
    echo "</svg>";
  }

  // create svg of given colored permutation, which is a list of pairs
  function svg_cperm($cperm, $dummy) {
    /*
    // each entry of the permutation is drawn as a box with these dimensions
    $box = 14;
    $sw = 1;  // stroke width
    $colbox = "white";
    $frac = 0.7;  // fraction of square taken by permutation entry; rest is taken by color value
    */

    $n = sizeof($cperm);
    // split pairs
    $perm = array_fill(0, $n, 0);
    $cols = array_fill(0, $n, 0);
    for ($i = 0; $i < $n; $i++) {
      $perm[$i] = $cperm[$i][0];
      $col[$i] = $cperm[$i][1];
    }

    /*
    echo "<svg width='" . strval($box*$n + $sw) . "' height='" . strval($box + $sw)
       . "' viewBox='" . strval(-0.5*$sw) . " " . strval(-0.5*$sw) . " " . strval($box*$n + $sw) . " " . strval($box + $sw). "'>";
    echo "<style>.r{stroke:" . $colbox . ";stroke-width:" . strval($sw) . "}</style>";
    for ($i = 0; $i < $n; $i++) {
      // top part is the permutation entry
      echo "<rect class='r' x='" . strval($box*$i) . "' y='0' width='" . strval($box) . "' height='" . strval($frac*$box) . "' style='fill:" . perm_col($perm, $i) . "'/>";
      // bottom part is the color
      echo "<rect class='r' x='" . strval($box*$i) . "' y='" . strval($frac*$box) . "' width='" . strval($box) . "' height='" . strval((1-$frac)*$box) . "' style='fill:" . perm_col($col, $i) . "'/>";
    }
    echo "</svg>";
    */

    for ($i = 0; $i < $n; $i++) {
      echo "<span style='color:" . perm_col($col, $i) . "'>" . $perm[$i] . "</span>";
    }
  }

  // print wheel of permutations/bitstrings
  function svg_wheel($objects, $fcol) {
    $strip = 20;  // width of each strip of the wheel
    $ir = 40;  // inner radius of the wheel
    $marks = 5;
    $colbox;
    if ($fcol == "bits_col") {
      $colbox = "lightgray";
    } else {
      $colbox = "white";
    }

    $d = sizeof($objects);
    if ($d < 2) {
      return;
    }

    $n = sizeof($objects[0]);
    $or = $ir + $n * $strip;  // outer radius of the wheel
    $box = 2*($or+$marks);  // bounding box

    echo "<svg width='" . strval($box) . "' height='" . strval($box)
       . "' viewBox='" . strval(-0.5*$box) . " " . strval(-0.5*$box) . " " . strval($box) . " " . strval($box). "'>";
    echo "<style>.p{stroke-width:0.6}.m{stroke-width:1;stroke:" . $colbox . "}.c{fill:none;stroke:" . $colbox . "}</style>";
    for ($k = 0; $k < $d; $k++) {
      $x0 = sin(2.0*pi()*$k / $d);
      $y0 = -cos(2.0*pi()*$k / $d);
      $x1 = sin(2.0*pi()*($k+1) / $d);
      $y1 = -cos(2.0*pi()*($k+1) / $d);
      for ($i = 0; $i < $n; $i++) {
        $r0 = $ir + 1.0*($or-$ir)/$n * $i;
        $r1 = $ir + 1.0*($or-$ir)/$n * ($i+1);
        $dr = $r1 - $r0;
        echo "<path class='p' d='m " . strval(round($x0*$r0,2)) . " " . strval(round($y0*$r0,2))
                             . " l " . strval(round($x0*$dr,2)) . " " . strval(round($y0*$dr,2))
                             . " a " . strval(round($r1)) . " " . strval(round($r1)) . " 0 0 1 " . strval(round(($x1-$x0)*$r1,2)) . " " . strval(round(($y1-$y0)*$r1,2))
                             . " l " . strval(round(-$x1*$dr,2)) . " " . strval(round(-$y1*$dr,2))
                             . " a " . strval(round($r0)) . " " . strval(round($r0)) . " 0 0 0 " . strval(round(($x0-$x1)*$r0,2)) . " " . strval(round(($y0-$y1)*$r0,2))
                             . " z' style='stroke:" . $fcol($objects[$k], $i) . ";fill:" . $fcol($objects[$k], $i) . "'/>";
      }
    }
    $s = round($n / ($or - $ir) * (min(max(round(2.0 * $d / pi()), $ir), $or) - $ir));
    for ($i = $s; $i <= $n; $i++) {
      $r = $ir + 1.0*($or-$ir)/$n  * $i;
      echo "<circle class='c' cx='0' cy='0' r='" . strval($r) . "'/>";
    }
    for ($k = 0; $k < $d; $k++) {
      $x0 = sin(2.0*pi()*$k / $d);
      $y0 = -cos(2.0*pi()*$k / $d);
      echo "<path class='m' d='m " . strval(round($x0*($ir + $s*$strip),2)) . " " . strval(round($y0*($ir + $s*$strip),2))
                           . " l " . strval(round($x0*($marks + ($n-$s)*$strip),2)) . " " . strval(round($y0*($marks + ($n-$s)*$strip),2)) . "'/>";
    }

    echo "</svg>";
  }

  // create svg of given meander
  function svg_meander($perm, $dummy) {
    // each arc of the meander is drawn inside a box of these dimensions
    $boxw = 12;
    $boxh = 24;
    $sw = 1.5;  // stroke width
    $rad = 3;  // radius of little dots at start of line
    $col1 = "yellow";
    $col2 = "lightgray";

    $n = sizeof($perm);
    $pos = array_fill(0, $n + 1, -1);  // inverse permutation
    for ($i = 0; $i < $n; $i++) {
      $pos[$perm[$i]] = $i;
    }

    // extract intervals spanned by upper and lower segments
    $upper_ints = array();
    $lower_ints = array();
    for ($i = 1; $i < $n; $i++) {
      $j = $pos[$i];
      $k = $pos[$i+1];
      $l = min($j, $k);
      $r = max($j, $k);
      $int = array($l, $r);
      if ($i % 2 == 1) {
        array_push($lower_ints, $int);
      } else {
        array_push($upper_ints, $int);
      }
    }
    // compute heights and depths when intervals are stacked
    $heights = interval_heights($upper_ints);
    $depths  = interval_heights($lower_ints);
    $max_height = max($heights);
    $max_depth = max($depths);

    // print svg
    $d = max($sw, 2*$rad);  // boundary
    echo "<svg width='" . strval($boxw*($n-1) + $d) . "' height='" . strval($boxh + $sw)
       . "' viewBox='" . strval(-0.5*$sw) . " " . strval(-0.5*$sw) . " " . strval($boxw*($n-1) + $sw) . " " . strval($boxh + $sw). "'>";
    echo "<style>.l{stroke:" . $col2 . ";stroke-width:" . strval($sw) . "}"
         . ".c{stroke-width:0;fill:" . $col1 . "}"
         . ".p{fill:none;stroke:" . $col1 . ";stroke-width:" . strval($sw) . "}</style>";
    echo "<line class='l' x1='0' y1='" . strval(0.5*$boxh) . "' x2='" . strval($boxw*($n-1)) . "' y2='" . strval(0.5*$boxh) . "'/>";
    echo "<circle class='c' cx='" . strval($boxw*$pos[1]) . "' cy='"  . strval(0.5*$boxh) . "' r='" . strval($rad) . "'/>";
    for ($i = 1; $i < $n; $i++) {
      $j = $pos[$i];
      $k = $pos[$i+1];
      $left = min($j, $k);
      $right = max($j, $k);

      if ($i % 2 == 1) {
        // lower segment
        if ($max_depth == 1) {
          $y = $boxh;
        } else {
          $y = (0.25 * $boxh / ($max_depth - 1)) * ($depths[($i - 1)/2] - 1) + 0.75 * $boxh;
        }
      } else {
        // upper segment
        if ($max_height == 1) {
          $y = 0;
        } else {
          $y = -(0.25 * $boxh / ($max_height - 1)) * ($heights[($i - 2)/2] - 1) + 0.25 * $boxh;
        }
      }
      echo "<polyline class='p' points='" . strval($boxw*$j) . "," . strval(0.5*$boxh) . " "
                                . strval($boxw*$j) . "," . strval($y) . " "
                                . strval($boxw*$k) . "," . strval($y) . " "
                                . strval($boxw*$k) . "," . strval(0.5*$boxh) . "'/>";
    }
    echo "</svg>";
  }

  // for a given set of intervals of non-negative integers, compute
  // the heights of each interval when they are stacked (needed for meanders)
  function interval_heights($ints) {
    $n = sizeof($ints);  // number of intervals
    $max = 0;  // the maximum integer
    // record length of each interval
    $len = array();
    for ($i = 0; $i < $n; $i++) {
      $l = $ints[$i][0];
      $r = $ints[$i][1];
      $max = max($max, $r);
      $len[$i] = $r - $l;
    }
    // sort them in increasing length
    asort($len);
    // keep track of the height of the stacks of intervals
    $stack = array_fill(0, $max+1, 0);
    $heights = array_fill(0, $n, 0);
    foreach ($len as $i => $d) {  // go through intervals by increasing length
      $l = $ints[$i][0];
      $r = $ints[$i][1];
      $h = max(array_slice($stack, $l, $d+1)) + 1;
      $heights[$i] = $h;
      for ($x = $l; $x <= $r; $x++) {
        $stack[$x] = $h;
      }
    }
    return $heights;
  }

  function bits_col($bits, $i) {
    $col0 = "white";
    $col1 = "dimgray";
    $col2 = "red";  // the 2s encode *s in the symmetric chain Gray code
    $bit = $bits[$i];
    if ($bit == 0) {
      return $col0;
    } else if ($bit == 1) {
      return $col1;
    } else {
      return $col2;
    }
  }

  // create svg of given bitstring
  function svg_bits($bits, $dummy) {
    // each bit is drawn as a box with these dimensions
    $box = 12;
    $sw = 1;  // stroke width
    $colbox = "lightgray";

    $n = sizeof($bits);
    echo "<svg width='" . strval($box*$n + $sw) . "' height='" . strval($box + $sw)
       . "' viewBox='" . strval(-0.5*$sw) . " " . strval(-0.5*$sw) . " " . strval($box*$n + $sw) . " " . strval($box + $sw). "'>";
    echo "<style>.r{stroke:" . $colbox . ";stroke-width:" . strval($sw) . "}</style>";
    for ($i = 0; $i < $n; $i++) {
      echo "<rect class='r' x='" . strval($box*$i) . "' y='0' width='" . strval($box) . "' height='" . strval($box) . "' style='fill:" . bits_col($bits, $i) . "'/>";
    }
    echo "</svg>";
  }

  function sgroup_col($sgroup, $i) {
    $colnongap = "red";
    $colgap = "dimgray";
    $x = $sgroup[$i];
    if ($x == 1) {
      return $colnongap;
    } else {
      return $colgap;
    }
  }

  // create svg of given gap array of numerical semigroup
  function svg_sgroup($sgroup, $dummy) {
    // each element/gap is drawn as a box with these dimensions
    $box = 12;
    $sw = 1;  // stroke width
    $colbox = "white";

    $n = sizeof($sgroup);
    echo "<svg width='" . strval($box*$n + $sw) . "' height='" . strval($box + $sw)
       . "' viewBox='" . strval(-0.5*$sw) . " " . strval(-0.5*$sw) . " " . strval($box*$n + $sw) . " " . strval($box + $sw). "'>";
    echo "<style>.r{stroke:" . $colbox . ";stroke-width:" . strval($sw) . "}</style>";
    for ($i = 0; $i < $n; $i++) {
      echo "<rect class='r' x='" . strval($box*$i) . "' y='0' width='" . strval($box) . "' height='" . strval($box) . "' style='fill:" . sgroup_col($sgroup, $i) . "'/>";
    }
    echo "</svg>";
  }

  // create svg of given bitstring as a Dyck path
  function svg_dyck($bits, $dummy) {
    // each bit is drawn as a box with these dimensions
    $box = 12;
    $sw1 = 1.5;  // stroke width
    $sw3 = 2.5;  // stroke width
    $rad = 2.5;
    $col1 = "yellow";
    $col2 = "lightgray";
    $col3 = "red";

    $n = sizeof($bits);
    // compute maximum height and depth of Dyck path
    $max_height = 0;
    $max_depth = 0;
    $h = 0;
    for ($i = 0; $i < $n; $i++) {
      $h += 2*$bits[$i] - 1;
      $max_height = max($max_height, $h);
      $max_depth = min($max_depth, $h);
    }
    $total_height = $max_height - $max_depth;

    $d = max($sw1, 2*$rad);  // boundary
    echo "<svg width='" . strval($box*$n + $d) . "' height='" . strval($box*$total_height + $d)
       . "' viewBox='" . strval(-0.5*$d) . " " . strval(-0.5*$d) . " " . strval($box*$n + $d) . " " . strval($box*$total_height + $d). "'>";
    echo "<style>.l{stroke:" . $col1 . ";stroke-width:" . strval($sw1) . "}.c{stroke-width:0;fill:" . $col1 . "}.f{stroke:" . $col3 . ";stroke-width:" . strval($sw3) . "}</style>";

    $y = $box * $max_height;
    echo "<line x1='0' y1='" . strval($y) . "' x2='" . strval($box*$n) . "' y2='" . strval($y)
       . "' style='stroke:" . $col2 . ";stroke-width:" . strval($sw1) . "'/>";
    $h = 0;
    for ($i = 0; $i < $n; $i++) {
      $d = 2*$bits[$i] - 1;
      $y1 = $box * ($max_height - $h);
      $y2 = $y1 - $box * $d;
      echo "<line class='" . (($h <= 0) && ($bits[$i] == 0) ? "f" : "l") . "' x1='" . strval($box*$i)     . "' y1='" . strval($y1) . "' x2='" . strval($box*($i+1)) . "' y2='" . strval($y2) . "'/>";
      echo "<circle class='c' cx='" . strval($box*$i) . "' cy='"  . strval($y1) . "' r='" . strval($rad) . "'/>";
      if ($i == $n-1) {
        echo "<circle class='c' cx='" . strval($box*($i+1)) . "' cy='"  . strval($y2) . "' r='" . strval($rad) . "'/>";
      }
      $h += $d;
    }
   echo "</svg>";
  }

  // create svg of given tree in level representation
  function svg_tree($tree, $rooted) {
    // each edge of the tree is drawn inside a box of these dimensions
    $boxw = 14;
    $edgeh = 14;
    $sw = 1.5;  // stroke width
    $rad = 2.5;  // radius of nodes
    $col1 = "yellow";  // ordinary node color
    $col2 = "red";  // root node color

    $n = sizeof($tree);  // number of nodes

    // compute levels, children, parent of nodes, and level sizes
    $level = array_fill(0, $n, 0);  // level of a node
    $pos = array_fill(0, $n, 0);  // position of a node on that level
    $children = array_fill(0, $n, array());  // children of the node
    $parent = array_fill(0, $n, -1);  // parent of the node
    $levels = array_fill(0, $n, array());  // nodes in every level
    $sizes = array_fill(0, $n, 0);  // level sizes
    for ($i = 0; $i < $n; $i++) {
      $li = $tree[$i];  // depth of node
      $level[$i] = $li;
      $pos[$i] = sizeof($levels[$li]);
      array_push($levels[$li], $i);
      $sizes[$li]++;
      if ($li > 0) {
        $p = end($levels[$li-1]);  // parent
        array_push($children[$p], $i);
        $parent[$i] = $p;
      }
    }

    // compute sparse levels: a level is sparse if all nodes on the level are the only child of their parent
    $sparse = array_fill(0, $n, true);
    for ($i = 0; $i < $n; $i++) {
      $li = $level[$i];
      if (sizeof($children[$i]) > 1) {
        $sparse[$li + 1] = false;
      }
    }

    // compute all x and y coordinates of nodes in this tree
    $boxh = $edgeh * (max($sizes) - 1);

    $x = array(0, $n, 0);
    $y = array(0, $n, 0);
    for ($i = 0; $i < $n; $i++) {
      $li = $level[$i];
      $pi = $pos[$i];
      $x[$i] = $boxw*$li;
      if ($li == 0) {  // root node placed in the middle
        $y[$i] = 0.5*$boxh;
      } else if ($sparse[$li]) {  // sparse level: place unique children directly below (=to the right) of their parent
        $y[$i] = $y[$parent[$i]];
      } else {  // distribute equidistantly
        $y[$i]  = -1.0*$boxh / ($sizes[$li] - 1) * $pi  + $boxh;
      }
    }

    $d = max($sw, 2*$rad);  // boundary
    echo "<svg width='" . strval($boxw*($n-1) + $d) . "' height='" . strval($boxh + $d)
       . "' viewBox='" . strval(-0.5*$d) . " " . strval(-0.5*$d) . " " . strval($boxw*($n-1) + $d) . " " . strval($boxh + $d). "'>";
    echo "<style>.l{stroke:" . $col1 . ";stroke-width:" . strval($sw) . "}.c{stroke-width:0;fill:" . $col1 . "}</style>";

    for ($i = 1; $i < $n; $i++) {  // draw nodes 1,...,n-1
      $j = $parent[$i];
      // draw edge from node to its parent
      echo "<line class='l' x1='" . strval($x[$i])  . "' y1='" . strval($y[$i]) . "' x2='" . strval($x[$j]) . "' y2='" . strval($y[$j]) . "'/>";
      // draw node itself
      echo "<circle class='c' cx='" . strval($x[$i]) . "' cy='"  . strval($y[$i]) . "' r='" . strval($rad) . "'/>";
    }
    // draw root node 0 last
    echo "<circle class='c' cx='" . strval($x[0]) ."' cy='"  . strval($y[0]) . "' r='" . strval($rad) . "' style='fill:" . ($rooted ? $col2 : $col1) . "'/>";

    echo "</svg>";
  }

  // create svg of given elimination forest represented as a list of pairs
  // (node, number of children) with nodes visited in DFS order
  function svg_elim($elim, $dummy) {
    // each edge of the tree is drawn inside a box of these dimensions
    $boxw = 28;
    $edgeh = 28;
    $ew = 1.5;  // stroke width for the edges
    $sw = 1;  // stroke width for the nodes
    $rad = 8;  // radius of nodes
    $gap = 19;  // gap between consecutive trees
    $space = 3;  // extra space between consecutive forests
    $col = "white";  // color of edges and nodes

    $n = sizeof($elim);  // number of nodes

    // compute the roots of all elimination trees and the children and parent of each node
    $roots = array();
    $children = array_fill(0, $n+1, array());
    $parent = array_fill(0, $n+1, 0);
    $stack = array();
    for ($i = 0; $i < $n; $i++) {
      $vc = $elim[$i];
      $v = $vc[0];  // node id
      $c = $vc[1];  // number of children
      if (sizeof($stack) == 0) {
        array_push($roots, $v);  // this is the root of an elimination tree
      } else {
        $p = array_pop($stack);  // parent
        array_push($children[$p], $v);  // make $v a child of $p
        $parent[$v] = $p;
      }
      for ($j = 0; $j < $c; $j++) {
        array_push($stack, $v);
      }
    }

    // compute the level of each node, its position on the level, and the level sizes for each tree
    $level = array_fill(0, $n+1, 0);
    $pos = array_fill(0, $n+1, 0);
    $sizes = array_fill(0, sizeof($roots), array_fill(0, $n, 0));
    $sparse = array_fill(0, sizeof($roots), array_fill(0, $n, true));  // a level is sparse if all nodes on the level are the only child of their parent
    for ($t = 0; $t < sizeof($roots); $t++) {
      $r = $roots[$t];
      $level[$r] = 0;
      $pos[$r] = $sizes[$t][0];
      $sizes[$t][0]++;
      $stack = array();
      array_push($stack, $r);
      while (sizeof($stack) != 0) {
        $v = array_pop($stack);
        if (sizeof($children[$v]) > 1) {
          $sparse[$t][$level[$v] + 1] = false;
        }
        for ($i = 0; $i < sizeof($children[$v]); $i++) {
          $w = $children[$v][$i];
          $l = $level[$v] + 1;
          $level[$w] = $l;
          $pos[$w] = $sizes[$t][$l];
          $sizes[$t][$l]++;
        }
        // children are added to the stack in reverse order, so that they are popped in the correct order
        $children_rev = array_reverse($children[$v]);
        $stack = array_merge($stack, $children_rev);
      }
    }

    $idperm = array();  // identity permutation (needed for color computation)
    for ($i = 0; $i < $n; $i++) {
      array_push($idperm, $i+1);
    }

    // compute full height needed for stacking all elimination trees
    $fullh = 0;
    for ($t = 0; $t < sizeof($roots); $t++) {
      $fullh += $edgeh * (max($sizes[$t]) - 1);
    }
    $fullh += (sizeof($roots) - 1) * $gap + $space;  // extra gap between any two trees, and extra space between any two forests

    $d = 2*$rad+$sw;  // boundary
    echo "<svg width='" . strval($boxw*($n-1) + $d) . "' height='" . strval($fullh + $d)
       . "' viewBox='" . strval(-0.5*$d) . " " . strval(-0.5*$d) . " " . strval($boxw*($n-1) + $d) . " " . strval($fullh + $d). "'>";
    echo "<style>.l{stroke:" . $col . ";stroke-width:" . strval($ew) . "}.c{stroke:" . $col .";stroke-width:" . strval($sw) . "}</style>";

    // x and y coordinates of all nodes
    $x = array(0, $n+1, 0);
    $y = array(0, $n+1, 0);
    $yoff = 0;  // vertical offset for printing the next tree

    for ($t = 0; $t < sizeof($roots); $t++) {  // process elimination trees one by one
      $boxh = $edgeh * (max($sizes[$t]) - 1);

      // compute x and y coordinates of nodes in this tree
      $r = $roots[$t];
      $stack = array();
      array_push($stack, $r);
      while (sizeof($stack) != 0) {
        $v = array_pop($stack);
        $lv = $level[$v];
        $pv = $pos[$v];
        $x[$v] = $boxw*$lv;
        if ($lv == 0) {  // root node placed in the middle
          $y[$v] = $yoff + 0.5*$boxh;
        } else if ($sparse[$t][$lv]) {  // sparse level: place unique children directly below (=to the right) of their parent
          $y[$v] = $y[$parent[$v]];
        } else {  // distribute equidistantly
          $y[$v]  = $yoff + -1.0*$boxh / ($sizes[$t][$lv] - 1) * $pv  + $boxh;
        }
        for ($i = 0; $i < sizeof($children[$v]); $i++) {
          $w = $children[$v][$i];
          array_push($stack, $w);
        }
      }

      // draw tree edges
      $r = $roots[$t];
      $stack = array();
      array_push($stack, $r);
      while (sizeof($stack) != 0) {
        $v = array_pop($stack);
        for ($i = 0; $i < sizeof($children[$v]); $i++) {
          $w = $children[$v][$i];
          array_push($stack, $w);
          // draw edge from v to w
          echo "<line class='l' x1='" . strval($x[$v])  . "' y1='" . strval($y[$v]) . "' x2='" . strval($x[$w]) . "' y2='" . strval($y[$w]) . "'/>";
        }
      }

      // draw tree nodes on top of the tree edges
      $r = $roots[$t];
      $stack = array();
      array_push($stack, $r);
      while (sizeof($stack) != 0) {
        $v = array_pop($stack);
        echo "<circle class='c' cx='" . strval($x[$v]) . "' cy='"  . strval($y[$v]) . "' r='" . strval($rad) . "' style='fill:" . perm_col($idperm, $v-1) . "'/>";
        for ($i = 0; $i < sizeof($children[$v]); $i++) {
          $w = $children[$v][$i];
          array_push($stack, $w);
        }
      }

      // one tree finished, increase offset for next one
      $yoff += $boxh + $gap;
    }

    echo "</svg>";
  }

  // convert 231-avoiding permutation to binary tree in parenthesis representation
  function perm_to_bracketstr($perm, $a, $b) {
    if ($a > $b) return " ";
    $r = $perm[$a];
    $i = $a;
    do {
      $i++;
    } while (($perm[$i] < $r) && ($i <= $b));
    $res = "[" . perm_to_bracketstr($perm, $a+1, $i-1) . "," . perm_to_bracketstr($perm, $i, $b) . "]";
    return $res;
  }

  // create svg of given 231-avoiding permutation as a parenthesis string
  function svg_bracketstr($perm, $dummy) {
    $n = sizeof($perm);
    $str = perm_to_bracketstr($perm, 0, $n-1);
    echo "<style>.r{color:red;} .b{color:blue;}</style>";
    for ($i = 0; $i < strlen($str); $i++) {
      if ($str[$i] == "[") {  // opening brackets
        echo "<span class='r'>" . $str[$i] . "</span>";
      } else if ($str[$i] == "]") {  // closing brackets
        echo "<span class='b'>" . $str[$i] . "</span>";
      } else {  // commas
        echo $str[$i];
      }
    }
  }

  // create svg with Ferrers diagram of given integer partition
  function svg_ferrers($part, $dummy) {
    // each entry of the permutation is drawn as a box with these dimensions
    $box = 12;
    $sw = 1;  // stroke width
    $col1 = "yellow";
    $col2 = "gray";

    $m = max($part);  // maximum part
    $p = sizeof($part);  // number of parts
    echo "<svg width='" . strval($box*$m + $sw) . "' height='" . strval($box*$p + $sw)
        . "' viewBox='" . strval(-0.5*$sw) . " " . strval(-0.5*$sw) . " " . strval($box*$m + $sw) . " " . strval($box*$p + $sw). "'>";
    echo "<style>.r{stroke:" . $col2 . ";fill:" . $col1 . ";stroke-width:" . strval($sw) . "}</style>";
    for ($i = 0; $i < $p; $i++) {
      $x = $part[$i];
      for ($j = 0; $j < $x; $j++) {
        echo "<rect class='r' x='" . strval($box*$j) . "' y='". strval($box*$i) . "' width='" . strval($box) . "' height='" . strval($box) . "'/>";
      }
    }
    echo "</svg>";
  }

  // create svg of given rectangulation
  // The rectangulation is given as a list of pairs of pairs of (x,y)-coordinates,
  // desribing the lower-left corner and the upper-right corner of each rectangle.
  // The second parameter is (false,0,diag) for the non-animated rectangles, and (true,i,diag) for
  // the animated rectangles, where i is the id of the given rectangle, and diag=true means that
  // a diagonal should drawn.
  function svg_rect($rect, $par) {
    $n = sizeof($rect);
    $animate = $par[0];
    $rectid = $par[1];
    $diag = $par[2];
    // each rectangulation is drawn as a box with these dimensions
    $box = $animate ? 24*$n : 12*pow($n,0.8);  // for listed rectangulations the total box size scales like 12*$n^0.8
                                               // for animated rectangulations the box size is bigger
    $sw = 1;  // stroke width
    $colbox = "white";

    // we need to be able to reference each of the animated rectangles by its id
    $idclass = "";
    if ($animate) {
      $idclass = " id='svg" . strval($rectid) . "' class='stack'";
    }
    echo "<svg" . $idclass . " width='" . strval(round($box + $sw,2)) . "' height='" . strval(round($box + $sw,2))
       . "' viewBox='" . strval(-0.5*$sw) . " " . strval(-0.5*$sw) . " " . strval(round($box + $sw,2)) . " " . strval(round($box + $sw,2)). "'>";
    echo "<style>.r{stroke:" . $colbox . ";stroke-width:" . strval($sw) . "}</style>";
    // compute subdivision in both directions
    $dx;
    $dy;
    $idperm = array();  // identity permutation (needed for color computation)
    for ($i = 0; $i < $n; $i++) {
      $dx = max($dx, $rect[$i][1][0]);  // maximum x-coordinate
      $dy = max($dy, $rect[$i][1][1]);  // maximum y-coordinate
      array_push($idperm, $i+1);
    }
    for ($i = 0; $i < $n; $i++) {
      $x1 = $rect[$i][0][0] / $dx * $box;  // these are ordinary x-y-cordinates, the same as used in our rectangulation paper
      $y1 = $rect[$i][0][1] / $dy * $box;
      $x2 = $rect[$i][1][0] / $dx * $box;
      $y2 = $rect[$i][1][1] / $dy * $box;
      // in SVG coordinates, the y-axis is turned upside down
      echo "<rect class='r' x='" . strval(round($x1,2)) . "' y='" . strval(round($box-$y2,2)) . "' width='" . strval(round($x2-$x1,2)) . "' height='" . strval(round($y2-$y1,2)) . "' style='fill:" . perm_col($idperm, $i) . "'/>";
    }
    if ($diag) {
      echo "<line class='r' x1='0' y1='0' x2='" . strval($box) . "' y2='" . strval($box) . "'/>";
    }
    echo "</svg>";
  }

  // create a stack of SVGs to animate rectangulations
  function animate_rects($rects, $diag) {
    $n = sizeof($rects);
    echo "<div id='animation'>";
    // create a cover for hiding the animation stack while it is loading
    echo "<svg class='cover' width='361' height='361'><rect x='0' y='0' width='361' height='361' style='fill:black'/></svg>";
    for ($i = 0; $i < $n; $i++) {
      $r = $rects[$i];
      svg_rect($r, array(true, $i, $diag));
    }
    echo "</div>";
  }

  // Create svg of given k-ary tree as a dissection of a convex polygon.
  // The first argument $inp is either only a tree for uncolored dissections
  // or a pair of tree and vertex 2-coloring for colored dissections.
  // This can be distinguished from the second argument, which is (false,_,_,_) in the
  // first case and (true,_,_,_) in the second case.
  // The tree is a list of vertex ids obtained from a preorder traversal
  // after labeling the vertices in a search tree fashion
  // (subtree 1 < root < subtree 2 < subtree 3 < ... < subtree k).
  // The vertex 2-coloring is a list of bits that describe the colors of the vertices.
  // The second argument $par is (_,num_points,false,0) for the non-animated trees, and (_,num_points,true,i) for
  // the animated trees, where i is the id of the given tree.
  // The argument num_points is the number of points in the case of colored dissections, and it is
  // needed for special treatment when the number of points is odd.
  function svg_kary($inp, $par) {
    $colored = $par[0];  // colored or uncolored dissection?
    $num_points = $par[1];
    $animate = $par[2];
    $treeid = $par[3];
    if (!$colored) {
      $tree = $inp;
    } else {
      $tree = $inp[0];
      $col  = $inp[1];
    }
    $n = sizeof($tree);  // the number of inner vertices plus the number of leaves (=empty subtrees)

    // count inner vertices and leaves separately
    $nv = 0;
    $nl = 0;
    for ($i = 0; $i < $n; $i++) {
      if ($tree[$i] == 0) {
        $nl++;
      } else {
        $nv++;
      }
    }
    // compute the arity of the tree
    $k = ($nl - 1) / $nv + 1;
    // the number of dissection vertices is one larger than the number of leaves
    $nd = $nl + 1;
    // for colored dissections on an odd number of points, the fake quadrangle needs special treatment,
    // namely contraction of the last two points to one point
    if ($colored && ($num_points % 2 == 1)) {
      $nd--;
    }

    $rg = $animate ? 4*$nd : round(3*pow($nd,0.8),2);  // radius of the polygon
    $rp = 3;  // radius of the points
    $sw = 1;  // stroke width
    $coll = "white";  // color of the lines
    $col0 = "red";  // fill color of the points
    $col1 = "blue";

    $box = 2*$rg + 2*$rp;

    // we need to be able to reference each of the animated trees/dissections by its id
    $idclass = "";
    if ($animate) {
      $idclass = " id='svg" . strval($treeid) . "' class='stack'";
    }
    echo "<svg" . $idclass . " width='" . strval($box + $sw) . "' height='" . strval($box + $sw)
       . "' viewBox='" . strval(-0.5*$sw-$rp) . " " . strval(-0.5*$sw-$rp) . " " . strval($box + $sw) . " " . strval($box + $sw). "'>";
    echo "<style>.p0{stroke:" . $coll . ";fill:" . $col0 .  ";stroke-width:" . strval($sw) . "}.p1{stroke:" . $coll . ";fill:" . $col1 .  ";stroke-width:" . strval($sw) . "}.l{stroke:" . $coll . ";stroke-width:" . strval($sw) . "}.l0{stroke:" . $col0 . ";stroke-width:" . strval($sw) . "}.l1{stroke:" . $col1 . ";stroke-width:" . strval($sw) . "}</style>";

    // translate k-ary tree into dissection of $nd-gon into (k+1)-gons
    $root = $tree[0];
    $pt = 0;  // point counter
    $lines = array();  // pairs of points connected by a line in the dissection
    $clines = array();  // colors of lines; -1 if uncolored
    $stack = array();
    $start = array();  // starting vertices of lines in the dissection
    $leaves = array_fill(0, $n+1, 0);  // number of leaves in each subtree
    $end = array_fill(0, $n+1, 0);  // end vertex of the dual line through the edge to the parent
    for ($i = 0; $i < $n; $i++) {
      $v = $tree[$i];
      array_push($stack, $v);
      if ($v == 0) {  // encountered next boundary edge of the polygon
        array_push($start, $pt);
        array_push($lines, array($pt, $pt+1));
        array_push($clines, -1);
        $pt++;
      }
      do {
        $s = sizeof($stack);
        $leaf = true;
        $c = 0;  // count leaves
        // check if the last k entries on the stack are leaves or have been processed already
        for ($j = 0; $j < $k; $j++) {
          $w = $stack[$s-1-$j];
          if ($w == $root) {
            $leaf = false;
            break;
          } else if ($w == 0) {
            $c++;
          } else if ($leaves[$w] > 0) {
            $c += $leaves[$w];
          } else {
            $leaf = false;
            break;
          }
        }
        if ($leaf) {  // remove last k entries of the stack
          $children = array(0, $k, 0);
          for ($j = 0; $j < $k; $j++) {
            $children[$k-1-$j] = array_pop($stack);
            if ($j < $k-1) {  // the starting point of the subtree is also the starting point of the parent, so this value is not removed
              array_pop($start);
            }
          }
          $leaves[$stack[sizeof($stack)-1]] = $c;  // record number of leaves in this subtree
          $a = $start[sizeof($start)-1];  // retrieve starting point
          array_push($lines, array($a, $a+$c));  // end point is determined by sum of leaves in the subtrees
          array_push($clines, -1);
          $v = end($stack);
          $end[$v] = $a + $c;
          if ($colored) {
            // this quadrangle has the four points p1,p2,p3,p4
            $p1 = $a;
            $p4 = $a + $c;
            $p2 = ($children[0] == 0) ? $a + 1 : $end[$children[0]];
            $p3 = ($children[1] == 0) ? $p2 + 1 : $end[$children[1]];
            // the bit determines which of the two diagonals is present
            if ($col[$v-1] == 0) {
              if (($p1 % 2) == 0) {
                $line = array($p1,$p3);
              } else {
                $line = array($p2,$p4);
              }
            } else {
              if (($p1 % 2) == 0) {
                $line = array($p2,$p4);
              } else {
                $line = array($p1,$p3);
              }
            }
            array_push($lines, $line);
            array_push($clines, $line[0] % 2);  // the color of the diagonal is determined by its end vertices
          }
        }
      } while ($leaf);
    }

    // compute coordinates of the points in a circular layout
    $px = array();
    $py = array();

    for ($i = 0; $i < $nd; $i++) {
      // the numbering starts at the top and then goes counterclockwise,
      // so the edge (0,$nd-1) is at the top
      $x = round($rg * (1 - cos(2*pi()*$i/$nd-pi()/2 + pi()/$nd)), 2);
      $y = round($rg * (1 + sin(2*pi()*$i/$nd-pi()/2 + pi()/$nd)), 2);
      array_push($px, $x);
      array_push($py, $y);
    }

    // draw lines
    for ($i = 0; $i < sizeof($lines); $i++) {
      $a = $lines[$i][0];
      $b = $lines[$i][1];
      if ($colored && ($num_points % 2 == 1)) {
        if (($a == $nd-1) || ($b == $nd-1)) {
          continue;  // skip lines connecting to the fake point
        } else if ($a == $nd) {  // redirect lines going to the last point
          $a--;
        } else if ($b == $nd) {
          $b--;
        }
      }
      $c = $clines[$i];
      $cl = ($c == -1) ? "" : strval($c);  // choose color
      echo "<line class='l" . $cl . "' x1='" . strval($px[$a]) . "' y1='" . strval($py[$a]) . "' x2='" . strval($px[$b]) . "' y2='" . strval($py[$b]). "'/>";
    }

    // draw points
    for ($i = 0; $i < $nd; $i++) {
      $pc = ($colored && (($i % 2) == 0) && ($i < $nd-1)) ? "0" : "1";
      echo "<circle class='p" . $pc . "' cx='" . strval($px[$i]) . "' cy='" . strval($py[$i]) . "' r='" . strval($rp) . "'/>";
    }

    echo "</svg>";
  }

  // create a stack of SVGs to animate k-ary trees via the corresponding dissections
  function animate_kary($trees, $par) {
    $colored = $par[0];
    $num_points = $par[1];
    $n = sizeof($trees);
    echo "<div id='animation'>";
    // create a cover for hiding the animation stack while it is loading
    echo "<svg class='cover' width='343' height='343'><rect x='0' y='0' width='343' height='343' style='fill:black'/></svg>";
    for ($i = 0; $i < $n; $i++) {
      $t = $trees[$i];
      svg_kary($t, array($colored, $num_points, true, $i));
    }
    echo "</div>";
  }

?>
