/*
 * Copyright (c) 2024 Rohan Acharya, Torsten Muetze, and Francesco Verciani
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cassert>
#include <cctype>
#include <getopt.h>
#include <iostream>
#include <string>
#include <utility>
#include <vector>
#include "coltria.hpp"

// display help
void help() {
  std::cout << "./ctria [options]  generate all colorful triangulations of a convex n-gon by flips" << std::endl;
  std::cout << "-h                 display this help" << std::endl;
  std::cout << "-n{8,9,...}        number of vertices" << std::endl;
  std::cout << "-l{-1,0,1,2,...}   number of triangulations to generate; -1 for all" << std::endl;
  std::cout << "-q                 quiet output" << std::endl;
  std::cout << "-c                 output number of triangulations" << std::endl;
  std::cout << "examples:  ./ctria -n8" << std::endl;
  std::cout << "           ./ctria -n9 -q -c" << std::endl;
}

int main(int argc, char* argv[]) {
  int n;
  bool n_set = false;
  long long steps = -1;  // compute all elimination forests by default
  bool quiet = false;  // print output by default
  int c;
  bool output_counts = false; // omit counts by default
  int quiet_dot = 10000000;  // print one dot every 10^7 elimination forests in quiet output mode

  while ((c = getopt (argc, argv, "hn:l:qc")) != -1) {
    switch (c) {
      case 'h':
        help();
        return 0;
      case 'n':
        {
        n = atoi(optarg);
        if (n < 8) {
          std::cerr << "option -n must be followed by an integer from {8,9,...}" << std::endl;
          return 1;
        }
        n_set = true;
        break;
        }
      case 'l':
        steps = atoi(optarg);
        if (steps < -1) {
          std::cerr << "option -l must be followed by an integer from {-1,0,1,2,...}" << std::endl;
          return 1;
        }
        break;
      case 'q':
        quiet = true;
        break;
      case 'c':
        output_counts = true;
        break;
    }
  }
  if (!n_set) {
    std::cerr << "option -n is mandatory" << std::endl;
    help();
    return 1;
  }

  Coltria triang(n);

  if (steps == 0) {
    std::cout << "output limit reached" << std::endl;
    return 0;
  }

  int num_triang = 0;
  bool next;
  do {
    num_triang++;
    if (!quiet) {
      triang.print();
    } else if (num_triang % quiet_dot == 0) {
      std::cout << "." << std::flush;
    }
    int v, dir;
    next = triang.next();
    if (next && (steps >= 0) && (num_triang >= steps)) {
      std::cout << "output limit reached" << std::endl;
      break;
    }
  } while (next);
  if (output_counts)
  {
    if (quiet && num_triang >= quiet_dot) {
      std::cout << std::endl;
    }
    std::cout << "number of colorful triangulations: " << num_triang << std::endl;
  }

  return 0;
}