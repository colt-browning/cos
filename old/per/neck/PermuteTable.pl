# Read in lines and store in table 
while (<>) {
	push @table , [split]
}

# Re-order and display the output
for($row=1; $row<=$n; ++$row) {

       print "<TR><TH ALIGN=RIGHT>";
       if ($row == 1) { print"<I>x</I> = $row"; }
       else { print "$row"; }
       for($i=1; $i <= $n; ++$i) {
          print "<TD ALIGN=RIGHT>$table[$reorder[$row]-1][$reorder[$i]-1] ";
       }	
       print "<BR></TD></TR>\n";
}
