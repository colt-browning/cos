/*===============================================*/
/* Program: Generating binary trees              */
/* Input: n,k                                    */
/* Output: All tree sequences with n 1's, and    */
/*         with prefix 1^k0                      */
/* Programmer: Frank Ruskey                      */
/* Output binary: binarytree (Feb.28,96)         */
/*===============================================*/

#ifndef BINARYTREE_C 
#define BINARYTREE_C 


#include <stdio.h>	
#include "ProcessInput.c"
#include "OutputTrees.c"

#define MaxN  50

int
  n,k,i,
  x[MaxN];

void T ( n, k, pos)
int n,k,pos;
{
   if (k == n) PrintIt(2*N,&x[0],&x[0]); else
   if (k == 1) {
      x[pos] = 1;  T(n, k+1, pos+1);  x[pos] = 0;
   } else {
      x[pos] = 1;  T(n, k+1, pos+1);
      x[pos] = 0;  T(n-1, k-1, pos+1);
   }
}

main(argc,argv)
int argc;
char *argv[];
{
  n=0;
  k=0;
  ProcessInput(argc,argv);
  if (out_format & 1) outputP = 1;
  if (out_format & 2) outputB = 1;
  if (out_format & 8) outGif = 1;
  n=N; k=M;

  if (k==0) {T(n+1,1,1);}
  else { 
    for(i=1;i<=k;i++) x[i] = 1;
    x[k+1] = 0; 
    T(n,k,k+2);
  }
  printf("</table><P>The total number of trees is: %4d<BR>\n",num);
  exit(0);
}

#endif
