#!/usr/bin/perl 
# processes makeg output.
# written by Frank Ruskey Sept 18, 1995.
# thanks to Will Kastelic showing me about substr.

require "../common.pl";

$n = $ARGV[0];  
$one_line = $ARGV[1];
$lists = $ARGV[2];
$matrix = $ARGV[3];
$lines = 0;
# print "<PRE>n = $n, $one_line, $lists, $matrix </PRE>";
while (<STDIN>) {
   $y = unpack("B*",$_);  # unpack output line into bits, high-to-low 
   substr($y,0,8) = "";   # delete first byte
   for ($i=0; $i<=($n-1)*$n/12; ++$i) { substr($y,$i*6,2) = ""; }  # omit 01's
   print "<TR>";
   if ($one_line) {
      print "<TD ALIGN=CENTER>";
      $k = 0;
      for ($i=1; $i<$n; ++$i) { 
         print ".";
         for ($j=0; $j<$i; ++$j) { print substr($y,$k++,1) }
      }
      print "<BR></TD>";
   }
   if ($lists) {
      print "<TD>";
      $k = 0;
      for ($i=0; $i<$n; ++$i) { 
         print "$i:";
         for ($j=0; $j<$i; ++$j) { 
           if (substr($y,$k++,1)=='1') { print "-$j" }
         }
         print "<BR>\n";
      }
      print "</TD>";
   }
   if ($matrix) {
      print "<TD>";
      $k = 0;
      for ($i=1; $i<$n; ++$i) { 
         for ($j=0; $j<$i; ++$j) { print substr($y,$k++,1) }
         print "<BR>";
      }
      print "</TD>";
   }
   print "</TR>\n";
#  die if (++$lines > $LIMIT);
   die if (++$lines > 2000);
}
print "</TABLE>\n<P>Graphs generated = $lines<BR>\n";

