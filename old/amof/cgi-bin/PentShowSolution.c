#include <stdio.h>

#define MAXBOARD 256
#define ICONDIR "Ico/"

char board[MAXBOARD][MAXBOARD];
int M,N;
int flag = 0;
char str[200];

int FindStartRow ()
{
    int i, j;

    /* skip blank lines */
    for (i=0; i< N; i++) {
	for (j=0; j< M; j++) {
	    if (board[i][j] != '.') return(i);
	}
    }
    return(-1);
}

int FindStartCol ()
{
    int i, j;

    /* skip blank lines */
    for (j=0; j< M; j++) {
	for (i=0; i< N; i++) {
	    if (board[i][j] != '.') return(j);
	}
    }
    return(-1);
}

int FindEndRow ()
{
    int i, j;

    /* skip blank lines */
    for (i=(N-1); i >=0; i--) {
	for (j=0; j< M; j++) {
	    if (board[i][j] != '.') return(i);
	}
    }
    return(-1);
}

int FindEndCol ()
{
    int i, j;

    /* skip blank lines */
    for (j=(M-1); j >=0; j--) {
	for (i=0; i< N; i++) {
	    if (board[i][j] != '.') return(j);
	}
    }
    return(-1);
}

void PrintRow (int format)
{
    int r,c;
    int SR, SC, ER, EC;
    char cell,right,down, cross;

    SR = FindStartRow();
    SC = FindStartCol();
    ER = FindEndRow();
    EC = FindEndCol();

    if (format & 1) {  /* user want the letter matrix output */
	printf("<td align=center><pre>");
	for (r = SR; r <= ER; r++) {
	    for (c = SC; c <= EC; c++) {
		printf("%c",((board[r][c]=='.')? ' ':board[r][c]));
	    }
	    printf("\n");
	}
	printf("</pre></td>");
    }

    if (format & 2) {    /* user want the pretty picture */
	printf("<td><table border=0 cellpadding=0 cellspacing=0 >\n");
	
	/* prints a square with a dot on lower right */
	printf("<tr><td>");
	if (board[SR][SC] != '.')
	    printf("<img src=\"%stsplined.gif\" border=0>",ICONDIR);
	printf("</td>\n");

	/* if there is an adjacent piece below, print a lower line */
	for (c=SC; c<=EC; c++) {
	    printf("<td>");
	    if (board[SR][c] != '.')
		printf("<img src=\"%stsplineb.gif\" border=0>",ICONDIR);
	    else if ((c<EC) && (board[SR][c+1] != '.'))
		printf("<img src=\"%stsplined.gif\" border=0>",ICONDIR);
	    printf("</td>\n");
	}
	printf("</tr>");

	/* now print the whole thing */
	for (r = SR; r <= ER; r++) {

	    /* print the first border */
	    printf("<tr><td>");
	    if (board[r][SC] != '.')
		printf("<img src=\"%stspliner.gif\" border=0>",ICONDIR);
	    else if ((r<ER) && (board[r+1][SC] != '.'))
		printf("<img src=\"%stsplined.gif\" border=0>",ICONDIR);
	    printf("</td>\n");

	    /* now the actual pieces */
	    for (c = SC; c <= EC; c++) {
		printf("<td>");
		cell = board[r][c];
		right = board[r][c+1];
		down = board[r+1][c];
		cross = board[r+1][c+1];

		if (cell == '.') {
		    if ((r == ER) && (c == EC)) {

		    } else if (c == EC) {
			if (cell != down) 
			    printf("<img src=\"%stsplineb.gif\" border=0>",ICONDIR);

		    } else if (r == ER) {
			if (cell != right) 
			    printf("<img src=\"%stspliner.gif\" border=0>",ICONDIR);

		    } else if (cell != right) {
			if (cell != down) 
			    printf("<img src=\"%stsplinerb.gif\" border=0>",ICONDIR);
			else printf("<img src=\"%stspliner.gif\" border=0>",ICONDIR);
		    } else {
			if (cell != down) 
			    printf("<img src=\"%stsplineb.gif\" border=0>",ICONDIR);
			else {
			    if (cell != cross)
				printf("<img src=\"%stsplined.gif\" border=0>",ICONDIR);
		      /*    else printf("<img src=\"%stspline.gif\" border=0>",ICONDIR);*/
			}
		    }
		}
		else {
		    if ((r == ER) && (c == EC)) {
			printf("<img src=\"%sgreenrb.gif\" border=0>",ICONDIR);
		    } else if (c == EC) {
			if (cell != down) 
			    printf("<img src=\"%sgreenrb.gif\" border=0>",ICONDIR);
			else printf("<img src=\"%sgreenr.gif\" border=0>",ICONDIR);
		    } else if (r == ER) {
			if (cell != right) 
			    printf("<img src=\"%sgreenrb.gif\" border=0>",ICONDIR);
			else printf("<img src=\"%sgreenb.gif\" border=0>",ICONDIR);
		    } else if (cell != right) {
			if (cell != down) 
			    printf("<img src=\"%sgreenrb.gif\" border=0>",ICONDIR);
			else printf("<img src=\"%sgreenr.gif\" border=0>",ICONDIR);
		    } else {
			if (cell != down) 
			    printf("<img src=\"%sgreenb.gif\" border=0>",ICONDIR);
			else {
			    if (cell != cross)
				printf("<img src=\"%sgreend.gif\" border=0>",ICONDIR);
			    else printf("<img src=\"%sgreen.gif\" border=0>",ICONDIR);
			}
		    }
		}
		printf("</td>\n"); 
	    }
	    printf("</tr>\n");
	}

	printf("</table></td>");
    }  /* end of pretty picture */
}

int GetMatrix ()
{
    int i, j;

    for (i = 0; i < N; i++) {
	for (j = 0; j < M; j++) {
	    if (scanf("%c",&board[i][j]) == EOF)
		return(0);
	    if (board[i][j] == '*') {
		flag = 1;
		fgets(str, 200 ,stdin);
		return(0);
	    }
	}
	scanf("\n");
    }
    scanf("\n");
    return(1);
}

int main (int argc, char *argv[])
{
    int i, format;

    format = atoi(argv[1]);

    scanf("%d %d\n",&N,&M);
    if ((N < 0) || (N > MAXBOARD)) 
	{printf("error, invalid n = %d.\n", N); exit(-1);}
    if ((M < 0) || (M > MAXBOARD)) 
	{printf("error, invalid m = %d.\n", M); exit(-1);}
    
    if (format == 0) { /* no data specified */
	printf("<b><h3>Please specify output format</h3></b>");
	exit(-1);
    }

    printf("<table border=1 cellspacing=2 cellpadding=2><tr>\n");
    if (format & 1) 
	printf("<th colspan=1><font size=\"+1\">Letters</font><br></th>\n");
    if (format & 2)
	printf("<th colspan=1><font size=\"+1\">Graphic</font><br></th>\n");
    printf("</tr>\n");

    while (GetMatrix()) {
	printf("<tr>\n");
	PrintRow(format);
	printf("</tr>\n");
    }

    printf("</table>\n");
    
    if (flag == 1) {
	printf("%s\n", str);
	return(1);
    }
    return(0);
}
