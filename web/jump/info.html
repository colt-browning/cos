Given two permutations $\pi=(a_1,\ldots,a_n)$ and $\tau=(b_1,\ldots,b_k)$, we say that <span class="def">$\pi$ contains the pattern $\tau$</span>, if there is a subsequence $(a_{i_1},a_{i_2},\ldots,a_{i_k})$ of $\pi$, where $i_1\lt i_2\lt \cdots\lt i_k$, whose entries appear in the same relative order as $(b_1,\ldots,b_k)$.
An occurrence of such a subsequence is referred to as a <span class="def">match</span> of $\tau$ in $\pi$.
Otherwise, we say that <span class="def">$\pi$ avoids the pattern $\tau$</span>.
For example, the permutation $\pi=451362$ contains the pattern $\tau=2314$, as witnessed by the subsequence $4516$, which is match of $\tau$ in $\pi$, whereas $\pi$ avoids $\tau'=1234$.
<p>

By imposing extra conditions for when a pattern matches, this classical notion of pattern-avoidance can be modified in several ways.
<ul class="enum">
<li>In a <span class="def">vincular pattern</span> [BS00] such as $\tau=2\underline{41}3$, two neighboring entries of $\tau$ are underlined, and a match of $\tau$ in $\pi$ requires that the two underlined entries match adjacent positions in $\pi$.</li>
<li>In a <span class="def">barred pattern</span> [Wes90] such as $\tau=3\bar{1}42$, one entry of $\tau$ is overlined, and $\pi$ avoids the pattern $\tau$ if every match of the unbarred entries can be extended to a match including the barred entry.</li>
<li>A match of a <span class="def">boxed pattern</span> [AKV13] such as $\tau=\;$<span style="border:1px; border-style:solid">$2143$</span> in a permutation $\pi$ occurs if in this match, no entry of $\pi$ at a position between the matched ones has a value between the smallest and largest value of the match.</li>
<li>
An even more general notion, which encompasses all the previously mentioned types of patterns as special cases, is that of a <span class="def">mesh pattern</span> [BC11].
A mesh pattern is a pair $(\tau,M)$, where $\tau=(b_1,\ldots,b_k)$ is a permutation and $M$ is a $(k+1)\times (k+1)$ matrix with entries from $\{0,1\}$.
Such a mesh pattern can be visualized by considering the <span class="def">grid representation</span> of $\tau$, i.e., the set of points $(i,b_i)$ for $i=1,\ldots,k$ in the plane.
All horizontal and vertical lines through these points define a grid with $(k+1)\times (k+1)$ cells, and we shade precisely those grid cells that correspond to 1-entries of $M$.
For example, the mesh pattern $(231,M)$ with $M=\begin{pmatrix}0100 \\ 0100 \\ 1111 \\ 0100\end{pmatrix}$ is drawn as
<span class="mesh">
<style>.l{stroke:black;stroke-width:1}.p{stroke-width:0;fill:black}.r{stroke-width:0;fill:lightgray}</style>
<svg width='80' height='80'>
<rect class='r' x='20' y='0' width='20' height='80'/>
<rect class='r' x='0' y='40' width='80' height='20'/>
<circle class='c' cx='20' cy='40' r='3'/>
<circle class='c' cx='40' cy='20' r='3'/>
<circle class='c' cx='60' cy='60' r='3'/>
<line class='l' x1='20' y1='0' x2='20' y2='80'/>
<line class='l' x1='40' y1='0' x2='40' y2='80'/>
<line class='l' x1='60' y1='0' x2='60' y2='80'/>
<line class='l' x1='0' y1='20' x2='80' y2='20'/>
<line class='l' x1='0' y1='40' x2='80' y2='40'/>
<line class='l' x1='0' y1='60' x2='80' y2='60'/>
</svg>
</span>.<br>
A permutation $\pi$ contains a mesh pattern $(\tau,M)$, if the grid representation of $\pi$ contains the grid representation of $\tau$, and no rectangle corresponding to a shaded cell that comes from a 1-entry of $M$ contains a point of $\pi$ in its interior.
</li>
</ul>
<p>

By these definitions, a classical pattern is a mesh pattern with no shaded cells, i.e., all entries of $M$ are 0s.
A vincular pattern is a mesh pattern with a single column of shaded cells.
A barred pattern is a mesh pattern with a single shaded cell.
A boxed pattern is a mesh pattern where all cells in the bounding box of the points of $\tau$ are shaded.
For more examples and illustrations of these different types of patterns, see [HHMW22] or one of the aforementioned papers where these patterns were introduced.
<p>

The paper [HHMW22] proposes a very general and versatile algorithm, called Algorithm J, for generating many different kinds of pattern-avoiding permutations, which works under some very mild constraints on the patterns.
A pattern satisfying these constraints is called <span class="def">tame</span>.
Specifically, a classical permutation pattern is tame if the largest entry is not at the boundary.
For example, $\tau=231$ is tame, but $\tau'=321$ is not tame.
For the conditions when a vincular, barred, boxed or mesh pattern is tame, see [HHMW22].
The algorithm developed in [HHMW22] can generate all pattern-avoiding permutations for any Boolean formula $F$ composed of tame patterns of one of the types discussed before, conjunctions $\wedge$ and disjunctions $\vee$.
A conjunction expresses that the permutation must avoid both of the patterns, and a disjunction expresses that the permutation must avoid at least one of the patterns.
We write $S_n(F)$ for the set of permutations of length $n$ satisfying the constraints expressed by $F$.
For example $S_n(231)$ is the set of permutations avoiding $231$, $S_n(231\wedge 132)$ is the set of permutations avoiding both $231$ and $132$, and $S_n(231\vee 132)$ is the set of permutations avoiding $231$ or $132$.
A more complicated set that the algorithm can generate would be $S_n(231\wedge (2\underline{41}3\vee 3\bar{1}42))$.
<p>

The implementation of Algorithm J running on this website is due to Chigozie Agomo.
The input field takes the formula $F$, with the following grammar rules:
<table style="border-spacing:15px 5px">
<tr><td>conjunction $\wedge$</td><td>&amp; (ampersand)</td></tr>
<tr><td>disjunction $\vee$</td><td>| (vertical bar)</td></tr>
<tr><td>classical pattern $231$</td><td>231 (consecutive digits)</td></tr>
<tr><td>vincular pattern $2\underline{41}3$</td><td>24_13 (underscore between the underlined entries)</td></tr>
<tr><td>barred pattern $3\bar{1}42$</td><td>3b142 (b before overlined entry)</td></tr>
<tr><td>mesh pattern $(231,M)$ with $M$ as above</td><td>231:0100 0100 1111 0100 (pattern, then colon, followed by matrix $M$ row by row from top to bottom)</td></tr>
<tr><td>precedence constraints</td><td>231 &amp; (24_13 | 3b142) versus (231 &amp; 24_13) | 3b142 (use parenthesis)</td></tr>
</table>
<p>