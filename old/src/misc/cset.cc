#define _CSET_CPP

//========================================================================
// By :  John Boyer
// On :  19 SEP 1995
// For:  Dr. Ruskey, CSC 520
//
// This module implements a class called CSet that provides a basic
// ability to manipulate sets, including set membership, intersection,
// union, and complement.
//
// A set containing N elements will be represented by this class as the
// subset of natural numbers given by {0, ..., N-1}.
/*
   Copyright 1995 by John M. Boyer
   Permission to use and distribute this program, its output and
   and its source is granted provided that the author is acknowledged
   in works that use this program, its output or any portion of its
   source code.
*/
//========================================================================

#include <stdlib.h>
#include <string.h>
#include <ctype.h>

#include "cset.h"

//========================================================================
// This internal macro computes the byte in which the given bit N must
// appear.  Note that we downshift by 3 because a byte contains 8 bits.
//========================================================================

#define BYTEINDEX(N) ((N) >> 3)

//========================================================================
// This internal macro computes which bit within a given byte that the
// number N will occupy.  The 3 least significant bits of a number tell us.
//========================================================================

#define BITINDEX(N) ((N) & 7)

//========================================================================
// This internal macro computes the size of a byte array needed to store
// n bits.
//========================================================================

#define ARRAYSIZE(n) (BYTEINDEX(n) + (BITINDEX(n)?1:0))

//========================================================================
// This internal macro simply consolidates the code for setting all CSet
// data members to indicate an empty set, which may occur under valid or
// invalid conditions as indicated by the __Status (OK or NOTOK).
//========================================================================

#define MAKE_EMPTY_SET(__Status) \
{ \
     Members=NULL; \
     MaxSize=0; \
     Status=(__Status); \
}

//========================================================================
// This internal macro is intended to be called within a CSet member
// function and only on sets that are not degenerately empty.
// The last byte of 'this' set and set S2 are corrected to ensure that
// all unused bits are cleared.  This allows equality tests to compare
// the whole last byte without regard for interference from unused bits.
//========================================================================

#define FIX_LAST_BYTE(S2) \
{ \
int Index, Mask; \
\
    if (BITINDEX(MaxSize)) \
    { \
        Index = BYTEINDEX(MaxSize); \
        Mask = 1 << (BITINDEX(MaxSize)-1); \
        Mask |= (Mask-1); \
        Members[Index] &= Mask; \
	(S2).Members[Index] &= Mask; \
    } \
}

//========================================================================
// This internal macro sets (turns on) the Mth bit (from a theoretic point
// of view) in the Members array of a set.  This indicates the fact that
// M is a member of the set.
//========================================================================

#define INCLUDEMEMBER(M)  Members[BYTEINDEX(M)] |= 1<<BITINDEX(M)

//========================================================================
// This internal macro receives the byte an bit indexes, and produces N,
// which is the value of the set element represented by (Byte,Bit)
//========================================================================

#define GETMEMBER(Byte, Bit) (((Byte)<<3) + (Bit))

//========================================================================
// CSet(int theMaxSize, int Initializer)
//
// This is the constructor that should be used to create an instance of
// the class CSet.
//
// theMaxSize - indicates the largest number of elements that the
//              CSet class instance can contain.
// Initializer - The new instance will either start empty or full
//               depending on whether or not, respectively, the value
//               passed for this parameter is zero.
//========================================================================

CSet::CSet(int theMaxSize, int Initializer)
{
     MAKE_EMPTY_SET(OK);
     Initialize(theMaxSize, Initializer);
}

//========================================================================
// CSet()
//
// This is a basic constructor that is not intended to be used, but does
// properly initialize the CSet instance to be a (degenerate) empty set
// should a user of this class not use the parameterized constructor
// given above.  An example of when this might occur would be the
// dynamic allocation of an array of CSet instances using the 'new'
// operator.  In this case, the Initialize() function must be called for
// each such set once it is created.
//========================================================================

CSet::CSet()
{
     MAKE_EMPTY_SET(OK);
}

//========================================================================
// Initialize(int theMaxSize, int Initializer)
//
// This is the constructor that should be used to create an instance of
// the class CSet.
//
// theMaxSize - indicates the largest number of elements that the
//              CSet class instance can contain.
// Initializer - The new instance will either start empty or full
//               depending on whether or not, respectively, the value
//               passed for this parameter is zero.
//========================================================================

void CSet::Initialize(int theMaxSize, int Initializer)
{
     if (MaxSize > 0 || Members != NULL)
         DestroySet();

     if (theMaxSize > 0)
     {
	 MaxSize = theMaxSize;
	 Members = new byte[ARRAYSIZE(MaxSize)];
	 if (Members != NULL)
	      memset(Members, Initializer ? ~0:0, ARRAYSIZE(MaxSize));
	 else MAKE_EMPTY_SET(NOTOK);
     }
}

//========================================================================
// CSet(CSet&)
//
// This copy constructor ensures that the dynamic memory allocated for a
// set is duplicated should a user of this class try to pass a CSet
// instance by value (i.e. as a value parameter).
//========================================================================

CSet::CSet(CSet& X)
{
     MAKE_EMPTY_SET(OK);

     if (X.MaxSize > 0 && X.Members != NULL)
     {
         MaxSize = X.MaxSize; 
	 Members = new byte[ARRAYSIZE(MaxSize)];
	 if (Members != NULL)
	      memcpy(Members, X.Members, ARRAYSIZE(MaxSize));
	 else MAKE_EMPTY_SET(NOTOK);
     }
}

//========================================================================
// ~CSet()
//
// The destructor here calls DestroySet() to free the bit array used to
// represent the set.
//========================================================================

CSet::~CSet()
{
     DestroySet();
}

//========================================================================
// DestroySet()
//
// This function simply frees the bit array used to represent the
// set and initializes all variables (makes a degenerate empty set).
// This function allows other functions to destroy the data members
// of the set without destroying the set itself.
//========================================================================

void CSet::DestroySet()
{
     if (Members != NULL)
	 delete Members;

     MAKE_EMPTY_SET(OK);
}

//========================================================================
// operator =(CSet& RHS);
//
// Duplicates the MaxSize and Members from the set given by RHS into the
// 'this' object.
//========================================================================

void CSet::operator =(CSet& RHS)
{
     if (Status != OK) return;

     DestroySet();

     if (RHS.MaxSize > 0)
     {
         MaxSize = RHS.MaxSize;
	 Members = new byte[ARRAYSIZE(MaxSize)];
	 if (Members != NULL)
	      memcpy(Members, RHS.Members, ARRAYSIZE(MaxSize));
	 else MAKE_EMPTY_SET(NOTOK);
     }
}

//========================================================================
// operator &=(CSet& RHS);
//
// Uses bitwise-and to performs set intersection between 'this' and the
// given set, RHS.
//========================================================================

void CSet::operator &=(CSet& RHS)
{
     if (Status != OK)
         ;
     else if (MaxSize <= 0)
	 ;
     else if (RHS.MaxSize <= 0)
	 DestroySet();
     else if (MaxSize != RHS.MaxSize)
	 Status = NOTOK;
     else
     {
	 for (int I=0, Len=ARRAYSIZE(MaxSize); I < Len; I++)
	      Members[I] &= RHS.Members[I];
     }
}

//========================================================================
// operator |=(CSet& RHS)
//
// Uses bitwise-or to perform set union between 'this' and the given
// set, RHS.
//========================================================================

void CSet::operator |=(CSet& RHS)
{
     if (Status != OK)
         ;  
     else if (RHS.MaxSize <= 0)
	 ;
     else if (MaxSize <= 0)
	 *this = RHS;
     else if (MaxSize != RHS.MaxSize)
	 Status = NOTOK;
     else
     {
	 for (int I=0, Len=ARRAYSIZE(MaxSize); I < Len; I++)
	      Members[I] |= RHS.Members[I];
     }
}

//========================================================================
// operator --()
//
// Invert this, i.e. take the complement of 'this' set with the universe.
//========================================================================

void CSet::operator --()
{
     if (Status != OK)
         ;                                                               
     else if (MaxSize > 0) 
	 for (int I=0, Len=ARRAYSIZE(MaxSize); I < Len; I++)
	      Members[I] = ~Members[I];
}


//========================================================================
// operator -=()
//
// Remove elements of RHS from this
//========================================================================

void CSet::operator -=(CSet& RHS)
{
     -- RHS;
     *this &= RHS;
     -- RHS;
}

//========================================================================
// operator <=(CSet& RHS)
//
// Is this a subset of RHS?  The successive bytes within 'this' set can be
// identical to those in RHS, or they can be different.  In any different
// bytes, we take the exclusive-or of the two bytes and then bitwise-and
// with the byte from the RHS set.  If the result from the bitwise-and is
// different from the exclusive-or, then it means that a difference
// between 'this' and RHS was detected (xor) but that the bit was off in
// the RHS set (and).  Therefore, 'this' contains an element not in RHS,
// so 'this' is not a subset of RHS.
//
// RETURNS:  1 if all elements of 'this' are also element of RHS
//           0 if there exists an element of 'this' that is not an
//                element of RHS.
//========================================================================

int  CSet::operator <=(CSet& RHS)
{
     if (Status != OK) return 0;

     if (MaxSize <= 0) return 1;
     else if (RHS.MaxSize <= 0) return 0;
     else if (MaxSize != RHS.MaxSize)
     {
	 Status = NOTOK;
         return 0;
     }

     FIX_LAST_BYTE(RHS);

int XOR, I, Len;

     for (I=0, Len=ARRAYSIZE(MaxSize); I < Len; I++)
	  if ((XOR = Members[I] ^ RHS.Members[I]) != 0)
	       if ((XOR & RHS.Members[I]) != XOR)
	            return 0; 

     return 1;  
}

//========================================================================
// operator <(CSet& RHS)
//
// Is 'this' a proper subset of RHS?  The answer is really the same as
// the subset detector (see above), except that we must be guaranteed that
// at least one of the exclusive-or operations resulted in a non-zero
// value.
// NOTE:  The function could've been written as calls to the overloaded
//        <= and == operators (<= but not ==), but this would have
//        resulted in a significantly slower operation.
//========================================================================

int  CSet::operator <(CSet& RHS)
{
     if (Status != OK) return 0;

     if (MaxSize <= 0) return 1;
     else if (RHS.MaxSize <= 0) return 0;
     else if (MaxSize != RHS.MaxSize)
     {
	 Status = NOTOK;
         return 0;
     }

     FIX_LAST_BYTE(RHS);

int XOR, I, Len, ProperFlag;

     for (I=ProperFlag=0, Len=ARRAYSIZE(MaxSize); I < Len; I++)
	  if ((XOR = Members[I] ^ RHS.Members[I]) != 0)
          {
               ProperFlag = 1;
	       if ((XOR & RHS.Members[I]) != XOR)
		    return 0;
	  } 

     return ProperFlag;
}

//========================================================================
// int  operator ==(CSet& RHS)
//
// Do 'this' and RHS contain exactly the same elements?
//========================================================================

int  CSet::operator ==(CSet& RHS)
{
     if (Status != OK) return 0;

     if (MaxSize != RHS.MaxSize) return 0;
     else if (MaxSize <= 0) return 1;

     FIX_LAST_BYTE(RHS);

     return memcmp(Members, RHS.Members, ARRAYSIZE(MaxSize)) ? 0 : 1;
}

//========================================================================
// operator <<=(int Member)
//
// Put Member into 'this' set by setting the bit corresponding to Member.
//========================================================================

void CSet::operator <<=(int Member)
{
     if (Status != OK || Member < 0 || Member >= MaxSize)
	  Status = NOTOK;
     else INCLUDEMEMBER(Member);
}

//========================================================================
// operator >>=(int Member)
//
// Take Member out of 'this' by clearing bit corresponding to Member.
//========================================================================

void CSet::operator >>=(int Member)
{
     if (Status != OK || Member < 0 || Member >= MaxSize)
	  Status = NOTOK;
     else Members[BYTEINDEX(Member)] &= ~ (byte) (1<<BITINDEX(Member));
}

//========================================================================
// operator <<(int Member, CSet& S)
//
// Is Member in S?
//========================================================================

int  operator <<(int Member, CSet& S)
{
     if (S.Status != OK || Member<0 || Member >= S.MaxSize)
     {
         S.Status = NOTOK; 
	 return 0;
     }

     return (S.Members[BYTEINDEX(Member)] & (1<<BITINDEX(Member)))
             ? 1 : 0;
}

//========================================================================
// GetLeastElement()
//
// The parameter N has a default of 0.  Finds first element that is
// greater than or equal to N.  The default of 0 causes the function to
// find the smallest element in the set.
// RETURNS -1 if there is no value >= N, or a number from 0 to MaxSize-1
//========================================================================

int CSet::GetLeastElement(int N)
{
    if (Status != OK) return -1;
    if (N < 0 || N >= MaxSize) return -1;
     
    FIX_LAST_BYTE(*this);

    for (int I=BYTEINDEX(N); I < ARRAYSIZE(MaxSize); I++)
	 if (Members[I])
         {
	     for (int J = 0; J < 8; J++)
		  if (((Members[I] >> J) & 1) && GETMEMBER(I, J) >= N)
		      return GETMEMBER(I, J);
	 }

    return -1;
}

//========================================================================
// GetGreatestElement()
//
// Finds first element that is less than or equal to N.
// Defaults to finding the largest element in the set; the default shown
// is -1 because MaxSize isn't known at compile time.
// RETURNS -1 if there is no value <= N, or a number from 0 to MaxSize-1
//========================================================================

int CSet::GetGreatestElement(int N)
{
    if (Status != OK) return -1;
    if (N >= MaxSize) return -1;
    if (N < 0) N = MaxSize-1;
     
    FIX_LAST_BYTE(*this);

    for (int I=BYTEINDEX(N); I >= 0; I--)
	 if (Members[I])
         {
	     for (int J = 7; J >= 0; J--)
		  if (((Members[I] >> J) & 1) && GETMEMBER(I, J) <= N)
		      return GETMEMBER(I, J);
	 }

    return -1;
}

//========================================================================
// IsEmpty()
//
// RETURNS 1 if 'this' set contains no elements; 0 otherwise 
//========================================================================

int CSet::IsEmpty()
{
    if (Status != OK) return 0;

    if (MaxSize <= 0) return 1;
     
    FIX_LAST_BYTE(*this);

    for (int I=0; I < ARRAYSIZE(MaxSize); I++)
	 if (Members[I])
	     return 0;

    return 1;
}

//========================================================================
// IsFull()
//
// RETURNS 1 if 'this' set contains all elements; 0 otherwise 
//========================================================================

int CSet::IsFull()
{
int RetVal;

    --(*this);
    RetVal = IsEmpty();
    --(*this);

    return RetVal;
}

//========================================================================
// FillSet(int Start, int Iterator)
//
// This function will add to 'this' set all elements in an equivalence
// class described by the parameters.
//
// Start - the first element to be added to 'this' set.
// Iterator - The value to add to Start at each iteration of a loop
//            that will pick out the elements of the equivalence class
//            Using a negative iterator will cause the loop to move
//            back from Start toward 0.  Using a Positive iterator will
//            cause the loop to skip forward to successive elements.
//            Using a loop iterator of 0 will cause the loop not to
//            run (the start element will still be added to the set).
//========================================================================

void CSet::FillSet(int Start, int Iterator)
{
     if (Status != OK || Start < 0 || Start >= MaxSize)
     {
	 Status = NOTOK;
         return;
     }

     INCLUDEMEMBER(Start);

     if (Iterator != 0)
	 while (!Stopper(Start, Iterator))
         {
                 Start += Iterator;
		 INCLUDEMEMBER(Start);
         }
}

//========================================================================
// Stopper(int,int)
//
// This function assists FillSet() by providing the basic test for
// its loop termination.  Its designation as a virtual function allows
// derived classes to redefine or add to the terminating conditions.
//
// It is recommended that the derived class start by calling this
// function by using scope resolution, i.e. only if the call
//
// 	CSet::Stopper(Start, Iterator)
//
// returns 0 (for don't stop) should the overloaded Stopper() be
// allowed to run its test.
//========================================================================

int CSet::Stopper(int Current, int Iterator)
{
    if (Status != OK) return 0;

    Current += Iterator;

    return (Current < 0 || Current >= MaxSize) ? 1 : 0;
}

//========================================================================
// operator <<(ostream&, CSet&)
//
// Display the elements in the given set.
//========================================================================

ostream& operator <<(ostream& cout, CSet& OSet)
{
int PrintComma = 0, I;

     if (OSet.Status != OK) return cout;

     cout << '{';

     for (I = 0; I < OSet.MaxSize; I++)
	  if (I << OSet)
          {
	      if (PrintComma) cout << ", ";
              PrintComma = 1;
	      cout << I;
	  }

     return cout << '}';
}

//========================================================================
// operator >>(istream&, CSet&)
//
// Read a line of text, look for a open brace as the start of a set,
// extract all numbers until the closing brace, and set the corresponding
// bits in the given set.  If any number cannot be represented by the set,
// then the set's Status will be set to NOTOK.
//========================================================================

istream& operator >>(istream& cin, CSet& ISet)
{
int  theNumber, FoundCloseBrace, FoundOpenBrace;
char Line[128], *NumberPos;

     if (ISet.MaxSize <= 0 || ISet.Status != OK) return cin;

     ISet.Initialize(ISet.MaxSize);

     FoundOpenBrace = FoundCloseBrace = 0;

     do {
	cin.getline(Line, 128, '\n');

	if (!FoundOpenBrace)
        {
	    if ((NumberPos = strchr(Line, '{')) != NULL)
		 FoundOpenBrace = 1;
	}
	else NumberPos = Line;

	if (strchr(Line, '}'))
	    FoundCloseBrace = 1;

	if (FoundOpenBrace)
	{
	    do
            {
	        while (! isdigit(*NumberPos))
	           if (*NumberPos == '\0') break;
	           else NumberPos ++;

	        if (*NumberPos != '\0')
		{
		    theNumber = atoi(NumberPos);

		    while (isdigit(*NumberPos))
			   NumberPos ++;

		    ISet <<= theNumber;
		}
	    } while (*NumberPos != '\0');
	}

     } while (!FoundCloseBrace);

     return cin;
}


