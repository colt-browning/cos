/*
 * Copyright (c) 2023 Torsten Muetze and Namrata
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "pattern.hpp"
#include <climits>
#include <stack>

Pattern::Pattern(const std::vector<int> &pat_pre, const std::vector<int> &e_pre)
{
  root_ = 0;
  pat_ = pat_pre;
  n_ = pat_.size();
  e_pre_ = e_pre;
  e_.resize(e_pre_.size()+1, Edgetype::non_cont);
  int l;
  for (int i = 0; i < e_pre_.size(); i++) {
    l = pat_[i];
    if (e_pre_[i] == 0) {
      e_[l] = Edgetype::non_cont;
    } else if (e_pre_[i] == 2) {
      e_[l] = Edgetype::semi_cont;
    } else {
      e_[l] = Edgetype::cont;
    }
  }
  init_pattern();
}

void Pattern::init_pattern()
{
  init();
  for (int i=0; i < n_; i++) {
    insert_leaf(pat_[i]);
  }
  return;
}

bool Pattern::is_friendly()
{
  if ((is_leaf(n_)) or (root_ == n_)) {
    // condition (i): the largest labeled vertex is neither the root nor a leaf
    return false;
  }

  // compute list of vertices in maximal right branch of a tree starting at its root
  std::vector<int> r_branch;
  int cur = root_;
  while (right_child_[cur] != 0) {
    r_branch.push_back(cur);
    cur = right_child_[cur];
  }

  for (int i = 0; i < r_branch.size(); i++) {
    Edgetype e_j = e_[r_branch[i]];
    if ((e_j == Edgetype::cont or (e_j == Edgetype::semi_cont)) and (r_branch[i] != n_)) {
      // condition (ii): e(x) must be 0 for all vertices on the rightmost branch from the root except for the largest labeled vertex
      return false;
    }
  }

  if (left_child_[n_] != 0) {
    int left_child_max = left_child_[n_];
    if ((e_[n_] == Edgetype::cont or (e_[n_] == Edgetype::semi_cont)) and (e_[left_child_max] == Edgetype::cont)) {
      // condition (iii): if e(k) == 1, then we require e(c_L(k)) == 0
      return false;
    }
  }
  return true;
}

bool Pattern::avoids_231()
{
  std::stack<int> s;
  int root = INT_MIN;
  for (int i = 0; i < n_; i++) {
    if (pat_[i] < root) {
      return false;
    }
    while (!s.empty() && s.top() < pat_[i]) {
      root = s.top();
      s.pop();
    }
    s.push(pat_[i]);
  }
  return true;
}
