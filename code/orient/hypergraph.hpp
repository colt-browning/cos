/* 
 * Copyright (c) 2022 Ondrej Micka, Arturo Merino and Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include<vector>
#include<string>
#include<ostream>
#include<iostream>
#include<cassert>

#include "interfaces.hpp"

// Class to represent a single oriented hyperedge
// Mostly just a thin wrapper around characteristic vector of the edge
class Hyperedge {
public:
  // Create hyperedge for n-vertex hypergraph from a list of vertices
  Hyperedge(int n, std::vector<int> vertices);
  // Does the hyperedge contain vertex i?
  inline bool contains(int i) const {
    assert((i >= 0) && (i < char_vector_.size()) && "Invalid vertex id");
    return char_vector_[i];
  };
  inline int get_head() const {return head_;};
  inline void set_head(int v) {
    assert((v >= 0 && v < char_vector_.size()) && "Invalid vertex");
    assert((char_vector_[v]) && "Vertex not in the hyperedge");
    head_ = v;
  };

  friend std::ostream &operator<<(std::ostream &output, const Hyperedge &h); // Allow easy debug printing
private:
  // Characteristic vector of the hyperedge. Position 0 is dummy because of 1-indexing 
  std::vector<bool> char_vector_;
  // Head of the edge, should always point to a valid vertex in the edge; 0 means not set
  int head_;
};


class Acyclic_hyperorientation : public Algorithm{

  enum class Direction {to_source, to_sink, none};

public:
  // Init algorithm on the graph given by list of hyperedges and with given hyperperfect elimination order
  // We reorder vertices with respect to HEO, so that vertex 1 is the vertex heo[1] and so on.
  // Algorithm assumes HEO is correct and does not check its correctness
  Acyclic_hyperorientation(
    int n, 
    const std::vector<std::string> &vertex_names, 
    const std::vector<std::vector<int>> &hyperedge_list,
    const std::vector<int> &heo
  );
  virtual ~Acyclic_hyperorientation() = default;

  
  virtual bool next();
  friend std::ostream &operator<<( std::ostream &output, const Direction &d );   // allow easy printing of enum
  
  virtual void print_edge_list(std::ostream &out) const;
  virtual void print_flip(std::ostream &out) const;   // print last flipped edge
  virtual void print_state() const; // Print debug info.
  virtual void print_adj_matrix([[maybe_unused]] std::ostream &out) const {std::cerr << "Not implemented" << std::endl;};

private:
  int n_;   // number of vertices of the underlying hypergraph
  const std::vector<std::string> vertex_names_;   // vertex labels/names given by the user
                                                  // must be indexed via heo_, e.g. vertex_names[heo_[1]],
                                                  // since algorithm reorders vertices w.r.t. heo
  std::pair<int,int> flip_pair;    // last flip pair (old_head, new_head)
  std::vector<Hyperedge *> flipped_hyperedges;  // List of hyperedges flipped during the last flip

  // Underlying hypergraph and its orientation (we don't really distinguish between these two)
  std::vector<std::vector<std::vector<Hyperedge *>>> adj_matrix_; // adjacency matrix
                        // adj_matrix_[i][j] is a list of all hyperedges containing both i and j
                        // Matrix is symmetric and diagonal empty
  std::vector<Hyperedge> hyperedge_list_;   // Should be constant, since we usually use pointers to this list, 
                                            // instead of directly working with Hyperedge instances
  std::vector<std::vector<int>> heo_adj_list_;  // adjacency list, but we only consider smaller neighbors
                                                // lists of neighbors are 0-indexed, in order to easily iterate ove them 

  std::vector<int> heo_;  // hyperfect elimination order -- heo_[i] is the vertex that is i-th in the HEO
  std::vector<int> oeh_;  // inverse of heo_ permutation

  // Auxiliary data structures for the algorithm
  std::vector<int> alpha_;  // auxiliary array used to deal with disconnected graphs
                // alpha_[i] is the largest vertex smaller than i such that it is not the smallest vertex in its connected component
                // alpha_[i] == 1 iff no such vertex exists
                // alpha_[i] is zero iff the vertex is smallest in its connected component
  int rho_;         // largest vertex with non-zero alpha
  std::vector<int> s_;        // vertex selection array
  std::vector<Direction> o_;  // zig-zag direction array

  void flip(int old_head, int new_head);  // Switch head from old_head to new_head in all applicable hyperedges
  Direction pair_orientation(int from, int to) const;  // Check how is the pair of vertices "oriented" within the orientation.
                                                 // If there is a hyperedge with from -> to, we return to_sink, similarly for the other direction.
                                                 // If there is no hyperedge with one of the vertices as head, we return none
                                                 // We assume the orientation is valid so theses options are exclusive.
  int get_flip_vertex(int i, Direction d) const;    // Find a vertex that i should be flipped with when moving in in direction d

  // Data structure initialization
  // Needs to be called in this order
  void init_hypergraph(const std::vector<std::vector<int>> &hyperedge_list);  // init adjacency matrix, hyperedge list etc.
                                                                              // also takes care of reordering vertices w.r.t. heo_
  void init_heads();      // Set heads of hyperedges to point to the largest vertex
  void init_alpha();      // build array alpha_ and set rho_
  void init_auxiliary();  // initialize s_, o_, t_
  
  // Utilities
  inline Direction reverse_dir(Direction d) const {
    return d == Direction::none ? Direction::none : (
      d == Direction::to_sink ? Direction::to_source : Direction::to_sink);
  };
  inline std::string get_vertex_name(int i) const {
    assert((i > 0) && (i <= n_) && "Invalid vertex");
    return vertex_names_[heo_[i]];
  }
  void print_hyperedge(std::ostream &out, const Hyperedge &h) const;  // Unlike << operator on Hyperedge, this uses vertex names

  // Debug
  void check_consistency() const;   // Check various invariants that should hold throughout the algorithm
  void check_heo_consistency() const; // Check whether heo_ and oeh_ are permutations and inverse to each other
                                      // Does not check whether heo_ is really HEO
  void check_adj_matrix_consistency() const;
  void check_heo_adj_list_consistency() const;  // For efficiency reasons assumes that adjacency matrix is consistent,
                                                // because searching the edge list for each entry can take a lots of time
  void check_head_consistency() const;  // Check that all heads are set and inside the hyperedge
  
};
