/*
 * Copyright (c) 2021 Arturo Merino and Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "edge.hpp"

Edge::Edge(){}

void Edge::init(Dir dir, int tail, int head, int prev, int next, int left, int right, int wall)
{
  dir_ = dir;
  tail_ = tail;
  head_ = head;
  left_ = left;
  right_ = right;
  wall_ = wall;
  prev_ = prev;
  next_ = next;
}