/*
 * Copyright (c) 2024 Rohan Acharya, Torsten Muetze, and Francesco Verciani
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#pragma once

#include <vector>
#include "coltria.hpp"
#include "graycode.hpp"
#include "tree.hpp"

// A colorful triangulation of a convex polygon whose vertices have been colored alternatingly
// 0 and 1 (red and blue), and all of whose triangles are colorful (not monochromatic).
// The triangulation is represented by a ternary tree, which is the dual graph of the
// quadrangulation obtained by removing all monochromatic edges, plus a binary string that
// describes whether the monochromatic edge in each quadrangle is of type (0,0) or (1,1)
// ((red,red) or (blue,blue)). If the number of points is odd, then the largest vertex of
// the ternary tree is special in that it is always located on the strictly rightmost branch
// from the root. This corresponds to a triangle instead of a quadrangle, but internally we still
// treat it as a "fake"-quadrangle with one boundary vertex always having degree 2, i.e., we can
// think of these two boundary edges as one.
class Coltria {

public:
  Coltria(int n);
  bool next();
  void print();

private:
  int n_;  // number of points of the triangulation
  bool even_;  // flag whether number of points is even
  int q_fake_;  // 0 if n is even and 1 if n is odd (=number of fake quadrangles)
  int q_;  // number of quadrangles after removing monochromatic edges (this only counts the actual quadrangles, excluding the fake one)
  int count_mono_flips_;  // count how many monochromatic edges have been flipped (=number of moves inside small hypercube)
  Graycode *brgc_;
  std::vector<int> perm_;  // permutation of BRGC bits to the internal bitstring
  bool first_next_, last_next_;  // we are currently traversing the first or last small hypercube (those need special treatment)
  
  Kary_tree tree_, tree_ahead_;  // ternary tree, one of them always being one rotation ahead
  std::vector<int> mono_edges_;  // types of monochromatic edges; 0=(0,0) or 1=(1,1)
  std::vector<int> quad_types_;  // types of quadrangles corresponding to each tree vertex; 0=(0,1,0,1) or 1=(1,0,1,0)
  
  int first_new(int a, int b);  // find first number from 1,2,... that is distinct from a and b
};
