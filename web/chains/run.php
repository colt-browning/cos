<?php
  if (isset($_GET["n"]) && isset($_GET["c"])  && isset($_GET["fmt"]) && isset($_GET["num"]) && isset($_GET["gfx"])) {
    $fmt = intval($_GET["fmt"]);
    $cmd = "code/chains/chains"
                             . " -n" . min(intval($_GET["n"]), 20)  /* hard-wired maximum bitstring length */
                             . " -c" . intval($_GET["c"])
                             . " -l" . strval(limit($fmt))  /* hard-wired maximum number of bitstrings */
                             . " 2>&1"  /* this command redirects stderr to stdout */
                             . (($fmt == 2) ? " > file.txt" : "");  /* redirect output to file */
    exec($cmd, $result);
    $num = filter_var($_GET["num"], FILTER_VALIDATE_BOOLEAN);
    $gfx = filter_var($_GET["gfx"], FILTER_VALIDATE_BOOLEAN);
    output($result, $fmt, "string_to_bits", 0, "svg_bits", 0, "svg_wheel", "bits_col", $num, $gfx);
  }
?>

