<?php
  // convert string to list of numbers
  function string_to_list($string, $dummy) {
    // is this really a string of numbers separated by whitespace
    if (!preg_match("/^[0-9\s]*$/", $string)) {
      return false;
    }
    // strip leading and trailing whitespace
    $string = preg_replace("/(^\s*|\s*$)/", "", $string);
    if (strlen($string) == 0) {
      return array();
    }
    // split at whitespace
    return array_map('intval', preg_split("/\s+/", $string));
  }

  // convert string of {0,1,*} to array of bits
  function string_to_bits($string, $dummy) {
    // is this really a bitstring, i.e., a string
    // with only 0s, 1s, *s and whitespace
    if (!preg_match("/^[01\*\s]*$/", $string)) {
      return false;
    }
    // strip leading and trailing whitespace
    $string = preg_replace("/(^\s*|\s*$)/", "", $string);
    if (strlen($string) == 0) {
      return array();
    }
    // split into individual bit characters
    $x = str_split($string);
    for ($i = 0; $i < count($x); $i++) {
      if ($x[$i] == '*') {
        $x[$i] = 2;
      } else {
        $x[$i] = intval($x[$i]);
      }
    }
    return $x;
  }

  // convert string to number partition
  function string_to_partition($string, $dummy) {
    // is this really a number partition, i.e., a string
    // of numbers separated by |
    if (!preg_match("/^[0-9\s\|]*$/", $string)) {
      return false;
    }
    // split into blocks of sets
    $blocks = preg_split('/\|/', $string);
    $part = array();
    foreach ($blocks as $set) {
      // strip leading and trailing whitespace
      $set = preg_replace("/(^\s*|\s*$)/", "", $set);
      array_push($part, array_map('intval', preg_split("/\s+/", $set)));
    }
    return $part;
  }

  // check if this string represents Lyndon brackets
  // (currently there is no visualization available for them, so
  // we do not really read the object in case of success)
  function string_to_brackets($string, $dummy) {
    if (!preg_match("/^[0-9\s,\[\]]*$/", $string)) {
      return false;
    }
    return true;
  }

  // convert string to Dyck path
  function string_to_dyck($string, $dummy) {
    // is this really a Dyck path, i.e., a string
    // with the number of flaws, then the | separator, then a bitstring
    if (!preg_match("/^[0-9\s]*\|[01\s]*$/", $string)) {
      return false;
    }
    // split into two blocks
    $blocks = preg_split('/\|/', $string);
    return string_to_bits($blocks[1], $dummy);
  }

  // convert string describing a set to array of bits (indicator array)
  function string_to_indicator($string, $n) {
    $set = string_to_list($string, 0);
    if ($set == false) {
      return false;
    }
    return set_to_indicator($set, $n, 1);
  }

  // convert subset representation to indicator array representation
  function set_to_indicator($set, $n, $rebase) {
    $bits = array_fill(0, $n, 0);
    for ($i = 0; $i < sizeof($set); $i++) {
      $bits[$set[$i] - $rebase] = 1;
    }
    return $bits;
  }

  // convert string to array of bits representation of numerical semigroup
  function string_to_sgroup($string, $genus) {
    $set = string_to_list($string, 0);
    if ($set == false) {
      return false;
    }
    // fill set in the end
    $min = $set[sizeof($set) - 1] + 1;
    $max = 2*$genus + 2;
    for ($i = $min; $i <= $max; $i++) {
      array_push($set, $i);
    }
    return set_to_indicator($set, $max+1, 0);
  }

  // convert string of pairs to list of pairs. Eg:  (6,5) (1,1) (2,9)
  function string_to_pairs($string, $dummy) {
    // is this really a string of numbers separated by whitespace and |
    if (!preg_match("/^[0-9\s|]*$/", $string)) {
      return false;
    }
    // replace |
    $string = preg_replace("/\|/", " ", $string);
    // strip leading and trailing whitespace
    $string = preg_replace("/(^\s*|\s*$)/", "", $string);
    if (strlen($string) == 0) {
      return array();
    }
    // split at whitespace
    $ints = array_map('intval', preg_split("/\s+/", $string));
    if (count($ints) % 2 != 0) {  // each pairs consists of two integers
      return false;
    }
    $pairs = array();
    for ($i = 0; $i < count($ints); $i+= 2) {
       array_push($pairs, array($ints[$i], $ints[$i+1]));
    }
    return $pairs;
  }

  // convert string to a list of pairs of pairs
  function string_to_ppairs($string, $dummy) {
    // is this really a string of numbers separated by whitespace and |
    if (!preg_match("/^[0-9\s\|]*$/", $string)) {
      return false;
    }
    // replace |
    $string = preg_replace("/\|/", " ", $string);
    // strip leading and trailing whitespace
    $string = preg_replace("/(^\s*|\s*$)/", "", $string);
    if (strlen($string) == 0) {
      return array();
    }
    // split at whitespace
    $ints = array_map('intval', preg_split("/\s+/", $string));
    if (count($ints) % 4 != 0) {  // each rectangle is specified by 2 pairs of (x,y) coordinates
      return false;
    }
    $ppairs = array();
    for ($i = 0; $i < count($ints); $i+= 4) {
       array_push($ppairs, array(array($ints[$i], $ints[$i+1]), array($ints[$i+2], $ints[$i+3])));
    }
    return $ppairs;
  }

  // convert string that represents an elimination forest into a list of pairs,
  // where each pair contains the node in DFS order, followed by the number of children,
  // which is determined by the number of dots in the string following the node id
  function string_to_elim($string, $dummy) {
    // is this really a string of numbers and dots separated by whitespace
    if (!preg_match("/^[0-9\.\s]*$/", $string)) {
      return false;
    }
    // strip leading and trailing whitespace
    $string = preg_replace("/(^\s*|\s*$)/", "", $string);
    if (strlen($string) == 0) {
      return array();
    }
    // split at whitespace
    $vstrings = preg_split("/\s+/", $string);
    $vpairs = array();
    for ($i = 0; $i < sizeof($vstrings); $i++) {
      $vi = $vstrings[$i];
      $v = intval(preg_replace("/(\.*)/", "", $vi));  // remove dots and convert to number
      $c = substr_count($vi, '.');  // count dots
      array_push($vpairs, array($v, $c));
    }
    return $vpairs;
  }

  // convert string to tree and vertex 2-coloring
  function string_to_coltree($string, $dummy) {
    // is this really a tree and a vertex 2-coloring, i.e., a string
    // of numbers separated by | plus a string of 0s and 1s
    if (!preg_match("/^[0-9\s\|]*$/", $string)) {
      return false;
    }
    // split into two parts
    $blocks = preg_split('/\|/', $string);
    if (sizeof($blocks) != 2) {
      return false; 
    }
    $tree = string_to_list($blocks[0], 0);
    if ($tree == false) {
      return false;
    }
    $bits = string_to_bits($blocks[1], 0);
    if ($bits == false) {
      return false; 
    }
    return array($tree, $bits);
  }

  function parse_objects($result, $fconv, $par) {
    $objects = array();
    $lines = array_fill(0, sizeof($result), 0);
    for ($i = 0; $i < sizeof($result); $i++) {
      $line = $result[$i];
      $obj = $fconv($line, $par);
      if ($obj == false) {
        continue;  // not a valid output object, skip it
      }
      array_push($objects, $obj);
      $lines[$i] = 1;
    }
    return array($objects, $lines);
  }

  function output($result, $fmt, $fconv, $par1, $fsvg_obj, $par2, $fsvg_all, $par3, $num, $gfx) {
    if ($fmt == 0) {  // graphical output
      $p = parse_objects($result, $fconv, $par1);
      $objects = $p[0];
      $lines = $p[1];
      $dtxt = $gfx ? "none" : "";
      $dgfx = $gfx ? "" : "none";
      $dnum = $num ? "" : "none";
      $count = 0;
      $limit = false;
      echo "<div id='output_list'>\n";
      for ($i = 0; $i < sizeof($lines); $i++) {
        if ($lines[$i] == 0) {  // line does not encode object
          if (strpos($result[$i], "output limit reached") !== false) {  // found output limit message
            $limit = true;
          }
          echo $result[$i] . "<br>\n";
        } else {  // line encodes an object
          $count++;
          // output number of object
          echo "<span class='num' style='display:" . $dnum . "'>" . strval($count) . ":&nbsp;</span>";
          // print textual and graphical output
          if ($fsvg_obj !== 0) {
            echo "<span class='txt' style='display:" . $dtxt . "'>" . $result[$i] . "</span>";
            echo "<span class='gfx' style='display:" . $dgfx . "'>";
            echo $fsvg_obj($objects[$count-1], $par2);
            echo "</span><br>\n";
          } else {
            echo $result[$i] . "<br>";
          }
        }
      }
      // pass information about total number of objects and limit exceedance outside
      echo "<div id='total' data-value='" . strval($count) . "' data-limit='" . ($limit ? "true" : "false") . "'></div>\n";
      echo "</div>\n";  // end div output_list
      if ($fsvg_all !== 0) {
        echo "<div id='output_all'>\n";
        echo "<span class='gfx' style='display:" . $dgfx . "'>";
        $fsvg_all($objects, $par3);
        echo "</span><span class='txt'>&nbsp;</span><br>\n";
        echo "</div>";  // end div output_all
      }
    } else if ($fmt == 1) {  // text-only output
      foreach ($result as $line) {
        echo $line . "<br>\n";
      }
    } else if ($fmt == 2) {  // file download
      exec("zip file.zip file.txt");
      ob_clean();  // clean output buffer so that no previous HTML code gets inserted into the zip file
      header("Content-disposition: attachment; filename=file.zip");
      header("Content-type: application/zip");
      readfile("file.zip");
      exit();  // need to call exit so that no subsequent HTML code gets inserted into the zip file
    }
  }
?>
