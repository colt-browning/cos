/****************************************************
 * Constant amortized time algorithm for generating *
 * length n permutations with index k               *
 *   index <n> <k>                                  *
 *                                                  *
 * Written by: Scott Effler, seffler@csc.uvic.ca    *
 *             March 5, 2002                        *
 ****************************************************/

#include <stdio.h>
#include <stdlib.h>

#define MAX_N 100

struct list
{
  int value;
  struct list *next;
  struct list *prev;
};

typedef struct list item;

int perm[MAX_N];
item *head;
item *tail;
int N;
int Tri[MAX_N];

void printPerm( void )
{
  int i;

  for( i=1; i<=N; i++ )
    printf( "%d ", perm[i] );
  printf( "\n" );
}

void initializeTri( int n )
{
  int i;

  Tri[0] = Tri[1] = 0;
  for( i=1; i<=n; i++ )
    Tri[i] = i-1 + Tri[i-1];
}

void permSwap( int i, int j )
{
  int temp;

  temp = perm[i];
  perm[i] = perm[j];
  perm[j] = temp;
}

void gen( int n, int k )
{
  item *currentPt;
  int rank, i;

  if( n == 0 )
  {
    printPerm();
  }
  else if( (k==0) && ((n==N) || ((n<N) && ((tail->value)<perm[n+1]))) )
  {
    currentPt = head;
    for( i=1; i<=n; i++,currentPt=currentPt->next )
      perm[i] = currentPt->value;
    printPerm();
  }
  else if( (k==1) && ((n==N) || ((n<N) && ((tail->value)<perm[n+1]))) )
  {
    currentPt = head;
    for( i=1; i<=n; i++,currentPt=currentPt->next )
      perm[i] = currentPt->value;
    for( i=1; i<=n-1; i++ )
    {
      permSwap( 1, i+1 );
      printPerm();
    }     
  }
  else if( (k==Tri[n]) && ((n==N) || ((n<N) && ((tail->value)<perm[n+1]))) )
  {
    currentPt = tail;
    for( i=1; i<=n; i++,currentPt=currentPt->prev )
      perm[i] = currentPt->value;
    printPerm();
  }
  else if( (k==Tri[n]-1) && ((n==N) || ((n<N) && ((tail->value)<perm[n+1]))) )
  {
    currentPt = tail;
    for( i=1; i<=n; i++,currentPt=currentPt->prev )
      perm[i] = currentPt->value;
    for( i=1; i<=n-1; i++ )
    {
      permSwap( 1, i+1 );
      printPerm();
    }
  }
  else if( n < 4 )
  {
    rank = 1;
    currentPt = head;
    while( currentPt != NULL )
    {
      if( (n==N && (n-rank)<=k && k<=(Tri[n]-rank+1))
       || (n<N && (currentPt->value)<perm[n+1] && (n-rank)<=k &&
		k<=(Tri[n]-rank+1))
       || ( n<N && (currentPt->value)>perm[n+1] && (n-rank)<=(k-n) &&
		(k-n)<=(Tri[n]-rank+1)))
      {
        perm[n] = currentPt->value;
        if( rank == 1 )  /* delete first item in list */
        {
          head = currentPt->next;
          if( currentPt->next != NULL )
            currentPt->next->prev = NULL;
          else
            tail = currentPt;
        }
        else
        {
          currentPt->prev->next = currentPt->next;
          if( currentPt->next != NULL )
            currentPt->next->prev = currentPt->prev;
          else
            tail = currentPt->prev;
        }
        if( ( n==N ) || ( perm[n] < perm[n+1] ) )
          gen( n-1, k );
	else
          gen( n-1, k-n );
        if( rank == 1 )
        {
          head = currentPt;
          if( currentPt->next != NULL )
            currentPt->next->prev = currentPt;
          else
            tail = currentPt;
        }
        else
        {
          currentPt->prev->next = currentPt;
          if( currentPt->next != NULL )
            currentPt->next->prev = currentPt;
          else
            tail = currentPt;
        }
      }
      currentPt = currentPt->next;
      rank++;
    }
  }  
  else if( k > 2*n-1 )
  {
    rank = 1;
    currentPt = head;
    while( currentPt != NULL )
    {
      if( (n==N && (n-rank)<=k && k<=(Tri[n]-rank+1))
       || (n<N && (currentPt->value)<perm[n+1] && (n-rank)<=k &&
		k<=(Tri[n]-rank+1))
       || ( n<N && (currentPt->value)>perm[n+1] && (n-rank)<=(k-n) &&
		(k-n)<=(Tri[n]-rank+1)))
      {
        perm[n] = currentPt->value;
        if( rank == 1 )  /* delete first item in list */
        {
          head = currentPt->next;
          if( currentPt->next != NULL )
            currentPt->next->prev = NULL;
          else
            tail = currentPt;
        }
        else
        {
          currentPt->prev->next = currentPt->next;
          if( currentPt->next != NULL )
            currentPt->next->prev = currentPt->prev;
          else
            tail = currentPt->prev;
        }
        if( ( n==N ) || ( perm[n] < perm[n+1] ) )
          gen( n-1, k );
	else
          gen( n-1, k-n );
        if( rank == 1 )
        {
          head = currentPt;
          if( currentPt->next != NULL )
            currentPt->next->prev = currentPt;
          else
            tail = currentPt;
        }
        else
        {
          currentPt->prev->next = currentPt;
          if( currentPt->next != NULL )
            currentPt->next->prev = currentPt;
          else
            tail = currentPt;
        }
      }
      else return;
      currentPt = currentPt->next;
      rank++;
    }
  }
  else
  {
    rank = n;
    currentPt = tail;
    while( currentPt != NULL )
    {
      if( (n==N && (n-rank)<=k && k<=(Tri[n]-rank+1))
       || (n<N && (currentPt->value)<perm[n+1] && (n-rank)<=k &&
		k<=(Tri[n]-rank+1))
       || ( n<N && (currentPt->value)>perm[n+1] && (n-rank)<=(k-n) &&
		(k-n)<=(Tri[n]-rank+1)))
      {
        perm[n] = currentPt->value;
        if( rank == n )  /* delete first item in list */
        {
          tail = currentPt->prev;
          if( currentPt->prev != NULL )
            currentPt->prev->next = NULL;
          else
            head = currentPt;
        }
        else
        {
          currentPt->next->prev = currentPt->prev;
          if( currentPt->prev != NULL )
            currentPt->prev->next = currentPt->next;
          else
            head = currentPt->next;
        }
        if( ( n==N ) || ( perm[n] < perm[n+1] ) )
          gen( n-1, k );
	else
          gen( n-1, k-n );
        if( rank == n )
        {
          tail = currentPt;
          if( currentPt->prev != NULL )
            currentPt->prev->next = currentPt;
          else
            head = currentPt;
        }
        else
        {
          currentPt->next->prev = currentPt;
          if( currentPt->prev != NULL )
            currentPt->prev->next = currentPt;
          else
            head = currentPt;
        }
      }
      else return;
      currentPt = currentPt->prev;
      rank--;
    }
  }
}

int main( int argc, char *argv[] )
{
  int i;
  item *current;

  if( argc != 3 )
  {
    printf( "inversions <n> <k>\n" );
    exit(1);
  }

  N = atoi( argv[1] );

  /* set up doubly linked list from n..1 */

  current = (item *)malloc(sizeof(item));
  current->value = N;
  current->next = NULL;
  current->prev = NULL;
  tail = current;
  head = current;
  
  for( i=N-1; i>=1; i-- )
  {
    current = (item *)malloc(sizeof(item));
    current->value = i;
    current->next = head;
    current->next->prev = current;
    current->prev = NULL;
    head = current;
  }

  initializeTri( N );
  gen( N, atoi( argv[2] ) );
  return 0;
}
