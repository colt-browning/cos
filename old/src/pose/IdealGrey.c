/* Anton An
 * June 1999
 * 
 * This program generates ideals in grey code order for a poset.
 */

#include <stdio.h>
#include <stdlib.h>
#include <string.h> 


#define MAX_POINTS 50	// max set size
#define STANDARD 0	// display ideals in gray code order
#define GRAY 1	        // highlight change of bit positions

typedef enum{FORWARD, BACKWARD}Direction;

unsigned int table[100];
unsigned int up[100];
unsigned int steiner[100];
unsigned int trans, last;
int flip;
int N;			
int counter = 0;
int input_mode;		
int Y;
int search_counter;


/* get the nth bit in the word */
unsigned getbit(unsigned int word, int n)
{
  return (word >> n) & 01;
}

/* set the nth bit in word to v */
unsigned int setbit(unsigned int word, int n, unsigned v)
{
  if (v != 0)
    return word | (01 << n);
  else
    return word & ~(01 << n);
}

/* process the relation table and generate upper/lower 
 * for each element in the poset.
 */
void Process()
{
  int i, j, k, count, bit; 

  // transitive closure
  for(k = 1; k <= N; k++){
    for(i = 1; i <= N; i++){
      for(j= 1; j <= N; j++){
	bit = getbit(table[i], j) | (getbit(table[i], k) 
				     & getbit(table[k], j));
	table[i] = setbit(table[i], j, bit);
      }
    }
  }

  for(i = 1; i <= N; i++){
    up[i] = 0;
    count = 0;
    for(j = 1; j <= N; j++){
      if(getbit(table[i], j)){
	up[i] = setbit(up[i], j, 1);
      }
      if(getbit(table[j], i)){
	count ++;
      }
    }
    steiner[i] = count - 1;
   // printf("steiner[%d] is %d\t", i, steiner[i]);
  }
}

void print(int number)
{
  int i, count = 1, flag = 0;
  if(!flip)
    flip = 1;
  else{
    flip = 0;
    if(input_mode == STANDARD){
      printf("<TR><TD ALIGN=CENTER>");
      printf("{");
      for(i = 1; i <= N; i++)
	if(getbit(number, i)){
	  if(flag)
	    printf(" %d", i);
	  else
	    printf("%d", i);
	  if(!flag)
	    flag = 1;
	}
      
      printf("}<BR></TD>\n");
      counter++;
    }
    else{
      if(counter > 0)
	trans = number ^ last;
      else
	trans = 0;
      printf("<TR><TD ALIGN=CENTER>");
      printf("{");
      for(i = 1; i<= N; i++)
	if(getbit(number, i)){
	  if(getbit(trans, i))
	    printf("<FONT COLOR=RED>");
	  if(flag)
	    printf(" %d", i);
	  else 
	    printf("%d", i);
	  if(getbit(trans, i))
	    printf("</FONT>");
	  if(!flag)
	    flag = 1;
	}
     
      printf("}<BR></TD>");
      counter++;
      flag = 0;
      printf("<TD ALIGN=CENTER>");
      for(i = 1; i<= N; i++)
	if(getbit(trans, i) && getbit(number,i))
	  printf("<FONT COLOR=GREEN>%d </FONT>", i);
	else if(getbit(trans,i) && getbit(last,i))
	  printf("<FONT COLOR=BLUE>%d </FONT>", i);
      last = number;
    }
  }
}

int search(unsigned int mask, int value, int start, int end){
  int pos;
  search_counter++;
  if(start > end){
    printf("index error\n");
    exit(1);
  }
  if(start == end)
    return start;
  if(end == start + 1){
    if(getbit(mask, end) == value)
      return end;
    else if(getbit(mask, start) == value)
      return start;
    else{
      printf("\nfatal error!!!\n"); 
      printf("mask is %d, start is %d, end is %d", mask, start, end);
      exit(1);}
  }
  else{
    pos = (start + end) / 2;
    if(getbit(mask, pos) == value)
      return (search(mask, value, pos, end));
    else
      return (search(mask, value, start, pos));
  }
}

// find the minimal element in the poset
int findMinimal(unsigned long poset)
{
  unsigned long res;
  int i, pos = 0; 

  while(poset != 0){
    res = poset ^ (poset -1);
    pos = search(res, 1, pos, N);
    if(steiner[pos] == 0)
      break;
    poset = setbit(poset, pos, 0);
  }
  return pos;
}

// update the list for finding minimal element
int Update(int poset, int min, int num)
{
  long unsigned mask, res;
  int pos;
  mask = poset & up[min];
  pos = 0;
  while(mask != 0){
    res = (mask-1) ^ mask;
    pos = search(res, 1, pos, N);
    steiner[pos]--;
    if(steiner[pos] == 0)
      num++;
    mask = setbit(mask,pos,0);
  }
  return num-1;
}

int Recover(int poset, int min, int num){
  long unsigned mask, res;
  int pos;
  mask = poset & up[min];
  pos = 0;
  while(mask != 0){
    res = (mask-1) ^ mask;
    pos = search(res, 1, pos, N);
    if(steiner[pos] == 0)
      num--;
    steiner[pos]++;
    mask = setbit(mask,pos,0);
  }
  return num;
}

// generate ideals recursively
void Ideal(unsigned long poset, Direction dir,int two_flag, int mask, int num)
{
  int i, j, min, count, temp;
  unsigned int poset1, ideal1, x, res;
  
  if(poset != 0){
    if(two_flag && num > 1) // use the second minimal element in last recursion
      min = Y;
    else
      min = findMinimal(poset);
    if(num == 1)
      {
	poset = setbit(poset, min, 0);
	num = Update(poset, min, num);
	mask = setbit(mask, min, 1);
	print(mask);
	Ideal(poset, dir, 0, mask, num);
	print(mask);
	num = Recover(poset, min, num);
      }
    else 
      {
	poset1 = poset;
	poset1 = setbit(poset1,min,0);
	Y= findMinimal(poset1);
	res = setbit(mask, min, 1);

 	if(dir == FORWARD){
	  print(res);
	  print(res);
	  num = Update(poset1, min, num);
	  Ideal(poset1,BACKWARD,1, res, num);
	  num = Recover(poset1, min, num);
	  poset = (~up[min]) & poset;
	  Y = findMinimal(poset);
	  Ideal(poset, FORWARD,1, mask, num);
	}
	else{
	  poset =(~up[min]) & poset;
	  Ideal(poset,BACKWARD,1,mask, num-1);
	  Y = findMinimal(poset1);
	  num = Update(poset1, min, num);
	  Ideal(poset1, FORWARD,1,res, num);
	  num = Recover(poset1, min,num);  
	  print(res);
	  print(res); 
	}
      }
  }  
}

void main(int argc, char* argv[])
{
  FILE *input;
  int i, j, left, right,num_zeros;
  unsigned long poset;
  char *fname;
  char *mode;
  struct cell *ideal, *temp;
  
  if(argc != 3){
    fprintf(stderr, "Usage: genIdeal -flag in_file_name\n");
    exit(1);
  }
  
  mode = argv[1];
  if(!strcmp(mode, "-s"))
    input_mode = STANDARD;
  else 
    input_mode = GRAY;

  printf("Ideals in Gray Code Order<p>");
  printf("<P><TABLE BORDER=1 CELLSPACING=2 CELLPADDING=2><TR>\n");
  if(input_mode == STANDARD)
    printf("<TH COLSPAN=1><FONT SIZE=+1>Ideals");
  else if(input_mode == GRAY){
    printf("<TH COLSPAN=1><FONT SIZE=+1>Ideals");
    printf("<TH COLSPAN=1><FONT SIZE=+1>Changes");
  }
    printf("</FONT><BR></TH>\n");
	

  fname = argv[2];
  if((input = fopen(fname, "r")) == NULL) {
    fprintf(stderr, "Error: could not read file %s\n", fname);
    exit(1);
  }

  // read number of elements in the set
  fscanf(input, "%d", &N);
  
  for(i = 1; i <= N; i++){
    table[i] = 0;
    table[i] = setbit(table[i], i, 1);
  }

  // store partial orderings
  do {
    fscanf(input,"%d %d", &left, &right);
    table[left] = setbit(table[left], right, 1);
  }while(left != 0);

  // initialize ideal and poset
  ideal = 0;
  poset = 0;
  for(i = 1; i <= N; i++)
    poset = setbit(poset, i, 1);

  
  Process();
  search_counter = 0;
  num_zeros = 0;
  for(i = 1; i <= N; i++){
    if(steiner[i] == 0)
      num_zeros ++;
  }
  flip = 1;
  print(0);
  flip = 0;
  Ideal(poset, FORWARD, 0, 0, num_zeros); 
    
  printf("</TABLE><P>%d ideals in total\n<br>", counter);
 // printf("search is called %d times\n", search_counter);
  fclose(input);
}


       
