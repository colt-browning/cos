#include <iostream>
#include <array>
#include <vector>
#include <string>
#include <memory>
#include <algorithm>
#include <cstdlib>

// Command line argment constants.
//{
// The character denoting the start of the commandline options.
constexpr char OPTIONS = '-';
// The option character indicating verbosity to be disabled.
constexpr char VERBOSE = 'q';
// The option character indicating debug mode to be enabled.
constexpr char DEBUG = 'd';
// The option character indicating statistics to be enabled.
constexpr char STATS = 's';
// The option character indicating prompts to be enabled.
constexpr char PROMPT = 'a';
// The option character indicating limits to be enabled.
constexpr char LIMIT = 'l';
// The option character indicating counts to be enabled.
constexpr char COUNT = 'c';
// The option character indicating the permutation length.
constexpr char LENGTH = 'n';
// The option character indicating the pattern to avoid.
constexpr char PATTERN = 'p';
// The option character indicating for help to be displayed.
constexpr char HELP = 'h';
// The option character indicating the pattern size.
constexpr char SIZE = 'k';

// The string representing an unset pattern.
constexpr const char* PROMPT_PATTERN = "default";
//}

// A macro defining the message for an invalid pattern.
//{
#define INVALID_PATTERN ( \
    std::string("The character: '") \
    + c \
    + "' can't be used to define a pattern." \
)
//}

/* An enumeration defining the three types of pattern.
   Basic patterns match based on their content and modifiers.
   Conjunctive patterns are avoided if both of their sub-patterns are avoided.
   Disjunctive patterns are avoided if either of their sub-patterns are avoided.
 */
enum PatternType {
    BASIC,
    CONJUNCTIVE,
    DISJUNCTIVE
};

/* An enumeration defining modifiers for patterns.
   Classical - the default modifier that uses the standard avoidance protocol.
   Vincular - requires permutation elements to be next to each other to count for a match.
   Barred - requires the permutation to avoid the pattern produced by removing the barred
    element or match the classical pattern using the same values to avoid the pattern.
   Boxed - requires that the classical pattern using the same values is avoided or there
    are values between the matched classical pattern in the permutation that are between
    the classical pattern's range to be avoided.
   Mesh - a generalisation of the other pattern modifiers.
 */
enum PatternMod {
    CLASSICAL,
    VINCULAR = '_',
    BARRED = 'b',
    BOXED = '[',
    MESH = ':'
};

/* Checks the pattern given and determines which PatternType it is.
   Sets the two halves of the compound pattern to the given head and tail.
   Throws exceptions for malformed input if the initial flag is set.
 */
PatternType pattern_type(
    const std::string& p,
    std::string& head,
    std::string& tail,
    bool initial = false
) {
    // Initial assumptions to be updated.
    PatternType type = BASIC;
    int depth = 0;
    int min;
    int index = -1;
    
    // Loop to determine the pattern's nature.
    for (int i = 0; i < p.size(); ++i) {
        // An opening bracket increments the depth.
        if (p[i] == '(') {
            ++depth;
        }
        
        // A closing bracket decrements the depth.
        else if (p[i] == ')') {
            // Too many closing brackets.
            if (initial && !depth) {
                throw std::runtime_error("Negative bracket depth reached.");
            }
            
            --depth;
        }
        
        // AND was found and it is the shallowest compound operator.
        else if (p[i] == '&' && (index < 0 || depth < min)) {
            // The compound splitting point is set.
            index = i;
            // The shallowest compound operator depth is set.
            min = depth;
            // The pattern type is set.
            type = CONJUNCTIVE;
        }
        
        // OR was found and it is the shallowest compound operator.
        // AND outprioritises OR as an operator, so OR
        //  is prioritised as a root for the parsing tree.
        else if (
            p[i] == '|' && (
                index < 0
                || depth < min
                || depth == min && type == CONJUNCTIVE
            )
        ) {
            // The compound splitting point is set.
            index = i;
            // The shallowest compound operator depth is set.
            min = depth;
            // The pattern type is set.
            type = DISJUNCTIVE;
        }
    }
    
    // Too many opening brackets.
    if (initial && depth) {
        throw std::runtime_error("Positive bracket depth at input end.");
    }
    
    // A compound pattern was found and the two halves are stored.
    if (type) {
        head = p.substr(0, index);
        tail = p.substr(index + 1);
    }
    
    // The type of pattern is returned.
    return type;
}

/* A general abstract class for all patterns.
   All patterns must be able to determine if a given permutation avoids them.
 */
class Pattern {
    public:
        /* Returns true if the permutation avoids this pattern.
         */
        virtual bool avoids(const std::vector<int>& permutation) const noexcept = 0;
        
        /* Returns true if the pattern is tame.
         */
        virtual bool tame() const noexcept = 0;
};

/* A class to define patterns without any conjunctions or disjunctions.
   Basic patterns can be augmented to have more complex behaviour.
 */
class BasicPattern: public Pattern {
    public:
        /* Constructs a non-compound pattern with the given string.
           Residual brackets and whitespace are ignored.
           Invalid patterns cause an exception to be thrown.
           Using multiple pattern modifiers also causes an exception to be thrown.
         */
        BasicPattern(const std::string& p, int max_size):
            string(p)
        {
            // The pattern is built by iterating through its string form.
            for (char c: p) {
                // The character can be used to build the pattern.
                if (valid(c)) {
                    // Mesh patterns build their matrix.
                    if (modifier == MESH) {
                        if (c > '1') {
                            throw std::runtime_error("Only 0s and 1s can be used to build the matrix.");
                        }
                        
                        else {
                            indices.push_back(c - '0');
                        }
                    }
                    
                    // The pattern is built for patterns not shown to be mesh.
                    else {
                        numbers.push_back(c - '0');
                    }
                }
                
                // The character defines a vincular pattern.
                else if (c == VINCULAR) {
                    // Multiple pattern modifiers have been used.
                    if (modifier) {
                        throw std::runtime_error(
                            "Only patterns with a single modifier are acceptable."
                        );
                    }
                    
                    // The first element of the vincular pair was omitted.
                    else if (!numbers.size()) {
                        throw std::runtime_error(
                            std::string("Vincular patterns require two values to surround the ")
                            + static_cast<char>(VINCULAR)
                        );
                    }
                    
                    // The pattern type is set and the index is stored.
                    else {
                        modifier = VINCULAR;
                        indices.push_back(numbers.size());
                    }
                }
                
                // The character defines a barred pattern.
                else if (c == BARRED) {
                    // Multiple pattern modifiers have been used.
                    if (modifier) {
                        throw std::runtime_error(
                            "Only patterns with a single modifier are acceptable."
                        );
                    }
                    
                    // The pattern type is set and the index is stored.
                    else {
                        modifier = BARRED;
                        indices.push_back(numbers.size());
                    }
                }
                
                // The character defines a boxed pattern.
                else if (c == BOXED) {
                    // Multiple pattern modifiers have been used.
                    if (modifier) {
                        throw std::runtime_error(
                            "Only patterns with a single modifier are acceptable."
                        );
                    }
                    
                    // The pattern type is set.
                    else {
                        modifier = BOXED;
                    }
                }
                
                // The character defines a mesh pattern.
                else if (c == MESH) {
                    // Multiple pattern modifiers have been used.
                    if (modifier) {
                        throw std::runtime_error(
                            "Only patterns with a single modifier are acceptable."
                        );
                    }
                    
                    // The pattern type is set.
                    else {
                        modifier = MESH;
                    }
                }
                
                // Invalid characters result in an exception thrown.
                // The invalid character is quoted.
                else if (invalid(c)) {
                    throw std::runtime_error(INVALID_PATTERN);
                }
                
            }
            
            // The modified element was not found.
            if (indices.size() == 1 && numbers.size() == indices.front()) {
                throw std::runtime_error("Modifiers must have a subsequent argument.");
            }
            
            // Abnormal pattern forms are normalised.
            normalise(numbers);
            
            // The mesh pattern's matrix is completed, if necessary.
            if (modifier == MESH) {
                try {
                    indices.insert(
                        indices.cend(),
                        (numbers.size() + 1) * (numbers.size() + 1) - indices.size(),
                        0
                    );
                }
                
                catch (const std::exception&) {
                    throw std::runtime_error(
                        "The matrix for permutations of size "
                        + std::to_string(numbers.size())
                        + " must be "
                        + std::to_string(numbers.size() + 1)
                        + 'x'
                        + std::to_string(numbers.size() + 1)
                        + '.'
                    );
                }
            }
            
            // Barred patterns are converted into mesh patterns.
            else if (modifier == BARRED) {
                indices.push_back(numbers[indices.front()]);
                numbers.erase(numbers.cbegin() + indices.front());
                normalise(numbers);
            }
            
            // The string used to generate the pattern is extracted without bracketing.
            int start, end;
            
            for (start = 0; start < p.size(); ++start) {
                if (edge(p[start])) {
                    break;
                }
            }
            
            for (end = p.size() - 1; end >= 0; --end) {
                if (edge(p[end])) {
                    break;
                }
            }
            
            string = p.substr(start, 1 + end - start);
            
            if (max_size && numbers.size() > max_size) {
                throw std::runtime_error(
                    string + " exceeds the maximum pattern size of " + std::to_string(max_size) + "."
                );
            }
            
            // The mesh matrix is flipped.
            if (modifier == MESH) {
                for (int i = 0; i < (numbers.size() + 1) / 2; ++i) {
                    for (int j = 0; j <= numbers.size(); ++j) {
                        int temp = indices[i * (numbers.size() + 1) + j];
                        indices[i * (numbers.size() + 1) + j] = indices[(numbers.size() - i) * (numbers.size() + 1) + j];
                        indices[(numbers.size() - i) * (numbers.size() + 1) + j] = temp;
                    }
                }
            }
        }
        
        /* Returns true if the permutation avoids this pattern.
           Avoidance is defined as checking all of the sub-permutations equal in
            length to this pattern, normalising them, and checking for equality.
         */
        bool avoids(const std::vector<int>& permutation) const noexcept {
            // No pattern was given or the pattern is too large.
            if (!numbers.size() || numbers.size() > permutation.size()) {
                return true;
            }
            
            // Permutations of size equal to the pattern can simply be compared.
            else if (permutation.size() == numbers.size()) {
                std::vector<int> copy(permutation);
                return numbers != normalise(copy);
            }
            
            // The vector used for pattern avoidance checks.
            std::vector<int> subperm(numbers.size());
            
            // All of the subpermutations are checked using recursion.
            return subavoid(permutation, subperm);
        }
    
        /* Returns true if the pattern is tame.
         */
        bool tame() const noexcept {
            // The index of the maximum value in tau minus is found for barred patterns.
            int index;
            
            if (modifier == BARRED) {
                for (index = 0; index < numbers.size(); ++index) {
                    if (numbers[index] == numbers.size() - 1) {
                        break;
                    }
                }
            }
            
            // Tameness tests.
            if (
                // Largest value on boundary --> non-tame.
                (
                    numbers.size() && (
                        numbers.front() == numbers.size() - 1
                        || numbers.back() == numbers.size() - 1
                    )
                )
                
                // Largest value outside of vincular pair --> non-tame.
                || (
                    modifier == VINCULAR
                    && numbers[indices.front()] != numbers.size() - 1
                    && numbers[indices.front() - 1] != numbers.size() - 1
                )
                
                // The barred entry is k and not next to k-1 --> non-tame.
                || (
                    modifier == BARRED
                    && indices.back() == numbers.size()
                    && (indices.front() < index || index + 1 < indices.front())
                )
                
                // Mesh rules 2-4 violated --> non-tame.
                || mesh_violation()
            ) {
                // The violating pattern is displayed.
                std::cerr << string << " is not tame.\n";
                
                // The pattern is not tame.
                return false;
            }
            
            // The pattern is tame.
            return true;
        }
    
    private:
        /* Recursively checks for pattern avoidance and returns the result.
           A true result indicates avoidance; a false result indicates a match.
           Checks are only made at depth numbers.size() = subperm.size(),
            other depths are simply used for iteration.
         */
        bool subavoid(
            const std::vector<int>& permutation,
            std::vector<int>& subperm,
            int index = 0,
            int depth = 0
        ) const noexcept {
            // A complete subpermutation of length equal to the pattern has been made.
            if (depth == subperm.size()) {
                // A normalised copy of the subpermutation is made.
                std::vector<int> normalised(subperm);
                normalise(normalised);
                
                // Boxed patterns require a value check for a match.
                if (modifier == BOXED) {
                    if (numbers == normalised) {
                        // The minimum and maximum values of the cloned
                        //  subpermutation are calculated and stored.
                        int max = subperm.front();
                        int min = max;
                        
                        for (int i: subperm) {
                            if (i > max) {
                                max = i;
                            }
                            
                            else if (i < min) {
                                min = i;
                            }
                        }
                        
                        // The current index in the subpermutation.
                        int index = 0;
                        
                        // The values between the matched entries are checked.
                        for (int i: permutation) {
                            // The index of the clones subpermutation is incremented.
                            // The match's values can't be used to prove avoidance.
                            if (i == subperm[index]) {
                                ++index;
                                
                                // The end of the match has been reached.
                                // No value between min and max --> pattern match.
                                if (index == subperm.size()) {
                                    return false;
                                }
                            }
                            
                            // Values are ignored until the match is reached.
                            // A match is not possible if a value that is between
                            //  min and max is found, so the loop is broken.
                            else if (index && min < i && i < max) {
                                break;
                            }
                        }
                    }
                }
                
                // Mesh patterns require a check for a match.
                else if (modifier == MESH) {
                    // The shaded cells are checked in the case of a match.
                    if (numbers == normalised) {
                        // The cells' y boundaries are the sorted subpermutation.
                        std::vector<int> y(subperm);
                        std::sort(y.begin(), y.end());
                        
                        // Both the index of the subpermutation and the grid's x-coordinate.
                        int index = 0;
                        
                        // The permutation's values are checked for inclusion within a cell.
                        for (int i = 0; i < permutation.size(); ++i) {
                            // The subpermutation's value positions define the cells' x boundaries.
                            if (index < subperm.size() && permutation[i] == subperm[index]) {
                                ++index;
                            }
                            
                            // The value is checked for shaded cell containment.
                            else {
                                // The column is checked, except for the top and bottom cells.
                                for (int j = 1; j < numbers.size(); ++j) {
                                    if (
                                        indices[j * (numbers.size() + 1) + index]
                                        && y[j - 1] < permutation[i]
                                        && permutation[i] < y[j]
                                    ) {
                                        return true;
                                    }
                                }
                                
                                // The top and bottom cells are checked.
                                if (
                                    indices[index]
                                    && permutation[i] < y.front()
                                    || indices[numbers.size() * (numbers.size() + 1) + index]
                                    && y.back() < permutation[i]
                                ) {
                                    return true;
                                }
                            }
                        }
                        
                        return false;
                    }
                }
                
                // Barred patterns require a check for tau prime.
                else if (modifier == BARRED) {
                    // Tau prime is checked if tau minus matches.
                    if (numbers == normalised) {
                        // A copy of the subpermutation is sorted.
                        std::vector<int> y(subperm);
                        std::sort(y.begin(), y.end());
                        
                        // The range of forbidden values for tau prime avoidance.
                        int min = -1;
                        int max = permutation.size();
                        
                        if (indices.back()) {
                            min = y[indices.back() - 1];
                        }
                        
                        if (indices.back() < numbers.size()) {
                            max = y[indices.back()];
                        }
                        
                        // The index of the subpermutation.
                        int index = 0;
                        
                        // The permutation's values are checked for a tau prime match.
                        for (int i = 0; i < permutation.size() && index <= indices.front(); ++i) {
                            // The index is incremented if a subpermutation value is reached.
                            if (index < subperm.size() && permutation[i] == subperm[index]) {
                                ++index;
                            }
                            
                            // The value is checked for a tau prime match by extending tau minus.
                            else if (index == indices.front() && min < permutation[i] && permutation[i] < max) {
                                return true;
                            }
                        }
                        
                        return false;
                    }
                }
                
                // The protocol for the other pattern types.
                else {
                    // The normalised subpermutation is compared with the pattern.
                    return numbers != normalised;
                }
            }
            
            // The subpermutation's value at the depth is iterated through.
            else {
                // The iteration begins at the given index.
                for (int i = index; i < permutation.size(); ++i) {
                    // The subpermutation index to be modified is equal to the depth.
                    subperm[depth] = permutation[i];
                    
                    // The result of a pattern match is propagated.
                    if (!subavoid(permutation, subperm, i + 1, depth + 1)) {
                        return false;
                    }
                    
                    // Vincular pairs must be next to each other.
                    else if (modifier == VINCULAR && indices.front() == depth) {
                        break;
                    }
                }
            }
            
            // The pattern was avoided.
            return true;
        }
        
        /* Reduces the given permutation to a permutation of 123...n,
            where n is the size of the given permutation.
           Modifies the given vector and returns a reference to it.
           Internally, the reduction is from 0 to n - 1.
         */
        static std::vector<int>& normalise(std::vector<int>& permutation) noexcept {
            // Loop to normalise each smallest unnormalised value.
            for (int i = 0; i < permutation.size(); ++i) {
                // The index of the smallest unnormalised value.
                int index = 0;
                
                // The smallest unnormalised value.
                int min = permutation[0];
                
                // Loop to find the smallest unnormalised value.
                for (int j = 1; j < permutation.size(); ++j) {
                    // The value is the unnormalised minimum.
                    if (permutation[j] >= i && (permutation[j] < min || min < i)) {
                        min = permutation[j];
                        index = j;
                    }
                }
                
                // The value is normalised.
                permutation[index] = i;
            }
            
            // A reference to the modified given permutation is returned.
            return permutation;
        }
        
        /* Returns true if the character is used to build the pattern.
           Numerical characters can be used to build the pattern.
         */
        static bool valid(char c) noexcept {
            return '0' <= c && c <= '9';
        }
        
        /* Returns true if the given character should not be ignored.
           Returns true for characters that would return true in valid() for efficiency.
           Therefore, this function should be used in conjunction with valid().
         */
        static bool invalid(char c) noexcept {
            // Brackets and whitespace are ignored.
            switch (c) {
                case '(':
                case ')':
                case ']':
                case ' ':
                    return false;
            }
            
            // The character should not be ignored.
            return true;
        }
    
        /* Returns true if the mesh tameness pattern rules 2-4 were violated.
           Rule 2 states that the top row must be devoid of filled-in
            cells, apart from the two cells above the top-most point.
           Rule 3 states that if the cell above and to the left of the
            top-most point is filled in, then all of the cells in the
            row below that cell must not be filled-in, except for the cell
            directly beneath it. Furthermore, any cells below it must be
            filled-in, if the cell to its right is filled-in.
           Rule 4 states that if the cell above and to the right of the
            top-most point is filled in, then all of the cells in the
            row below the cell must not be filled-in, except for the cell
            directly beneath it. Furthermore, any cells below it must be
            filled-in, if the cell to its right is filled-in.
         */
        bool mesh_violation() const noexcept {
            if (modifier == MESH && numbers.size()) {
                // A vector of restrictions on indices is produced and initialised.
                std::vector<int> restrictions(indices.size(), -1);
                
                // The position of the top point is found.
                int index;
                
                for (int i = 0; i < numbers.size(); ++i) {
                    if (numbers[i] == numbers.size() - 1) {
                        index = i;
                        break;
                    }
                }
                
                // Rule 2 is enforced.
                for (int i = numbers.size() * (numbers.size() + 1); i < restrictions.size(); ++i) {
                    // The cell is not above the top-point.
                    if (
                        i < numbers.size() * (numbers.size() + 1) + index
                        || numbers.size() * (numbers.size() + 1) + index + 1 < i
                    ) {
                        restrictions[i] = 0;
                    }
                }
                
                // Rule 3 is conditionally enforced.
                if (indices[numbers.size() * (numbers.size() + 1) + index]) {
                    // Blank row clause.
                    for (
                        int i = (numbers.size() - 1) * (numbers.size() + 1);
                        i < numbers.size() * (numbers.size() + 1);
                        ++i
                    ) {
                        if (i != (numbers.size() - 1) * (numbers.size() + 1) + index) {
                            restrictions[i] = 0;
                        }
                    }
                    
                    // Filled column clause.
                    for (int i = index; i < (numbers.size() - 1) * (numbers.size() + 1); i += numbers.size() + 1) {
                        if (indices[i + 1]) {
                            restrictions[i] = 1;
                        }
                    }
                }
                
                // Rule 4 is conditionally enforced.
                if (indices[numbers.size() * (numbers.size() + 1) + index + 1]) {
                    // Blank row clause.
                    for (
                        int i = (numbers.size() - 1) * (numbers.size() + 1);
                        i < numbers.size() * (numbers.size() + 1);
                        ++i
                    ) {
                        if (i != (numbers.size() - 1) * (numbers.size() + 1) + index + 1) {
                            restrictions[i] = 0;
                        }
                    }
                    
                    // Filled column clause.
                    for (int i = index + 1; i < (numbers.size() - 1) * (numbers.size() + 1); i += numbers.size() + 1) {
                        if (indices[i - 1]) {
                            restrictions[i] = 1;
                        }
                    }
                }
                
                // Violation check.
                for (int i = 0; i < indices.size(); ++i) {
                    if (restrictions[i] >= 0 && indices[i] != restrictions[i]) {
                        return true;
                    }
                }
            }
            
            return false;
        }
    
        /* Returns true if the given character can be used to start or end a basic pattern.
         */
        static bool edge(char c) noexcept {
            return valid(c) || c == BARRED || c == BOXED || c == ']' || c == MESH;
        }
    
        // The numerical content of the pattern.
        std::vector<int> numbers;
        // Values used for modifier indices or mesh pattern co-ordinates.
        std::vector<int> indices;
        // Stores the pattern's modifier for advanced avoidance rules.
        PatternMod modifier = CLASSICAL;
        // The string that defines this pattern.
        std::string string;
};

/* A class to define conjuctive patterns.
   A conjunctive pattern is avoided if both of its sub-patterns are avoided.
 */
class ConjunctivePattern: public Pattern {
    public:
        /* Compound conjunctions are constructed by constructing and storing their parts.
         */
        ConjunctivePattern(const std::string&, const std::string&, int);
        
        /* Returns true if the permutation avoids this pattern.
           The permutation avoids this pattern if it avoids both of the sub-patterns.
         */
        bool avoids(const std::vector<int>& permutation) const noexcept {
            return head->avoids(permutation) && tail->avoids(permutation);
        }
    
        /* Returns true if the pattern is tame.
         */
        bool tame() const noexcept {
            return head->tame() && tail->tame();
        }
        
    private:
        // The two sub-patterns.
        std::unique_ptr<Pattern> head;
        std::unique_ptr<Pattern> tail;
};

/* A class to define disjuctive patterns.
   A disjunctive pattern is avoided if either of its sub-patterns are avoided.
 */
class DisjunctivePattern: public Pattern {
    public:
        /* Compound conjunctions are constructed by constructing and storing their parts.
         */
        DisjunctivePattern(const std::string& h, const std::string& t, int max_size) {
            // Stirngs to store the parts of the parts.
            std::string subh;
            std::string subt;
            
            // The head's type is checked and it is constructed.
            switch (pattern_type(h, subh, subt)) {
                case CONJUNCTIVE:
                    head = std::make_unique<ConjunctivePattern>(subh, subt, max_size);
                    break;
                
                case DISJUNCTIVE:
                    head = std::make_unique<DisjunctivePattern>(subh, subt, max_size);
                    break;
                
                default:
                    head = std::make_unique<BasicPattern>(h, max_size);
            }
            
            // The tail's type is checked and it is constructed.
            switch (pattern_type(t, subh, subt)) {
                case CONJUNCTIVE:
                    tail = std::make_unique<ConjunctivePattern>(subh, subt, max_size);
                    break;
                
                case DISJUNCTIVE:
                    tail = std::make_unique<DisjunctivePattern>(subh, subt, max_size);
                    break;
                
                default:
                    tail = std::make_unique<BasicPattern>(t, max_size);
            }
        }
        
        /* Returns true if the permutation avoids this pattern.
           The permutation avoids this pattern if it avoids one of the sub-patterns.
         */
        virtual bool avoids(const std::vector<int>& permutation) const noexcept {
            return head->avoids(permutation) || tail->avoids(permutation);
        }
    
        /* Returns true if the pattern is tame.
         */
        bool tame() const noexcept {
            return head->tame() && tail->tame();
        }
    
    private:
        // The two sub-patterns.
        std::unique_ptr<Pattern> head;
        std::unique_ptr<Pattern> tail;
};

/* Compound conjunctions are constructed by constructing and storing their parts.
 */
ConjunctivePattern::ConjunctivePattern(const std::string& h, const std::string& t, int max_size) {
    // Stirngs to store the parts of the parts.
    std::string subh;
    std::string subt;
    
    // The head's type is checked and it is constructed.
    switch (pattern_type(h, subh, subt)) {
        case CONJUNCTIVE:
            head = std::make_unique<ConjunctivePattern>(subh, subt, max_size);
            break;
        
        case DISJUNCTIVE:
            head = std::make_unique<DisjunctivePattern>(subh, subt, max_size);
            break;
        
        default:
            head = std::make_unique<BasicPattern>(h, max_size);
    }
    
    // The tail's type is checked and it is constructed.
    switch (pattern_type(t, subh, subt)) {
        case CONJUNCTIVE:
            tail = std::make_unique<ConjunctivePattern>(subh, subt, max_size);
            break;
        
        case DISJUNCTIVE:
            tail = std::make_unique<DisjunctivePattern>(subh, subt, max_size);
            break;
        
        default:
            tail = std::make_unique<BasicPattern>(t, max_size);
    }
}

/* Updates arrays (vectors in this implementation) o and s according to step M5.
 */
void aux(
    std::vector<int>& s,
    std::vector<bool>& o,
    const std::vector<int>& permutation,
    const std::vector<int>& inverse,
    int n
) noexcept {
    // j is stored before sn is set to n.
    int j = s.back();
    
    // The final element of s is set to the maximum permutation value.
    s.back() = n - 1;
    
    // Value j has completed its movement across the permutation.
    if (
        o[j] ?
        inverse[j] == n - 1 || permutation[inverse[j] + 1] > j:
        !inverse[j] || permutation[inverse[j] - 1] > j
    ) {
        // Value j's direction should be reversed.
        o[j] = !o[j];
        
        // A smaller value should be moved next.
        s[j] = s[j - 1];
        
        // The next time this happens, the value one less than this should be moved.
        s[j - 1] = j - 1;
    }
}

/* Performs a jump in the permutation and updates the inverse permutation.
   Returns true if the permutation produced by the jump avoids the pattern.
 */
void jump(
    std::vector<int>& permutation,
    std::vector<int>& inverse,
    const std::vector<bool>& o,
    int j,
    int n,
    const Pattern& pattern,
    bool verbose,
    int& stats
) noexcept {
    // The range of permutation indices that changed.
    std::array<int, 2> range;
    
    // True if the pattern was avoided.
    bool avoided = true;
    
    // Jump to the right.
    if (o[j]) {
        // Loop to test different jump sizes.
        for (int i = inverse[j]; i < n - 1; ++i) {
            // The value j is shifted one position to the right.
            permutation[i] = permutation[i + 1];
            permutation[i + 1] = j;
            
            // The newly produced permutation avoids the patterns.
            if (pattern.avoids(permutation)) {
                range = {inverse[j], i + 1};
                break;
            }
            
            // All patterns are displayed in stats mode.
            // The number of matches is recorded.
            else if (stats) {
                ++stats;
                avoided = false;
                range = {inverse[j], i + 1};
                break;
            }
        }
    }
    
    // Jump to the left.
    else {
        // Loop to test different jump sizes.
        for (int i = inverse[j]; i > 0; --i) {
            // The value j is shifted one position to the left.
            permutation[i] = permutation[i - 1];
            permutation[i - 1] = j;
            
            // The newly produced permutation avoids the patterns.
            if (pattern.avoids(permutation)) {
                range = {i - 1, inverse[j]};
                break;
            }
            
            // All patterns are displayed in stats mode.
            // The number of matches is recorded.
            else if (stats) {
                ++stats;
                avoided = false;
                range = {i - 1, inverse[j]};
                break;
            }
        }
    }
    
    // The inverse permutation is recalculated.
    for (int i = range[0]; i <= range[1]; ++i) {
        inverse[permutation[i]] = i;
    }
    
    // The newly produced permutation is displayed.
    if (verbose) {
        for (int i: permutation) {
            std::cout << i + 1 << ' ';
        }
        
        if (stats) {
            std::cout << (avoided ? "avoids" : "matches");
        }
        
        std::cout << '\n';
    }
}

/* Continually prompts the user for permutations and indicates if they avoid the pattern.
 */
void prompt_mode(const Pattern& pattern) noexcept {
    while (true) {
        // Stores the given permutation.
        std::vector<int> permutation;
        std::string p;
        
        // The permutation is prompted for.
        std::cout << "Permutation: ";
        std::getline(std::cin, p);
        
        // The permutation's characters are taken.
        for (char c: p) {
            // Numerical characters build the permutation.
            if ('0' <= c && c <= '9') {
                permutation.push_back(c - '0');
            }
            
            // Non-numerical characters end the program.
            else {
                return;
            }
        }
        
        std::cout << (pattern.avoids(permutation) ? "avoids" : "matches") << '\n';
    }
}

/* Displays the help message.
 */
void help() noexcept {
    std::cout
        << "\nPermute M by Chigozie Agomo.\n"
        << "\nAn implementation of Algorithm M by Torsten Mutze and Arturo Merino.\n"
        
        << "\nStandard use case:\n"
        << "Run the program. Give the desired permutation length on one line and the pattern to avoid on the next.\n"
        << "The pattern should be some permutation of a sequence in the form 123...k.\n"
        
        << "\nComplex pattern:\n"
        << "More complex patterns can be defined using conjunction, disjunction, parenthesis, and other symbols.\n"
        << "Conjunction outprioritises disjunction.\n"
        << "Only a single modifier may be used per pattern.\n"
        << "Conjunction - &\n"
        << "Disjunction - |\n"
        << "Parenthesis - ()\n"
        << "Vincular    - 13_42 (3 and 4 are paired)\n"
        << "Barred      - 13b42 (4 is barred)\n"
        << "Boxed       - [1342]\n"
        << "Mesh        - 1342: 00100 00100 11111 00100 00100\n"
        << "Mesh patterns are defined by a matrix with 0s indicating empty cells and 1s indicating shaded cells.\n"
        << "Mesh matrices are defined left-to-right top-to-bottom.\n"
        
        << "\nOptions:\n"
        << "Options are specified by preceding the option character with a dash (-).\n"
        << "Quiet mode (permutations are no longer output):                          q\n"
        << "Count mode (the number of avoiding patterns are output):                 c\n"
        << "Length specifier (the following integer defines the permutation length): n\n"
        << "Pattern specifier (the following argument defines the pattern to avoid): p\n"
        << "Limit specifier (the following argument defines the max permutations):   l\n"
        << "Size specifier (the following argument defines the max pattern size):    k\n"
        << "Stats mode (more detailed statistics are displayed):                     s\n"
        << "Prompt mode (prompts for permutations to test against the pattern):      a\n"
        << "Debug mode (ensures that the program does not enter an infinite loop:    d\n"
        << "Help mode (displays this information):                                   h\n"
    ;
}

/* An implementation of Algorithm M described in "Combinatorial Generation
    via Permutation Languages".
   
   Takes in input from stdin with the first number taken as the length of the permutations
    and the the rest of the input is used as the definition of the pattern to avoid.
   
   The permutations are output to stdout.
 */
int main(int argc, char** argv) noexcept {
    // The verbosity of the program is set according to the command line arguments.
    bool verbose = true;
    
    // The debug mode of the program is set according to the command line arguments.
    bool debug = false;
    
    // The statistics mode of the program is set according to the command line arguments.
    int stats = 0;
    
    // The prompt mode of the program is set according to the command line arguments.
    bool prompt = false;
    
    // The limit mode of the program is set according to the command line arguments.
    int limit = 0;
    
    // The verbosity of the permutation count is set according to the command line arguments.
    bool vcount = false;
    
    // The length of a permutation.
    int n = 0;
    
    // The pattern's string form.
    std::string p(PROMPT_PATTERN);
    
    // The maximum size of a pattern.
    int k = 0;
    
    for (int i = 1; i < argc; ++i) {
        // The argument is converted from a character array to a standard string.
        std::string argument(argv[i]);
        
        // Check that the argument is an option list.
        if (argument[0] == OPTIONS) {
            // The position reached within the argument.
            int j;
            
            // Iterate through the given options to set flags.
            for (j = 0; j < argument.size(); ++j) {
                switch (argument[j]) {
                    // The user wishes to not display the permutations.
                    case VERBOSE:
                        verbose = false;
                        break;
                    
                    // The user wishes to check for algorithm failure.
                    case DEBUG:
                        debug = true;
                        break;
                    
                    // The user wishes to display pattern-avoidance statistics.
                    case STATS:
                        stats = 1;
                        break;
                    
                    // The user wishes to test individually-chosen
                    //  permutations for pattern avoidance.
                    case PROMPT:
                        prompt = true;
                        break;
                    
                    // The user wishes to limit the number of permutations produced.
                    case LIMIT:
                        // The argument was paired with the flag.
                        if (j + 1 < argument.size()) {
                            // The argument is extracted.
                            std::string arg(argument.substr(j + 1));
                            
                            // Invalid input is accounted for.
                            try {
                                limit = std::stoi(arg);
                            }
                            
                            // Invalid input results in the user being informed.
                            catch (const std::exception&) {
                                std::cerr << "Invalid limit: " << arg << '\n';
                                return EXIT_FAILURE;
                            }
                            
                            // The option should no longer be parsed.
                            j = argument.size();
                        }
                        
                        // The argument follows the option.
                        else if (i + 1 < argc) {
                            // The argument is converted to a string.
                            std::string arg(argv[i + 1]);
                            
                            // Invalid input is accounted for.
                            try {
                                limit = std::stoi(arg);
                            }
                            
                            // Invalid input results in the user being informed.
                            catch (const std::exception&) {
                                std::cerr << "Invalid limit: " << arg << '\n';
                                return EXIT_FAILURE;
                            }
                            
                            // The argument should be skipped for option handling.
                            i++;
                        }
                        
                        // The flag was passed with no argument.
                        else {
                            std::cerr << "The limit flag must have an argument.\n";
                            return EXIT_FAILURE;
                        }
                        
                        // Check for a positive value.
                        if (limit <= 0) {
                            std::cerr << "The limit must be a positive integer.\n";
                            return EXIT_FAILURE;
                        }
                    
                        break;
                    
                    // The user wishes to display the total number of permutations produced.
                    case COUNT:
                        vcount = true;
                        break;
                    
                    // The user wishes to define the permutation length through the command line.
                    case LENGTH:
                        // The argument was paired with the flag.
                        if (j + 1 < argument.size()) {
                            // The argument is extracted.
                            std::string arg(argument.substr(j + 1));
                            
                            // Invalid input is accounted for.
                            try {
                                n = std::stoi(arg);
                            }
                            
                            // Invalid input results in the user being informed.
                            catch (const std::exception&) {
                                std::cerr << "Invalid length: " << arg << '\n';
                                return EXIT_FAILURE;
                            }
                            
                            // The option should no longer be parsed.
                            j = argument.size();
                        }
                        
                        // The argument follows the option.
                        else if (i + 1 < argc) {
                            // The argument is converted to a string.
                            std::string arg(argv[i + 1]);
                            
                            // Invalid input is accounted for.
                            try {
                                n = std::stoi(arg);
                            }
                            
                            // Invalid input results in the user being informed.
                            catch (const std::exception&) {
                                std::cerr << "Invalid length: " << arg << '\n';
                                return EXIT_FAILURE;
                            }
                            
                            // The argument should be skipped for option handling.
                            i++;
                        }
                        
                        // The flag was passed with no argument.
                        else {
                            std::cerr << "The length flag must have an argument.\n";
                            return EXIT_FAILURE;
                        }
                        
                        // Check for a positive value.
                        if (n <= 0) {
                            std::cerr << "The length must be a positive integer.\n";
                            return EXIT_FAILURE;
                        }
                    
                        break;
                    
                    // The user wishes to define the pattern to avoid through the command line.
                    case PATTERN:
                        // The argument was paired with the flag.
                        if (j + 1 < argument.size()) {
                            // The pattern is set.
                            p = argument.substr(j + 1);
                            
                            // The option should no longer be parsed.
                            j = argument.size();
                        }
                        
                        // The argument follows the option.
                        else if (i + 1 < argc) {
                            // The empty pattern was given.
                            if (argv[i + 1][0] == OPTIONS) {
                                p = "";
                            }
                            
                            // A non-empty pattern was given.
                            else {
                                // The pattern is set.
                                p = argv[i + 1];
                            
                                // The argument should be skipped for option handling.
                                i++;
                            }
                        }
                        
                        // The flag was passed with no argument.
                        else {
                            p = "";
                        }
                    
                        break;
                    
                    // The user wishes to view the help message.
                    case HELP:
                        help();
                        return EXIT_SUCCESS;
                    
                    // The user wishes to define the pattern length through the command line.
                    case SIZE:
                        // The argument was paired with the flag.
                        if (j + 1 < argument.size()) {
                            // The argument is extracted.
                            std::string arg(argument.substr(j + 1));
                            
                            // Invalid input is accounted for.
                            try {
                                k = std::stoi(arg);
                            }
                            
                            // Invalid input results in the user being informed.
                            catch (const std::exception&) {
                                std::cerr << "Invalid size: " << arg << '\n';
                                return EXIT_FAILURE;
                            }
                            
                            // The option should no longer be parsed.
                            j = argument.size();
                        }
                        
                        // The argument follows the option.
                        else if (i + 1 < argc) {
                            // The argument is converted to a string.
                            std::string arg(argv[i + 1]);
                            
                            // Invalid input is accounted for.
                            try {
                                k = std::stoi(arg);
                            }
                            
                            // Invalid input results in the user being informed.
                            catch (const std::exception&) {
                                std::cerr << "Invalid size: " << arg << '\n';
                                return EXIT_FAILURE;
                            }
                            
                            // The argument should be skipped for option handling.
                            i++;
                        }
                        
                        // The flag was passed with no argument.
                        else {
                            std::cerr << "The size flag must have an argument.\n";
                            return EXIT_FAILURE;
                        }
                        
                        // Check for a positive value.
                        if (k <= 0) {
                            std::cerr << "The size must be a positive integer.\n";
                            return EXIT_FAILURE;
                        }
                    
                        break;
                }
            }
        }
        
        else {
            std::cerr << "Argument " << argument << " passed without a valid preceding option.\n";
            return EXIT_FAILURE;
        }
    }
    
    // Prompt mode allows permutations of all sizes.
    if (!prompt && !n) {
        std::cin >> n;
    
        // Only positive values of n are acceptable.
        if (n <= 0) {
            std::cerr << "The permutation size must be a positive integer.\n";
            return EXIT_FAILURE;
        }
        
        std::cin.ignore();
    }
    
    // The pattern to avoid.
    std::unique_ptr<Pattern> pattern;
    
    // The pattern's string form is gotten from stdin, if it wasn't set through the command line.
    if (p == PROMPT_PATTERN) {
        std::getline(std::cin, p);
    }
    
    // These store the two parts of conjunctive patterns.
    std::string head;
    std::string tail;
    
    // Try block to handle errors from pattern_type() and Pattern constructors.
    try {
        // The type of pattern is determined.
        switch (pattern_type(p, head, tail, true)) {
            case CONJUNCTIVE:
                pattern = std::make_unique<ConjunctivePattern>(head, tail, k);
                break;
            
            case DISJUNCTIVE:
                pattern = std::make_unique<DisjunctivePattern>(head, tail, k);
                break;
            
            default:
                pattern = std::make_unique<BasicPattern>(p, k);
        }
    }
    
    // The error is displayed and the program terminates.
    catch (const std::exception& e) {
        std::cerr << e.what() << '\n';
        return EXIT_FAILURE;
    }
    
    if (prompt) {
        prompt_mode(*pattern);
        return EXIT_SUCCESS;
    }
    
    if (!pattern->tame()) {
        std::cerr << "Only tame patterns are acceptable.\n";
        return EXIT_FAILURE;
    }
    
    // The most recently produced permuation.
    std::vector<int> permutation;
    
    // The auxiliary array, o, is initialised.
    std::vector<bool> o(n, false);
    
    // The permutation is set to the identity for the size.
    for (int i = 0; i < n; ++i) {
        permutation.push_back(i);
    }
    
    // The number of pattern-avoiding permutations produced.
    int count;
    
    // The identity permutation is displayed.
    if (verbose) {
        for (int i: permutation) {
            std::cout << i + 1 << ' ';
        }

        if (stats) {
            std::cout << "avoids";
        }
        
        std::cout << '\n';
    }
    
    // The auxiliary array, s, and the inverse permutation are identical
    //  to the most recently produced permutation, initially.
    std::vector<int> s(permutation);
    std::vector<int> inverse(permutation);
    
    // Stores the old permutation. Used for debugging.
    std::vector<int> old(permutation);
    
    // Loop for steps M2 to M5.
    // Step M3 dictates that the algorithm terminates if j = 1 (0 internally).
    for (count = 1; s.back(); ++count) {
        // The given output limit was reached.
        if (limit && count >= limit) {
            std::cerr << "Output Limit Reached.\n";
            break;
        }
        
        // Step M4 dictates that a minimal jump of value j is performed in direction o[j].
        jump(permutation, inverse, o, s.back(), n, *pattern, verbose, stats);
        
        // If the debug flag was set, duplicate permutations are checked for.
        if (debug) {
            // A duplicate permutation was found. The algorithm is terminated.
            if (old == permutation) {
                std::cout << "Duplicate permutation: ";
                
                for (int i: old) {
                    std::cout << i;
                }
                
                std::cout << '\n';
                
                break;
            }
            
            // The new permutation is stored for checking in the next iteration.
            old = permutation;
        }
        
        // Step M5 updates the auxiliary arrays.
        aux(s, o, permutation, inverse, n);
    }
    
    // The number of pattern-avoiding permutations is displayed.
    if (stats) {
        std::cout
            << count - stats + 1 << '/' << count << '\n'
            << 100 * (count - stats + 1) / count << "%\n"
        ;
    }
    
    else if (vcount) {
        std::cout << count << '\n';
    }
    
    // The program executed without error.
    return EXIT_SUCCESS;
}