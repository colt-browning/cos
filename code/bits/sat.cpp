/*
 * Copyright (c) 2016 Petr Gregor, Torsten Muetze
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <algorithm>
#include <cassert>
#include <stack>
#include <vector>
#include "math.hpp"
#include "mlc_hamcycle.hpp"
#include "sat.hpp"
#include "trim.hpp"

SatCycle::SatCycle(int n, int k, int l, long long limit, visit_f_t visit_f) :
  AbstractCycle(n, limit, visit_f) {
  assert((n >= 2) && (k >= 0) && (l > k) && (l <= n));  // 0 <= k < l <= n
  assert((k >= 1) || (l >= 2));  // (k == 0) implies (l >= 2)
  assert((l <= n - 1) || (k <= n - 2));  // (l == n) implies (k <= n - 2)

  if ((k == 0) || (l == n) || ((l - k) % 2 == 0)) {  // odd number of levels k,k+1,...,l
    // in this case we compute a saturated cycle by trimming a Gray code to levels k,k+1,...,l
    long long limitp = num_vertices_Qnkl(n, k, l) - delta_Qnkl(n, k, l);
    if (limit_ >= 0) {
      limitp = std::min(limitp, limit_);
    }
    TrimmedGrayCode gc(n, k, l, true, limitp, visit_f);
    const std::vector<int>& x0 = gc.get_first_vertex();
    x0_.insert(x0_.end(), x0.begin(), x0.end());
    length_ = gc.get_length();
  } else {  // even number of levels k,k+1,...,l
    assert((k >= 1) && (l <= n-1));
    assert((l <= (n + 1)/2) || (k >= n/2));
    if (l <= (n+1)/2) {  // all levels lie below the middle level
      sat_even(k, l, false);
    } else {  // all levels lie above the middle level
      sat_even(n - l, n - k, true);
    }
    // once the generalized middle levels conjecture is proved and
    // an algorithm for it is available, then it can be included here ;)
  }
}

void SatCycle::sat_even(int k, int l, bool complement) {
  assert((k >= 1) && ((l - k) % 2 == 1) && (l <= (n_ + 1)/2));

  // bitflip sequence for complementary traversal is exactly the same,
  // only the roles of 0's and 1's are interchanged
  int zero_bit = complement ? 1 : 0;
  int one_bit = complement ? 0 : 1;

  // initialize starting and current vertex to a_{n,k} = 0^{n-k}1^k
  x0_.resize(n_, zero_bit);
  for (int i = 1; i <= k; ++i) {
    x0_[n_ - i] = one_bit;
  }
  assert(is_a_vertex(x0_, complement, n_, k));
  x_ = x0_;

  length_ = 0;
  flip_seq_.clear();
  if (length_ == limit_) {  // compute no vertices at all
    return;
  }

  // the cycle between levels k and k+1 must visit a_{n,k}, a_{n,k+1}, b_{n,k} consecutively, and must not visit b_{n,k+1}
  // visit all vertices from a_{n,k} to b_{n,k} except possibly a_{n,k+1} and b_{n,k+1}
  long long limit = 2 * binom(n_, k);  // number of vertices to be visited in levels [k,k+1]
  std::vector<int> id;
  id_permutation(n_, id);
  if (l - k == 1) {  // special case of only two consecutive levels
    if (sat_2(k, forward, limit, 0, true, id)) {  // visit all vertices
      return;
    }
    assert(is_a_vertex(x_, complement, n_, k));
    return;
  }
  assert(l - k >= 3);  // at least four consecutive levels
  if (sat_2(k, backward, limit - 2, 0, true, id)) {  // visit all except last two vertices a_{n,k+1} and a_{n,k}
    return;
  }
  assert(is_b_vertex(x_, complement, n_, k));

  // move up from b_{n,k} to b_{n,k+1}
  int i = n_ - k - 2;
  assert(x_[i] == zero_bit);
  if (flip_and_visit(i, true)) {
    return;
  }
  assert(is_b_vertex(x_, complement, n_, k+1));

  // move up from level k to level l traversing every second cycle
  int jmax = (l - k - 1) / 2;
  for (int j = 1; j <= jmax; ++j) {
    int kp = k + 2*j;  // k'
    assert(kp <= (n_ + 1)/2);
    long long limit = 2 * binom(n_, kp);  // number of vertices to be visited in levels [k',k'+1]
    if ((j % 2) == 0) {
      // move up from a_{n,k'-1} to a_{n,k'}
      assert(is_a_vertex(x_, complement, n_, kp - 1));
      i = n_ - (kp - 1) - 1;
      assert(x_[i] == zero_bit);
      if (flip_and_visit(i, true)) {
        return;
      }
      assert(is_a_vertex(x_, complement, n_, kp));
      // compute permutation so that cycle between levels k' and k'+1
      // visits a_{n,k'}, a_{n,k'+1}, b_{n,k'}, b_{n,k'+1} consecutively
      std::vector<int> pi;
      cycle_permutation(kp, pi);
      // visit all vertices from a_{n,k'} to b_{n,k'+1} except a_{n,k'+1} and b_{n,k'}
      // (in the uppermost cycle also visit b_{n,k'})
      if (sat_2(kp, backward, limit - 2 - (j < jmax ? 1 : 0), 0, true, pi)) {
        return;
      }
      assert(is_b_vertex(x_, complement, n_, kp + (j < jmax ? 1 : 0)));
    } else {  // (j % 2) == 1
      // move up from b_{n,k'-1} to b_{n,k'}
      assert(is_b_vertex(x_, complement, n_, kp - 1));
      i = n_ - (kp - 1) - 2;
      assert(x_[i] == zero_bit);
      if (flip_and_visit(i, true)) {
        return;
      }
      assert(is_b_vertex(x_, complement, n_, kp));
      if (j < jmax) {
        // move up from b_{n,k'} to a_{n,k'+1}
        i = n_ - 1;
        assert(x_[i] == zero_bit);
        if (flip_and_visit(i, true)) {
          return;
        }
        assert(is_a_vertex(x_, complement, n_, kp + 1));
      } else {
        assert(kp == l - 1);
        // in the uppermost cycle visit all vertices from b_{n,l-1} to a_{n,l-1} except a_{n,l}
        if (sat_2(kp, forward, limit, 2, true, id)) {
          return;
        }
        assert(is_a_vertex(x_, complement, n_, kp));
      }
    }
  }

  // move down from level l to level k traversing every other second cycle
  for (int j = (l - k - 1)/2 - 1; j >= 1; --j) {
    int kp = k + 2*j;
    if ((j % 2) == 1) {
      // move down from b_{n,k'+2} to b_{n,k'+1}
      assert(is_b_vertex(x_, complement, n_, kp + 2));
      i = n_ - (kp + 1) - 2;
      assert(x_[i] == one_bit);
      if (flip_and_visit(i, true)) {
        return;
      }
      assert(is_b_vertex(x_, complement, n_, kp + 1));
      long long limit = 2 * binom(n_, kp);  // number of vertices to be visited in levels [k',k'+1]
      // compute permutation so that cycle between levels k' and k'+1
      // visits a_{n,k'}, a_{n,k'+1}, b_{n,k'}, b_{n,k'+1} consecutively
      std::vector<int> pi;
      cycle_permutation(kp, pi);
      // visit all vertices from b_{n,k'+1} to a_{n,k'} except a_{n,k'+1} and b_{n,k'}
      if (sat_2(kp, forward, limit, 3, true, pi)) {
        return;
      }
      assert(is_a_vertex(x_, complement, n_, kp));
    } else {  // (j % 2) == 0
      // move down from a_{n,k'+2} to a_{n,k'+1}
      assert(is_a_vertex(x_, complement, n_, kp + 2));
      i = n_ - (kp + 1) - 1;
      assert(x_[i] == one_bit);
      if (flip_and_visit(i, true)) {
        return;
      }
      assert(is_a_vertex(x_, complement, n_, kp + 1));
      // move down from a_{n,k'+1} to b_{n,k'}
      i = n_ - 1;
      assert(x_[i] == one_bit);
      if (flip_and_visit(i, true)) {
        return;
      }
      assert(is_b_vertex(x_, complement, n_, kp));
    }
  }

  // move down from a_{n,k+2} to a_{n,k+1}
  i = n_ - (k + 1) - 1;
  assert(x_[i] == one_bit);
  if (flip_and_visit(i, true)) {
    return;
  }
  assert(is_a_vertex(x_, complement, n_, k + 1));

  // move down from a_{n,k+1} to starting vertex a_{n,k}
  i = n_ - k - 1;
  assert(x_[i] == one_bit);
  if (flip_and_visit(i, true)) {
    return;
  }
  assert(is_a_vertex(x_, complement, n_, k));
}

bool SatCycle::sat_2(int k, direction_t dir, long long limit, long long skip_start, bool visit, const std::vector<int>& pi) {
  // local variable that counts the number of vertices visited
  // along this cycle in the cube of dimension n_ in levels [k,k+1]
  long long length = 0;

  // There is no local current vertex we need to keep track of.
  // All transformations are done directly on the vertex x_
  // (according to the permutation pi).

  std::stack<SatStackItem> s;
  if (dir == forward) {
    s.push(SatStackItem(n_, k, forward));  // (3) visit all vertices from b_{n,k} to a_{n,k} except a_{n,k+1}
    s.push(SatStackItem(n_ - 1));          // (2) a_{n,k+1} --> b_{n,k}
    s.push(SatStackItem(n_ - k - 1));      // (1) a_{n,k} --> a_{n,k+1}
  } else {
    assert(dir == backward);
    s.push(SatStackItem(n_ - k - 1));       // (3) a_{n,k+1} --> a_{n,k}
    s.push(SatStackItem(n_ - 1));           // (2) b_{n,k} --> a_{n,k+1}
    s.push(SatStackItem(n_, k, backward));  // (1) visit all vertices from a_{n,k} to b_{n,k} except a_{n,k+1}
  }
  while (!s.empty()) {
    SatStackItem it = s.top();
    s.pop();
    if (it.type_ == flip) {  // popped a flip-item, perform one bitflip
      int i = it.flip_pos_;
      if (skip_start > 0) {
        --skip_start;
      } else {
        if (flip_and_visit(pi[i], visit)) {
          return true;
        }
        ++length;
        if (length == limit) {
          return false;
        }
      }
    } else {  // popped a rec-item, perform one recursion step
      assert(it.type_ == recurse);
      int n = it.rec_n_;
      int k = it.rec_k_;
      if ((k >= 1) && (n == 2*k + 1)) {  // middle levels conjecture cycle
        long long mlc_length = num_vertices_Qnkl(n, k, k + 1);
        long long limitp = std::min(mlc_length, limit - length + 2);  // first 2 vertices will be skipped
        if (limit_ >= 0) {
           limitp = std::min(limitp, limit_ - length_ + 2);   // first 2 vertices will be skipped
        }
        if (it.dir_ == forward) {
          std::vector<int> pi_mlc;
          map_cde_onto_aab(k, pi_mlc);
          product_permutation(pi, pi_mlc, pi_);  // multiply global permutation pi with local permutation pi_mlc
          // Visit all vertices from b_{n,k} to a_{n,k} except a_{n,k+1}
          // (this is achieved by skipping the first 2 vertices).
          // If visit == false, then we enforce that return values are written in the variable flip_seq_.
          HamCycle hc(k, limitp, 2, this, visit);
          if (visit) {
            length_ += limitp - 2;
            if ((limit_ >= 0) && (length_ == limit_)) {
              return true;
            }
          }
          length += limitp - 2;
          if (length == limit) {
            return false;
          }
        } else {
          assert(it.dir_ == backward);
          std::vector<int> pi_mlc;
          map_cde_onto_baa(k, pi_mlc);
          product_permutation(pi, pi_mlc, pi_);  // multiply global permutation pi with local permutation pi_mlc
          // Visit all vertices from a_{n,k} to b_{n,k} except a_{n,k+1}
          // (this is achieved by skipping the first 2 vertices).
          // If visit == false, then return values are written in the variable flip_seq_.
          HamCycle hc(k, limitp, 2, this, visit);
          if (visit) {
            length_ += limitp - 2;
            if ((limit_ >= 0) && (length_ == limit_)) {
              return true;
            }
          }
          length  += limitp - 2;
          if (length == limit) {
            return false;
          }
        }
      } else if ((k >= 1) && (n > 2*k + 1)) {  // recursion step
        if (it.dir_ == forward) {
          // visit all vertices from b_{n,k} to a_{n,k} except a_{n,k+1}
          // push actions onto the stack in reverse order
          s.push(SatStackItem(n - 1, k - 1, forward));  // (4) (b_{n-1,k-1},1) -- > (a_{n-1,k-1},1) == a_{n,k}
          s.push(SatStackItem(n - k - 2));              // (3) (b_{n-1,k},1) --> (b_{n-1,k-1},1)
          s.push(SatStackItem(n - 1));                  // (2) (b_{n-1,k},0) --> (b_{n-1,k},1)
          s.push(SatStackItem(n - 1, k, backward));     // (1) b_{n,k} == (a_{n-1,k},0) --> (b_{n-1,k},0)
        } else {
          assert(it.dir_ == backward);
          // visit all vertices from a_{n,k} to b_{n,k} except a_{n,k+1}
          // push actions onto the stack in reverse order
          s.push(SatStackItem(n - 1, k, forward));       // (4) (b_{n-1,k},0) --> (a_{n-1,k},0) == b_{n,k}
          s.push(SatStackItem(n - 1));                   // (3) (b_{n-1,k},1) --> (b_{n-1,k},0)
          s.push(SatStackItem(n - k - 2));               // (2) (b_{n-1,k-1},1) --> (b_{n-1,k},1)
          s.push(SatStackItem(n - 1, k - 1, backward));  // (1) a_{n,k}=(a_{n-1,k-1},1) --> (b_{n-1,k-1},1)
        }
      } else {
        assert(k == 0);  // do nothing
      }
    }
  }
  return false;  // stack is empty
}

bool SatCycle::flip_and_visit(int i, bool visit) {
  if (visit) {
    assert((0 <= i) && (i <= n_ - 1));
    x_[i] = 1 - x_[i];  // perform bitflip
    #ifndef NVISIT
    std::vector<int> pos(1, i);  // a single bit has been flipped in the last step
    visit_f_(x_, pos);
    #endif
    ++length_;
    if ((limit_ >= 0) && (length_ == limit_)) {
      return true;
    }
  } else {
    flip_seq_.push_back(i);
  }
  return false;
}

void SatCycle::map_cde_onto_aab(int k, std::vector<int>& pi) {
  pi.resize(2*k + 1);
  pi[0] = 2*k;
  for (int i = 1; i <= k - 1; ++i) {
    pi[i] = k + i;
  }
  for (int i = k; i <= 2*k - 2; ++i) {
    pi[i] = i - k;
  }
  pi[2*k - 1] = k;
  pi[2*k]     = k - 1;
}

void SatCycle::map_cde_onto_baa(int k, std::vector<int>& pi) {
  pi.resize(2*k + 1);
  for (int i = 0; i <= k - 1; ++i) {
    pi[i] = k + i;
  }
  for (int i = k; i <= 2*k - 2; ++i) {
    pi[i] = i - k;
  }
  pi[2*k - 1] = 2*k;
  pi[2*k]     = k - 1;
}

void SatCycle::cycle_permutation(int k, std::vector<int>& pi) {
  std::vector<int> id;
  id_permutation(n_, id);
  flip_seq_.clear();
  sat_2(k, forward, 3, 0, false, id);  // compute triple of first three vertices of this cycle
                                       // return values are recorded in the variable flip_seq_
  int s = flip_seq_.size();
  assert(s == 3);
  assert(flip_seq_[0] == n_ - k - 1);  // this should be a flip from a_{n,k} to a_{n,k+1}
  assert(flip_seq_[1] == n_ - 1);      // this should be a flip from a_{n,k+1} to b_{n,k}
  int p = flip_seq_[2];                // we do not know what this last flip is
  assert((p >= 0) && (p <= n_ - k - 2));

  id_permutation(n_, pi);
  pi[p] = n_ - k - 2;  // permute cycle so that third bitflip leads from b_{n,k} to b_{n,k+1}
  pi[n_ - k - 2] = p;
}
