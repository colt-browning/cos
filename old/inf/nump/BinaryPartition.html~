<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 3.2//EN">
<HTML><TITLE>Info on Binary Partitions</TITLE>

<BASE HREF="http://theory.cs.uvic.ca/">

<A HREF=gen/nump.html><IMG SRC=ico/gen.gif align=left border=0 height=33 width=35></A>
<!--#include file="inc/Border.include"-->
<HR>

<BODY>
<B>Info on Binary Partitions</B>
<P>
A <I>binary partition</I> of an integer <I>m</I> is a is a numerical
  partition of <I>m</I>, all of whose parts are powers of 2.

<P> For example, the four binary partitions of <I>m</I> = 5 are
<B>1 1 1 1 1</B>, &nbsp; <B>1 1 1 2</B>, 
&nbsp; <B>1 2 2</B>, and <B>1 4</B>.

<p>
For <I>n</I> = 0, 1, 2, 3, ..., 11 the number of binary 
  partitions is 1, 1, 2, 2, 4, 4, 6, 6, 10, 10, 14, 14. 
This is sequence  <A HREF="<!--#include file=
"inc/IntSeq.include"-->Anum=A000123"> <B>A000123</B></A> in 
<!--#include file="inc/Sloane.include"-->

<P>
Binary partitions satisfy the following recurrence relation.

<P>
<blockquote>
If <i>n</i> is odd, <i>b<sub>n</sub></i> = <i>b<sub>n-1</sub></i>, and<BR>
if <i>n</i> is even, <i>b<sub>n</sub></i> = <i>b<sub>n-1</sub></i> +
  <i>b<sub>n/2</sub></i>.
</blockquote>

<P>
To see that this recurrence relation is correct, classify the partitions
  according to whether 1 is a part or not.  
If 1 is a part, remove it, obtaining a partition counted by 
  <I>b</I><sub><I>n-</I>1</sub>.
If 1 is not a part, then <i>n</I> must be even, and each part can be
  divided by two to obtain a partition counted by 
  <I>b</I><sub><I>n</I>/2</sub>.

<P>
The ordinary generating function for binary partitions is

<blockquote>
<TABLE>
<TR><TD><TD ALIGN=CENTER>1
<TR><TD>B(<I>x</I>) &nbsp; = &nbsp;
<TD><HR noshade>
<TR><TD><TD>(1<I>-x</I>)(1<I>-x</I><sup>2</sup>)(1<I>-x</I><sup>4</sup>)(1<I>-x</I><sup>8</sup>)...,
</TABLE>
</blockquote>
giving rise to the functional equation
  B(<I>x</I><sup>2</sup>) &nbsp; = &nbsp; (1<I>-x</I>)B(<I>x</I>).

<HR>
<A HREF=gen/nump.html><IMG SRC=ico/gen.gif align=left border=0 heigth=33 width=35></A>
<!--#include file="inc/Border.include"-->


<HR>
<FONT SIZE=-1>
<!--#include file="inc/Wizard.include"--><br>
<!-- This page maintained by -->
<!-- <A HREF="mailto:cos@theory.csc.uvic.ca">Bullwinkle</A> -->
<!-- <IMG SRC="http://www.engr.uvic.ca/proj-images/gif/mosaic-pics/Cool_icons/Bullwinkle.gif">.<br> -->
It was last updated <!--#echo var="LAST_MODIFIED" -->.<br>
<!--#exec cgi="inc/cos-counter/counter-nl"--><br>
<!--#include file="inc/Copyright.include"-->    
</FONT>
</HTML>









