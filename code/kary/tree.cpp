/*
 * Copyright (c) 2024 Rohan Acharya, Torsten Muetze, and Francesco Verciani
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <cassert>
#include <cmath>
#include <iostream>
#include <vector>
#include "tree.hpp"

Kary_tree::Kary_tree(int n, int k, bool special) : n_(n), k_(k), special_(special)
{
  init_tree();
  init_bookkeeping();
}

void Kary_tree::init_tree()
{
  root_ = 1;
  children_.resize(n_+1, std::vector<int>(k_, 0));
  parent_.resize(n_+1, 0);

  int max = (special_ ? n_-1 : n_);
  for (int i=1; i<=max; i++) {
    if (i < max) { children_[i][1] = i+1; }  // the second child of i is the vertex i+1
    if (i > 1) { parent_[i] = i-1; }
  }
  if (special_) {
    // the vertex n is the rightmost child of the root 1
    children_[1][k_-1] = n_;
    parent_[n_] = 1;
  }

  if (DEBUG) {
    check_consistency();
  }
}

void Kary_tree::init_bookkeeping()
{
  s_.resize(n_+1, 0);
  o_.resize(n_+1, Direction::up);
  ins_steps_.resize(n_+1, std::vector<int> ());
  for (int j=1; j<=n_; ++j) {
    s_[j] = j;
  }
  for (int j=1; j<=(special_ ? n_-1 : n_); ++j) {
    ins_steps_[j].resize((k_-1)*(j-1), 1);
  }
  if (special_) {
    ins_steps_[n_].resize(1, 1);
  }
  ins_pos_.resize(n_+1, 0);
  dir_change_.resize(n_+1, false);
}

bool Kary_tree::next(int &v, int &dir)
{
  int j = s_[n_];
  if (j == 1) {
    return false;
  }

  if (dir_change_[j]) {
    // precompute insertion sequence for next zigzag move of vertex j
    insertion_steps(j);
  }

  // rotate vertex j in direction o_[j]
  int d = ins_steps_[j][ins_pos_[j]];
  v = j; dir = d;
  if (DEBUG) {
    std::cout << "rotate " << j << " by " << d << std::endl;
  }
  for (int i=0; i<abs(d); i++) {
    rotate(j, d > 0 ? Direction::up : Direction::down);
  }
  ins_pos_[j]++;
  if (DEBUG) {
    check_consistency();
  }

  s_[n_] = n_;
  if (ins_pos_[j] == ins_steps_[j].size()) {  // insertion sequence is exhausted
    s_[j] = s_[j-1];
    s_[j-1] = j-1;
    if (o_[j] == Direction::up) {  // change direction
      o_[j] = Direction::down;
    } else {
      assert(o_[j] == Direction::down);
      o_[j] = Direction::up;
    }
    dir_change_[j] = true;  // j changed direction, so the insertion sequence needs to be recomputed
  }

  return true;
}

void Kary_tree::insertion_steps(int j)
{
  std::vector<int>& isj = ins_steps_[j];
  isj.clear();
  ins_pos_[j] = 0;
  if (o_[j] == Direction::up) {
    // when moving up, we lazily detect whether we are at the last insertion position or not
    int p = parent_[j];
    int a = child_index(p, j);
    int b = a;
    if (special_ && (j == n_)) {
      isj.push_back(1);
    } else {
      while (b > 1) {  // we are not at the last insertion position, so move back down to it with steps of size -1
        isj.push_back(-1);
        b--;
      }
      isj.push_back(a);  // big jump back up +a to one position after initial positio
    }
    do {  // from here move up in steps of size +1
      b = a;
      assert((!special_) || (j < n_) || (b == k_-1));
      while (b < k_-1) {
        isj.push_back(1);  // move up (=to the right) among children at this vertex
        b++;
      }
      int pp = parent_[p];  // grandparent
      if ((pp == 0) || (pp > j)) break;
      a = child_index(pp, p);
      p = pp;  // move up to next higher group of children
      isj.push_back(1);
    } while (true);
  } else {
    // when moving down, we have to eagerly check whether the last part of the insertion sequence needs to be reversed
    // (because of smaller vertices rotating there)
    assert(o_[j] == Direction::down);
    assert((children_[j][0] > 0) && (children_[j][0] < j));
    int c = children_[j][0];

    do {
      int a = k_-1;
      while ((a > 0) && (children_[c][a] == 0)) {
        isj.push_back(-1);  // move down (=to the left) among children at this vertex
        if (special_ && (j == n_)) {
          a = 0;
        } else {
          a--;
        }
      }
      if (a == 0) break;
      int cc = children_[c][a];  // grandchild
      c = cc;  // move down to next lower group of children
      isj.push_back(-1);
    } while (true);
    // so far the insertion sequence is -1,-1,...,-1
    // we may now have to reverse (i.e., overwrite) the last part of it
    if (s_[j-1] == c) {  // next vertex to move is the bottom vertex of the insertion path
      if (dir_change_[c]) {  // this may require recomputing the insertion sequence of that vertex
        insertion_steps(c);
      }
      int d = ins_steps_[c][ins_pos_[c]];  // next step of vertex c
      int s = isj.size();
      int p = parent_[c];
      int a = child_index(p, c);
      assert((!special_) || (j < n_) || (d < 0) || ((a == k_-1) && (d == 1)));
      if (d > 0) {
        if (a + d >= k_) d--;  // the rotation of c from rightmost child to parent does not change the children order
        isj[s-1-d] = -d-1;
        for (int i=0; i<d; i++) {
          isj[s-1-i] = 1;
        }
      }
    }
  }
  dir_change_[j] = false;  // update complete

  if (DEBUG) {
    std::cout << "insertion sequence " << j << ": ";
    for (int i=0; i<ins_steps_[j].size(); i++) {
      std::cout << ins_steps_[j][i] << " ";
    }
    std::cout << std::endl;
  }
}

void Kary_tree::rotate(int j, Direction d)
{
  int p = parent_[j];
  int a = -1;
  if (p > 0) {
    a = child_index(p, j);
  }
  int c = children_[j][0];
  if (d == Direction::down) {
    // rotate j down in the tree
    if (c == 0) {
      // j does not change depth, but moves to the left
      left_rotate(j);
    } else {
      // depth of j increases
      int cc = children_[c][k_-1];  // grandchild
      if (p > 0) {
        children_[p][a] = c;
      } else {
        root_ = c;
      }
      parent_[c] = p;
      children_[j][0] = children_[c][k_-1];
      if (cc > 0) {
        parent_[cc] = j;
      }
      children_[c][k_-1] = j;
      parent_[j] = c;
    }
  } else if (d == Direction::up) {
    // rotate j up in the tree
    if (a < k_-1) {
      // j does not change depth, but moves to the right
      right_rotate(j);
    } else {
      // depth of j decreases
      int pp = parent_[p];  // grandparent
      if (pp > 0) {
        int b = child_index(pp, p);
        children_[pp][b] = j;
      } else {
        root_ = j;
      }
      parent_[j] = pp;
      children_[p][k_-1] = children_[j][0];
      if (c > 0) {
         parent_[c] = p;
      }
      children_[j][0] = p;
      parent_[p] = j;
    }
  }
}

void Kary_tree::left_rotate(int j)
{
  int p = parent_[j];
  int a = child_index(p, j);
  int c = children_[j][k_-1];  // rightmost child of j
  int i = children_[p][a-1];  // left sibling of j

  children_[p][a] = c;
  if (c > 0) parent_[c] = p;
  for (int a=k_-1; a>0; a--) {
    children_[j][a] = children_[j][a-1];
  }
  children_[j][0] = i;
  if (i > 0) parent_[i] = j;
  children_[p][a-1] = j;
}

void Kary_tree::right_rotate(int j)
{
  int p = parent_[j];
  int a = child_index(p, j);
  int c = children_[j][0];  // leftmost child of j
  int i = children_[p][a+1];  // right sibling of j

  children_[p][a] = c;
  if (c > 0) parent_[c] = p;
  for (int a=0; a<k_-1; a++) {
    children_[j][a] = children_[j][a+1];
  }
  children_[j][k_-1] = i;
  if (i > 0) parent_[i] = j;
  parent_[i] = j;
  children_[p][a+1] = j;
}

int Kary_tree::child_index(int p, int c)
{
  for (int i=0; i<k_; i++) {
    if (children_[p][i] == c) return i;
  }
  return -1;
}

void Kary_tree::print(bool nl)
{
  print_rec(root_);
  if (nl) {
    std::cout << std::endl;
  }
}

void Kary_tree::print_rec(int i)
{
  std::cout << i << " ";
  std::vector<int> &ci = children_[i];
  for (int j=0; j<k_; j++) {
    if (ci[j] == 0) {
      std::cout << "0 ";
    } else {
      print_rec(ci[j]);
    }
  }
}

void Kary_tree::check_consistency()
{
  assert((root_ >= 1) && (root_ <= n_));
  assert(parent_[root_] == 0);
  assert(parent_.size() == n_+1);
  assert(children_.size() == n_+1);
  for (int i=1; i<=n_; i++) {
    assert(children_[i].size() == k_);
  }
  for (int i=1; i<=n_; i++) {
    for (int j=0; j<k_; j++) {
      int c = children_[i][j];
      if (c == 0) continue;
      assert(parent_[c] == i);
    }
  }
}