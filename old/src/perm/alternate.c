/* Program to Generate Alternating Permutations */

int P[10];
int Pi[10];
int n;

void seq2perm(int *seq, int *perm, int n); /* proto for P-sequence to perm converter */

int main(void) {
    int looper;
    int yunk;

    printf("Gimmie n:");
    scanf("%d*",&n);
    
    for(looper=0;looper < n;looper++) {
	printf("Gimmie P[%d] :",looper);
	scanf("%d*",&yunk);
	P[looper] = yunk;
    }

    seq2perm(P, Pi, n);
    printf("we gots the perm!!!\n\n");

    for(looper = 0; looper < n; looper++)
	printf("P[%d] = %d",looper, P[looper]);

    printf("\n");
}
    

/* seq2perm - convert P-Sequence to permutation */

void seq2perm(int *seq, int *perm, int n)
{
    int i,j,c;
    int m[100];  /* or max size. Whatever */
    
    /* preinitialize the M-array to zerow */
    for (i=0;i<n;i++) m[i] = 0;
    
    for(i=0;i<n;i++) {
	j=0;
	c=0;
       	while(j<(seq[i]+1)) { if(!m[c]) { j++; m[c]=1;} c++;}
	perm[i] = (j-1);
    }
}
