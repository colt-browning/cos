/* Generating Numerical Partitions into Odd parts */

#include <stdio.h>
#include <string.h>
#include "../ProcessInput.c"  /* input routines, globals below */

extern int N;  /* partitions of integer N into odd parts */
extern int M;  /*   with largest part M   */

#include "NumPart.h"    /* output routines, globals below */

extern int p[100];      /* stores the partition */

void check(int x)
{
  int i, j, temp;
  for(i = (M==0)?2:1; i <= x; i++)
      {
	temp = p[i];
	for(j = i+1; j <= x; j++)
	  if(p[j] == temp)
	    return;
      }
  PrintIt(x);
}
/* Partitions of n into odd parts with largest part k */
void PartO ( int n, int k, int t )
{ int j;
  p[t] = k;
  if (n == k || k == 1) 
     check( t+n-k );
  else for (j=1; j<=min((k+1)/2,(n-k+1)/2); j++) PartO( n-k, 2*j-1, t+1 );
  p[t] = 1;
}

int main (int argc, char *argv[])
{ int i;
  ProcessInput( argc, argv );
  if (N != 0) {	
     for (i=1; i<=N+1; i++) p[i] = 1;
     count = 0;  
     if (M == 0) {
        if (N%2) PartO( N+N, N, 1 );
        else PartO( N+N-1, N-1, 1 );
     } else PartO( N, M, 1 );
     printf("</TABLE><P>Partitions = %d",count);
  } else printf("Sorry.  N equals zero.  Goodbye\n");
  exit( 0 );
}

